﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.GuestBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class GuestBL
  {
    private string connStr { get; set; }

    public GuestBL()
    {
      this.connStr = string.Empty;
    }

    public GuestBL(string connStr)
    {
      this.connStr = connStr;
    }

    private GuestDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new GuestDL();
      return new GuestDL(this.connStr);
    }

    public bool Insert(Guest obj)
    {
      try
      {
        return this.GetDL().Insert(obj);
      }
      catch
      {
        throw;
      }
    }

    public List<Guest> GetList()
    {
      try
      {
        return this.GetDL().GetList();
      }
      catch
      {
        throw;
      }
    }

    public bool Update(Guest obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
