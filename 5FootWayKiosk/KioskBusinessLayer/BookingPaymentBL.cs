﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.BookingPaymentBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class BookingPaymentBL
  {
    private string connStr { get; set; }

    public BookingPaymentBL()
    {
      this.connStr = string.Empty;
    }

    public BookingPaymentBL(string connStr)
    {
      this.connStr = connStr;
    }

    private BookingPaymentDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new BookingPaymentDL();
      return new BookingPaymentDL(this.connStr);
    }

    public bool Insert(BookingPayment obj)
    {
      try
      {
        return this.GetDL().Insert(obj);
      }
      catch
      {
        throw;
      }
    }

    public List<BookingPayment> GetList()
    {
      try
      {
        return this.GetDL().GetList();
      }
      catch
      {
        throw;
      }
    }

    public bool Update(BookingPayment obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
