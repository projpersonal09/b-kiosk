﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.CashInOutBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class CashInOutBL
  {
    private string connStr { get; set; }

    public CashInOutBL()
    {
      this.connStr = string.Empty;
    }

    public CashInOutBL(string connStr)
    {
      this.connStr = connStr;
    }

    private CashInOutDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new CashInOutDL();
      return new CashInOutDL(this.connStr);
    }

    public bool Insert(ref CashInOut obj)
    {
      try
      {
        return this.GetDL().Insert(ref obj);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public CashInOutSummary GetCashSummary(long shiftId)
    {
      try
      {
        return this.GetDL().GetCashSummary(shiftId);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public List<CashInOut> GetList(long shiftId, DateTime startDate, DateTime endDate)
    {
      try
      {
        return this.GetDL().GetList(shiftId, startDate, endDate);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public List<CashInOut> GetListForUpload()
    {
      try
      {
        return this.GetDL().GetListForUpload();
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public bool Update(CashInOut obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
