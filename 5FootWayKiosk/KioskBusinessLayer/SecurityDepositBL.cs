﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.SecurityDepositBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class SecurityDepositBL
  {
    private string connStr { get; set; }

    public SecurityDepositBL()
    {
      this.connStr = string.Empty;
    }

    public SecurityDepositBL(string connStr)
    {
      this.connStr = connStr;
    }

    private SecurityDepositDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new SecurityDepositDL();
      return new SecurityDepositDL(this.connStr);
    }

    public bool Insert(SecurityDeposit obj)
    {
      try
      {
        return this.GetDL().Insert(obj);
      }
      catch
      {
        throw;
      }
    }

    public List<SecurityDeposit> GetList()
    {
      try
      {
        return this.GetDL().GetList();
      }
      catch
      {
        throw;
      }
    }

    public bool Update(SecurityDeposit obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
