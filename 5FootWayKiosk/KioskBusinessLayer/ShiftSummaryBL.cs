﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.ShiftSummaryBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;

namespace KioskBusinessLayer
{
  public class ShiftSummaryBL
  {
    private string connStr { get; set; }

    public ShiftSummaryBL()
    {
      this.connStr = string.Empty;
    }

    public ShiftSummaryBL(string connStr)
    {
      this.connStr = connStr;
    }

    private ShiftSummaryDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new ShiftSummaryDL();
      return new ShiftSummaryDL(this.connStr);
    }

    public ShiftSummary GetByShiftId(long shiftId)
    {
      return this.GetDL().Get(shiftId);
    }

    public bool StartShift(ShiftSummary shift)
    {
      return this.GetDL().StartShift(shift);
    }

    public bool EndShift(ShiftSummary shift)
    {
      return this.GetDL().EndShift(shift);
    }
  }
}
