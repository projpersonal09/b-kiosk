﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.RoomGuestCardBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class RoomGuestCardBL
  {
    private string connStr { get; set; }

    public RoomGuestCardBL()
    {
      this.connStr = string.Empty;
    }

    public RoomGuestCardBL(string connStr)
    {
      this.connStr = connStr;
    }

    private RoomGuestCardDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new RoomGuestCardDL();
      return new RoomGuestCardDL(this.connStr);
    }

    public bool Insert(RoomGuestCard obj)
    {
      try
      {
        return this.GetDL().Insert(obj);
      }
      catch
      {
        throw;
      }
    }

    public List<RoomGuestCard> GetList()
    {
      try
      {
        return this.GetDL().GetList();
      }
      catch
      {
        throw;
      }
    }

    public bool Update(RoomGuestCard obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
