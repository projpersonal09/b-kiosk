﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.BookingBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System.Collections.Generic;

namespace KioskBusinessLayer
{
  public class BookingBL
  {
    private string connStr { get; set; }

    public BookingBL()
    {
      this.connStr = string.Empty;
    }

    public BookingBL(string connStr)
    {
      this.connStr = connStr;
    }

    private BookingDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new BookingDL();
      return new BookingDL(this.connStr);
    }

    public bool Insert(Booking obj)
    {
      try
      {
        return this.GetDL().Insert(obj);
      }
      catch
      {
        throw;
      }
    }

    public List<Booking> GetList()
    {
      try
      {
        return this.GetDL().GetList();
      }
      catch
      {
        throw;
      }
    }

    public bool Update(Booking obj)
    {
      try
      {
        return this.GetDL().Update(obj);
      }
      catch
      {
        throw;
      }
    }
  }
}
