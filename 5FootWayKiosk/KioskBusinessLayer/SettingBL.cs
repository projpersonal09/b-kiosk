﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.SettingBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System;

namespace KioskBusinessLayer
{
  public class SettingBL
  {
    private string connStr { get; set; }

    public SettingBL()
    {
      this.connStr = string.Empty;
    }

    public SettingBL(string connStr)
    {
      this.connStr = connStr;
    }

    private SettingDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new SettingDL();
      return new SettingDL(this.connStr);
    }

    public Setting Get(Setting s)
    {
      try
      {
        return this.GetDL().Get(s);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public bool Update(Setting s)
    {
      try
      {
        return this.GetDL().Update(s);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
