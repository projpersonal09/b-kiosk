﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.SurveyResponseBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using Helper;
using KioskBusinessEntity.Database_Entity;
using KioskBusinessEntity.Kiosk_Entity;
using System;

namespace KioskBusinessLayer
{
  public class SurveyResponseBL
  {
    private string connStr { get; set; }

    public SurveyResponseBL()
    {
      this.connStr = string.Empty;
    }

    public SurveyResponseBL(string connStr)
    {
      this.connStr = connStr;
    }

    public SurveyResponse GenerateRateExperience(string selectedRate, GuestDetail guestDetail)
    {
      return new SurveyResponse()
      {
        SurveyId = ConstantVariable_SurveyResponse.RateExperience_SurveyId,
        SurveyResponseId = 0,
        RecipientType = ConstantVariable_SurveyResponse.RecipientType_Guest,
        RecipientId = guestDetail != null ? guestDetail.guestId : 0L,
        RecipientIpAddr = string.Empty,
        CreatedDate = DateTime.Now,
        ModifiedDate = DateTime.Now,
        Answers = {
          new SurveyResponseAnswer()
          {
            SurveyId = ConstantVariable_SurveyResponse.RateExperience_SurveyId,
            SurveyResponseId = 0L,
            QuestionId = ConstantVariable_SurveyResponse.RateExperience_QuestionId,
            AnswerChoice = !string.IsNullOrWhiteSpace(selectedRate) ? selectedRate : string.Empty,
            AnswerText = string.Empty,
            AnswerLongText = string.Empty
          }
        }
      };
    }
  }
}
