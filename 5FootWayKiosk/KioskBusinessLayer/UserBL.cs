﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.UserBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;
using System;

namespace KioskBusinessLayer
{
  public class UserBL
  {
    private string connStr { get; set; }

    public UserBL()
    {
      this.connStr = string.Empty;
    }

    public UserBL(string connStr)
    {
      this.connStr = connStr;
    }

    private UserDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new UserDL();
      return new UserDL(this.connStr);
    }

    public User Get(User u)
    {
      try
      {
        return this.GetDL().Get(u);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public bool Update(User u)
    {
      try
      {
        return this.GetDL().Update(u);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
