﻿// Decompiled with JetBrains decompiler
// Type: KioskBusinessLayer.CashNoteTransactionBL
// Assembly: KioskBusinessLayer, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 76E457CC-A8CC-43E8-852F-441C1AF927CE
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\KioskBusinessLayer.dll

using KioskBusinessEntity.Database_Entity;
using KioskDataLayer;

namespace KioskBusinessLayer
{
  public class CashNoteTransactionBL
  {
    private string connStr { get; set; }

    public CashNoteTransactionBL()
    {
      this.connStr = string.Empty;
    }

    public CashNoteTransactionBL(string connStr)
    {
      this.connStr = connStr;
    }

    private CashNoteTransactionDL GetDL()
    {
      if (string.IsNullOrEmpty(this.connStr))
        return new CashNoteTransactionDL();
      return new CashNoteTransactionDL(this.connStr);
    }

    public bool InsertOrUpdate(ref CashNoteTransaction cnt)
    {
      try
      {
        return this.GetDL().InsertOrUpdate(ref cnt);
      }
      catch
      {
        throw;
      }
    }
  }
}
