﻿// Decompiled with JetBrains decompiler
// Type: WebService.BookingWS.Booking_CheckOutItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BookingWS
{
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/BookingWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class Booking_CheckOutItem : INotifyPropertyChanged
  {
    private DateTime bookingDateField;
    private DateTime checkInDateField;
    private DateTime checkOutDateField;
    private string bookingOrderCodeField;
    private long roomStayIdField;
    private string guestNameField;

    [XmlElement(Order = 0)]
    public DateTime bookingDate
    {
      get
      {
        return this.bookingDateField;
      }
      set
      {
        this.bookingDateField = value;
        this.RaisePropertyChanged(nameof (bookingDate));
      }
    }

    [XmlElement(Order = 1)]
    public DateTime checkInDate
    {
      get
      {
        return this.checkInDateField;
      }
      set
      {
        this.checkInDateField = value;
        this.RaisePropertyChanged(nameof (checkInDate));
      }
    }

    [XmlElement(Order = 2)]
    public DateTime checkOutDate
    {
      get
      {
        return this.checkOutDateField;
      }
      set
      {
        this.checkOutDateField = value;
        this.RaisePropertyChanged(nameof (checkOutDate));
      }
    }

    [XmlElement(Order = 3)]
    public string bookingOrderCode
    {
      get
      {
        return this.bookingOrderCodeField;
      }
      set
      {
        this.bookingOrderCodeField = value;
        this.RaisePropertyChanged(nameof (bookingOrderCode));
      }
    }

    [XmlElement(Order = 4)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 5)]
    public string guestName
    {
      get
      {
        return this.guestNameField;
      }
      set
      {
        this.guestNameField = value;
        this.RaisePropertyChanged(nameof (guestName));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
