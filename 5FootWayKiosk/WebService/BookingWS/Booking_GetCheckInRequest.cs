﻿// Decompiled with JetBrains decompiler
// Type: WebService.BookingWS.Booking_GetCheckInRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BookingWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/BookingWS")]
  [DesignerCategory("code")]
  [Serializable]
  public class Booking_GetCheckInRequest : INotifyPropertyChanged
  {
    private string bookingNumberField;
    private string guestNameField;
    private int propertyIdField;
    private DateTime checkInDateField;

    [XmlElement(Order = 0)]
    public string bookingNumber
    {
      get
      {
        return this.bookingNumberField;
      }
      set
      {
        this.bookingNumberField = value;
        this.RaisePropertyChanged(nameof (bookingNumber));
      }
    }

    [XmlElement(Order = 1)]
    public string guestName
    {
      get
      {
        return this.guestNameField;
      }
      set
      {
        this.guestNameField = value;
        this.RaisePropertyChanged(nameof (guestName));
      }
    }

    [XmlElement(Order = 2)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 3)]
    public DateTime checkInDate
    {
      get
      {
        return this.checkInDateField;
      }
      set
      {
        this.checkInDateField = value;
        this.RaisePropertyChanged(nameof (checkInDate));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
