﻿// Decompiled with JetBrains decompiler
// Type: WebService.BookingWS.Booking_CheckInItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BookingWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/BookingWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class Booking_CheckInItem : INotifyPropertyChanged
  {
    private long bookingIdField;
    private int numberOfRoomField;
    private int numberOfBedField;
    private string bookingOrderCodeField;
    private DateTime bookingDateField;
    private DateTime checkInDateField;
    private DateTime checkOutDateField;
    private string sourceNameField;

    [XmlElement(Order = 0)]
    public long bookingId
    {
      get
      {
        return this.bookingIdField;
      }
      set
      {
        this.bookingIdField = value;
        this.RaisePropertyChanged(nameof (bookingId));
      }
    }

    [XmlElement(Order = 1)]
    public int numberOfRoom
    {
      get
      {
        return this.numberOfRoomField;
      }
      set
      {
        this.numberOfRoomField = value;
        this.RaisePropertyChanged(nameof (numberOfRoom));
      }
    }

    [XmlElement(Order = 2)]
    public int numberOfBed
    {
      get
      {
        return this.numberOfBedField;
      }
      set
      {
        this.numberOfBedField = value;
        this.RaisePropertyChanged(nameof (numberOfBed));
      }
    }

    [XmlElement(Order = 3)]
    public string bookingOrderCode
    {
      get
      {
        return this.bookingOrderCodeField;
      }
      set
      {
        this.bookingOrderCodeField = value;
        this.RaisePropertyChanged(nameof (bookingOrderCode));
      }
    }

    [XmlElement(Order = 4)]
    public DateTime bookingDate
    {
      get
      {
        return this.bookingDateField;
      }
      set
      {
        this.bookingDateField = value;
        this.RaisePropertyChanged(nameof (bookingDate));
      }
    }

    [XmlElement(Order = 5)]
    public DateTime checkInDate
    {
      get
      {
        return this.checkInDateField;
      }
      set
      {
        this.checkInDateField = value;
        this.RaisePropertyChanged(nameof (checkInDate));
      }
    }

    [XmlElement(Order = 6)]
    public DateTime checkOutDate
    {
      get
      {
        return this.checkOutDateField;
      }
      set
      {
        this.checkOutDateField = value;
        this.RaisePropertyChanged(nameof (checkOutDate));
      }
    }

    [XmlElement(Order = 7)]
    public string sourceName
    {
      get
      {
        return this.sourceNameField;
      }
      set
      {
        this.sourceNameField = value;
        this.RaisePropertyChanged(nameof (sourceName));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
