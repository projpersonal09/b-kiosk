﻿// Decompiled with JetBrains decompiler
// Type: WebService.BookingWS.BookingWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.BookingWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "BookingWS.BookingWSSoap", Namespace = "http://pms-mq/WebService/BookingWS")]
  public interface BookingWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/BookingWS/GetBookingCheckIn", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    Booking_GetCheckInResponse GetBookingCheckIn(Booking_GetCheckInRequest bookingReq);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/BookingWS/GetBookingCheckOut", ReplyAction = "*")]
    Booking_GetCheckOutResponse GetBookingCheckOut(Booking_GetCheckOutRequest bookingReq);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/BookingWS/UpdateBooking", ReplyAction = "*")]
    Booking_UpdateResponse UpdateBooking(Booking_UpdateRequest bookingReq);
  }
}
