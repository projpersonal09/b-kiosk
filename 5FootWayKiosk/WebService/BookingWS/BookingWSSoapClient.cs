﻿// Decompiled with JetBrains decompiler
// Type: WebService.BookingWS.BookingWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.BookingWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [DebuggerStepThrough]
  public class BookingWSSoapClient : ClientBase<BookingWSSoap>, BookingWSSoap
  {
    public BookingWSSoapClient()
    {
    }

    public BookingWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public BookingWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public BookingWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public BookingWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public Booking_GetCheckInResponse GetBookingCheckIn(Booking_GetCheckInRequest bookingReq)
    {
      return this.Channel.GetBookingCheckIn(bookingReq);
    }

    public Booking_GetCheckOutResponse GetBookingCheckOut(Booking_GetCheckOutRequest bookingReq)
    {
      return this.Channel.GetBookingCheckOut(bookingReq);
    }

    public Booking_UpdateResponse UpdateBooking(Booking_UpdateRequest bookingReq)
    {
      return this.Channel.UpdateBooking(bookingReq);
    }
  }
}
