﻿// Decompiled with JetBrains decompiler
// Type: WebService.WebServiceException
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;

namespace WebService
{
  public class WebServiceException : Exception
  {
    public WebServiceException()
    {
    }

    public WebServiceException(string message)
      : base(message)
    {
    }

    public WebServiceException(string message, Exception inner)
      : base(message, inner)
    {
    }
  }
}
