﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.SurveyResponseSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.CRM.SurveyResponseWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [DebuggerStepThrough]
  public class SurveyResponseSoapClient : ClientBase<SurveyResponseSoap>, SurveyResponseSoap
  {
    public SurveyResponseSoapClient()
    {
    }

    public SurveyResponseSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public SurveyResponseSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public SurveyResponseSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public SurveyResponseSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public SurveyResponse_SaveResponse Save(SurveyResponse_SaveRequest request)
    {
      return this.Channel.Save(request);
    }
  }
}
