﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.SurveyResponse_SaveResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [Serializable]
  public class SurveyResponse_SaveResponse : WebServiceResponseBase
  {
    private SurveyResponseModel surveyResponseField;

    [XmlElement(Order = 0)]
    public SurveyResponseModel SurveyResponse
    {
      get
      {
        return this.surveyResponseField;
      }
      set
      {
        this.surveyResponseField = value;
        this.RaisePropertyChanged(nameof (SurveyResponse));
      }
    }
  }
}
