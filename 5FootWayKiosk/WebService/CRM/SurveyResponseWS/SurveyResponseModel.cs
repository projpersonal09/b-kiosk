﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.SurveyResponseModel
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class SurveyResponseModel : INotifyPropertyChanged
  {
    private string surveyIdField;
    private long surveyResponseIdField;
    private string recipientTypeField;
    private long recipientIdField;
    private string recipientIpAddrField;
    private DateTime createdDateField;
    private DateTime modifiedDateField;
    private SurveyResponseAnswerModel[] answersField;

    [XmlElement(Order = 0)]
    public string SurveyId
    {
      get
      {
        return this.surveyIdField;
      }
      set
      {
        this.surveyIdField = value;
        this.RaisePropertyChanged(nameof (SurveyId));
      }
    }

    [XmlElement(Order = 1)]
    public long SurveyResponseId
    {
      get
      {
        return this.surveyResponseIdField;
      }
      set
      {
        this.surveyResponseIdField = value;
        this.RaisePropertyChanged(nameof (SurveyResponseId));
      }
    }

    [XmlElement(Order = 2)]
    public string RecipientType
    {
      get
      {
        return this.recipientTypeField;
      }
      set
      {
        this.recipientTypeField = value;
        this.RaisePropertyChanged(nameof (RecipientType));
      }
    }

    [XmlElement(Order = 3)]
    public long RecipientId
    {
      get
      {
        return this.recipientIdField;
      }
      set
      {
        this.recipientIdField = value;
        this.RaisePropertyChanged(nameof (RecipientId));
      }
    }

    [XmlElement(Order = 4)]
    public string RecipientIpAddr
    {
      get
      {
        return this.recipientIpAddrField;
      }
      set
      {
        this.recipientIpAddrField = value;
        this.RaisePropertyChanged(nameof (RecipientIpAddr));
      }
    }

    [XmlElement(Order = 5)]
    public DateTime CreatedDate
    {
      get
      {
        return this.createdDateField;
      }
      set
      {
        this.createdDateField = value;
        this.RaisePropertyChanged(nameof (CreatedDate));
      }
    }

    [XmlElement(Order = 6)]
    public DateTime ModifiedDate
    {
      get
      {
        return this.modifiedDateField;
      }
      set
      {
        this.modifiedDateField = value;
        this.RaisePropertyChanged(nameof (ModifiedDate));
      }
    }

    [XmlArray(Order = 7)]
    public SurveyResponseAnswerModel[] Answers
    {
      get
      {
        return this.answersField;
      }
      set
      {
        this.answersField = value;
        this.RaisePropertyChanged(nameof (Answers));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
