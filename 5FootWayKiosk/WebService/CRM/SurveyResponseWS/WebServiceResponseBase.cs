﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.WebServiceResponseBase
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [XmlInclude(typeof (SurveyResponse_SaveResponse))]
  [DesignerCategory("code")]
  [Serializable]
  public abstract class WebServiceResponseBase : ResponseBase
  {
    private bool hasErrorField;
    private bool hasWarningField;
    private bool hasInformationField;
    private ErrorItem firstErrorField;
    private ErrorItem lastErrorField;

    [XmlElement(Order = 0)]
    public bool HasError
    {
      get
      {
        return this.hasErrorField;
      }
      set
      {
        this.hasErrorField = value;
        this.RaisePropertyChanged(nameof (HasError));
      }
    }

    [XmlElement(Order = 1)]
    public bool HasWarning
    {
      get
      {
        return this.hasWarningField;
      }
      set
      {
        this.hasWarningField = value;
        this.RaisePropertyChanged(nameof (HasWarning));
      }
    }

    [XmlElement(Order = 2)]
    public bool HasInformation
    {
      get
      {
        return this.hasInformationField;
      }
      set
      {
        this.hasInformationField = value;
        this.RaisePropertyChanged(nameof (HasInformation));
      }
    }

    [XmlElement(Order = 3)]
    public ErrorItem FirstError
    {
      get
      {
        return this.firstErrorField;
      }
      set
      {
        this.firstErrorField = value;
        this.RaisePropertyChanged(nameof (FirstError));
      }
    }

    [XmlElement(Order = 4)]
    public ErrorItem LastError
    {
      get
      {
        return this.lastErrorField;
      }
      set
      {
        this.lastErrorField = value;
        this.RaisePropertyChanged(nameof (LastError));
      }
    }
  }
}
