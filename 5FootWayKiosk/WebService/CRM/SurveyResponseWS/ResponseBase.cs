﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.ResponseBase
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [XmlInclude(typeof (SurveyResponse_SaveResponse))]
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlInclude(typeof (WebServiceResponseBase))]
  [Serializable]
  public abstract class ResponseBase : INotifyPropertyChanged
  {
    private ErrorItem[] errorsField;

    [XmlArray(Order = 0)]
    public ErrorItem[] Errors
    {
      get
      {
        return this.errorsField;
      }
      set
      {
        this.errorsField = value;
        this.RaisePropertyChanged(nameof (Errors));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
