﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.SurveyResponseSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.CRM.SurveyResponseWS
{
  [ServiceContract(ConfigurationName = "CRM.SurveyResponseWS.SurveyResponseSoap", Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface SurveyResponseSoap
  {
    [ServiceKnownType(typeof (ResponseBase))]
    [OperationContract(Action = "http://pms-mq/CRM/SurveyResponse/Save", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    SurveyResponse_SaveResponse Save(SurveyResponse_SaveRequest request);
  }
}
