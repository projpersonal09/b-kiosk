﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.ErrorItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [Serializable]
  public class ErrorItem : INotifyPropertyChanged
  {
    private ErrorType errorTypeField;
    private string idField;
    private string messageField;

    [XmlElement(Order = 0)]
    public ErrorType ErrorType
    {
      get
      {
        return this.errorTypeField;
      }
      set
      {
        this.errorTypeField = value;
        this.RaisePropertyChanged(nameof (ErrorType));
      }
    }

    [XmlElement(Order = 1)]
    public string Id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
        this.RaisePropertyChanged(nameof (Id));
      }
    }

    [XmlElement(Order = 2)]
    public string Message
    {
      get
      {
        return this.messageField;
      }
      set
      {
        this.messageField = value;
        this.RaisePropertyChanged(nameof (Message));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
