﻿// Decompiled with JetBrains decompiler
// Type: WebService.CRM.SurveyResponseWS.SurveyResponseAnswerModel
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.CRM.SurveyResponseWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/CRM/SurveyResponse")]
  [Serializable]
  public class SurveyResponseAnswerModel : INotifyPropertyChanged
  {
    private string surveyIdField;
    private long surveyResponseIdField;
    private string questionIdField;
    private string answerChoiceField;
    private string answerTextField;
    private string answerLongTextField;

    [XmlElement(Order = 0)]
    public string SurveyId
    {
      get
      {
        return this.surveyIdField;
      }
      set
      {
        this.surveyIdField = value;
        this.RaisePropertyChanged(nameof (SurveyId));
      }
    }

    [XmlElement(Order = 1)]
    public long SurveyResponseId
    {
      get
      {
        return this.surveyResponseIdField;
      }
      set
      {
        this.surveyResponseIdField = value;
        this.RaisePropertyChanged(nameof (SurveyResponseId));
      }
    }

    [XmlElement(Order = 2)]
    public string QuestionId
    {
      get
      {
        return this.questionIdField;
      }
      set
      {
        this.questionIdField = value;
        this.RaisePropertyChanged(nameof (QuestionId));
      }
    }

    [XmlElement(Order = 3)]
    public string AnswerChoice
    {
      get
      {
        return this.answerChoiceField;
      }
      set
      {
        this.answerChoiceField = value;
        this.RaisePropertyChanged(nameof (AnswerChoice));
      }
    }

    [XmlElement(Order = 4)]
    public string AnswerText
    {
      get
      {
        return this.answerTextField;
      }
      set
      {
        this.answerTextField = value;
        this.RaisePropertyChanged(nameof (AnswerText));
      }
    }

    [XmlElement(Order = 5)]
    public string AnswerLongText
    {
      get
      {
        return this.answerLongTextField;
      }
      set
      {
        this.answerLongTextField = value;
        this.RaisePropertyChanged(nameof (AnswerLongText));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
