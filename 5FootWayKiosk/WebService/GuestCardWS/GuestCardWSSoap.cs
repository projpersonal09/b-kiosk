﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestCardWS.GuestCardWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.GuestCardWS
{
  [ServiceContract(ConfigurationName = "GuestCardWS.GuestCardWSSoap", Namespace = "http://pms-mq/WebService/GuestCardWS")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface GuestCardWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/GuestCardWS/CheckInGuestCard", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    GuestCard_CheckInResponse CheckInGuestCard(GuestCard_CheckInRequest validateRes);

    [OperationContract(Action = "http://pms-mq/WebService/GuestCardWS/CheckOutGuestCard", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    GuestCard_CheckOutResponse CheckOutGuestCard(GuestCard_CheckOutRequest checkOutReq);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/GuestCardWS/ValidateGuestCard", ReplyAction = "*")]
    GuestCard_ValidateResponse ValidateGuestCard(GuestCard_ValidateRequest validateRes);
  }
}
