﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestCardWS.GuestCardWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.GuestCardWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class GuestCardWSSoapClient : ClientBase<GuestCardWSSoap>, GuestCardWSSoap
  {
    public GuestCardWSSoapClient()
    {
    }

    public GuestCardWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public GuestCardWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public GuestCardWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public GuestCardWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public GuestCard_CheckInResponse CheckInGuestCard(GuestCard_CheckInRequest validateRes)
    {
      return this.Channel.CheckInGuestCard(validateRes);
    }

    public GuestCard_CheckOutResponse CheckOutGuestCard(GuestCard_CheckOutRequest checkOutReq)
    {
      return this.Channel.CheckOutGuestCard(checkOutReq);
    }

    public GuestCard_ValidateResponse ValidateGuestCard(GuestCard_ValidateRequest validateRes)
    {
      return this.Channel.ValidateGuestCard(validateRes);
    }
  }
}
