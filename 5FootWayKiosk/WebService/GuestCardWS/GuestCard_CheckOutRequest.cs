﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestCardWS.GuestCard_CheckOutRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.GuestCardWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [XmlType(Namespace = "http://pms-mq/WebService/GuestCardWS")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class GuestCard_CheckOutRequest : INotifyPropertyChanged
  {
    private string cardUIDField;
    private string cardNoField;
    private DateTime returnDateField;
    private long userIdField;

    [XmlElement(Order = 0)]
    public string cardUID
    {
      get
      {
        return this.cardUIDField;
      }
      set
      {
        this.cardUIDField = value;
        this.RaisePropertyChanged(nameof (cardUID));
      }
    }

    [XmlElement(Order = 1)]
    public string cardNo
    {
      get
      {
        return this.cardNoField;
      }
      set
      {
        this.cardNoField = value;
        this.RaisePropertyChanged(nameof (cardNo));
      }
    }

    [XmlElement(Order = 2)]
    public DateTime returnDate
    {
      get
      {
        return this.returnDateField;
      }
      set
      {
        this.returnDateField = value;
        this.RaisePropertyChanged(nameof (returnDate));
      }
    }

    [XmlElement(Order = 3)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
