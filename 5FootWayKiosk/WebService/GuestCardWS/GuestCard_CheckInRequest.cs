﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestCardWS.GuestCard_CheckInRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.GuestCardWS
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/GuestCardWS")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class GuestCard_CheckInRequest : INotifyPropertyChanged
  {
    private string cardUIDField;
    private DateTime issueDateField;
    private DateTime startDateField;
    private DateTime endDateField;
    private long guestIdField;
    private long bookingIdField;
    private long roomStayIdField;
    private int propertyIdField;
    private long roomIdField;
    private long bedIdField;
    private string doorIdField;
    private string lockerIdField;
    private long userIdField;

    [XmlElement(Order = 0)]
    public string cardUID
    {
      get
      {
        return this.cardUIDField;
      }
      set
      {
        this.cardUIDField = value;
        this.RaisePropertyChanged(nameof (cardUID));
      }
    }

    [XmlElement(Order = 1)]
    public DateTime issueDate
    {
      get
      {
        return this.issueDateField;
      }
      set
      {
        this.issueDateField = value;
        this.RaisePropertyChanged(nameof (issueDate));
      }
    }

    [XmlElement(Order = 2)]
    public DateTime startDate
    {
      get
      {
        return this.startDateField;
      }
      set
      {
        this.startDateField = value;
        this.RaisePropertyChanged(nameof (startDate));
      }
    }

    [XmlElement(Order = 3)]
    public DateTime endDate
    {
      get
      {
        return this.endDateField;
      }
      set
      {
        this.endDateField = value;
        this.RaisePropertyChanged(nameof (endDate));
      }
    }

    [XmlElement(Order = 4)]
    public long guestId
    {
      get
      {
        return this.guestIdField;
      }
      set
      {
        this.guestIdField = value;
        this.RaisePropertyChanged(nameof (guestId));
      }
    }

    [XmlElement(Order = 5)]
    public long bookingId
    {
      get
      {
        return this.bookingIdField;
      }
      set
      {
        this.bookingIdField = value;
        this.RaisePropertyChanged(nameof (bookingId));
      }
    }

    [XmlElement(Order = 6)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 7)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 8)]
    public long roomId
    {
      get
      {
        return this.roomIdField;
      }
      set
      {
        this.roomIdField = value;
        this.RaisePropertyChanged(nameof (roomId));
      }
    }

    [XmlElement(Order = 9)]
    public long bedId
    {
      get
      {
        return this.bedIdField;
      }
      set
      {
        this.bedIdField = value;
        this.RaisePropertyChanged(nameof (bedId));
      }
    }

    [XmlElement(Order = 10)]
    public string doorId
    {
      get
      {
        return this.doorIdField;
      }
      set
      {
        this.doorIdField = value;
        this.RaisePropertyChanged(nameof (doorId));
      }
    }

    [XmlElement(Order = 11)]
    public string lockerId
    {
      get
      {
        return this.lockerIdField;
      }
      set
      {
        this.lockerIdField = value;
        this.RaisePropertyChanged(nameof (lockerId));
      }
    }

    [XmlElement(Order = 12)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
