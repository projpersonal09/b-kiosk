﻿// Decompiled with JetBrains decompiler
// Type: WebService.UpdateService
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using Helper;
using KioskBusinessEntity.Database_Entity;
using Newtonsoft.Json;
using System;
using System.ServiceModel;
using WebService.BillingWS;
using WebService.BookingWS;
using WebService.CRM.SurveyResponseWS;
using WebService.GuestCardWS;
using WebService.GuestWS;

namespace WebService
{
  public static class UpdateService
  {
    public static bool UpdateBookingStatus(KioskBusinessEntity.Database_Entity.Booking b)
    {
      try
      {
        using (BookingWSSoapClient bookingWsSoapClient = new BookingWSSoapClient("BookingWSSoap12"))
        {
          bookingWsSoapClient.Open();
          if (!bookingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Booking Web service is not available.");
          Booking_UpdateResponse bookingUpdateResponse = bookingWsSoapClient.UpdateBooking(new Booking_UpdateRequest()
          {
            bookingId = b.bookingId,
            propertyId = b.propertyId,
            bookingStatusCode = b.bookingStatusCode,
            userId = b.userId,
            date = DateTimeHelper.RemoveTimezone(DateTime.Now)
          });
          if (!bookingUpdateResponse.status)
            throw new WebServiceException(bookingUpdateResponse.errorMessage);
          return bookingUpdateResponse.status;
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) b), ex);
      }
    }

    public static long GuestCheckIn(KioskBusinessEntity.Database_Entity.Guest g)
    {
      try
      {
        using (GuestWSSoapClient guestWsSoapClient = new GuestWSSoapClient("GuestWSSoap12"))
        {
          guestWsSoapClient.Open();
          if (!guestWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Guest Web service is not available.");
          Guest_AddResponse guestAddResponse = guestWsSoapClient.AddGuest(new Guest_AddRequest()
          {
            guestId = g.Id,
            firstName = g.firstName,
            lastName = g.lastName,
            propertyId = g.propertyId,
            roomStayId = g.roomStayId,
            dateOfBirth = DateTimeHelper.RemoveTimezone(g.dateOfBirth),
            nationality = g.nationality,
            passportNumber = g.passportNumber,
            userId = g.userId,
            address = g.address,
            company = g.company,
            email = g.email,
            gender = g.gender,
            guestCategoryCode = g.guestCategoryCode,
            phone = g.phone,
            postalCode = g.postalCode,
            occupation = g.occupation,
            prevDestination = g.prevDestination,
            nextDestination = g.nextDestination
          });
          if (guestAddResponse.status)
            return guestAddResponse.guestId;
          throw new WebServiceException(guestAddResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) g), ex);
      }
    }

    public static bool CheckInGuestCard(KioskBusinessEntity.Database_Entity.RoomGuestCard rgc)
    {
      try
      {
        using (GuestCardWSSoapClient cardWsSoapClient = new GuestCardWSSoapClient("GuestCardWSSoap12"))
        {
          cardWsSoapClient.Open();
          if (!cardWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Guest Card Web service is not available.");
          GuestCard_CheckInResponse cardCheckInResponse = cardWsSoapClient.CheckInGuestCard(new GuestCard_CheckInRequest()
          {
            cardUID = rgc.cardUID,
            bookingId = rgc.bookingId,
            startDate = DateTimeHelper.RemoveTimezone(rgc.startDate),
            endDate = DateTimeHelper.RemoveTimezone(rgc.endDate),
            issueDate = DateTimeHelper.RemoveTimezone(rgc.issueDate),
            propertyId = rgc.propertyId,
            bedId = rgc.bedId,
            roomId = rgc.roomId,
            lockerId = rgc.lockerId,
            doorId = rgc.doorId,
            roomStayId = rgc.roomStayId,
            userId = rgc.userId,
            guestId = rgc.guestId
          });
          if (!cardCheckInResponse.status)
            throw new WebServiceException(cardCheckInResponse.errorMessage);
          return true;
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) rgc), ex);
      }
    }

    public static bool CheckOutGuestCard(KioskBusinessEntity.Database_Entity.RoomGuestCard rgc)
    {
      try
      {
        using (GuestCardWSSoapClient cardWsSoapClient = new GuestCardWSSoapClient("GuestCardWSSoap12"))
        {
          cardWsSoapClient.Open();
          if (!cardWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Guest Card Web service is not available.");
          GuestCard_CheckOutResponse checkOutResponse = cardWsSoapClient.CheckOutGuestCard(new GuestCard_CheckOutRequest()
          {
            cardUID = rgc.cardUID,
            returnDate = DateTimeHelper.RemoveTimezone(rgc.returnDate),
            userId = rgc.userId,
            cardNo = rgc.cardNo
          });
          if (!checkOutResponse.status)
            throw new WebServiceException(checkOutResponse.errorMessage);
          return true;
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) rgc), ex);
      }
    }

    public static bool PaySecurityDeposit(KioskBusinessEntity.Database_Entity.SecurityDeposit sd)
    {
      try
      {
        using (BillingWSSoapClient billingWsSoapClient = new BillingWSSoapClient("BillingWSSoap12"))
        {
          billingWsSoapClient.Open();
          if (!billingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Security Deposit Web service is not available.");
          Billing_PaySecurityDepositResponse securityDepositResponse = billingWsSoapClient.PaySecurityDeposit(new Billing_PaySecurityDepositRequest()
          {
            shiftId = sd.shiftId,
            bookingId = sd.bookingId,
            amount = sd.amount,
            currencyCode = sd.currencyCode,
            invoiceNumber = sd.invoiceNumber,
            propertyId = sd.propertyId,
            roomStayId = sd.roomStayId,
            userId = sd.userId,
            payDate = sd.date
          });
          if (securityDepositResponse.status)
            return true;
          throw new WebServiceException("WebService Error : " + securityDepositResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) sd), ex);
      }
    }

    public static bool ReturnSecurityDeposit(KioskBusinessEntity.Database_Entity.SecurityDeposit sd)
    {
      try
      {
        using (BillingWSSoapClient billingWsSoapClient = new BillingWSSoapClient("BillingWSSoap12"))
        {
          billingWsSoapClient.Open();
          if (!billingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Security Deposit Web service is not available.");
          Billing_ReturnSecurityDepositResponse securityDepositResponse = billingWsSoapClient.ReturnSecurityDeposit(new Billing_ReturnSecurityDepositRequest()
          {
            shiftId = sd.shiftId,
            bookingId = sd.bookingId,
            amount = sd.amount,
            currencyCode = sd.currencyCode,
            invoiceNumber = sd.invoiceNumber,
            propertyId = sd.propertyId,
            roomStayId = sd.roomStayId,
            userId = sd.userId,
            returnDate = sd.date
          });
          if (securityDepositResponse.status)
            return true;
          throw new WebServiceException("WebService Error : " + securityDepositResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) sd), ex);
      }
    }

    public static string UpdatePayment(KioskBusinessEntity.Database_Entity.BookingPayment p)
    {
      try
      {
        using (BillingWSSoapClient billingWsSoapClient = new BillingWSSoapClient("BillingWSSoap12"))
        {
          billingWsSoapClient.Open();
          if (!billingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Billing Web service is not available.");
          Billing_UpdatePaymentResponse updatePaymentResponse = billingWsSoapClient.UpdatePayment(new Billing_UpdatePaymentRequest()
          {
            shiftId = p.shiftId,
            bookingId = p.bookingId,
            amountPaid = p.amountPaid,
            invoiceNumber = p.invoiceNumber,
            paymentDateTime = p.paymentDateTime,
            paymentModeCode = p.paymentModeCode,
            currencyCode = p.currencyCode,
            propertyId = p.propertyId,
            userId = p.userId,
            surcharge = p.surcharge,
            roomStayId = p.roomStayId
          });
          if (updatePaymentResponse.status)
            return updatePaymentResponse.invoice;
          throw new WebServiceException("WebService Error : " + updatePaymentResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed, Data : " + JsonConvert.SerializeObject((object) p), ex);
      }
    }

    public static bool SaveSurveyResponse(SurveyResponse survey)
    {
      try
      {
        using (SurveyResponseSoapClient responseSoapClient = new SurveyResponseSoapClient("SurveyResponseSoap12"))
        {
          responseSoapClient.Open();
          if (!responseSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new WebServiceException("Survey Response Web service is not available.");
          SurveyResponse_SaveRequest request = new SurveyResponse_SaveRequest();
          request.SurveyResponse = new SurveyResponseModel();
          request.SurveyResponse.SurveyId = survey.SurveyId;
          request.SurveyResponse.SurveyResponseId = survey.SurveyResponseId;
          request.SurveyResponse.RecipientType = survey.RecipientType;
          request.SurveyResponse.RecipientId = survey.RecipientId;
          request.SurveyResponse.RecipientIpAddr = survey.RecipientIpAddr;
          request.SurveyResponse.CreatedDate = DateTimeHelper.RemoveTimezone(survey.CreatedDate);
          request.SurveyResponse.ModifiedDate = DateTimeHelper.RemoveTimezone(survey.ModifiedDate);
          if (survey.Answers != null && survey.Answers.Count > 0)
          {
            request.SurveyResponse.Answers = new SurveyResponseAnswerModel[survey.Answers.Count];
            for (int index = 0; index < survey.Answers.Count; ++index)
            {
              request.SurveyResponse.Answers[index] = new SurveyResponseAnswerModel();
              request.SurveyResponse.Answers[index].SurveyId = survey.Answers[index].SurveyId;
              request.SurveyResponse.Answers[index].SurveyResponseId = survey.Answers[index].SurveyResponseId;
              request.SurveyResponse.Answers[index].QuestionId = survey.Answers[index].QuestionId;
              request.SurveyResponse.Answers[index].AnswerChoice = survey.Answers[index].AnswerChoice;
              request.SurveyResponse.Answers[index].AnswerText = survey.Answers[index].AnswerText;
              request.SurveyResponse.Answers[index].AnswerLongText = survey.Answers[index].AnswerLongText;
            }
          }
          SurveyResponse_SaveResponse responseSaveResponse = responseSoapClient.Save(request);
          if (!responseSaveResponse.HasError)
            return true;
          throw new WebServiceException("Failed save Survey Response: " + responseSaveResponse.FirstError.Message + ", Data : " + JsonConvert.SerializeObject((object) survey));
        }
      }
      catch (Exception ex)
      {
        throw new WebServiceException("Failed save Survey Response, Data : " + JsonConvert.SerializeObject((object) survey), ex);
      }
    }
  }
}
