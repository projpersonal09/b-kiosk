﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStayWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.RoomStayWS
{
  [ServiceContract(ConfigurationName = "RoomStayWS.RoomStayWSSoap", Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface RoomStayWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/RoomStayWS/GetCheckIn", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    RoomStay_GetCheckInResponse GetCheckIn(RoomStay_GetCheckInRequest roomStayReq);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/RoomStayWS/GetCheckOut", ReplyAction = "*")]
    RoomStay_GetCheckOutResponse GetCheckOut(RoomStay_GetCheckOutRequest roomStayReq);
  }
}
