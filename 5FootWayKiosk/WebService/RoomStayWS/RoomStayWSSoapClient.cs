﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStayWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.RoomStayWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class RoomStayWSSoapClient : ClientBase<RoomStayWSSoap>, RoomStayWSSoap
  {
    public RoomStayWSSoapClient()
    {
    }

    public RoomStayWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public RoomStayWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public RoomStayWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public RoomStayWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public RoomStay_GetCheckInResponse GetCheckIn(RoomStay_GetCheckInRequest roomStayReq)
    {
      return this.Channel.GetCheckIn(roomStayReq);
    }

    public RoomStay_GetCheckOutResponse GetCheckOut(RoomStay_GetCheckOutRequest roomStayReq)
    {
      return this.Channel.GetCheckOut(roomStayReq);
    }
  }
}
