﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStay_CheckOutItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.RoomStayWS
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class RoomStay_CheckOutItem : INotifyPropertyChanged
  {
    private long roomStayIdField;
    private string roomDescField;
    private string cardUIDField;

    [XmlElement(Order = 0)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 1)]
    public string roomDesc
    {
      get
      {
        return this.roomDescField;
      }
      set
      {
        this.roomDescField = value;
        this.RaisePropertyChanged(nameof (roomDesc));
      }
    }

    [XmlElement(Order = 2)]
    public string cardUID
    {
      get
      {
        return this.cardUIDField;
      }
      set
      {
        this.cardUIDField = value;
        this.RaisePropertyChanged(nameof (cardUID));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
