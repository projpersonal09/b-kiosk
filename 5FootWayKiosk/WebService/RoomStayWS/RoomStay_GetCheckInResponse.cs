﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStay_GetCheckInResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.RoomStayWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [XmlType(Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class RoomStay_GetCheckInResponse : INotifyPropertyChanged
  {
    private RoomStay_CheckInItem[] listField;
    private bool statusField;
    private string errorMessageField;

    [XmlArray(Order = 0)]
    public RoomStay_CheckInItem[] list
    {
      get
      {
        return this.listField;
      }
      set
      {
        this.listField = value;
        this.RaisePropertyChanged(nameof (list));
      }
    }

    [XmlElement(Order = 1)]
    public bool status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    [XmlElement(Order = 2)]
    public string errorMessage
    {
      get
      {
        return this.errorMessageField;
      }
      set
      {
        this.errorMessageField = value;
        this.RaisePropertyChanged(nameof (errorMessage));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
