﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStay_GetCheckOutRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.RoomStayWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class RoomStay_GetCheckOutRequest : INotifyPropertyChanged
  {
    private long bookingIdField;

    [XmlElement(Order = 0)]
    public long bookingId
    {
      get
      {
        return this.bookingIdField;
      }
      set
      {
        this.bookingIdField = value;
        this.RaisePropertyChanged(nameof (bookingId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
