﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStay_CheckInGuestItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.RoomStayWS
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class RoomStay_CheckInGuestItem : INotifyPropertyChanged
  {
    private long guestIdField;
    private string guestNameField;
    private string passportNumberField;
    private string genderField;
    private string nationalityField;
    private DateTime? dOBField;
    private string guestEmailField;

    [XmlElement(Order = 0)]
    public long guestId
    {
      get
      {
        return this.guestIdField;
      }
      set
      {
        this.guestIdField = value;
        this.RaisePropertyChanged(nameof (guestId));
      }
    }

    [XmlElement(Order = 1)]
    public string guestName
    {
      get
      {
        return this.guestNameField;
      }
      set
      {
        this.guestNameField = value;
        this.RaisePropertyChanged(nameof (guestName));
      }
    }

    [XmlElement(Order = 2)]
    public string passportNumber
    {
      get
      {
        return this.passportNumberField;
      }
      set
      {
        this.passportNumberField = value;
        this.RaisePropertyChanged(nameof (passportNumber));
      }
    }

    [XmlElement(Order = 3)]
    public string gender
    {
      get
      {
        return this.genderField;
      }
      set
      {
        this.genderField = value;
        this.RaisePropertyChanged(nameof (gender));
      }
    }

    [XmlElement(Order = 4)]
    public string nationality
    {
      get
      {
        return this.nationalityField;
      }
      set
      {
        this.nationalityField = value;
        this.RaisePropertyChanged(nameof (nationality));
      }
    }

    [XmlElement(IsNullable = true, Order = 5)]
    public DateTime? DOB
    {
      get
      {
        return this.dOBField;
      }
      set
      {
        this.dOBField = value;
        this.RaisePropertyChanged(nameof (DOB));
      }
    }

    [XmlElement(Order = 6)]
    public string guestEmail
    {
      get
      {
        return this.guestEmailField;
      }
      set
      {
        this.guestEmailField = value;
        this.RaisePropertyChanged(nameof (guestEmail));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
