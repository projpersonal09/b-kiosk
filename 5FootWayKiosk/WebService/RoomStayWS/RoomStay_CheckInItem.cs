﻿// Decompiled with JetBrains decompiler
// Type: WebService.RoomStayWS.RoomStay_CheckInItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.RoomStayWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/RoomStayWS")]
  [DebuggerStepThrough]
  [Serializable]
  public class RoomStay_CheckInItem : INotifyPropertyChanged
  {
    private long roomStayIdField;
    private string roomNameField;
    private string roomDescField;
    private long roomIdField;
    private long bedIdField;
    private string doorIdField;
    private string lockerIdField;
    private int capabilityField;
    private DateTime startDateField;
    private DateTime endDateField;
    private string roomTypeGenderField;
    private bool roomTypeIsPrivateRoomField;
    private RoomStay_CheckInGuestItem[] guestListField;

    [XmlElement(Order = 0)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 1)]
    public string roomName
    {
      get
      {
        return this.roomNameField;
      }
      set
      {
        this.roomNameField = value;
        this.RaisePropertyChanged(nameof (roomName));
      }
    }

    [XmlElement(Order = 2)]
    public string roomDesc
    {
      get
      {
        return this.roomDescField;
      }
      set
      {
        this.roomDescField = value;
        this.RaisePropertyChanged(nameof (roomDesc));
      }
    }

    [XmlElement(Order = 3)]
    public long roomId
    {
      get
      {
        return this.roomIdField;
      }
      set
      {
        this.roomIdField = value;
        this.RaisePropertyChanged(nameof (roomId));
      }
    }

    [XmlElement(Order = 4)]
    public long bedId
    {
      get
      {
        return this.bedIdField;
      }
      set
      {
        this.bedIdField = value;
        this.RaisePropertyChanged(nameof (bedId));
      }
    }

    [XmlElement(Order = 5)]
    public string doorId
    {
      get
      {
        return this.doorIdField;
      }
      set
      {
        this.doorIdField = value;
        this.RaisePropertyChanged(nameof (doorId));
      }
    }

    [XmlElement(Order = 6)]
    public string lockerId
    {
      get
      {
        return this.lockerIdField;
      }
      set
      {
        this.lockerIdField = value;
        this.RaisePropertyChanged(nameof (lockerId));
      }
    }

    [XmlElement(Order = 7)]
    public int capability
    {
      get
      {
        return this.capabilityField;
      }
      set
      {
        this.capabilityField = value;
        this.RaisePropertyChanged(nameof (capability));
      }
    }

    [XmlElement(Order = 8)]
    public DateTime startDate
    {
      get
      {
        return this.startDateField;
      }
      set
      {
        this.startDateField = value;
        this.RaisePropertyChanged(nameof (startDate));
      }
    }

    [XmlElement(Order = 9)]
    public DateTime endDate
    {
      get
      {
        return this.endDateField;
      }
      set
      {
        this.endDateField = value;
        this.RaisePropertyChanged(nameof (endDate));
      }
    }

    [XmlElement(Order = 10)]
    public string RoomTypeGender
    {
      get
      {
        return this.roomTypeGenderField;
      }
      set
      {
        this.roomTypeGenderField = value;
        this.RaisePropertyChanged(nameof (RoomTypeGender));
      }
    }

    [XmlElement(Order = 11)]
    public bool RoomTypeIsPrivateRoom
    {
      get
      {
        return this.roomTypeIsPrivateRoomField;
      }
      set
      {
        this.roomTypeIsPrivateRoomField = value;
        this.RaisePropertyChanged(nameof (RoomTypeIsPrivateRoom));
      }
    }

    [XmlArray(Order = 12)]
    public RoomStay_CheckInGuestItem[] guestList
    {
      get
      {
        return this.guestListField;
      }
      set
      {
        this.guestListField = value;
        this.RaisePropertyChanged(nameof (guestList));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
