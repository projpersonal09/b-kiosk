﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftService
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using Helper;
using KioskBusinessEntity.Database_Entity;
using System;
using System.ServiceModel;
using WebService.KioskShiftWS;
using WebService.ShiftWS;

namespace WebService
{
  public static class ShiftService
  {
    public static KioskBusinessEntity.Database_Entity.ShiftSummary StartShiftReport(Decimal amount, string currencyCode, int propertyId, string remark, long userId)
    {
      KioskBusinessEntity.Database_Entity.ShiftSummary shiftSummary = new KioskBusinessEntity.Database_Entity.ShiftSummary();
      try
      {
        using (KioskShiftWSSoapClient shiftWsSoapClient = new KioskShiftWSSoapClient("KioskShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_StartShiftReportRequest req = new Shift_StartShiftReportRequest();
          req.amount = amount;
          req.currencyCode = currencyCode;
          req.propertyId = propertyId;
          req.remark = remark;
          req.shiftDate = DateTimeHelper.RemoveTimezone(DateTime.Now);
          req.userId = userId;
          Shift_StartShiftReportResponse shiftReportResponse = shiftWsSoapClient.StartShiftReport(req);
          if (!shiftReportResponse.status)
            throw new Exception("WebService Error : " + shiftReportResponse.errorMessage);
          shiftSummary.propertyId = propertyId;
          shiftSummary.PMS_shiftId = shiftReportResponse.shiftId;
          shiftSummary.shiftNumber = shiftReportResponse.shiftNumber;
          shiftSummary.currencyCode = currencyCode;
          shiftSummary.startDate = req.shiftDate;
          shiftSummary.floatStart = amount;
          return shiftSummary;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static void EmptyCashBox(int propertyId, long shiftId, string remarks, long userId, string currencyCode, Decimal totalCash)
    {
      try
      {
        using (KioskShiftWSSoapClient shiftWsSoapClient = new KioskShiftWSSoapClient("KioskShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_CashSubmitResponse cashSubmitResponse = shiftWsSoapClient.SubmitCash(new Shift_CashSubmitRequest()
          {
            PropertyId = propertyId,
            ShiftId = shiftId,
            Remarks = remarks,
            UserId = userId,
            CurrencyCode = currencyCode,
            TransactionDate = DateTimeHelper.RemoveTimezone(DateTime.Now),
            TotalCash = totalCash
          });
          if (!cashSubmitResponse.status)
            throw new Exception("WebService Error : " + cashSubmitResponse.errorMessage);
        }
      }
      catch
      {
        throw;
      }
    }

    public static Decimal EndShiftReport(long shiftId, int propertyId, string remark, long userId, Decimal cashCounted)
    {
      try
      {
        using (KioskShiftWSSoapClient shiftWsSoapClient = new KioskShiftWSSoapClient("KioskShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_EndShiftReportResponse shiftReportResponse = shiftWsSoapClient.EndShiftReport(new Shift_EndShiftReportRequest()
          {
            shiftId = shiftId,
            propertyId = propertyId,
            remark = remark,
            userId = userId,
            endDate = DateTimeHelper.RemoveTimezone(DateTime.Now),
            cashCounted = cashCounted
          });
          if (!shiftReportResponse.status)
            throw new Exception("WebService Error : " + shiftReportResponse.errorMessage);
          return shiftReportResponse.totalCashAmount;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static void CashInOut(KioskBusinessEntity.Database_Entity.CashInOut cashInOut)
    {
      try
      {
        using (KioskShiftWSSoapClient shiftWsSoapClient = new KioskShiftWSSoapClient("KioskShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_CashInOutResponse cashInOutResponse = shiftWsSoapClient.CashInOut(new Shift_CashInOutRequest()
          {
            amount = cashInOut.amount,
            currencyCode = cashInOut.currencyCode,
            propertyId = cashInOut.propertyId,
            remark = cashInOut.remarks,
            shiftId = cashInOut.shiftId,
            userId = cashInOut.userId,
            transactionDate = DateTimeHelper.RemoveTimezone(cashInOut.transactionDate),
            mode = cashInOut.paymentModeCode
          });
          if (!cashInOutResponse.status)
            throw new Exception("WebService Error : " + cashInOutResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static void CashInOutDetail(CashInOutDetail obj)
    {
      try
      {
        using (KioskShiftWSSoapClient shiftWsSoapClient = new KioskShiftWSSoapClient("KioskShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_CashInOutDetailRequest req = new Shift_CashInOutDetailRequest();
          req.shiftId = obj.shiftId;
          req.paymentModeCode = obj.paymentModeCode;
          req.cashNoteCountList = obj.cashNoteCountList.ToArray();
          req.cashNoteValueList = obj.cashNoteValueList.ToArray();
          Shift_CashInOutDetailResponse outDetailResponse = shiftWsSoapClient.CashInOutDetail(req);
          if (!outDetailResponse.status)
            throw new Exception("WebService Error : " + outDetailResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static KioskBusinessEntity.Database_Entity.ShiftSummary GetFrontDeskShift(int propertyId)
    {
      try
      {
        using (ShiftWSSoapClient shiftWsSoapClient = new ShiftWSSoapClient("ShiftWSSoap12"))
        {
          shiftWsSoapClient.Open();
          if (!shiftWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Shift Web service is not available.");
          Shift_GetActiveShiftResponse frontDeskShift = shiftWsSoapClient.GetFrontDeskShift(new Shift_GetActiveShiftRequest()
          {
            PropertyId = propertyId
          });
          if (!frontDeskShift.Status)
            throw new Exception(frontDeskShift.ErrorMessage);
          return new KioskBusinessEntity.Database_Entity.ShiftSummary()
          {
            PMS_shiftId = frontDeskShift.ShiftWork.Id,
            shiftNumber = frontDeskShift.ShiftWork.ShiftNo,
            startByName = frontDeskShift.ShiftWork.UserName,
            startDate = frontDeskShift.ShiftWork.StartDate
          };
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
