﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestWS.Guest_AddResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.GuestWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/GuestWS")]
  [DebuggerStepThrough]
  [Serializable]
  public class Guest_AddResponse : INotifyPropertyChanged
  {
    private long guestIdField;
    private bool statusField;
    private string errorMessageField;

    [XmlElement(Order = 0)]
    public long guestId
    {
      get
      {
        return this.guestIdField;
      }
      set
      {
        this.guestIdField = value;
        this.RaisePropertyChanged(nameof (guestId));
      }
    }

    [XmlElement(Order = 1)]
    public bool status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    [XmlElement(Order = 2)]
    public string errorMessage
    {
      get
      {
        return this.errorMessageField;
      }
      set
      {
        this.errorMessageField = value;
        this.RaisePropertyChanged(nameof (errorMessage));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
