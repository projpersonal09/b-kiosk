﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestWS.Guest_AddRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.GuestWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/GuestWS")]
  [Serializable]
  public class Guest_AddRequest : INotifyPropertyChanged
  {
    private long guestIdField;
    private long roomStayIdField;
    private string firstNameField;
    private string lastNameField;
    private string passportNumberField;
    private string nationalityField;
    private DateTime dateOfBirthField;
    private string emailField;
    private string genderField;
    private string addressField;
    private string phoneField;
    private string postalCodeField;
    private string companyField;
    private string guestCategoryCodeField;
    private int propertyIdField;
    private long userIdField;
    private string occupationField;
    private string nextDestinationField;
    private string prevDestinationField;

    [XmlElement(Order = 0)]
    public long guestId
    {
      get
      {
        return this.guestIdField;
      }
      set
      {
        this.guestIdField = value;
        this.RaisePropertyChanged(nameof (guestId));
      }
    }

    [XmlElement(Order = 1)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 2)]
    public string firstName
    {
      get
      {
        return this.firstNameField;
      }
      set
      {
        this.firstNameField = value;
        this.RaisePropertyChanged(nameof (firstName));
      }
    }

    [XmlElement(Order = 3)]
    public string lastName
    {
      get
      {
        return this.lastNameField;
      }
      set
      {
        this.lastNameField = value;
        this.RaisePropertyChanged(nameof (lastName));
      }
    }

    [XmlElement(Order = 4)]
    public string passportNumber
    {
      get
      {
        return this.passportNumberField;
      }
      set
      {
        this.passportNumberField = value;
        this.RaisePropertyChanged(nameof (passportNumber));
      }
    }

    [XmlElement(Order = 5)]
    public string nationality
    {
      get
      {
        return this.nationalityField;
      }
      set
      {
        this.nationalityField = value;
        this.RaisePropertyChanged(nameof (nationality));
      }
    }

    [XmlElement(Order = 6)]
    public DateTime dateOfBirth
    {
      get
      {
        return this.dateOfBirthField;
      }
      set
      {
        this.dateOfBirthField = value;
        this.RaisePropertyChanged(nameof (dateOfBirth));
      }
    }

    [XmlElement(Order = 7)]
    public string email
    {
      get
      {
        return this.emailField;
      }
      set
      {
        this.emailField = value;
        this.RaisePropertyChanged(nameof (email));
      }
    }

    [XmlElement(Order = 8)]
    public string gender
    {
      get
      {
        return this.genderField;
      }
      set
      {
        this.genderField = value;
        this.RaisePropertyChanged(nameof (gender));
      }
    }

    [XmlElement(Order = 9)]
    public string address
    {
      get
      {
        return this.addressField;
      }
      set
      {
        this.addressField = value;
        this.RaisePropertyChanged(nameof (address));
      }
    }

    [XmlElement(Order = 10)]
    public string phone
    {
      get
      {
        return this.phoneField;
      }
      set
      {
        this.phoneField = value;
        this.RaisePropertyChanged(nameof (phone));
      }
    }

    [XmlElement(Order = 11)]
    public string postalCode
    {
      get
      {
        return this.postalCodeField;
      }
      set
      {
        this.postalCodeField = value;
        this.RaisePropertyChanged(nameof (postalCode));
      }
    }

    [XmlElement(Order = 12)]
    public string company
    {
      get
      {
        return this.companyField;
      }
      set
      {
        this.companyField = value;
        this.RaisePropertyChanged(nameof (company));
      }
    }

    [XmlElement(Order = 13)]
    public string guestCategoryCode
    {
      get
      {
        return this.guestCategoryCodeField;
      }
      set
      {
        this.guestCategoryCodeField = value;
        this.RaisePropertyChanged(nameof (guestCategoryCode));
      }
    }

    [XmlElement(Order = 14)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 15)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    [XmlElement(Order = 16)]
    public string occupation
    {
      get
      {
        return this.occupationField;
      }
      set
      {
        this.occupationField = value;
        this.RaisePropertyChanged(nameof (occupation));
      }
    }

    [XmlElement(Order = 17)]
    public string nextDestination
    {
      get
      {
        return this.nextDestinationField;
      }
      set
      {
        this.nextDestinationField = value;
        this.RaisePropertyChanged(nameof (nextDestination));
      }
    }

    [XmlElement(Order = 18)]
    public string prevDestination
    {
      get
      {
        return this.prevDestinationField;
      }
      set
      {
        this.prevDestinationField = value;
        this.RaisePropertyChanged(nameof (prevDestination));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
