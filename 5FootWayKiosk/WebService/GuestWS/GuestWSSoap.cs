﻿// Decompiled with JetBrains decompiler
// Type: WebService.GuestWS.GuestWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.GuestWS
{
  [ServiceContract(ConfigurationName = "GuestWS.GuestWSSoap", Namespace = "http://pms-mq/WebService/GuestWS")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface GuestWSSoap
  {
    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/GuestWS/AddGuest", ReplyAction = "*")]
    Guest_AddResponse AddGuest(Guest_AddRequest guestReq);
  }
}
