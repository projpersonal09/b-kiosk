﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftWS.ShiftWork
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.ShiftWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/ShiftWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class ShiftWork : INotifyPropertyChanged
  {
    private long idField;
    private DateTime startDateField;
    private string shiftNoField;
    private string userNameField;

    [XmlElement(Order = 0)]
    public long Id
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
        this.RaisePropertyChanged(nameof (Id));
      }
    }

    [XmlElement(Order = 1)]
    public DateTime StartDate
    {
      get
      {
        return this.startDateField;
      }
      set
      {
        this.startDateField = value;
        this.RaisePropertyChanged(nameof (StartDate));
      }
    }

    [XmlElement(Order = 2)]
    public string ShiftNo
    {
      get
      {
        return this.shiftNoField;
      }
      set
      {
        this.shiftNoField = value;
        this.RaisePropertyChanged(nameof (ShiftNo));
      }
    }

    [XmlElement(Order = 3)]
    public string UserName
    {
      get
      {
        return this.userNameField;
      }
      set
      {
        this.userNameField = value;
        this.RaisePropertyChanged(nameof (UserName));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
