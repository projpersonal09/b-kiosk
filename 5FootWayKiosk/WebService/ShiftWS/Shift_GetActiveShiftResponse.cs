﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftWS.Shift_GetActiveShiftResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.ShiftWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/ShiftWS")]
  [DebuggerStepThrough]
  [Serializable]
  public class Shift_GetActiveShiftResponse : _ResponseMessage
  {
    private ShiftWork shiftWorkField;

    [XmlElement(Order = 0)]
    public ShiftWork ShiftWork
    {
      get
      {
        return this.shiftWorkField;
      }
      set
      {
        this.shiftWorkField = value;
        this.RaisePropertyChanged(nameof (ShiftWork));
      }
    }
  }
}
