﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftWS.ShiftWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

namespace WebService.ShiftWS
{
  [ServiceContract(ConfigurationName = "ShiftWS.ShiftWSSoap", Namespace = "http://pms-mq/WebService/ShiftWS")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface ShiftWSSoap
  {
    [ServiceKnownType(typeof (_ResponseMessage))]
    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/ShiftWS/GetFrontDeskShift", ReplyAction = "*")]
    Shift_GetActiveShiftResponse GetFrontDeskShift(Shift_GetActiveShiftRequest request);

    [OperationContract(Action = "http://pms-mq/WebService/ShiftWS/GetFrontDeskShift", ReplyAction = "*")]
    Task<Shift_GetActiveShiftResponse> GetFrontDeskShiftAsync(Shift_GetActiveShiftRequest request);
  }
}
