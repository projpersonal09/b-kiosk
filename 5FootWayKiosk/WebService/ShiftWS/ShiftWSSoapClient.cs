﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftWS.ShiftWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace WebService.ShiftWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class ShiftWSSoapClient : ClientBase<ShiftWSSoap>, ShiftWSSoap
  {
    public ShiftWSSoapClient()
    {
    }

    public ShiftWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public ShiftWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ShiftWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public ShiftWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public Shift_GetActiveShiftResponse GetFrontDeskShift(Shift_GetActiveShiftRequest request)
    {
      return this.Channel.GetFrontDeskShift(request);
    }

    public Task<Shift_GetActiveShiftResponse> GetFrontDeskShiftAsync(Shift_GetActiveShiftRequest request)
    {
      return this.Channel.GetFrontDeskShiftAsync(request);
    }
  }
}
