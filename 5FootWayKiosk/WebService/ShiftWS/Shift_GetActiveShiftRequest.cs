﻿// Decompiled with JetBrains decompiler
// Type: WebService.ShiftWS.Shift_GetActiveShiftRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.ShiftWS
{
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/ShiftWS")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class Shift_GetActiveShiftRequest : INotifyPropertyChanged
  {
    private int propertyIdField;
    private string departmentCodeField;

    [XmlElement(Order = 0)]
    public int PropertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (PropertyId));
      }
    }

    [XmlElement(Order = 1)]
    public string DepartmentCode
    {
      get
      {
        return this.departmentCodeField;
      }
      set
      {
        this.departmentCodeField = value;
        this.RaisePropertyChanged(nameof (DepartmentCode));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
