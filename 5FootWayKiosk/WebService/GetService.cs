﻿// Decompiled with JetBrains decompiler
// Type: WebService.GetService
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using Helper;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using WebService.BillingWS;
using WebService.BookingWS;
using WebService.GuestCardWS;
using WebService.KioskUserWS;
using WebService.PropertyWS;
using WebService.RoomStayWS;

namespace WebService
{
  public static class GetService
  {
    public static List<BookingData> GetBookingCheckIn(string bookingNumber, string guestName, KioskData kioskData)
    {
      List<BookingData> bookingDataList = new List<BookingData>();
      try
      {
        using (BookingWSSoapClient bookingWsSoapClient = new BookingWSSoapClient("BookingWSSoap12"))
        {
          bookingWsSoapClient.Open();
          if (!bookingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Booking Web service is not available.");
          Booking_GetCheckInResponse bookingCheckIn = bookingWsSoapClient.GetBookingCheckIn(new Booking_GetCheckInRequest()
          {
            bookingNumber = bookingNumber,
            guestName = guestName,
            propertyId = kioskData.propertyId,
            checkInDate = DateTimeHelper.RemoveTimezone(DateTime.Now)
          });
          if (!bookingCheckIn.status)
            throw new Exception(bookingCheckIn.errorMessage);
          foreach (Booking_CheckInItem bookingCheckInItem in bookingCheckIn.list)
            bookingDataList.Add(new BookingData()
            {
              bookingId = bookingCheckInItem.bookingId,
              bookingDate = bookingCheckInItem.bookingDate,
              bookingOrderCode = bookingCheckInItem.bookingOrderCode,
              checkInDate = bookingCheckInItem.checkInDate,
              checkOutDate = bookingCheckInItem.checkOutDate,
              numberOfBed = bookingCheckInItem.numberOfBed,
              numberOfRoom = bookingCheckInItem.numberOfRoom,
              sourceName = bookingCheckInItem.sourceName
            });
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return bookingDataList;
    }

    public static List<BookingData> GetBookingCheckOut(long bookingId, KioskData kioskData)
    {
      List<BookingData> bookingDataList = new List<BookingData>();
      try
      {
        using (BookingWSSoapClient bookingWsSoapClient = new BookingWSSoapClient("BookingWSSoap12"))
        {
          bookingWsSoapClient.Open();
          if (!bookingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Booking Web service is not available.");
          Booking_GetCheckOutResponse bookingCheckOut = bookingWsSoapClient.GetBookingCheckOut(new Booking_GetCheckOutRequest()
          {
            bookingId = bookingId,
            propertyId = kioskData.propertyId
          });
          if (!bookingCheckOut.status)
            throw new Exception(bookingCheckOut.errorMessage);
          foreach (Booking_CheckOutItem bookingCheckOutItem in bookingCheckOut.list)
            bookingDataList.Add(new BookingData()
            {
              bookingDate = bookingCheckOutItem.bookingDate,
              bookingOrderCode = bookingCheckOutItem.bookingOrderCode,
              checkInDate = bookingCheckOutItem.checkInDate,
              checkOutDate = bookingCheckOutItem.checkOutDate,
              guestName = bookingCheckOutItem.guestName,
              roomStayId = bookingCheckOutItem.roomStayId
            });
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return bookingDataList;
    }

    public static long ValidateGuestCard(string cardUID)
    {
      try
      {
        using (GuestCardWSSoapClient cardWsSoapClient = new GuestCardWSSoapClient("GuestCardWSSoap12"))
        {
          cardWsSoapClient.Open();
          if (!cardWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Guest Card Web service is not available.");
          GuestCard_ValidateResponse validateResponse = cardWsSoapClient.ValidateGuestCard(new GuestCard_ValidateRequest()
          {
            cardUID = cardUID,
            cardNo = ""
          });
          if (validateResponse.status)
            return validateResponse.bookingId;
          throw new Exception(validateResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static List<SecurityDepositData> GetSecurityDepositList(BookingData bookingRes)
    {
      List<SecurityDepositData> securityDepositDataList = new List<SecurityDepositData>();
      try
      {
        using (BillingWSSoapClient billingWsSoapClient = new BillingWSSoapClient("BillingWSSoap12"))
        {
          billingWsSoapClient.Open();
          if (!billingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Security Deposit Web service is not available.");
          Billing_GetSecurityDepositResponse securityDeposit = billingWsSoapClient.GetSecurityDeposit(new Billing_GetSecurityDepositRequest()
          {
            bookingId = bookingRes.bookingId
          });
          if (!securityDeposit.status)
            throw new Exception("WebService Error : " + securityDeposit.errorMessage);
          foreach (SecurityDepositItem securityDepositItem in securityDeposit.list)
            securityDepositDataList.Add(new SecurityDepositData()
            {
              amount = securityDepositItem.amount,
              currencyCode = securityDepositItem.currencyCode,
              outstandingAmount = securityDepositItem.outstandingAmount,
              paymentItemNo = securityDepositItem.paymentItemNo,
              paymentNo = securityDepositItem.paymentNo,
              transactionCode = securityDepositItem.transactionCode
            });
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return securityDepositDataList;
    }

    public static List<BookingChargeData> GetChargeList(BookingData bookingRes, KioskData kioskData)
    {
      List<BookingChargeData> bookingChargeDataList = new List<BookingChargeData>();
      try
      {
        using (BillingWSSoapClient billingWsSoapClient = new BillingWSSoapClient("BillingWSSoap12"))
        {
          billingWsSoapClient.Open();
          if (!billingWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Billing Web service is not available.");
          Billing_GetChargeResponse charge = billingWsSoapClient.GetCharge(new Billing_GetChargeRequest()
          {
            bookingId = bookingRes.bookingId,
            propertyId = kioskData.propertyId
          });
          if (!charge.status)
            throw new Exception("WebService Error : " + charge.errorMessage);
          foreach (ChargeItem chargeItem in charge.list)
            bookingChargeDataList.Add(new BookingChargeData()
            {
              accountNo = chargeItem.accountNo,
              charge = chargeItem.charge,
              chargeCode = chargeItem.chargeCode,
              chargeDateTime = chargeItem.chargeDateTime,
              chargeItemNo = chargeItem.chargeItemNo,
              chargeName = chargeItem.chargeName,
              chargeNo = chargeItem.chargeNo,
              chargeSubCode = chargeItem.chargeSubCode,
              currencyCode = chargeItem.currencyCode,
              gst = chargeItem.gst,
              outstanding = chargeItem.outstanding,
              paid = chargeItem.paid,
              roomStayId = chargeItem.roomStayId,
              serviceCharge = chargeItem.serviceCharge,
              totalCharge = chargeItem.totalCharge
            });
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return bookingChargeDataList;
    }

    public static List<RoomStayData> GetRoomStayCheckIn(BookingData bookingRes, KioskData kioskData)
    {
      List<RoomStayData> roomStayDataList = new List<RoomStayData>();
      try
      {
        using (RoomStayWSSoapClient stayWsSoapClient = new RoomStayWSSoapClient("RoomStayWSSoap12"))
        {
          stayWsSoapClient.Open();
          if (!stayWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Room Stay Web service is not available.");
          RoomStay_GetCheckInResponse checkIn = stayWsSoapClient.GetCheckIn(new RoomStay_GetCheckInRequest()
          {
            bookingId = bookingRes.bookingId,
            propertyId = kioskData.propertyId
          });
          if (!checkIn.status)
            throw new Exception(checkIn.errorMessage);
          foreach (RoomStay_CheckInItem roomStayCheckInItem in checkIn.list)
          {
            RoomStayData roomStayData = new RoomStayData();
            roomStayData.bedId = roomStayCheckInItem.bedId;
            roomStayData.capability = roomStayCheckInItem.capability;
            roomStayData.doorId = roomStayCheckInItem.doorId;
            roomStayData.endDate = roomStayCheckInItem.endDate;
            roomStayData.lockerId = roomStayCheckInItem.lockerId;
            roomStayData.roomDesc = roomStayCheckInItem.roomDesc;
            roomStayData.roomId = roomStayCheckInItem.roomId;
            roomStayData.roomStayId = roomStayCheckInItem.roomStayId;
            roomStayData.startDate = roomStayCheckInItem.startDate;
            roomStayData.roomName = roomStayCheckInItem.roomName;
            roomStayData.RoomTypeGender = roomStayCheckInItem.RoomTypeGender;
            roomStayData.RoomTypeIsPrivateRoom = roomStayCheckInItem.RoomTypeIsPrivateRoom;
            roomStayData.guestList = new List<GuestData>();
            if (roomStayCheckInItem.guestList != null)
            {
              foreach (RoomStay_CheckInGuestItem guest in roomStayCheckInItem.guestList)
                roomStayData.guestList.Add(new GuestData()
                {
                  guestId = guest.guestId,
                  guestName = guest.guestName,
                  passportNumber = guest.passportNumber,
                  gender = guest.gender,
                  nationality = guest.nationality,
                  DOB = guest.DOB.HasValue ? DateTimeHelper.RemoveTimezone(guest.DOB.Value) : DateTime.MinValue,
                  guestEmail = guest.guestEmail
                });
            }
            roomStayDataList.Add(roomStayData);
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return roomStayDataList;
    }

    public static List<RoomStayData> GetRoomStayCheckOut(BookingData bookingRes)
    {
      List<RoomStayData> roomStayDataList = new List<RoomStayData>();
      try
      {
        using (RoomStayWSSoapClient stayWsSoapClient = new RoomStayWSSoapClient("RoomStayWSSoap12"))
        {
          stayWsSoapClient.Open();
          if (!stayWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Room Stay Web service is not available.");
          RoomStay_GetCheckOutResponse checkOut = stayWsSoapClient.GetCheckOut(new RoomStay_GetCheckOutRequest()
          {
            bookingId = bookingRes.bookingId
          });
          if (!checkOut.status)
            throw new Exception(checkOut.errorMessage);
          foreach (RoomStay_CheckOutItem stayCheckOutItem in checkOut.list)
            roomStayDataList.Add(new RoomStayData()
            {
              cardUID = stayCheckOutItem.cardUID,
              roomDesc = stayCheckOutItem.roomDesc,
              roomStayId = stayCheckOutItem.roomStayId
            });
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return roomStayDataList;
    }

    public static Property_GetResponse GetProperty(string propertyCode)
    {
      Property_GetResponse propertyGetResponse = (Property_GetResponse) null;
      try
      {
        using (PropertyWSSoapClient propertyWsSoapClient = new PropertyWSSoapClient("PropertyWSSoap12"))
        {
          propertyWsSoapClient.Open();
          if (propertyWsSoapClient.State.Equals((object) CommunicationState.Opened))
          {
            propertyGetResponse = propertyWsSoapClient.GetProperty(new Property_GetRequest()
            {
              propertyCode = propertyCode
            });
            if (!propertyGetResponse.status)
              throw new Exception("WebService Error : " + propertyGetResponse.errorMessage);
          }
        }
      }
      catch(Exception ex)
      {
        throw;
      }
      return propertyGetResponse;
    }

    public static KioskUser_GetKioskUserIdResponse GetKioskUser(string kioskUserName)
    {
      try
      {
        using (KioskUserWSSoapClient userWsSoapClient = new KioskUserWSSoapClient("KioskUserWSSoap12"))
        {
          userWsSoapClient.Open();
          if (!userWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("KioskUserWSSoap12 is not available");
          KioskUser_GetKioskUserIdResponse kioskUserId = userWsSoapClient.GetKioskUserId(new KioskUser_GetKioskUserIdRequest()
          {
            kioskUserName = kioskUserName
          });
          if (kioskUserId.status)
            return kioskUserId;
          throw new Exception("WebService Error : " + kioskUserId.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static string LoginValidation(string kioskUserName, string password)
    {
      try
      {
        using (KioskUserWSSoapClient userWsSoapClient = new KioskUserWSSoapClient("KioskUserWSSoap12"))
        {
          userWsSoapClient.Open();
          if (!userWsSoapClient.State.Equals((object) CommunicationState.Opened))
            throw new Exception("Web service is not available");
          KioskUser_LoginValidationResponse validationResponse = userWsSoapClient.LoginValidation(new KioskUser_LoginValidationRequest()
          {
            userName = kioskUserName,
            password = password
          });
          if (validationResponse.status)
            return validationResponse.name;
          throw new Exception(validationResponse.errorMessage);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static KioskData InitializeKioskData(string kioskUserName, string propertyCode)
    {
      KioskData kioskData = new KioskData();
      kioskData.kioskUserName = kioskUserName;
      kioskData.propertyCode = propertyCode;
      try
      {
        Property_GetResponse property = GetService.GetProperty(propertyCode);
        if (property == null)
          throw new Exception("Failed to load kiosk property data.");
        kioskData.propertyId = property.propertyId;
        kioskData.propertyName = property.propertyName;
        kioskData.creditCardPerc = property.creditCardPerc;
        kioskData.doorAccessPropertyId = property.doorAccessPropertyId;
        kioskData.customerCode = property.doorAccessCustomerCode;
        kioskData.address1 = property.address1;
        kioskData.address2 = property.address2;
        kioskData.securityDepositAmount = property.securityDepositAmount;
        kioskData.registrationNumber = property.registrationNumber == null ? new List<string>() : ((IEnumerable<string>) property.registrationNumber).ToList<string>();
        kioskData.webSite = property.webSite;
        kioskData.currency = property.currency;
        kioskData.logoWidth = property.logoWidth;
        kioskData.logoHeight = property.logoHeight;
        kioskData.logoUrl = property.logoUrl;
        kioskData.defaultEmail = property.defaultEmail;
        TimeSpan result1;
        if (!TimeSpan.TryParse(property.timeStart, out result1))
          throw new Exception("TIMECARD_ST setting is unable to parse to TimeSpan.");
        kioskData.timeStart = result1;
        TimeSpan result2;
        if (!TimeSpan.TryParse(property.timeEnd, out result2))
          throw new Exception("TIMECARD_EN setting is unable to parse to TimeSpan.");
        kioskData.timeEnd = result2;
        KioskUser_GetKioskUserIdResponse kioskUser = GetService.GetKioskUser(kioskUserName);
        if (kioskUser == null)
          throw new Exception("Failed to load kiosk user data.");
        kioskData.kioskUserId = Convert.ToInt64(kioskUser.userId);
        if (kioskData.propertyId == 0)
          throw new Exception("Kiosk property for property code " + propertyCode + " is missing or invalid property code.");
        if (string.IsNullOrEmpty(kioskData.customerCode))
          throw new Exception("Kiosk customer code for property code " + propertyCode + " is missing or not configured.");
        if (kioskData.doorAccessPropertyId == 0)
          throw new Exception("Kiosk door access property for property code " + propertyCode + " is missing or not configured.");
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return kioskData;
    }
  }
}
