﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [KnownType(typeof (BetafaceFaceImageResponse))]
  [DataContract(Name = "BetafaceResponse", Namespace = "")]
  [KnownType(typeof (BetafaceImageInfoResponse))]
  [DebuggerStepThrough]
  [KnownType(typeof (BetafaceTransformResponse))]
  [KnownType(typeof (BetafaceRecognizeRequestResponse))]
  [KnownType(typeof (BetafaceRecognizeResponse))]
  [KnownType(typeof (BetafaceTransformRequestResponse))]
  [KnownType(typeof (BetafaceImageResponse))]
  [KnownType(typeof (BetafaceGetVerifyResultResponse))]
  [KnownType(typeof (BetafaceNewFaceResponse))]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [Serializable]
  public class BetafaceResponse : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private int int_responseField;
    [OptionalField]
    private string string_responseField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public int int_response
    {
      get
      {
        return this.int_responseField;
      }
      set
      {
        if (this.int_responseField.Equals(value))
          return;
        this.int_responseField = value;
        this.RaisePropertyChanged(nameof (int_response));
      }
    }

    [DataMember]
    public string string_response
    {
      get
      {
        return this.string_responseField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.string_responseField, (object) value))
          return;
        this.string_responseField = value;
        this.RaisePropertyChanged(nameof (string_response));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
