﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.Service1Client
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class Service1Client : ClientBase<IService1>, IService1
  {
    public Service1Client()
    {
    }

    public Service1Client(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public Service1Client(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public Service1Client(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public Service1Client(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public void GetOptions()
    {
      this.Channel.GetOptions();
    }

    public BetafaceImageResponse UploadNewImage_File(ImageRequestBinary img)
    {
      return this.Channel.UploadNewImage_File(img);
    }

    public BetafaceImageResponse UploadNewImage_Url(ImageRequestUrl img)
    {
      return this.Channel.UploadNewImage_Url(img);
    }

    public BetafaceImageResponse UploadImage(ImageRequest img)
    {
      return this.Channel.UploadImage(img);
    }

    public BetafaceImageInfoResponse GetImageInfo(ImageInfoRequestUid info)
    {
      return this.Channel.GetImageInfo(info);
    }

    public BetafaceImageInfoResponse GetImageFileInfo(ImageInfoRequestChecksum info)
    {
      return this.Channel.GetImageFileInfo(info);
    }

    public BetafaceFaceImageResponse GetFaceImage(FaceRequestId face)
    {
      return this.Channel.GetFaceImage(face);
    }

    public BetafaceResponse FaceInfo_Delete(FaceRequestId face)
    {
      return this.Channel.FaceInfo_Delete(face);
    }

    public BetafaceResponse FaceInfo_Update(FaceUpdateInfoRequest face_info)
    {
      return this.Channel.FaceInfo_Update(face_info);
    }

    public BetafaceNewFaceResponse FaceInfo_New(FaceNewInfoRequest face_new)
    {
      return this.Channel.FaceInfo_New(face_new);
    }

    public BetafaceResponse FaceInfo_SetPoints(FaceSetPointsRequest face_points)
    {
      return this.Channel.FaceInfo_SetPoints(face_points);
    }

    public BetafaceResponse FaceInfo_SetFaceImagePoints(FaceSetPointsRequest face_points)
    {
      return this.Channel.FaceInfo_SetFaceImagePoints(face_points);
    }

    public BetafaceResponse FaceInfo_SetTags(FaceSetTagsRequest face_tags)
    {
      return this.Channel.FaceInfo_SetTags(face_tags);
    }

    public BetafaceResponse SetPerson(SetPersonRequest request)
    {
      return this.Channel.SetPerson(request);
    }

    public BetafaceRecognizeRequestResponse RecognizeFaces(RecognizeFacesRequest request)
    {
      return this.Channel.RecognizeFaces(request);
    }

    public BetafaceRecognizeResponse GetRecognizeResult(RecognizeResultRequest recognize)
    {
      return this.Channel.GetRecognizeResult(recognize);
    }

    public BetafaceTransformRequestResponse TransformFaces(TransformFacesRequest request)
    {
      return this.Channel.TransformFaces(request);
    }

    public BetafaceTransformResponse GetTransformResult(TransformResultRequest compare)
    {
      return this.Channel.GetTransformResult(compare);
    }

    public BetafaceGetVerifyResultResponse GetVerifyResult(GetVerifyResultRequest request)
    {
      return this.Channel.GetVerifyResult(request);
    }
  }
}
