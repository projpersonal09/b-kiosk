﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageRequestUrl
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "ImageRequestUrl", Namespace = "")]
  [Serializable]
  public class ImageRequestUrl : ImageRequestBase
  {
    [OptionalField]
    private string image_urlField;
    [OptionalField]
    private string original_filenameField;

    [DataMember]
    public string image_url
    {
      get
      {
        return this.image_urlField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.image_urlField, (object) value))
          return;
        this.image_urlField = value;
        this.RaisePropertyChanged(nameof (image_url));
      }
    }

    [DataMember]
    public string original_filename
    {
      get
      {
        return this.original_filenameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.original_filenameField, (object) value))
          return;
        this.original_filenameField = value;
        this.RaisePropertyChanged(nameof (original_filename));
      }
    }
  }
}
