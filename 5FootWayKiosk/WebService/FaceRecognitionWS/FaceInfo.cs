﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceInfo
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "FaceInfo", Namespace = "")]
  [Serializable]
  public class FaceInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private double angleField;
    [OptionalField]
    private double heightField;
    [OptionalField]
    private Guid image_uidField;
    [OptionalField]
    private string person_nameField;
    [OptionalField]
    private List<PointInfo> pointsField;
    [OptionalField]
    private double scoreField;
    [OptionalField]
    private List<TagInfo> tagsField;
    [OptionalField]
    private Guid uidField;
    [OptionalField]
    private List<PointInfo> user_pointsField;
    [OptionalField]
    private double widthField;
    [OptionalField]
    private double xField;
    [OptionalField]
    private double yField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public double angle
    {
      get
      {
        return this.angleField;
      }
      set
      {
        if (this.angleField.Equals(value))
          return;
        this.angleField = value;
        this.RaisePropertyChanged(nameof (angle));
      }
    }

    [DataMember]
    public double height
    {
      get
      {
        return this.heightField;
      }
      set
      {
        if (this.heightField.Equals(value))
          return;
        this.heightField = value;
        this.RaisePropertyChanged(nameof (height));
      }
    }

    [DataMember]
    public Guid image_uid
    {
      get
      {
        return this.image_uidField;
      }
      set
      {
        if (this.image_uidField.Equals(value))
          return;
        this.image_uidField = value;
        this.RaisePropertyChanged(nameof (image_uid));
      }
    }

    [DataMember]
    public string person_name
    {
      get
      {
        return this.person_nameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.person_nameField, (object) value))
          return;
        this.person_nameField = value;
        this.RaisePropertyChanged(nameof (person_name));
      }
    }

    [DataMember]
    public List<PointInfo> points
    {
      get
      {
        return this.pointsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.pointsField, (object) value))
          return;
        this.pointsField = value;
        this.RaisePropertyChanged(nameof (points));
      }
    }

    [DataMember]
    public double score
    {
      get
      {
        return this.scoreField;
      }
      set
      {
        if (this.scoreField.Equals(value))
          return;
        this.scoreField = value;
        this.RaisePropertyChanged(nameof (score));
      }
    }

    [DataMember]
    public List<TagInfo> tags
    {
      get
      {
        return this.tagsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.tagsField, (object) value))
          return;
        this.tagsField = value;
        this.RaisePropertyChanged(nameof (tags));
      }
    }

    [DataMember]
    public Guid uid
    {
      get
      {
        return this.uidField;
      }
      set
      {
        if (this.uidField.Equals(value))
          return;
        this.uidField = value;
        this.RaisePropertyChanged(nameof (uid));
      }
    }

    [DataMember]
    public List<PointInfo> user_points
    {
      get
      {
        return this.user_pointsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.user_pointsField, (object) value))
          return;
        this.user_pointsField = value;
        this.RaisePropertyChanged(nameof (user_points));
      }
    }

    [DataMember]
    public double width
    {
      get
      {
        return this.widthField;
      }
      set
      {
        if (this.widthField.Equals(value))
          return;
        this.widthField = value;
        this.RaisePropertyChanged(nameof (width));
      }
    }

    [DataMember]
    public double x
    {
      get
      {
        return this.xField;
      }
      set
      {
        if (this.xField.Equals(value))
          return;
        this.xField = value;
        this.RaisePropertyChanged(nameof (x));
      }
    }

    [DataMember]
    public double y
    {
      get
      {
        return this.yField;
      }
      set
      {
        if (this.yField.Equals(value))
          return;
        this.yField = value;
        this.RaisePropertyChanged(nameof (y));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
