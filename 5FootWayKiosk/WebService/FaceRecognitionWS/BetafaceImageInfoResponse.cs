﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceImageInfoResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [DataContract(Name = "BetafaceImageInfoResponse", Namespace = "")]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [Serializable]
  public class BetafaceImageInfoResponse : BetafaceResponse
  {
    [OptionalField]
    private string checksumField;
    [OptionalField]
    private List<FaceInfo> facesField;
    [OptionalField]
    private string original_filenameField;
    [OptionalField]
    private Guid uidField;

    [DataMember]
    public string checksum
    {
      get
      {
        return this.checksumField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.checksumField, (object) value))
          return;
        this.checksumField = value;
        this.RaisePropertyChanged(nameof (checksum));
      }
    }

    [DataMember]
    public List<FaceInfo> faces
    {
      get
      {
        return this.facesField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.facesField, (object) value))
          return;
        this.facesField = value;
        this.RaisePropertyChanged(nameof (faces));
      }
    }

    [DataMember]
    public string original_filename
    {
      get
      {
        return this.original_filenameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.original_filenameField, (object) value))
          return;
        this.original_filenameField = value;
        this.RaisePropertyChanged(nameof (original_filename));
      }
    }

    [DataMember]
    public Guid uid
    {
      get
      {
        return this.uidField;
      }
      set
      {
        if (this.uidField.Equals(value))
          return;
        this.uidField = value;
        this.RaisePropertyChanged(nameof (uid));
      }
    }
  }
}
