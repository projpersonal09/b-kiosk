﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "ImageRequest", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class ImageRequest : ImageRequestBase
  {
    [OptionalField]
    private byte[] imageField;
    [OptionalField]
    private string image_base64Field;
    [OptionalField]
    private string original_filenameField;
    [OptionalField]
    private string urlField;

    [DataMember]
    public byte[] image
    {
      get
      {
        return this.imageField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.imageField, (object) value))
          return;
        this.imageField = value;
        this.RaisePropertyChanged(nameof (image));
      }
    }

    [DataMember]
    public string image_base64
    {
      get
      {
        return this.image_base64Field;
      }
      set
      {
        if (object.ReferenceEquals((object) this.image_base64Field, (object) value))
          return;
        this.image_base64Field = value;
        this.RaisePropertyChanged(nameof (image_base64));
      }
    }

    [DataMember]
    public string original_filename
    {
      get
      {
        return this.original_filenameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.original_filenameField, (object) value))
          return;
        this.original_filenameField = value;
        this.RaisePropertyChanged(nameof (original_filename));
      }
    }

    [DataMember]
    public string url
    {
      get
      {
        return this.urlField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.urlField, (object) value))
          return;
        this.urlField = value;
        this.RaisePropertyChanged(nameof (url));
      }
    }
  }
}
