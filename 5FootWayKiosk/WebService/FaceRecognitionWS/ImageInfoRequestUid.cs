﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageInfoRequestUid
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DataContract(Name = "ImageInfoRequestUid", Namespace = "")]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class ImageInfoRequestUid : BetafaceRequest
  {
    [OptionalField]
    private Guid img_uidField;

    [DataMember]
    public Guid img_uid
    {
      get
      {
        return this.img_uidField;
      }
      set
      {
        if (this.img_uidField.Equals(value))
          return;
        this.img_uidField = value;
        this.RaisePropertyChanged(nameof (img_uid));
      }
    }
  }
}
