﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceRecognizeResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "BetafaceRecognizeResponse", Namespace = "")]
  [Serializable]
  public class BetafaceRecognizeResponse : BetafaceResponse
  {
    [OptionalField]
    private List<FaceRecognizeInfo> faces_matchesField;
    [OptionalField]
    private Guid recognize_uidField;

    [DataMember]
    public List<FaceRecognizeInfo> faces_matches
    {
      get
      {
        return this.faces_matchesField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faces_matchesField, (object) value))
          return;
        this.faces_matchesField = value;
        this.RaisePropertyChanged(nameof (faces_matches));
      }
    }

    [DataMember]
    public Guid recognize_uid
    {
      get
      {
        return this.recognize_uidField;
      }
      set
      {
        if (this.recognize_uidField.Equals(value))
          return;
        this.recognize_uidField = value;
        this.RaisePropertyChanged(nameof (recognize_uid));
      }
    }
  }
}
