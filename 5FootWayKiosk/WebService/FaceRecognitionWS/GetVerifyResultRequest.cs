﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.GetVerifyResultRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DataContract(Name = "GetVerifyResultRequest", Namespace = "")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [Serializable]
  public class GetVerifyResultRequest : BetafaceRequest
  {
    [OptionalField]
    private string verify_stringField;

    [DataMember]
    public string verify_string
    {
      get
      {
        return this.verify_stringField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.verify_stringField, (object) value))
          return;
        this.verify_stringField = value;
        this.RaisePropertyChanged(nameof (verify_string));
      }
    }
  }
}
