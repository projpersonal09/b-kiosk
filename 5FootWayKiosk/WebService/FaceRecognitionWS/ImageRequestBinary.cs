﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageRequestBinary
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "ImageRequestBinary", Namespace = "")]
  [Serializable]
  public class ImageRequestBinary : ImageRequestBase
  {
    [OptionalField]
    private byte[] imagefile_dataField;
    [OptionalField]
    private string original_filenameField;

    [DataMember]
    public byte[] imagefile_data
    {
      get
      {
        return this.imagefile_dataField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.imagefile_dataField, (object) value))
          return;
        this.imagefile_dataField = value;
        this.RaisePropertyChanged(nameof (imagefile_data));
      }
    }

    [DataMember]
    public string original_filename
    {
      get
      {
        return this.original_filenameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.original_filenameField, (object) value))
          return;
        this.original_filenameField = value;
        this.RaisePropertyChanged(nameof (original_filename));
      }
    }
  }
}
