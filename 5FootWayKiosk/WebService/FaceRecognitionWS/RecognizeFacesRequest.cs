﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.RecognizeFacesRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "RecognizeFacesRequest", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class RecognizeFacesRequest : BetafaceRequest
  {
    [OptionalField]
    private string faces_uidsField;
    [OptionalField]
    private string parametersField;
    [OptionalField]
    private string targetsField;

    [DataMember]
    public string faces_uids
    {
      get
      {
        return this.faces_uidsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faces_uidsField, (object) value))
          return;
        this.faces_uidsField = value;
        this.RaisePropertyChanged(nameof (faces_uids));
      }
    }

    [DataMember]
    public string parameters
    {
      get
      {
        return this.parametersField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.parametersField, (object) value))
          return;
        this.parametersField = value;
        this.RaisePropertyChanged(nameof (parameters));
      }
    }

    [DataMember]
    public string targets
    {
      get
      {
        return this.targetsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.targetsField, (object) value))
          return;
        this.targetsField = value;
        this.RaisePropertyChanged(nameof (targets));
      }
    }
  }
}
