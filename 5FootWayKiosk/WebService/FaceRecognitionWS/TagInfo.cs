﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.TagInfo
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "TagInfo", Namespace = "")]
  [Serializable]
  public class TagInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private double confidenceField;
    [OptionalField]
    private string nameField;
    [OptionalField]
    private string valueField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public double confidence
    {
      get
      {
        return this.confidenceField;
      }
      set
      {
        if (this.confidenceField.Equals(value))
          return;
        this.confidenceField = value;
        this.RaisePropertyChanged(nameof (confidence));
      }
    }

    [DataMember]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.nameField, (object) value))
          return;
        this.nameField = value;
        this.RaisePropertyChanged(nameof (name));
      }
    }

    [DataMember]
    public string value
    {
      get
      {
        return this.valueField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.valueField, (object) value))
          return;
        this.valueField = value;
        this.RaisePropertyChanged(nameof (value));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
