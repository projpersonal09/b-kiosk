﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceGetVerifyResultResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "BetafaceGetVerifyResultResponse", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class BetafaceGetVerifyResultResponse : BetafaceResponse
  {
    [OptionalField]
    private string resultField;
    [OptionalField]
    private string result_dateField;
    [OptionalField]
    private string result_dbgField;
    [OptionalField]
    private string result_stringField;
    [OptionalField]
    private string result_userField;

    [DataMember]
    public string result
    {
      get
      {
        return this.resultField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.resultField, (object) value))
          return;
        this.resultField = value;
        this.RaisePropertyChanged(nameof (result));
      }
    }

    [DataMember]
    public string result_date
    {
      get
      {
        return this.result_dateField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.result_dateField, (object) value))
          return;
        this.result_dateField = value;
        this.RaisePropertyChanged(nameof (result_date));
      }
    }

    [DataMember]
    public string result_dbg
    {
      get
      {
        return this.result_dbgField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.result_dbgField, (object) value))
          return;
        this.result_dbgField = value;
        this.RaisePropertyChanged(nameof (result_dbg));
      }
    }

    [DataMember]
    public string result_string
    {
      get
      {
        return this.result_stringField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.result_stringField, (object) value))
          return;
        this.result_stringField = value;
        this.RaisePropertyChanged(nameof (result_string));
      }
    }

    [DataMember]
    public string result_user
    {
      get
      {
        return this.result_userField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.result_userField, (object) value))
          return;
        this.result_userField = value;
        this.RaisePropertyChanged(nameof (result_user));
      }
    }
  }
}
