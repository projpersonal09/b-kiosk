﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.TransformFacesRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "TransformFacesRequest", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class TransformFacesRequest : BetafaceRequest
  {
    [OptionalField]
    private string actionField;
    [OptionalField]
    private string faces_pointsField;
    [OptionalField]
    private string faces_uidsField;
    [OptionalField]
    private string parametersField;

    [DataMember]
    public string action
    {
      get
      {
        return this.actionField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.actionField, (object) value))
          return;
        this.actionField = value;
        this.RaisePropertyChanged(nameof (action));
      }
    }

    [DataMember]
    public string faces_points
    {
      get
      {
        return this.faces_pointsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faces_pointsField, (object) value))
          return;
        this.faces_pointsField = value;
        this.RaisePropertyChanged(nameof (faces_points));
      }
    }

    [DataMember]
    public string faces_uids
    {
      get
      {
        return this.faces_uidsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faces_uidsField, (object) value))
          return;
        this.faces_uidsField = value;
        this.RaisePropertyChanged(nameof (faces_uids));
      }
    }

    [DataMember]
    public string parameters
    {
      get
      {
        return this.parametersField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.parametersField, (object) value))
          return;
        this.parametersField = value;
        this.RaisePropertyChanged(nameof (parameters));
      }
    }
  }
}
