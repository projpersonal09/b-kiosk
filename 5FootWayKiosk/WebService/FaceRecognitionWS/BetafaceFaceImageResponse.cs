﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceFaceImageResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [DataContract(Name = "BetafaceFaceImageResponse", Namespace = "")]
  [Serializable]
  public class BetafaceFaceImageResponse : BetafaceResponse
  {
    [OptionalField]
    private byte[] face_imageField;
    [OptionalField]
    private FaceInfo face_infoField;
    [OptionalField]
    private Guid uidField;

    [DataMember]
    public byte[] face_image
    {
      get
      {
        return this.face_imageField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.face_imageField, (object) value))
          return;
        this.face_imageField = value;
        this.RaisePropertyChanged(nameof (face_image));
      }
    }

    [DataMember]
    public FaceInfo face_info
    {
      get
      {
        return this.face_infoField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.face_infoField, (object) value))
          return;
        this.face_infoField = value;
        this.RaisePropertyChanged(nameof (face_info));
      }
    }

    [DataMember]
    public Guid uid
    {
      get
      {
        return this.uidField;
      }
      set
      {
        if (this.uidField.Equals(value))
          return;
        this.uidField = value;
        this.RaisePropertyChanged(nameof (uid));
      }
    }
  }
}
