﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceSetTagsRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "FaceSetTagsRequest", Namespace = "")]
  [Serializable]
  public class FaceSetTagsRequest : FaceRequestId
  {
    [OptionalField]
    private List<TagInfo> tagsField;

    [DataMember]
    public List<TagInfo> tags
    {
      get
      {
        return this.tagsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.tagsField, (object) value))
          return;
        this.tagsField = value;
        this.RaisePropertyChanged(nameof (tags));
      }
    }
  }
}
