﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.PointInfo
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "PointInfo", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class PointInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string nameField;
    [OptionalField]
    private int typeField;
    [OptionalField]
    private double xField;
    [OptionalField]
    private double yField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string name
    {
      get
      {
        return this.nameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.nameField, (object) value))
          return;
        this.nameField = value;
        this.RaisePropertyChanged(nameof (name));
      }
    }

    [DataMember]
    public int type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        if (this.typeField.Equals(value))
          return;
        this.typeField = value;
        this.RaisePropertyChanged(nameof (type));
      }
    }

    [DataMember]
    public double x
    {
      get
      {
        return this.xField;
      }
      set
      {
        if (this.xField.Equals(value))
          return;
        this.xField = value;
        this.RaisePropertyChanged(nameof (x));
      }
    }

    [DataMember]
    public double y
    {
      get
      {
        return this.yField;
      }
      set
      {
        if (this.yField.Equals(value))
          return;
        this.yField = value;
        this.RaisePropertyChanged(nameof (y));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
