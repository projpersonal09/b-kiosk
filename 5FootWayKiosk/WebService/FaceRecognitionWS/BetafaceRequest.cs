﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [KnownType(typeof (ImageInfoRequestChecksum))]
  [KnownType(typeof (FaceRequestId))]
  [KnownType(typeof (FaceUpdateInfoRequest))]
  [KnownType(typeof (FaceSetPointsRequest))]
  [KnownType(typeof (FaceSetTagsRequest))]
  [KnownType(typeof (FaceNewInfoRequest))]
  [KnownType(typeof (SetPersonRequest))]
  [KnownType(typeof (RecognizeFacesRequest))]
  [KnownType(typeof (RecognizeResultRequest))]
  [KnownType(typeof (TransformFacesRequest))]
  [KnownType(typeof (TransformResultRequest))]
  [KnownType(typeof (GetVerifyResultRequest))]
  [KnownType(typeof (ImageRequestBase))]
  [KnownType(typeof (ImageRequestUrl))]
  [KnownType(typeof (ImageRequest))]
  [KnownType(typeof (ImageRequestBinary))]
  [DataContract(Name = "BetafaceRequest", Namespace = "")]
  [KnownType(typeof (ImageInfoRequestUid))]
  [Serializable]
  public class BetafaceRequest : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private string api_keyField;
    [OptionalField]
    private string api_secretField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public string api_key
    {
      get
      {
        return this.api_keyField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.api_keyField, (object) value))
          return;
        this.api_keyField = value;
        this.RaisePropertyChanged(nameof (api_key));
      }
    }

    [DataMember]
    public string api_secret
    {
      get
      {
        return this.api_secretField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.api_secretField, (object) value))
          return;
        this.api_secretField = value;
        this.RaisePropertyChanged(nameof (api_secret));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
