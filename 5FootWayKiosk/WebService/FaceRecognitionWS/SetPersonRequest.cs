﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.SetPersonRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "SetPersonRequest", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class SetPersonRequest : BetafaceRequest
  {
    [OptionalField]
    private string faces_uidsField;
    [OptionalField]
    private string person_idField;

    [DataMember]
    public string faces_uids
    {
      get
      {
        return this.faces_uidsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faces_uidsField, (object) value))
          return;
        this.faces_uidsField = value;
        this.RaisePropertyChanged(nameof (faces_uids));
      }
    }

    [DataMember]
    public string person_id
    {
      get
      {
        return this.person_idField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.person_idField, (object) value))
          return;
        this.person_idField = value;
        this.RaisePropertyChanged(nameof (person_id));
      }
    }
  }
}
