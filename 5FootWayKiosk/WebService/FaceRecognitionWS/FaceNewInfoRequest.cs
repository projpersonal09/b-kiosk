﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceNewInfoRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "FaceNewInfoRequest", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class FaceNewInfoRequest : BetafaceRequest
  {
    [OptionalField]
    private FaceInfo faceinfoField;
    [OptionalField]
    private Guid img_uidField;

    [DataMember]
    public FaceInfo faceinfo
    {
      get
      {
        return this.faceinfoField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.faceinfoField, (object) value))
          return;
        this.faceinfoField = value;
        this.RaisePropertyChanged(nameof (faceinfo));
      }
    }

    [DataMember]
    public Guid img_uid
    {
      get
      {
        return this.img_uidField;
      }
      set
      {
        if (this.img_uidField.Equals(value))
          return;
        this.img_uidField = value;
        this.RaisePropertyChanged(nameof (img_uid));
      }
    }
  }
}
