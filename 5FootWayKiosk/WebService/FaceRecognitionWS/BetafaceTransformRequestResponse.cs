﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceTransformRequestResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [DataContract(Name = "BetafaceTransformRequestResponse", Namespace = "")]
  [Serializable]
  public class BetafaceTransformRequestResponse : BetafaceResponse
  {
    [OptionalField]
    private Guid transform_uidField;

    [DataMember]
    public Guid transform_uid
    {
      get
      {
        return this.transform_uidField;
      }
      set
      {
        if (this.transform_uidField.Equals(value))
          return;
        this.transform_uidField = value;
        this.RaisePropertyChanged(nameof (transform_uid));
      }
    }
  }
}
