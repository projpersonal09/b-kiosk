﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.BetafaceNewFaceResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [DataContract(Name = "BetafaceNewFaceResponse", Namespace = "")]
  [Serializable]
  public class BetafaceNewFaceResponse : BetafaceResponse
  {
    [OptionalField]
    private Guid uidField;

    [DataMember]
    public Guid uid
    {
      get
      {
        return this.uidField;
      }
      set
      {
        if (this.uidField.Equals(value))
          return;
        this.uidField = value;
        this.RaisePropertyChanged(nameof (uid));
      }
    }
  }
}
