﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceRecognizeInfo
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DataContract(Name = "FaceRecognizeInfo", Namespace = "")]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class FaceRecognizeInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private Guid face_uidField;
    [OptionalField]
    private List<PersonMatchInfo> matchesField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public Guid face_uid
    {
      get
      {
        return this.face_uidField;
      }
      set
      {
        if (this.face_uidField.Equals(value))
          return;
        this.face_uidField = value;
        this.RaisePropertyChanged(nameof (face_uid));
      }
    }

    [DataMember]
    public List<PersonMatchInfo> matches
    {
      get
      {
        return this.matchesField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.matchesField, (object) value))
          return;
        this.matchesField = value;
        this.RaisePropertyChanged(nameof (matches));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
