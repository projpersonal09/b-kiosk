﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceSetPointsRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [DataContract(Name = "FaceSetPointsRequest", Namespace = "")]
  [Serializable]
  public class FaceSetPointsRequest : FaceRequestId
  {
    [OptionalField]
    private List<PointInfo> pointsField;

    [DataMember]
    public List<PointInfo> points
    {
      get
      {
        return this.pointsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.pointsField, (object) value))
          return;
        this.pointsField = value;
        this.RaisePropertyChanged(nameof (points));
      }
    }
  }
}
