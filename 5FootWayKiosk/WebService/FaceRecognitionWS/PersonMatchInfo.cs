﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.PersonMatchInfo
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [DataContract(Name = "PersonMatchInfo", Namespace = "")]
  [Serializable]
  public class PersonMatchInfo : IExtensibleDataObject, INotifyPropertyChanged
  {
    [NonSerialized]
    private ExtensionDataObject extensionDataField;
    [OptionalField]
    private double confidenceField;
    [OptionalField]
    private Guid face_uidField;
    [OptionalField]
    private bool is_matchField;
    [OptionalField]
    private string person_nameField;

    [Browsable(false)]
    public ExtensionDataObject ExtensionData
    {
      get
      {
        return this.extensionDataField;
      }
      set
      {
        this.extensionDataField = value;
      }
    }

    [DataMember]
    public double confidence
    {
      get
      {
        return this.confidenceField;
      }
      set
      {
        if (this.confidenceField.Equals(value))
          return;
        this.confidenceField = value;
        this.RaisePropertyChanged(nameof (confidence));
      }
    }

    [DataMember]
    public Guid face_uid
    {
      get
      {
        return this.face_uidField;
      }
      set
      {
        if (this.face_uidField.Equals(value))
          return;
        this.face_uidField = value;
        this.RaisePropertyChanged(nameof (face_uid));
      }
    }

    [DataMember]
    public bool is_match
    {
      get
      {
        return this.is_matchField;
      }
      set
      {
        if (this.is_matchField.Equals(value))
          return;
        this.is_matchField = value;
        this.RaisePropertyChanged(nameof (is_match));
      }
    }

    [DataMember]
    public string person_name
    {
      get
      {
        return this.person_nameField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.person_nameField, (object) value))
          return;
        this.person_nameField = value;
        this.RaisePropertyChanged(nameof (person_name));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
