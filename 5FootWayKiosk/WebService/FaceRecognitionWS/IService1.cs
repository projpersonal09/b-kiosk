﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.IService1
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "FaceRecognitionWS.IService1")]
  public interface IService1
  {
    [OperationContract(Action = "http://tempuri.org/IService1/GetOptions", ReplyAction = "http://tempuri.org/IService1/GetOptionsResponse")]
    void GetOptions();

    [OperationContract(Action = "http://tempuri.org/IService1/UploadNewImage_File", ReplyAction = "http://tempuri.org/IService1/UploadNewImage_FileResponse")]
    BetafaceImageResponse UploadNewImage_File(ImageRequestBinary img);

    [OperationContract(Action = "http://tempuri.org/IService1/UploadNewImage_Url", ReplyAction = "http://tempuri.org/IService1/UploadNewImage_UrlResponse")]
    BetafaceImageResponse UploadNewImage_Url(ImageRequestUrl img);

    [OperationContract(Action = "http://tempuri.org/IService1/UploadImage", ReplyAction = "http://tempuri.org/IService1/UploadImageResponse")]
    BetafaceImageResponse UploadImage(ImageRequest img);

    [OperationContract(Action = "http://tempuri.org/IService1/GetImageInfo", ReplyAction = "http://tempuri.org/IService1/GetImageInfoResponse")]
    BetafaceImageInfoResponse GetImageInfo(ImageInfoRequestUid info);

    [OperationContract(Action = "http://tempuri.org/IService1/GetImageFileInfo", ReplyAction = "http://tempuri.org/IService1/GetImageFileInfoResponse")]
    BetafaceImageInfoResponse GetImageFileInfo(ImageInfoRequestChecksum info);

    [OperationContract(Action = "http://tempuri.org/IService1/GetFaceImage", ReplyAction = "http://tempuri.org/IService1/GetFaceImageResponse")]
    BetafaceFaceImageResponse GetFaceImage(FaceRequestId face);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_Delete", ReplyAction = "http://tempuri.org/IService1/FaceInfo_DeleteResponse")]
    BetafaceResponse FaceInfo_Delete(FaceRequestId face);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_Update", ReplyAction = "http://tempuri.org/IService1/FaceInfo_UpdateResponse")]
    BetafaceResponse FaceInfo_Update(FaceUpdateInfoRequest face_info);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_New", ReplyAction = "http://tempuri.org/IService1/FaceInfo_NewResponse")]
    BetafaceNewFaceResponse FaceInfo_New(FaceNewInfoRequest face_new);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_SetPoints", ReplyAction = "http://tempuri.org/IService1/FaceInfo_SetPointsResponse")]
    BetafaceResponse FaceInfo_SetPoints(FaceSetPointsRequest face_points);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_SetFaceImagePoints", ReplyAction = "http://tempuri.org/IService1/FaceInfo_SetFaceImagePointsResponse")]
    BetafaceResponse FaceInfo_SetFaceImagePoints(FaceSetPointsRequest face_points);

    [OperationContract(Action = "http://tempuri.org/IService1/FaceInfo_SetTags", ReplyAction = "http://tempuri.org/IService1/FaceInfo_SetTagsResponse")]
    BetafaceResponse FaceInfo_SetTags(FaceSetTagsRequest face_tags);

    [OperationContract(Action = "http://tempuri.org/IService1/SetPerson", ReplyAction = "http://tempuri.org/IService1/SetPersonResponse")]
    BetafaceResponse SetPerson(SetPersonRequest request);

    [OperationContract(Action = "http://tempuri.org/IService1/RecognizeFaces", ReplyAction = "http://tempuri.org/IService1/RecognizeFacesResponse")]
    BetafaceRecognizeRequestResponse RecognizeFaces(RecognizeFacesRequest request);

    [OperationContract(Action = "http://tempuri.org/IService1/GetRecognizeResult", ReplyAction = "http://tempuri.org/IService1/GetRecognizeResultResponse")]
    BetafaceRecognizeResponse GetRecognizeResult(RecognizeResultRequest recognize);

    [OperationContract(Action = "http://tempuri.org/IService1/TransformFaces", ReplyAction = "http://tempuri.org/IService1/TransformFacesResponse")]
    BetafaceTransformRequestResponse TransformFaces(TransformFacesRequest request);

    [OperationContract(Action = "http://tempuri.org/IService1/GetTransformResult", ReplyAction = "http://tempuri.org/IService1/GetTransformResultResponse")]
    BetafaceTransformResponse GetTransformResult(TransformResultRequest compare);

    [OperationContract(Action = "http://tempuri.org/IService1/GetVerifyResult", ReplyAction = "http://tempuri.org/IService1/GetVerifyResultResponse")]
    BetafaceGetVerifyResultResponse GetVerifyResult(GetVerifyResultRequest request);
  }
}
