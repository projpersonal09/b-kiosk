﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageInfoRequestChecksum
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "ImageInfoRequestChecksum", Namespace = "")]
  [DebuggerStepThrough]
  [Serializable]
  public class ImageInfoRequestChecksum : BetafaceRequest
  {
    [OptionalField]
    private string img_checksumField;

    [DataMember]
    public string img_checksum
    {
      get
      {
        return this.img_checksumField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.img_checksumField, (object) value))
          return;
        this.img_checksumField = value;
        this.RaisePropertyChanged(nameof (img_checksum));
      }
    }
  }
}
