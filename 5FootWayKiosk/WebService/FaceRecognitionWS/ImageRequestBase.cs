﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.ImageRequestBase
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DataContract(Name = "ImageRequestBase", Namespace = "")]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DebuggerStepThrough]
  [KnownType(typeof (ImageRequestUrl))]
  [KnownType(typeof (ImageRequest))]
  [KnownType(typeof (ImageRequestBinary))]
  [Serializable]
  public class ImageRequestBase : BetafaceRequest
  {
    [OptionalField]
    private string detection_flagsField;

    [DataMember]
    public string detection_flags
    {
      get
      {
        return this.detection_flagsField;
      }
      set
      {
        if (object.ReferenceEquals((object) this.detection_flagsField, (object) value))
          return;
        this.detection_flagsField = value;
        this.RaisePropertyChanged(nameof (detection_flags));
      }
    }
  }
}
