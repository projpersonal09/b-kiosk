﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.RecognizeResultRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [DataContract(Name = "RecognizeResultRequest", Namespace = "")]
  [Serializable]
  public class RecognizeResultRequest : BetafaceRequest
  {
    [OptionalField]
    private Guid recognize_uidField;

    [DataMember]
    public Guid recognize_uid
    {
      get
      {
        return this.recognize_uidField;
      }
      set
      {
        if (this.recognize_uidField.Equals(value))
          return;
        this.recognize_uidField = value;
        this.RaisePropertyChanged(nameof (recognize_uid));
      }
    }
  }
}
