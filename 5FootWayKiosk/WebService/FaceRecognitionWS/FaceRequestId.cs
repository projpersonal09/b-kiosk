﻿// Decompiled with JetBrains decompiler
// Type: WebService.FaceRecognitionWS.FaceRequestId
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace WebService.FaceRecognitionWS
{
  [KnownType(typeof (FaceSetPointsRequest))]
  [KnownType(typeof (FaceSetTagsRequest))]
  [DataContract(Name = "FaceRequestId", Namespace = "")]
  [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
  [KnownType(typeof (FaceUpdateInfoRequest))]
  [DebuggerStepThrough]
  [Serializable]
  public class FaceRequestId : BetafaceRequest
  {
    [OptionalField]
    private Guid face_uidField;

    [DataMember]
    public Guid face_uid
    {
      get
      {
        return this.face_uidField;
      }
      set
      {
        if (this.face_uidField.Equals(value))
          return;
        this.face_uidField = value;
        this.RaisePropertyChanged(nameof (face_uid));
      }
    }
  }
}
