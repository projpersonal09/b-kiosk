﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskUserWS.KioskUser_LoginValidationRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskUserWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/KioskUserWS")]
  [Serializable]
  public class KioskUser_LoginValidationRequest : INotifyPropertyChanged
  {
    private string userNameField;
    private string passwordField;

    [XmlElement(Order = 0)]
    public string userName
    {
      get
      {
        return this.userNameField;
      }
      set
      {
        this.userNameField = value;
        this.RaisePropertyChanged(nameof (userName));
      }
    }

    [XmlElement(Order = 1)]
    public string password
    {
      get
      {
        return this.passwordField;
      }
      set
      {
        this.passwordField = value;
        this.RaisePropertyChanged(nameof (password));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
