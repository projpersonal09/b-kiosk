﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskUserWS.KioskUserWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.KioskUserWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class KioskUserWSSoapClient : ClientBase<KioskUserWSSoap>, KioskUserWSSoap
  {
    public KioskUserWSSoapClient()
    {
    }

    public KioskUserWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public KioskUserWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public KioskUserWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public KioskUserWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public KioskUser_GetKioskUserIdResponse GetKioskUserId(KioskUser_GetKioskUserIdRequest req)
    {
      return this.Channel.GetKioskUserId(req);
    }

    public KioskUser_LoginValidationResponse LoginValidation(KioskUser_LoginValidationRequest req)
    {
      return this.Channel.LoginValidation(req);
    }
  }
}
