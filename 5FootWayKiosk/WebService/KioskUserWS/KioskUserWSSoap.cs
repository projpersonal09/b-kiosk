﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskUserWS.KioskUserWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.KioskUserWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "KioskUserWS.KioskUserWSSoap", Namespace = "http://pms-mq/WebService/KioskUserWS")]
  public interface KioskUserWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/KioskUserWS/GetKioskUserId", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    KioskUser_GetKioskUserIdResponse GetKioskUserId(KioskUser_GetKioskUserIdRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/KioskUserWS/LoginValidation", ReplyAction = "*")]
    KioskUser_LoginValidationResponse LoginValidation(KioskUser_LoginValidationRequest req);
  }
}
