﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.Shift_StartShiftReportRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class Shift_StartShiftReportRequest : INotifyPropertyChanged
  {
    private Decimal amountField;
    private DateTime shiftDateField;
    private string currencyCodeField;
    private string remarkField;
    private int propertyIdField;
    private long userIdField;

    [XmlElement(Order = 0)]
    public Decimal amount
    {
      get
      {
        return this.amountField;
      }
      set
      {
        this.amountField = value;
        this.RaisePropertyChanged(nameof (amount));
      }
    }

    [XmlElement(Order = 1)]
    public DateTime shiftDate
    {
      get
      {
        return this.shiftDateField;
      }
      set
      {
        this.shiftDateField = value;
        this.RaisePropertyChanged(nameof (shiftDate));
      }
    }

    [XmlElement(Order = 2)]
    public string currencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (currencyCode));
      }
    }

    [XmlElement(Order = 3)]
    public string remark
    {
      get
      {
        return this.remarkField;
      }
      set
      {
        this.remarkField = value;
        this.RaisePropertyChanged(nameof (remark));
      }
    }

    [XmlElement(Order = 4)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 5)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
