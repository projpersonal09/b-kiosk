﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.Shift_StartShiftReportResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class Shift_StartShiftReportResponse : INotifyPropertyChanged
  {
    private long shiftIdField;
    private string shiftNumberField;
    private bool statusField;
    private string errorMessageField;

    [XmlElement(Order = 0)]
    public long shiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (shiftId));
      }
    }

    [XmlElement(Order = 1)]
    public string shiftNumber
    {
      get
      {
        return this.shiftNumberField;
      }
      set
      {
        this.shiftNumberField = value;
        this.RaisePropertyChanged(nameof (shiftNumber));
      }
    }

    [XmlElement(Order = 2)]
    public bool status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    [XmlElement(Order = 3)]
    public string errorMessage
    {
      get
      {
        return this.errorMessageField;
      }
      set
      {
        this.errorMessageField = value;
        this.RaisePropertyChanged(nameof (errorMessage));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
