﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.Shift_EndShiftReportRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [Serializable]
  public class Shift_EndShiftReportRequest : INotifyPropertyChanged
  {
    private int propertyIdField;
    private long userIdField;
    private long shiftIdField;
    private string remarkField;
    private DateTime endDateField;
    private Decimal cashCountedField;

    [XmlElement(Order = 0)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 1)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    [XmlElement(Order = 2)]
    public long shiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (shiftId));
      }
    }

    [XmlElement(Order = 3)]
    public string remark
    {
      get
      {
        return this.remarkField;
      }
      set
      {
        this.remarkField = value;
        this.RaisePropertyChanged(nameof (remark));
      }
    }

    [XmlElement(Order = 4)]
    public DateTime endDate
    {
      get
      {
        return this.endDateField;
      }
      set
      {
        this.endDateField = value;
        this.RaisePropertyChanged(nameof (endDate));
      }
    }

    [XmlElement(Order = 5)]
    public Decimal cashCounted
    {
      get
      {
        return this.cashCountedField;
      }
      set
      {
        this.cashCountedField = value;
        this.RaisePropertyChanged(nameof (cashCounted));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
