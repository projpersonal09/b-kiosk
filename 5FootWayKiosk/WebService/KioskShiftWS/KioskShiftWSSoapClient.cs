﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.KioskShiftWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.KioskShiftWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class KioskShiftWSSoapClient : ClientBase<KioskShiftWSSoap>, KioskShiftWSSoap
  {
    public KioskShiftWSSoapClient()
    {
    }

    public KioskShiftWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public KioskShiftWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public KioskShiftWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public KioskShiftWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public Shift_StartShiftReportResponse StartShiftReport(Shift_StartShiftReportRequest req)
    {
      return this.Channel.StartShiftReport(req);
    }

    public Shift_EndShiftReportResponse EndShiftReport(Shift_EndShiftReportRequest req)
    {
      return this.Channel.EndShiftReport(req);
    }

    public Shift_CashInOutResponse CashInOut(Shift_CashInOutRequest req)
    {
      return this.Channel.CashInOut(req);
    }

    public Shift_CashInOutDetailResponse CashInOutDetail(Shift_CashInOutDetailRequest req)
    {
      return this.Channel.CashInOutDetail(req);
    }

    public Shift_CashSubmitResponse SubmitCash(Shift_CashSubmitRequest req)
    {
      return this.Channel.SubmitCash(req);
    }
  }
}
