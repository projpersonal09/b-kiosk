﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.KioskShiftWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.KioskShiftWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "KioskShiftWS.KioskShiftWSSoap", Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  public interface KioskShiftWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/KioskShiftWS/StartShiftReport", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [ServiceKnownType(typeof (CashInOutDetailItem))]
    Shift_StartShiftReportResponse StartShiftReport(Shift_StartShiftReportRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [ServiceKnownType(typeof (CashInOutDetailItem))]
    [OperationContract(Action = "http://pms-mq/WebService/KioskShiftWS/EndShiftReport", ReplyAction = "*")]
    Shift_EndShiftReportResponse EndShiftReport(Shift_EndShiftReportRequest req);

    [OperationContract(Action = "http://pms-mq/WebService/KioskShiftWS/CashInOut", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    [ServiceKnownType(typeof (CashInOutDetailItem))]
    Shift_CashInOutResponse CashInOut(Shift_CashInOutRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/KioskShiftWS/CashInOutDetail", ReplyAction = "*")]
    [ServiceKnownType(typeof (CashInOutDetailItem))]
    Shift_CashInOutDetailResponse CashInOutDetail(Shift_CashInOutDetailRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/KioskShiftWS/SubmitCash", ReplyAction = "*")]
    [ServiceKnownType(typeof (CashInOutDetailItem))]
    Shift_CashSubmitResponse SubmitCash(Shift_CashSubmitRequest req);
  }
}
