﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.Shift_CashSubmitRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [Serializable]
  public class Shift_CashSubmitRequest : INotifyPropertyChanged
  {
    private int propertyIdField;
    private long shiftIdField;
    private string remarksField;
    private long userIdField;
    private string currencyCodeField;
    private DateTime transactionDateField;
    private Decimal totalCashField;

    [XmlElement(Order = 0)]
    public int PropertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (PropertyId));
      }
    }

    [XmlElement(Order = 1)]
    public long ShiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (ShiftId));
      }
    }

    [XmlElement(Order = 2)]
    public string Remarks
    {
      get
      {
        return this.remarksField;
      }
      set
      {
        this.remarksField = value;
        this.RaisePropertyChanged(nameof (Remarks));
      }
    }

    [XmlElement(Order = 3)]
    public long UserId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (UserId));
      }
    }

    [XmlElement(Order = 4)]
    public string CurrencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (CurrencyCode));
      }
    }

    [XmlElement(Order = 5)]
    public DateTime TransactionDate
    {
      get
      {
        return this.transactionDateField;
      }
      set
      {
        this.transactionDateField = value;
        this.RaisePropertyChanged(nameof (TransactionDate));
      }
    }

    [XmlElement(Order = 6)]
    public Decimal TotalCash
    {
      get
      {
        return this.totalCashField;
      }
      set
      {
        this.totalCashField = value;
        this.RaisePropertyChanged(nameof (TotalCash));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
