﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.CashInOutDetailItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [XmlInclude(typeof (Shift_CashInOutDetailRequest))]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [Serializable]
  public class CashInOutDetailItem : INotifyPropertyChanged
  {
    private long shiftIdField;
    private string paymentModeCodeField;
    private Decimal[] cashNoteValueListField;
    private int[] cashNoteCountListField;

    [XmlElement(Order = 0)]
    public long shiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (shiftId));
      }
    }

    [XmlElement(Order = 1)]
    public string paymentModeCode
    {
      get
      {
        return this.paymentModeCodeField;
      }
      set
      {
        this.paymentModeCodeField = value;
        this.RaisePropertyChanged(nameof (paymentModeCode));
      }
    }

    [XmlArray(Order = 2)]
    public Decimal[] cashNoteValueList
    {
      get
      {
        return this.cashNoteValueListField;
      }
      set
      {
        this.cashNoteValueListField = value;
        this.RaisePropertyChanged(nameof (cashNoteValueList));
      }
    }

    [XmlArray(Order = 3)]
    public int[] cashNoteCountList
    {
      get
      {
        return this.cashNoteCountListField;
      }
      set
      {
        this.cashNoteCountListField = value;
        this.RaisePropertyChanged(nameof (cashNoteCountList));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
