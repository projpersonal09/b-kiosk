﻿// Decompiled with JetBrains decompiler
// Type: WebService.KioskShiftWS.Shift_CashInOutDetailRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.KioskShiftWS
{
  [XmlType(Namespace = "http://pms-mq/WebService/KioskShiftWS")]
  [DesignerCategory("code")]
  [DebuggerStepThrough]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [Serializable]
  public class Shift_CashInOutDetailRequest : CashInOutDetailItem
  {
  }
}
