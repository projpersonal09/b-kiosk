﻿// Decompiled with JetBrains decompiler
// Type: WebService.PropertyWS.PropertyWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.PropertyWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [ServiceContract(ConfigurationName = "PropertyWS.PropertyWSSoap", Namespace = "http://pms-mq/WebService/PropertyWS")]
  public interface PropertyWSSoap
  {
    [OperationContract(Action = "http://pms-mq/WebService/PropertyWS/GetProperty", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    Property_GetResponse GetProperty(Property_GetRequest propertyReq);
  }
}
