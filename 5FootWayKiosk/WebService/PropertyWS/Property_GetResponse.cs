﻿// Decompiled with JetBrains decompiler
// Type: WebService.PropertyWS.Property_GetResponse
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.PropertyWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/PropertyWS")]
  [DebuggerStepThrough]
  [Serializable]
  public class Property_GetResponse : INotifyPropertyChanged
  {
    private int propertyIdField;
    private string propertyNameField;
    private int doorAccessPropertyIdField;
    private string doorAccessCustomerCodeField;
    private Decimal creditCardPercField;
    private string address1Field;
    private string address2Field;
    private string timeStartField;
    private string timeEndField;
    private Decimal securityDepositAmountField;
    private bool statusField;
    private string errorMessageField;
    private string[] registrationNumberField;
    private string webSiteField;
    private string currencyField;
    private float logoWidthField;
    private float logoHeightField;
    private string logoUrlField;
    private string defaultEmailField;

    [XmlElement(Order = 0)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 1)]
    public string propertyName
    {
      get
      {
        return this.propertyNameField;
      }
      set
      {
        this.propertyNameField = value;
        this.RaisePropertyChanged(nameof (propertyName));
      }
    }

    [XmlElement(Order = 2)]
    public int doorAccessPropertyId
    {
      get
      {
        return this.doorAccessPropertyIdField;
      }
      set
      {
        this.doorAccessPropertyIdField = value;
        this.RaisePropertyChanged(nameof (doorAccessPropertyId));
      }
    }

    [XmlElement(Order = 3)]
    public string doorAccessCustomerCode
    {
      get
      {
        return this.doorAccessCustomerCodeField;
      }
      set
      {
        this.doorAccessCustomerCodeField = value;
        this.RaisePropertyChanged(nameof (doorAccessCustomerCode));
      }
    }

    [XmlElement(Order = 4)]
    public Decimal creditCardPerc
    {
      get
      {
        return this.creditCardPercField;
      }
      set
      {
        this.creditCardPercField = value;
        this.RaisePropertyChanged(nameof (creditCardPerc));
      }
    }

    [XmlElement(Order = 5)]
    public string address1
    {
      get
      {
        return this.address1Field;
      }
      set
      {
        this.address1Field = value;
        this.RaisePropertyChanged(nameof (address1));
      }
    }

    [XmlElement(Order = 6)]
    public string address2
    {
      get
      {
        return this.address2Field;
      }
      set
      {
        this.address2Field = value;
        this.RaisePropertyChanged(nameof (address2));
      }
    }

    [XmlElement(Order = 7)]
    public string timeStart
    {
      get
      {
        return this.timeStartField;
      }
      set
      {
        this.timeStartField = value;
        this.RaisePropertyChanged(nameof (timeStart));
      }
    }

    [XmlElement(Order = 8)]
    public string timeEnd
    {
      get
      {
        return this.timeEndField;
      }
      set
      {
        this.timeEndField = value;
        this.RaisePropertyChanged(nameof (timeEnd));
      }
    }

    [XmlElement(Order = 9)]
    public Decimal securityDepositAmount
    {
      get
      {
        return this.securityDepositAmountField;
      }
      set
      {
        this.securityDepositAmountField = value;
        this.RaisePropertyChanged(nameof (securityDepositAmount));
      }
    }

    [XmlElement(Order = 10)]
    public bool status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
        this.RaisePropertyChanged(nameof (status));
      }
    }

    [XmlElement(Order = 11)]
    public string errorMessage
    {
      get
      {
        return this.errorMessageField;
      }
      set
      {
        this.errorMessageField = value;
        this.RaisePropertyChanged(nameof (errorMessage));
      }
    }

    [XmlArray(Order = 12)]
    public string[] registrationNumber
    {
      get
      {
        return this.registrationNumberField;
      }
      set
      {
        this.registrationNumberField = value;
        this.RaisePropertyChanged(nameof (registrationNumber));
      }
    }

    [XmlElement(Order = 13)]
    public string webSite
    {
      get
      {
        return this.webSiteField;
      }
      set
      {
        this.webSiteField = value;
        this.RaisePropertyChanged(nameof (webSite));
      }
    }

    [XmlElement(Order = 14)]
    public string currency
    {
      get
      {
        return this.currencyField;
      }
      set
      {
        this.currencyField = value;
        this.RaisePropertyChanged(nameof (currency));
      }
    }

    [XmlElement(Order = 15)]
    public float logoWidth
    {
      get
      {
        return this.logoWidthField;
      }
      set
      {
        this.logoWidthField = value;
        this.RaisePropertyChanged(nameof (logoWidth));
      }
    }

    [XmlElement(Order = 16)]
    public float logoHeight
    {
      get
      {
        return this.logoHeightField;
      }
      set
      {
        this.logoHeightField = value;
        this.RaisePropertyChanged(nameof (logoHeight));
      }
    }

    [XmlElement(Order = 17)]
    public string logoUrl
    {
      get
      {
        return this.logoUrlField;
      }
      set
      {
        this.logoUrlField = value;
        this.RaisePropertyChanged(nameof (logoUrl));
      }
    }

    [XmlElement(Order = 18)]
    public string defaultEmail
    {
      get
      {
        return this.defaultEmailField;
      }
      set
      {
        this.defaultEmailField = value;
        this.RaisePropertyChanged(nameof (defaultEmail));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
