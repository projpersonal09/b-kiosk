﻿// Decompiled with JetBrains decompiler
// Type: WebService.PropertyWS.PropertyWSSoapChannel
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.PropertyWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface PropertyWSSoapChannel : PropertyWSSoap, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
  {
  }
}
