﻿// Decompiled with JetBrains decompiler
// Type: WebService.PropertyWS.PropertyWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.PropertyWS
{
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  [DebuggerStepThrough]
  public class PropertyWSSoapClient : ClientBase<PropertyWSSoap>, PropertyWSSoap
  {
    public PropertyWSSoapClient()
    {
    }

    public PropertyWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public PropertyWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public PropertyWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public PropertyWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public Property_GetResponse GetProperty(Property_GetRequest propertyReq)
    {
      return this.Channel.GetProperty(propertyReq);
    }
  }
}
