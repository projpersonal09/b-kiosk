﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.BillingWSSoapClient
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace WebService.BillingWS
{
  [DebuggerStepThrough]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public class BillingWSSoapClient : ClientBase<BillingWSSoap>, BillingWSSoap
  {
    public BillingWSSoapClient()
    {
    }

    public BillingWSSoapClient(string endpointConfigurationName)
      : base(endpointConfigurationName)
    {
    }

    public BillingWSSoapClient(string endpointConfigurationName, string remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public BillingWSSoapClient(string endpointConfigurationName, EndpointAddress remoteAddress)
      : base(endpointConfigurationName, remoteAddress)
    {
    }

    public BillingWSSoapClient(Binding binding, EndpointAddress remoteAddress)
      : base(binding, remoteAddress)
    {
    }

    public Billing_GetChargeResponse GetCharge(Billing_GetChargeRequest chargeReq)
    {
      return this.Channel.GetCharge(chargeReq);
    }

    public Billing_UpdatePaymentResponse UpdatePayment(Billing_UpdatePaymentRequest req)
    {
      return this.Channel.UpdatePayment(req);
    }

    public Billing_PaySecurityDepositResponse PaySecurityDeposit(Billing_PaySecurityDepositRequest req)
    {
      return this.Channel.PaySecurityDeposit(req);
    }

    public Billing_GetSecurityDepositResponse GetSecurityDeposit(Billing_GetSecurityDepositRequest req)
    {
      return this.Channel.GetSecurityDeposit(req);
    }

    public Billing_ReturnSecurityDepositResponse ReturnSecurityDeposit(Billing_ReturnSecurityDepositRequest req)
    {
      return this.Channel.ReturnSecurityDeposit(req);
    }
  }
}
