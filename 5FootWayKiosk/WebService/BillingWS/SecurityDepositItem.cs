﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.SecurityDepositItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BillingWS
{
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/BiilingWS")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [Serializable]
  public class SecurityDepositItem : INotifyPropertyChanged
  {
    private string paymentNoField;
    private int paymentItemNoField;
    private string currencyCodeField;
    private Decimal amountField;
    private Decimal outstandingAmountField;
    private string transactionCodeField;

    [XmlElement(Order = 0)]
    public string paymentNo
    {
      get
      {
        return this.paymentNoField;
      }
      set
      {
        this.paymentNoField = value;
        this.RaisePropertyChanged(nameof (paymentNo));
      }
    }

    [XmlElement(Order = 1)]
    public int paymentItemNo
    {
      get
      {
        return this.paymentItemNoField;
      }
      set
      {
        this.paymentItemNoField = value;
        this.RaisePropertyChanged(nameof (paymentItemNo));
      }
    }

    [XmlElement(Order = 2)]
    public string currencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (currencyCode));
      }
    }

    [XmlElement(Order = 3)]
    public Decimal amount
    {
      get
      {
        return this.amountField;
      }
      set
      {
        this.amountField = value;
        this.RaisePropertyChanged(nameof (amount));
      }
    }

    [XmlElement(Order = 4)]
    public Decimal outstandingAmount
    {
      get
      {
        return this.outstandingAmountField;
      }
      set
      {
        this.outstandingAmountField = value;
        this.RaisePropertyChanged(nameof (outstandingAmount));
      }
    }

    [XmlElement(Order = 5)]
    public string transactionCode
    {
      get
      {
        return this.transactionCodeField;
      }
      set
      {
        this.transactionCodeField = value;
        this.RaisePropertyChanged(nameof (transactionCode));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
