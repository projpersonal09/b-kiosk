﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.Billing_UpdatePaymentRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BillingWS
{
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [XmlType(Namespace = "http://pms-mq/WebService/BiilingWS")]
  [Serializable]
  public class Billing_UpdatePaymentRequest : INotifyPropertyChanged
  {
    private long bookingIdField;
    private int propertyIdField;
    private long roomStayIdField;
    private long shiftIdField;
    private string paymentModeCodeField;
    private Decimal amountPaidField;
    private Decimal surchargeField;
    private string currencyCodeField;
    private string invoiceNumberField;
    private DateTime paymentDateTimeField;
    private long userIdField;

    [XmlElement(Order = 0)]
    public long bookingId
    {
      get
      {
        return this.bookingIdField;
      }
      set
      {
        this.bookingIdField = value;
        this.RaisePropertyChanged(nameof (bookingId));
      }
    }

    [XmlElement(Order = 1)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 2)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 3)]
    public long shiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (shiftId));
      }
    }

    [XmlElement(Order = 4)]
    public string paymentModeCode
    {
      get
      {
        return this.paymentModeCodeField;
      }
      set
      {
        this.paymentModeCodeField = value;
        this.RaisePropertyChanged(nameof (paymentModeCode));
      }
    }

    [XmlElement(Order = 5)]
    public Decimal amountPaid
    {
      get
      {
        return this.amountPaidField;
      }
      set
      {
        this.amountPaidField = value;
        this.RaisePropertyChanged(nameof (amountPaid));
      }
    }

    [XmlElement(Order = 6)]
    public Decimal surcharge
    {
      get
      {
        return this.surchargeField;
      }
      set
      {
        this.surchargeField = value;
        this.RaisePropertyChanged(nameof (surcharge));
      }
    }

    [XmlElement(Order = 7)]
    public string currencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (currencyCode));
      }
    }

    [XmlElement(Order = 8)]
    public string invoiceNumber
    {
      get
      {
        return this.invoiceNumberField;
      }
      set
      {
        this.invoiceNumberField = value;
        this.RaisePropertyChanged(nameof (invoiceNumber));
      }
    }

    [XmlElement(Order = 9)]
    public DateTime paymentDateTime
    {
      get
      {
        return this.paymentDateTimeField;
      }
      set
      {
        this.paymentDateTimeField = value;
        this.RaisePropertyChanged(nameof (paymentDateTime));
      }
    }

    [XmlElement(Order = 10)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
