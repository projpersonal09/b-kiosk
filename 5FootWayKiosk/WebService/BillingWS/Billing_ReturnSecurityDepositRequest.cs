﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.Billing_ReturnSecurityDepositRequest
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BillingWS
{
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(Namespace = "http://pms-mq/WebService/BiilingWS")]
  [Serializable]
  public class Billing_ReturnSecurityDepositRequest : INotifyPropertyChanged
  {
    private Decimal amountField;
    private long shiftIdField;
    private DateTime returnDateField;
    private long bookingIdField;
    private int propertyIdField;
    private long roomStayIdField;
    private long userIdField;
    private string currencyCodeField;
    private string invoiceNumberField;

    [XmlElement(Order = 0)]
    public Decimal amount
    {
      get
      {
        return this.amountField;
      }
      set
      {
        this.amountField = value;
        this.RaisePropertyChanged(nameof (amount));
      }
    }

    [XmlElement(Order = 1)]
    public long shiftId
    {
      get
      {
        return this.shiftIdField;
      }
      set
      {
        this.shiftIdField = value;
        this.RaisePropertyChanged(nameof (shiftId));
      }
    }

    [XmlElement(Order = 2)]
    public DateTime returnDate
    {
      get
      {
        return this.returnDateField;
      }
      set
      {
        this.returnDateField = value;
        this.RaisePropertyChanged(nameof (returnDate));
      }
    }

    [XmlElement(Order = 3)]
    public long bookingId
    {
      get
      {
        return this.bookingIdField;
      }
      set
      {
        this.bookingIdField = value;
        this.RaisePropertyChanged(nameof (bookingId));
      }
    }

    [XmlElement(Order = 4)]
    public int propertyId
    {
      get
      {
        return this.propertyIdField;
      }
      set
      {
        this.propertyIdField = value;
        this.RaisePropertyChanged(nameof (propertyId));
      }
    }

    [XmlElement(Order = 5)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 6)]
    public long userId
    {
      get
      {
        return this.userIdField;
      }
      set
      {
        this.userIdField = value;
        this.RaisePropertyChanged(nameof (userId));
      }
    }

    [XmlElement(Order = 7)]
    public string currencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (currencyCode));
      }
    }

    [XmlElement(Order = 8)]
    public string invoiceNumber
    {
      get
      {
        return this.invoiceNumberField;
      }
      set
      {
        this.invoiceNumberField = value;
        this.RaisePropertyChanged(nameof (invoiceNumber));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
