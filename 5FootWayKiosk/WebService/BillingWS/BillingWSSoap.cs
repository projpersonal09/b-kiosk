﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.BillingWSSoap
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System.CodeDom.Compiler;
using System.ServiceModel;

namespace WebService.BillingWS
{
  [ServiceContract(ConfigurationName = "BillingWS.BillingWSSoap", Namespace = "http://pms-mq/WebService/BiilingWS")]
  [GeneratedCode("System.ServiceModel", "4.0.0.0")]
  public interface BillingWSSoap
  {
    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/BiilingWS/GetCharge", ReplyAction = "*")]
    Billing_GetChargeResponse GetCharge(Billing_GetChargeRequest chargeReq);

    [OperationContract(Action = "http://pms-mq/WebService/BiilingWS/UpdatePayment", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    Billing_UpdatePaymentResponse UpdatePayment(Billing_UpdatePaymentRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/BiilingWS/PaySecurityDeposit", ReplyAction = "*")]
    Billing_PaySecurityDepositResponse PaySecurityDeposit(Billing_PaySecurityDepositRequest req);

    [OperationContract(Action = "http://pms-mq/WebService/BiilingWS/GetSecurityDeposit", ReplyAction = "*")]
    [XmlSerializerFormat(SupportFaults = true)]
    Billing_GetSecurityDepositResponse GetSecurityDeposit(Billing_GetSecurityDepositRequest req);

    [XmlSerializerFormat(SupportFaults = true)]
    [OperationContract(Action = "http://pms-mq/WebService/BiilingWS/ReturnSecurityDeposit", ReplyAction = "*")]
    Billing_ReturnSecurityDepositResponse ReturnSecurityDeposit(Billing_ReturnSecurityDepositRequest req);
  }
}
