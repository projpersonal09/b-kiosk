﻿// Decompiled with JetBrains decompiler
// Type: WebService.BillingWS.ChargeItem
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace WebService.BillingWS
{
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [GeneratedCode("System.Xml", "4.7.2102.0")]
  [XmlType(Namespace = "http://pms-mq/WebService/BiilingWS")]
  [Serializable]
  public class ChargeItem : INotifyPropertyChanged
  {
    private string chargeNoField;
    private int chargeItemNoField;
    private long roomStayIdField;
    private string chargeCodeField;
    private string chargeSubCodeField;
    private string chargeNameField;
    private string currencyCodeField;
    private Decimal outstandingField;
    private Decimal gstField;
    private Decimal taxField;
    private Decimal serviceChargeField;
    private Decimal chargeField;
    private Decimal totalChargeField;
    private Decimal paidField;
    private string accountNoField;
    private DateTime chargeDateTimeField;

    [XmlElement(Order = 0)]
    public string chargeNo
    {
      get
      {
        return this.chargeNoField;
      }
      set
      {
        this.chargeNoField = value;
        this.RaisePropertyChanged(nameof (chargeNo));
      }
    }

    [XmlElement(Order = 1)]
    public int chargeItemNo
    {
      get
      {
        return this.chargeItemNoField;
      }
      set
      {
        this.chargeItemNoField = value;
        this.RaisePropertyChanged(nameof (chargeItemNo));
      }
    }

    [XmlElement(Order = 2)]
    public long roomStayId
    {
      get
      {
        return this.roomStayIdField;
      }
      set
      {
        this.roomStayIdField = value;
        this.RaisePropertyChanged(nameof (roomStayId));
      }
    }

    [XmlElement(Order = 3)]
    public string chargeCode
    {
      get
      {
        return this.chargeCodeField;
      }
      set
      {
        this.chargeCodeField = value;
        this.RaisePropertyChanged(nameof (chargeCode));
      }
    }

    [XmlElement(Order = 4)]
    public string chargeSubCode
    {
      get
      {
        return this.chargeSubCodeField;
      }
      set
      {
        this.chargeSubCodeField = value;
        this.RaisePropertyChanged(nameof (chargeSubCode));
      }
    }

    [XmlElement(Order = 5)]
    public string chargeName
    {
      get
      {
        return this.chargeNameField;
      }
      set
      {
        this.chargeNameField = value;
        this.RaisePropertyChanged(nameof (chargeName));
      }
    }

    [XmlElement(Order = 6)]
    public string currencyCode
    {
      get
      {
        return this.currencyCodeField;
      }
      set
      {
        this.currencyCodeField = value;
        this.RaisePropertyChanged(nameof (currencyCode));
      }
    }

    [XmlElement(Order = 7)]
    public Decimal outstanding
    {
      get
      {
        return this.outstandingField;
      }
      set
      {
        this.outstandingField = value;
        this.RaisePropertyChanged(nameof (outstanding));
      }
    }

    [XmlElement(Order = 8)]
    public Decimal gst
    {
      get
      {
        return this.gstField;
      }
      set
      {
        this.gstField = value;
        this.RaisePropertyChanged(nameof (gst));
      }
    }

    [XmlElement(Order = 9)]
    public Decimal tax
    {
      get
      {
        return this.taxField;
      }
      set
      {
        this.taxField = value;
        this.RaisePropertyChanged(nameof (tax));
      }
    }

    [XmlElement(Order = 10)]
    public Decimal serviceCharge
    {
      get
      {
        return this.serviceChargeField;
      }
      set
      {
        this.serviceChargeField = value;
        this.RaisePropertyChanged(nameof (serviceCharge));
      }
    }

    [XmlElement(Order = 11)]
    public Decimal charge
    {
      get
      {
        return this.chargeField;
      }
      set
      {
        this.chargeField = value;
        this.RaisePropertyChanged(nameof (charge));
      }
    }

    [XmlElement(Order = 12)]
    public Decimal totalCharge
    {
      get
      {
        return this.totalChargeField;
      }
      set
      {
        this.totalChargeField = value;
        this.RaisePropertyChanged(nameof (totalCharge));
      }
    }

    [XmlElement(Order = 13)]
    public Decimal paid
    {
      get
      {
        return this.paidField;
      }
      set
      {
        this.paidField = value;
        this.RaisePropertyChanged(nameof (paid));
      }
    }

    [XmlElement(Order = 14)]
    public string accountNo
    {
      get
      {
        return this.accountNoField;
      }
      set
      {
        this.accountNoField = value;
        this.RaisePropertyChanged(nameof (accountNo));
      }
    }

    [XmlElement(Order = 15)]
    public DateTime chargeDateTime
    {
      get
      {
        return this.chargeDateTimeField;
      }
      set
      {
        this.chargeDateTimeField = value;
        this.RaisePropertyChanged(nameof (chargeDateTime));
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected void RaisePropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
