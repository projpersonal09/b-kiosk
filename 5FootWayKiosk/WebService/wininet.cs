﻿// Decompiled with JetBrains decompiler
// Type: WebService.wininet
// Assembly: WebService, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F729EA26-FC37-4F51-82AD-0B324D7EB8E0
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\WebService.dll

using System;
using System.Runtime.InteropServices;

namespace WebService
{
  public class wininet
  {
    [DllImport("kernel32")]
    public static extern int LoadLibrary(string strDllName);

    [DllImport("wininet.dll")]
    private static extern bool InternetGetConnectedState(out int Description, int ReservedValue);

    public static bool CheckInternetConnection()
    {
      int Description = 0;
      try
      {
        return wininet.InternetGetConnectedState(out Description, 0);
      }
      catch (Exception ex)
      {
        throw new Exception(((wininet.InternetConnectionState) Description).ToString(), ex);
      }
    }

    public enum InternetConnectionState
    {
      INTERNET_CONNECTION_MODEM = 1,
      INTERNET_CONNECTION_LAN = 2,
      INTERNET_CONNECTION_PROXY = 4,
      INTERNET_CONNECTION_MODEM_BUSY = 8,
      INTERNET_RAS_INSTALLED = 16, // 0x00000010
      INTERNET_CONNECTION_OFFLINE = 32, // 0x00000020
      INTERNET_CONNECTION_CONFIGURED = 64, // 0x00000040
    }
  }
}
