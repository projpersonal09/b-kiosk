using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class RateExperienceModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public string selectedRate
		{
			get;
			set;
		}

		public RateExperienceModel()
		{
		}
	}
}