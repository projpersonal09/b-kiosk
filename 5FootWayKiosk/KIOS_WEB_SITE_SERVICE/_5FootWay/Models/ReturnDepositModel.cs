using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ReturnDepositModel
	{
		public decimal amountReturned
		{
			get;
			set;
		}

		public decimal amountToReturn
		{
			get;
			set;
		}

		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public ReturnDepositModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}