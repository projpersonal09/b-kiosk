using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class Card
	{
		public string cardStatus
		{
			get;
			set;
		}

		public string cardUID
		{
			get;
			set;
		}

		public Card()
		{
		}

		public Card(string cardStatus, string cardUID)
		{
			this.cardStatus = cardStatus;
			this.cardUID = cardUID;
		}
	}
}