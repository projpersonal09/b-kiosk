using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class PassportDetailView
	{
		public string dob
		{
			get;
			set;
		}

		public string expiryDate
		{
			get;
			set;
		}

		public string gender
		{
			get;
			set;
		}

		public string guestName
		{
			get;
			set;
		}

		public string nationality
		{
			get;
			set;
		}

		public string nationalityCode
		{
			get;
			set;
		}

		public string passportNumber
		{
			get;
			set;
		}

		public PassportDetailView()
		{
		}
	}
}