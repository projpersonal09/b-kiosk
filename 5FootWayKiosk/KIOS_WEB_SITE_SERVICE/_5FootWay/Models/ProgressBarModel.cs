using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ProgressBarModel
	{
		public int currentStep
		{
			get;
			set;
		}

		public List<int> pointList
		{
			get;
			set;
		}

		public List<string> stepList
		{
			get;
			set;
		}

		public ProgressBarModel(int stepCount, int currentStep, List<int> pointList)
		{
			this.currentStep = currentStep;
			this.pointList = pointList;
			this.stepList = new List<string>();
			int num = 0;
			for (int i = 0; i <= stepCount; i++)
			{
				if (i > currentStep)
				{
					this.stepList.Add("");
				}
				else
				{
					while (pointList.Count > num)
					{
						while (pointList[num] > i)
						{
							this.stepList.Add("visited");
							i++;
						}
						if (currentStep < pointList[num])
						{
							num++;
						}
						else
						{
							if (pointList.Count - 1 <= num || currentStep < pointList[num + 1])
							{
								break;
							}
							num++;
						}
					}
					this.stepList.Add("active");
				}
			}
		}
	}
}