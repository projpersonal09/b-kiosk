using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class OtherParticularView
	{
		public string address1
		{
			get;
			set;
		}

		public string address2
		{
			get;
			set;
		}

		public string company
		{
			get;
			set;
		}

		public string contactNumber
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string nextDestination
		{
			get;
			set;
		}

		public string occupation
		{
			get;
			set;
		}

		public string postalCode
		{
			get;
			set;
		}

		public string prevDestination
		{
			get;
			set;
		}

		public OtherParticularView()
		{
		}
	}
}