using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class SecurityDepositModel
	{
		public decimal amountChange
		{
			get;
			set;
		}

		public decimal amountInserted
		{
			get;
			set;
		}

		public decimal amountPayable
		{
			get;
			set;
		}

		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public SecurityDepositModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}