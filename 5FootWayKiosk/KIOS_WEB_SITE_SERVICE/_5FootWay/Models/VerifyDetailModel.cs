using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class VerifyDetailModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public OtherParticularView otherParticular
		{
			get;
			set;
		}

		public PassportDetailView passportDetail
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public bool SkipOtherParticular
		{
			get;
			set;
		}

		public VerifyDetailModel()
		{
			this.otherParticular = new OtherParticularView();
			this.passportDetail = new PassportDetailView();
			this.checkInData = new CheckInData();
			this.SkipOtherParticular = false;
		}
	}
}