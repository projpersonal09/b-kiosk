using Helper;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace _5FootWay
{
	public class RouteConfig
	{
		public RouteConfig()
		{
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.MapRoute("Default", "{culture}/{controller}/{action}/{id}", new { culture = CultureHelper.GetDefaultCulture(), controller = "Home", action = "Index", id = UrlParameter.Optional });
		}
	}
}