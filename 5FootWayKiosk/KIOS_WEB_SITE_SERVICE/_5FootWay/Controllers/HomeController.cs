using Helper;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Runtime.CompilerServices;
using System.Web.Mvc;
using System.Web.Routing;

namespace _5FootWay.Controllers
{
	public class HomeController : BaseController
	{
		public HomeController()
		{
		}

		public ActionResult About()
		{
			return base.View();
		}

		public ActionResult Contact()
		{
			return base.View();
		}

		[HttpGet]
		public ActionResult Index(string errorMessage)
		{
			((dynamic)base.ViewBag).Message = errorMessage;
			return base.View();
		}

		[ActionName("Index")]
		[HttpPost]
		public ActionResult IndexPost(string command)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.View();
			}
			if (command.Equals("CheckIn"))
			{
				return base.RedirectToAction("PDA", "CheckIn");
			}
			if (!command.Equals("CheckOut"))
			{
				return base.View();
			}
			return base.RedirectToAction("CheckOut", "CheckOut");
		}

		[HttpGet]
		public JsonResult SetCulture(string newCulture)
		{
			newCulture = CultureHelper.GetImplementedCulture(newCulture);
			base.RouteData.Values["culture"] = newCulture;
			return base.Json(base.Url.Action("Index", "Home", new { culture = newCulture }), JsonRequestBehavior.AllowGet);
		}

		public class HomeCommand
		{
			public const string checkIn = "CheckIn";

			public const string checkOut = "CheckOut";

			public HomeCommand()
			{
			}
		}
	}
}