using Helper;
using Newtonsoft.Json;
using SharedBusinessEntity;
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _5FootWay.Controllers
{
	public class BaseController : Controller
	{
		public BaseController()
		{
		}

		[NonAction]
		protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
		{
			string userLanguages;
			string str = Convert.ToString(base.RouteData.Values["culture"]);
			if (str == null)
			{
				if (base.Request.UserLanguages == null || (int)base.Request.UserLanguages.Length <= 0)
				{
					userLanguages = null;
				}
				else
				{
					userLanguages = base.Request.UserLanguages[0];
				}
				str = userLanguages;
			}
			str = CultureHelper.GetImplementedCulture(str);
			if (base.RouteData.Values["culture"] as string != str)
			{
				base.RouteData.Values["culture"] = str.ToLowerInvariant();
				base.Response.RedirectToRoute(base.RouteData.Values);
			}
			Thread.CurrentThread.CurrentCulture = new CultureInfo(str);
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			return base.BeginExecuteCore(callback, state);
		}

		[NonAction]
		protected CheckInData initializeCheckInData(CheckInData CM)
		{
			CM = new CheckInData();
			if (base.Session["checkInData"] != null)
			{
				CM = (CheckInData)base.Session["checkInData"];
			}
			return CM;
		}

		[HttpGet]
		public JsonResult refreshSession()
		{
			if (base.Session["endTime"] != null)
			{
				HttpSessionStateBase session = base.Session;
				DateTime now = DateTime.Now;
				DateTime dateTime = now.AddMinutes((double)base.Session.Timeout);
				session["endTime"] = dateTime.ToString("yyyy/MM/dd HH:mm:ss");
			}
			return base.Json(base.Session["endTime"], JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult SetBookingDetail(string checkInDataString)
		{
			bool flag = false;
			try
			{
				if (!string.IsNullOrEmpty(checkInDataString))
				{
					CheckInData checkInDatum = JsonConvert.DeserializeObject<CheckInData>(checkInDataString);
					base.Session["checkInData"] = checkInDatum;
					flag = true;
				}
			}
			catch (Exception exception)
			{
				(new MyLog()).Log(MyLog.LogType.Error, exception.Message);
			}
			return base.Json(flag, JsonRequestBehavior.AllowGet);
		}
	}
}