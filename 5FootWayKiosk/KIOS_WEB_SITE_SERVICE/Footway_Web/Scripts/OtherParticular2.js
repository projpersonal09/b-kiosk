﻿function ShowPersonalDetail() {
    $("#passportDetail").removeClass("hide");
    $("#otherPaticular").addClass("hide");
}

function ShowOtherParticular() {
    $("#passportDetail").addClass("hide");
    $("#otherPaticular").removeClass("hide");
}

function GetOtherParticularData() {
    var obj = {
        "company": $("#otherParticular_company") ? $("#otherParticular_company").val() : ""
        , "email": $("#otherParticular_email") ? $("#otherParticular_email").val() : ""
        , "address1": $("#otherParticular_address1") ? $("#otherParticular_address1").val() : ""
        , "address2": $("#otherParticular_address2") ? $("#otherParticular_address2").val() : ""
        , "postalCode": $("#otherParticular_postalCode") ? $("#otherParticular_postalCode").val() : ""
        , "contactNumber": $("#otherParticular_contactNumber") ? $("#otherParticular_contactNumber").val() : ""
        , "occupation": $("#otherParticular_occupation") ? $("#otherParticular_occupation").val() : ""
        , "prevDestination": $("#otherParticular_prevDestination") ? $("#otherParticular_prevDestination").val() : ""
        , "nextDestination": $("#otherParticular_nextDestination") ? $("#otherParticular_nextDestination").val() : ""
    };

    return JSON.stringify(obj);
}

function validateEmail(email) {

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);

}

function validatePhone(phone) {
   
    var regex = /^[a-zA-Z0-9\-().+\s]{6,15}$/;
    return regex.test(phone);
    
}

function validateOtherParticular(skip)
{
    if (skip) {
        return true;
    } else {
        var prevdestination = $("#otherParticular_prevDestination").val();
        var nextdestination = $("#otherParticular_nextDestination").val();
        var occupation = $("#otherParticular_occupation").val();

        if (prevdestination != "" && nextdestination != "" && occupation != "") {
            return true;
        }
        else {
            return false;
        }
    }
}