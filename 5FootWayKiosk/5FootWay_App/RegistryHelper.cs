﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.RegistryHelper
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using Microsoft.Win32;
using System;
using System.Security.AccessControl;
using WebService;
namespace _5FootWay_App
{
  public class RegistryHelper
  {
    public void SetupRegistry(string processName)
    {
      try
      {
        string appName = processName + ".exe";
        string name = "SOFTWARE\\\\Microsoft\\\\Internet Explorer\\\\Zoom";
        using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(name, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl))
        {
          if (registryKey == null)
            throw new Exception("Address Not found or no admin right granted.");
          object obj1 = registryKey.GetValue("ZoomDisabled");
          if (obj1 != null)
          {
            if (obj1.Equals((object) 1))
              goto label_10;
          }
          registryKey.SetValue("ZoomDisabled", (object) 1, RegistryValueKind.DWord);
          object obj2 = registryKey.GetValue("ZoomDisabled");
          if (!obj2.Equals((object) 1))
            throw new Exception("Ref: " + obj2 + ".");
        }
label_10:
        this.SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", appName, this.GetBrowserEmulationMode());
        this.SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", appName, 1U);
        this.SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", appName, 0U);
        this.SetBrowserFeatureControlKey("FEATURE_XMLHTTP", appName, 1U);
      }
      catch (Exception ex)
      {
        new MyLog().Log(MyLog.LogType.Warning, "Application Settings Failed, " + ex.Message);
      }
    }

    private uint GetBrowserEmulationMode()
    {
      int result = 7;
      using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer", RegistryKeyPermissionCheck.ReadSubTree, RegistryRights.QueryValues))
      {
        object obj = registryKey.GetValue("svcVersion");
        if (obj == null)
        {
          obj = registryKey.GetValue("Version");
          if (obj == null)
            throw new ApplicationException("Microsoft Internet Explorer is required!");
        }
        int.TryParse(obj.ToString().Split('.')[0], out result);
      }
      uint num = 11000;
      switch (result)
      {
        case 7:
          num = 7000U;
          break;
        case 8:
          num = 8000U;
          break;
        case 9:
          num = 9000U;
          break;
        case 10:
          num = 10000U;
          break;
      }
      return num;
    }

    private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
    {
      using (RegistryKey subKey = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\" + feature, RegistryKeyPermissionCheck.ReadWriteSubTree))
        subKey.SetValue(appName, (object) value, RegistryValueKind.DWord);
    }
  }
}
