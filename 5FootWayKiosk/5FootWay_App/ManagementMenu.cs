﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.ManagementMenu
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using KioskBusinessEntity.Kiosk_Entity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class ManagementMenu : Form
  {
    private KioskData kioskData;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btnEditConfig;
    private Button btnManagement;
    private Button btnReturn;
    private Button btnCardManagement;
    private Button btnShiftManagement;
    private Button btnUserManagement;

    public ManagementMenu(KioskData kioskData)
    {
      this.kioskData = kioskData;
      this.InitializeComponent();
    }

    private void btnEditConfig_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new ConfigEditor().ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    private void btnManagement_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new CashNoteManagement(this.kioskData).ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void btnCardManagement_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new CardManagement(this.kioskData).ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    private void btnShiftManagement_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new ShiftManagement().ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    private void btnUserManagement_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new _5FootWay_App.UserManagement.UserManagement(this.kioskData.kioskUserName).ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUserManagement = new System.Windows.Forms.Button();
            this.btnShiftManagement = new System.Windows.Forms.Button();
            this.btnCardManagement = new System.Windows.Forms.Button();
            this.btnManagement = new System.Windows.Forms.Button();
            this.btnEditConfig = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnUserManagement, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnShiftManagement, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCardManagement, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnManagement, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEditConfig, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnReturn, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUserManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserManagement.Location = new System.Drawing.Point(96, 415);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(200, 100);
            this.btnUserManagement.TabIndex = 5;
            this.btnUserManagement.Text = "User Management";
            this.btnUserManagement.UseVisualStyleBackColor = true;
            this.btnUserManagement.Click += new System.EventHandler(this.btnUserManagement_Click);
            // 
            // btnShiftManagement
            // 
            this.btnShiftManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnShiftManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShiftManagement.Location = new System.Drawing.Point(488, 227);
            this.btnShiftManagement.Name = "btnShiftManagement";
            this.btnShiftManagement.Size = new System.Drawing.Size(200, 100);
            this.btnShiftManagement.TabIndex = 3;
            this.btnShiftManagement.Text = "Shift Management";
            this.btnShiftManagement.UseVisualStyleBackColor = true;
            this.btnShiftManagement.Click += new System.EventHandler(this.btnShiftManagement_Click);
            // 
            // btnCardManagement
            // 
            this.btnCardManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCardManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCardManagement.Location = new System.Drawing.Point(96, 42);
            this.btnCardManagement.Name = "btnCardManagement";
            this.btnCardManagement.Size = new System.Drawing.Size(200, 100);
            this.btnCardManagement.TabIndex = 0;
            this.btnCardManagement.Text = "Card Management";
            this.btnCardManagement.UseVisualStyleBackColor = true;
            this.btnCardManagement.Click += new System.EventHandler(this.btnCardManagement_Click);
            // 
            // btnManagement
            // 
            this.btnManagement.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManagement.Location = new System.Drawing.Point(488, 42);
            this.btnManagement.Name = "btnManagement";
            this.btnManagement.Size = new System.Drawing.Size(200, 100);
            this.btnManagement.TabIndex = 1;
            this.btnManagement.Text = "Cash Management";
            this.btnManagement.UseVisualStyleBackColor = true;
            this.btnManagement.Click += new System.EventHandler(this.btnManagement_Click);
            // 
            // btnEditConfig
            // 
            this.btnEditConfig.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEditConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditConfig.Location = new System.Drawing.Point(96, 227);
            this.btnEditConfig.Name = "btnEditConfig";
            this.btnEditConfig.Size = new System.Drawing.Size(200, 100);
            this.btnEditConfig.TabIndex = 2;
            this.btnEditConfig.Text = "Edit Configuration";
            this.btnEditConfig.UseVisualStyleBackColor = true;
            this.btnEditConfig.Click += new System.EventHandler(this.btnEditConfig_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReturn.BackColor = System.Drawing.Color.RosyBrown;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(488, 415);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(200, 100);
            this.btnReturn.TabIndex = 4;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // ManagementMenu
            // 
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ManagementMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Management Menu";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
    {

    }
  }
}
