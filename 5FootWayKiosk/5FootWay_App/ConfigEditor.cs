﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.ConfigEditor
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using MQuest.Printing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class ConfigEditor : Form
  {
    private CreditCardReaderManager creditCardReaderManager;
    private ConstantVariable_CashNote cashNoteManager;
    private CashDispenserManager CashDispenserManager;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private ComboBox cbCardAcceptor;
    private TextBox tbChipImage;
    private Label label6;
    private Label label5;
    private Label label4;
    private Label label3;
    private Label label2;
    private Label label1;
    private ComboBox cbCreditCardReader;
    private ComboBox cbCashAcceptor;
    private ComboBox cbDocumentReader;
    private Button btnChange;
    private Button btnCancel;
    private Label label7;
    private ComboBox cbPrinter;
    private TextBox tbDocumentImage;
    private Label label8;

    public ConfigEditor()
    {
      this.InitializeComponent();
    }

    private void btnChange_Click(object sender, EventArgs e)
    {
      try
      {
        ConfigurationHelper.updateKeyValue(new string[6]
        {
          "Credit_Card_Reader_COM_Port",
          "Card_Dispenser_RS323_Port_Number",
          "Cash_Dispenser_COM_Port",
          "chipImageRate",
          "documentImageRate",
          "printerName"
        }, new string[6]
        {
          Convert.ToString(this.cbCreditCardReader.SelectedItem),
          Convert.ToString(this.cbCardAcceptor.SelectedItem),
          Convert.ToString(this.cbCashAcceptor.SelectedItem),
          this.tbChipImage.Text,
          this.tbDocumentImage.Text,
          Convert.ToString(this.cbPrinter.SelectedItem)
        });
        int num = (int) MessageBox.Show("Change successfully.");
        this.DialogResult = DialogResult.OK;
        this.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void ConfigEditor_Load(object sender, EventArgs e)
    {
      try
      {
        PrinterManager printerManager = new PrinterManager();
        this.tbChipImage.Text = ConfigurationHelper.ReadValue("chipImageRate");
        this.tbDocumentImage.Text = ConfigurationHelper.ReadValue("documentImageRate");
        string[] portNames = SerialPort.GetPortNames();
        this.cbCreditCardReader.Items.Clear();
        this.cbCashAcceptor.Items.Clear();
        this.cbCardAcceptor.Items.Clear();
        this.cbDocumentReader.Items.Clear();
        foreach (IEnumerable<char> source in portNames)
        {
          string str = string.Join<char>("", source.Where<char>(new Func<char, bool>(char.IsDigit)));
          this.cbCreditCardReader.Items.Add((object) str);
          this.cbCashAcceptor.Items.Add((object) str);
          this.cbCardAcceptor.Items.Add((object) str);
          this.cbDocumentReader.Items.Add((object) str);
        }
        this.cbCreditCardReader.SelectedItem = (object) ConfigurationHelper.ReadValue("Credit_Card_Reader_COM_Port");
        this.cbCashAcceptor.SelectedItem = (object) ConfigurationHelper.ReadValue("Cash_Dispenser_COM_Port");
        this.cbCardAcceptor.SelectedItem = (object) ConfigurationHelper.ReadValue("Card_Dispenser_RS323_Port_Number");
        this.cbPrinter.Items.Clear();
        this.cbPrinter.Items.AddRange((object[]) printerManager.GetPrinterList().ToArray());
        this.cbPrinter.SelectedItem = (object) ConstantVariable_App.printerName;
        this.cbDocumentReader.Enabled = false;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
    }

    private void cbCreditCardReader_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        this.creditCardReaderManager = new CreditCardReaderManager((Control) this);
        this.creditCardReaderManager.InitCreditCardReader(ConstantVariable_App.creditCardReaderPort);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
      finally
      {
        if (this.creditCardReaderManager != null)
        {
          this.creditCardReaderManager.Dispose();
          this.creditCardReaderManager = (CreditCardReaderManager) null;
        }
      }
    }

    private void cbCashAcceptor_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        this.cashNoteManager = new ConstantVariable_CashNote();
        this.CashDispenserManager = new CashDispenserManager(ConstantVariable_App.cashDispenserPort, this.cashNoteManager.acceptCashNoteList, false);
        this.CashDispenserManager.Init(this.cashNoteManager.appRouteCashNoteList);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
      finally
      {
        if (this.CashDispenserManager != null)
        {
          this.CashDispenserManager.Dispose();
          this.CashDispenserManager = (CashDispenserManager) null;
        }
      }
    }

    private void cbCardAcceptor_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCreditCardReader = new System.Windows.Forms.ComboBox();
            this.cbCashAcceptor = new System.Windows.Forms.ComboBox();
            this.cbDocumentReader = new System.Windows.Forms.ComboBox();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbCardAcceptor = new System.Windows.Forms.ComboBox();
            this.tbChipImage = new System.Windows.Forms.TextBox();
            this.cbPrinter = new System.Windows.Forms.ComboBox();
            this.tbDocumentImage = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbCreditCardReader, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbCashAcceptor, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbDocumentReader, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnChange, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbCardAcceptor, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbChipImage, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbPrinter, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbDocumentImage, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1045, 690);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(108, 229);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(230, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Document Reader Port";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(108, 160);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(249, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cash Note Acceptor Port";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Credit Card Reader Port";
            // 
            // cbCreditCardReader
            // 
            this.cbCreditCardReader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCreditCardReader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCreditCardReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCreditCardReader.Location = new System.Drawing.Point(526, 87);
            this.cbCreditCardReader.Margin = new System.Windows.Forms.Padding(4);
            this.cbCreditCardReader.Name = "cbCreditCardReader";
            this.cbCreditCardReader.Size = new System.Drawing.Size(410, 33);
            this.cbCreditCardReader.TabIndex = 1;
            this.cbCreditCardReader.SelectedIndexChanged += new System.EventHandler(this.cbCreditCardReader_SelectedIndexChanged);
            // 
            // cbCashAcceptor
            // 
            this.cbCashAcceptor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCashAcceptor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCashAcceptor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCashAcceptor.Location = new System.Drawing.Point(526, 156);
            this.cbCashAcceptor.Margin = new System.Windows.Forms.Padding(4);
            this.cbCashAcceptor.Name = "cbCashAcceptor";
            this.cbCashAcceptor.Size = new System.Drawing.Size(410, 33);
            this.cbCashAcceptor.TabIndex = 2;
            this.cbCashAcceptor.SelectedIndexChanged += new System.EventHandler(this.cbCashAcceptor_SelectedIndexChanged);
            // 
            // cbDocumentReader
            // 
            this.cbDocumentReader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDocumentReader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocumentReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDocumentReader.Location = new System.Drawing.Point(526, 225);
            this.cbDocumentReader.Margin = new System.Windows.Forms.Padding(4);
            this.cbDocumentReader.Name = "cbDocumentReader";
            this.cbDocumentReader.Size = new System.Drawing.Size(410, 33);
            this.cbDocumentReader.TabIndex = 3;
            // 
            // btnChange
            // 
            this.btnChange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChange.Location = new System.Drawing.Point(597, 559);
            this.btnChange.Margin = new System.Windows.Forms.Padding(4);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(267, 123);
            this.btnChange.TabIndex = 12;
            this.btnChange.Text = "Change";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(179, 559);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(267, 123);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label7, 2);
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(418, 22);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(208, 25);
            this.label7.TabIndex = 14;
            this.label7.Text = "Application Setttings";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 436);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(241, 25);
            this.label5.TabIndex = 7;
            this.label5.Text = "Chip Photo Accept Rate";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(108, 298);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(256, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Smart Card Acceptor Port";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(108, 367);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Printer";
            // 
            // cbCardAcceptor
            // 
            this.cbCardAcceptor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCardAcceptor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCardAcceptor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCardAcceptor.Location = new System.Drawing.Point(526, 294);
            this.cbCardAcceptor.Margin = new System.Windows.Forms.Padding(4);
            this.cbCardAcceptor.Name = "cbCardAcceptor";
            this.cbCardAcceptor.Size = new System.Drawing.Size(410, 33);
            this.cbCardAcceptor.TabIndex = 11;
            this.cbCardAcceptor.SelectedIndexChanged += new System.EventHandler(this.cbCardAcceptor_SelectedIndexChanged);
            // 
            // tbChipImage
            // 
            this.tbChipImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChipImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbChipImage.Location = new System.Drawing.Point(526, 433);
            this.tbChipImage.Margin = new System.Windows.Forms.Padding(4);
            this.tbChipImage.Name = "tbChipImage";
            this.tbChipImage.Size = new System.Drawing.Size(410, 31);
            this.tbChipImage.TabIndex = 9;
            // 
            // cbPrinter
            // 
            this.cbPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbPrinter.FormattingEnabled = true;
            this.cbPrinter.Location = new System.Drawing.Point(526, 363);
            this.cbPrinter.Margin = new System.Windows.Forms.Padding(4);
            this.cbPrinter.Name = "cbPrinter";
            this.cbPrinter.Size = new System.Drawing.Size(410, 33);
            this.cbPrinter.TabIndex = 15;
            // 
            // tbDocumentImage
            // 
            this.tbDocumentImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDocumentImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDocumentImage.Location = new System.Drawing.Point(526, 502);
            this.tbDocumentImage.Margin = new System.Windows.Forms.Padding(4);
            this.tbDocumentImage.Name = "tbDocumentImage";
            this.tbDocumentImage.Size = new System.Drawing.Size(410, 31);
            this.tbDocumentImage.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(108, 505);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(294, 25);
            this.label8.TabIndex = 16;
            this.label8.Text = "Document Photo Accept Rate";
            // 
            // ConfigEditor
            // 
            this.AcceptButton = this.btnChange;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1045, 690);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ConfigEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Config Editor";
            this.Load += new System.EventHandler(this.ConfigEditor_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

    }

    private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
    {

    }
  }
}
