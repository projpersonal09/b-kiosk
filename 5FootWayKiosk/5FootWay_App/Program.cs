﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.Program
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using System;
using System.Threading;
using System.Windows.Forms;

namespace _5FootWay_App
{
  internal static class Program
  {
    public static string APP_VERSION_LABEL = "V 1.2.4.0";
    private static Mutex mutex = new Mutex(false, "5Footway Application");

    [STAThread]
    private static void Main()
    {
      if (!Program.mutex.WaitOne(TimeSpan.FromSeconds(1.0), false))
      {
        int num = (int) MessageBox.Show("5Footway Application already started!", "Application Running Detected", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
      {
        try
        {
          Application.EnableVisualStyles();
          Application.SetCompatibleTextRenderingDefault(false);
          if (new MainMenu().ShowDialog() != DialogResult.OK)
            return;
          Application.Run((Form) new _5FootWayKiosk());
        }
        finally
        {
          Program.mutex.ReleaseMutex();
        }
      }
    }
  }
}
