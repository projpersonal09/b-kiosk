﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.CashNoteManagement
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using KioskBusinessEntity;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessLayer;
using MQuest.HardwareInterface.CashDispenser;
using MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using WebService;

namespace _5FootWay_App
{
  public class CashNoteManagement : Form
  {
    private const string enableRefillText = "Refill Cash Note To Recycle Box";
    private const string disableRefillText = "Stop Refill";
    private const int maximumAvailableNoteCount = 80;
    private ICashDispenser cashDispenserManager;
    private ConstantVariable_CashNote cashNoteManager;
    private int currentAvailableNoteCount;
    private List<int> acceptedNoteCountList;
    private KioskData kioskData;
    private Decimal emptyedValue;
    private SettingBL sBL;
    private CashInOutBL cBL;
    private ShiftSummaryBL shiftBL;
    private IContainer components;
    private Button btnEmpty;
    private Label label1;
    private TextBox txtRecycleNote;
    private Label label3;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btnRefill;
    private Label label2;
    private Button btnReturn;
    private System.Windows.Forms.Timer timerCashNote;
    private Label lblStatus;
    private DataGridView dgvCashNote;

    public CashNoteManagement(KioskData kioskData)
    {
      this.kioskData = kioskData;
      this.InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      this.lblStatus.Text = CashDispenserStatus.Initialzing.ToString();
      this.ChangeControlState(false);
      try
      {
        DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
        this.sBL = new SettingBL();
        this.cBL = new CashInOutBL();
        this.shiftBL = new ShiftSummaryBL();
        new Thread((ThreadStart) (() => this.InitCashDispenser((Control) this, new Action(this.InitComplete), new Action<Exception, bool>(this.ExceptionHandling))))
        {
          IsBackground = true
        }.Start();
        this.ChangeControlState(true); //to bypass cashdispenser
      }
      catch (Exception ex)
      {
        this.ExceptionHandling(ex, false);
        this.DialogResult = DialogResult.OK;
        this.Close();
      }
    }

    private void ApplicationExit(bool status)
    {
      if (!status)
        return;
      Application.ExitThread();
      Application.Exit();
    }

    private void ExceptionHandling(Exception ex, bool showDetail)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke((Delegate) new Action<Exception, bool>(this.ExceptionHandling), (object) ex, (object) showDetail);
      }
      else
      {

       int num = (int) MessageBox.Show(ex.Message); 
       this.LogException(ex);
       this.btnReturn.Enabled = true; 
      
    }
    }

    private void LogException(Exception ex)
    {
      new MyLog().LogException(ex);
    }

    private void InitCashDispenser(Control control, Action actionComplete, Action<Exception, bool> exception)
    {
      try
      {
        this.cashNoteManager = new ConstantVariable_CashNote();
        this.acceptedNoteCountList = new List<int>();
        this.cashDispenserManager = (ICashDispenser) new CashDispenserManager(ConstantVariable_App.cashDispenserPort, this.cashNoteManager.acceptCashNoteList, false);
        this.cashDispenserManager.Init(this.cashNoteManager.managementRouteCashNoteList);
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionComplete);
        else
          actionComplete();
      }
      catch (Exception ex)
      {
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
    }

    private void InitComplete()
    {
      this.timerCashNote_Tick((object) this.timerCashNote, new EventArgs());
      this.lblStatus.Text = CashDispenserStatus.Ready_To_Use.ToString();
      this.ChangeControlState(true);
    }

    private void Form1_FormClosing(object sender, FormClosingEventArgs e)
    {
      try
      {
        if (this.cashDispenserManager != null)
        {
          this.cashDispenserManager.StopThread();
          this.cashDispenserManager.Dispose();
        }
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
      if (this.timerCashNote != null)
        this.timerCashNote.Stop();
      Thread.Sleep(100);
    }

    private void btnEmpty_Click(object sender, EventArgs e)
    {
      try
      {
        KioskBusinessEntity.Database_Entity.ShiftSummary frontDeskShift = ShiftService.GetFrontDeskShift(this.kioskData.propertyId);
                if (frontDeskShift == null || frontDeskShift.PMS_shiftId == 0L)
        {
         MessageBox.Show("Please start Front Desk Shift before do closing");
        }
        else
        {
          this.btnReturn.Enabled = false;
          this.ChangeControlState(false);
          this.emptyedValue = new Decimal(0);
         //this.cashDispenserManager.StartEmptyThread((Control) this, new Action<Decimal>(this.DoneEmpty), new Action<Decimal>(this.EmptyedValue), new Action<int>(this.UpdateCashDispenserStatus), new Action<Exception, bool>(this.ExceptionHandling));
                    this.ChangeControlState(true); //temp
                    this.btnReturn.Enabled = true;// temp
                    this.closeShift(); //temp
                    MessageBox.Show("The Shift ends now!");
                }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
    }

    private void EmptyedValue(Decimal accumulateAmount)
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke((Delegate) new Action<Decimal>(this.EmptyedValue), (object) accumulateAmount);
          return;
        }
        this.lblStatus.Text = "Emptyed note, value " + (object) accumulateAmount;
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
      if (!(accumulateAmount - this.emptyedValue > new Decimal(0)))
        return;
      try
      {
        int int32 = Convert.ToInt32(new SettingBL().Get(new KioskBusinessEntity.Database_Entity.Setting()
        {
          code = "ShiftId"
        }).value);
      KioskBusinessEntity.Database_Entity.CashInOut dataentry2= new KioskBusinessEntity.Database_Entity.CashInOut()
        {
          amount = accumulateAmount - this.emptyedValue,
          currencyCode = this.kioskData.currency,
          shiftId = (long) int32,
          propertyId = this.kioskData.propertyId,
          remarks = "Empty Cash Box",
          userId = this.kioskData.kioskUserId,
          paymentModeCode = ShiftCash.CASHSUBMIT.ToString(),
          transactionDate = DateTime.Now,
          IsUploaded = true
        };
      if (!this.cBL.Insert(ref dataentry2))
          throw new Exception("Failed to insert CashInOut records.");
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
      finally
      {
        this.emptyedValue = accumulateAmount;
      }
    }

    private void DoneEmpty(Decimal totalNoteValue)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke((Delegate) new Action<Decimal>(this.DoneEmpty), (object) totalNoteValue);
      }
      else
      {
        this.timerCashNote_Tick((object) this.timerCashNote, new EventArgs());
        this.ChangeControlState(true);
        this.closeShift();
        this.btnReturn.Enabled = true;
      }
    }

    private void btnRefill_Click(object sender, EventArgs e)
    {
      if (this.btnRefill.Text.Equals("Refill Cash Note To Recycle Box"))
      {
        this.btnReturn.Enabled = false;
        this.acceptedNoteCountList.Clear();
        foreach (int managementRouteCashNote in this.cashNoteManager.managementRouteCashNoteList)
          this.acceptedNoteCountList.Add(0);
        this.ChangeControlState(false);
        this.timerCashNote.Stop();
        this.cashDispenserManager.StartRefillThread((Control) this, new Action<Decimal>(this.completeRefill), new Action<int>(this.UpdateCashDispenserStatus), new Action<int, Decimal>(this.updateRefillAmount), new Action<Exception, bool>(this.ExceptionHandling), this.currentAvailableNoteCount);
        this.btnRefill.Text = "Stop Refill";
        this.btnRefill.Enabled = true;
      }
      else
      {
        if (!this.btnRefill.Text.Equals("Stop Refill"))
          return;
        this.cashDispenserManager.StopThread();
      }
    }

    private void updateRefillAmount(int remainingCashNote, Decimal refillAmount)
    {
      try
      {
        if (this.InvokeRequired)
        {
          this.BeginInvoke((Delegate) new Action<int, Decimal>(this.updateRefillAmount), (object) remainingCashNote, (object) refillAmount);
          return;
        }
        int index1 = this.cashNoteManager.managementRouteCashNoteList.FindIndex((Predicate<int>) (x => x.Equals(Convert.ToInt32(refillAmount))));
        if (index1 > 0 && index1 < this.acceptedNoteCountList.Count)
        {
          List<int> acceptedNoteCountList;
          int index2;
          (acceptedNoteCountList = this.acceptedNoteCountList)[index2 = index1] = acceptedNoteCountList[index2] + 1;
        }
        this.lblStatus.Text = "Accepted note, value " + (object) refillAmount;
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
      if (!(refillAmount > new Decimal(0)))
        return;
      try
      {
        long int64 = Convert.ToInt64(new SettingBL().Get(new KioskBusinessEntity.Database_Entity.Setting()
        {
          code = "ShiftId"
        }).value);
        
          KioskBusinessEntity.Database_Entity.CashInOut dataentry=new KioskBusinessEntity.Database_Entity.CashInOut()
        {
          amount = refillAmount,
          currencyCode = this.kioskData.currency,
          shiftId = int64,
          propertyId = this.kioskData.propertyId,
          remarks = "Refill",
          userId = this.kioskData.kioskUserId,
          paymentModeCode = ShiftCash.CASHIN.ToString(),
          transactionDate = DateTime.Now,
          IsUploaded = true
        };
          if (!this.cBL.Insert(ref dataentry))
          throw new Exception("Failed to insert CashInOut records.");
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
      finally
      {
        this.btnRefill.Enabled = true;
      }
    }

    private void UpdateCashDispenserStatus(int step)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke((Delegate) new Action<int>(this.UpdateCashDispenserStatus), (object) step);
      }
      else
      {
        CashDispenserStatus cashDispenserStatus = (CashDispenserStatus) step;
        this.lblStatus.Text = cashDispenserStatus.ToString();
        if (cashDispenserStatus.Equals((object) CashDispenserStatus.Accepting))
        {
          this.btnRefill.Enabled = false;
        }
        else
        {
          if (!cashDispenserStatus.Equals((object) CashDispenserStatus.Ready_To_Insert) && !cashDispenserStatus.Equals((object) CashDispenserStatus.Rejected))
            return;
          this.btnRefill.Enabled = true;
        }
      }
    }

    private void completeRefill(Decimal totalNoteValue)
    {
      if (this.InvokeRequired)
      {
        this.BeginInvoke((Delegate) new Action<Decimal>(this.completeRefill), (object) totalNoteValue);
      }
      else
      {
        this.timerCashNote_Tick((object) this.timerCashNote, new EventArgs());
        this.btnRefill.Text = "Refill Cash Note To Recycle Box";
        this.lblStatus.Text = CashDispenserStatus.Ready_To_Use.ToString();
        this.ChangeControlState(true);
        this.btnReturn.Enabled = true;
      }
    }

    private void timerCashNote_Tick(object sender, EventArgs e)
    {
      new Thread((ThreadStart) (() => this.UpdateCashNoteLevel((Control) this, new Action<List<CashNoteInformation>>(this.DoneUpdateCashNoteLevel), new Action<Exception, bool>(this.ExceptionHandling))))
      {
        IsBackground = true
      }.Start();
    }

    private void UpdateCashNoteLevel(Control control, Action<List<CashNoteInformation>> actionDone, Action<Exception, bool> exception)
    {
      try
      {
        List<CashNoteInformation> noteCount = this.cashDispenserManager.GetNoteCount(this.cashNoteManager.allCashNoteList.Select<Decimal, int>((Func<Decimal, int>) (x => Convert.ToInt32(x))).ToList<int>());
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionDone, (object) noteCount);
        else
          actionDone(noteCount);
      }
      catch (Exception ex)
      {
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
    }

    private void DoneUpdateCashNoteLevel(List<CashNoteInformation> cashNoteList)
    {
      DataTable dataTable = new DataTable();
      dataTable.Columns.Add(new DataColumn("Currency"));
      dataTable.Columns.Add(new DataColumn("Value"));
      dataTable.Columns.Add(new DataColumn("Stored"));
      this.currentAvailableNoteCount = 80;
      foreach (CashNoteInformation cashNote in cashNoteList)
      {
        DataRow row = dataTable.NewRow();
        row["Currency"] = (object) cashNote.currency;
        row["Value"] = (object) cashNote.cashNoteValue.ToString("0.00");
        row["Stored"] = (object) cashNote.cashNoteStoredCount.ToString();
        this.currentAvailableNoteCount -= cashNote.cashNoteStoredCount;
        dataTable.Rows.Add(row);
      }
      this.dgvCashNote.DataSource = (object) dataTable;
      this.dgvCashNote.Refresh();
      this.txtRecycleNote.Text = this.currentAvailableNoteCount.ToString();
    }

    private void ChangeControlState(bool state)
    {
      this.btnEmpty.Enabled = state;
      this.btnRefill.Enabled = state;
      if (state)
      {
        this.UpdateCashDispenserStatus(Convert.ToInt32((object) CashDispenserStatus.Ready_To_Use));
        this.timerCashNote.Start();
      }
      else
        this.timerCashNote.Stop();
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void dgvCashNote_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
    }

    private void label2_Click(object sender, EventArgs e)
    {
    }

    private void closeShift()
    {
      try
      {
        KioskBusinessEntity.Database_Entity.Setting s = new KioskBusinessEntity.Database_Entity.Setting();
        s.code = "ShiftId";
        long int64 = Convert.ToInt64(this.sBL.Get(s).value);
        Decimal cashCollected = this.cBL.GetCashSummary(int64).cashCollected;
        ShiftService.EmptyCashBox(this.kioskData.propertyId, int64, "Empty Cash Box", this.kioskData.kioskUserId, this.kioskData.currency, cashCollected);
        ShiftService.EndShiftReport(int64, this.kioskData.propertyId, "End Shift", this.kioskData.kioskUserId, new Decimal(0));
        long pmsShiftId = ShiftService.StartShiftReport(new Decimal(0), this.kioskData.currency, this.kioskData.propertyId, "Start Shift", this.kioskData.kioskUserId).PMS_shiftId;
        s.code = "ShiftId";
        s.value = Convert.ToString(pmsShiftId);
        this.sBL.Update(s);
      }
      catch (Exception ex)
      {
        this.LogException(ex);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRecycleNote = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRefill = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.dgvCashNote = new System.Windows.Forms.DataGridView();
            this.timerCashNote = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCashNote)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEmpty
            // 
            this.btnEmpty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEmpty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmpty.Location = new System.Drawing.Point(127, 283);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(4);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(267, 123);
            this.btnEmpty.TabIndex = 30;
            this.btnEmpty.Text = "Empty to Cashbox";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(172, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 29);
            this.label1.TabIndex = 35;
            this.label1.Text = "Cash Note Info:";
            // 
            // txtRecycleNote
            // 
            this.txtRecycleNote.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtRecycleNote.Location = new System.Drawing.Point(857, 23);
            this.txtRecycleNote.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecycleNote.Name = "txtRecycleNote";
            this.txtRecycleNote.ReadOnly = true;
            this.txtRecycleNote.Size = new System.Drawing.Size(113, 22);
            this.txtRecycleNote.TabIndex = 37;
            this.txtRecycleNote.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(551, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 58);
            this.label3.TabIndex = 38;
            this.label3.Text = "Current Aavilable Recycle Note";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtRecycleNote, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnRefill, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnReturn, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnEmpty, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dgvCashNote, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1044, 692);
            this.tableLayoutPanel1.TabIndex = 42;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // btnRefill
            // 
            this.btnRefill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.SetColumnSpan(this.btnRefill, 2);
            this.btnRefill.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefill.Location = new System.Drawing.Point(649, 283);
            this.btnRefill.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefill.Name = "btnRefill";
            this.btnRefill.Size = new System.Drawing.Size(267, 123);
            this.btnRefill.TabIndex = 39;
            this.btnRefill.Text = "Refill Cash Note To Recycle Box";
            this.btnRefill.UseVisualStyleBackColor = true;
            this.btnRefill.Click += new System.EventHandler(this.btnRefill_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.SetColumnSpan(this.btnReturn, 3);
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(388, 560);
            this.btnReturn.Margin = new System.Windows.Forms.Padding(4);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(267, 123);
            this.btnReturn.TabIndex = 41;
            this.btnReturn.Text = "Return To Menu";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(31, 468);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(459, 29);
            this.label2.TabIndex = 40;
            this.label2.Text = "This Cash Note Acceptor Only Accept $10";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 2);
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(742, 468);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(82, 29);
            this.lblStatus.TabIndex = 42;
            this.lblStatus.Text = "Ready";
            // 
            // dgvCashNote
            // 
            this.dgvCashNote.AllowUserToAddRows = false;
            this.dgvCashNote.AllowUserToDeleteRows = false;
            this.dgvCashNote.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCashNote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.dgvCashNote, 3);
            this.dgvCashNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCashNote.Location = new System.Drawing.Point(4, 73);
            this.dgvCashNote.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCashNote.Name = "dgvCashNote";
            this.dgvCashNote.ShowEditingIcon = false;
            this.dgvCashNote.Size = new System.Drawing.Size(1036, 199);
            this.dgvCashNote.TabIndex = 43;
            this.dgvCashNote.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCashNote_CellContentClick);
            // 
            // timerCashNote
            // 
            this.timerCashNote.Interval = 5000;
            this.timerCashNote.Tick += new System.EventHandler(this.timerCashNote_Tick);
            // 
            // CashNoteManagement
            // 
            this.ClientSize = new System.Drawing.Size(1044, 692);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CashNoteManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cash Note Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCashNote)).EndInit();
            this.ResumeLayout(false);

    }

    private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
    {

    }
  }
}
