﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.UpdateTransactionStatus
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using CardTransactionBusinessEntity;
using CardTransactionBusinessLayer;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class UpdateTransactionStatus : Form
  {
    private CardTransactionSummaryBL tsBL;
    private CardTransactionDetailBL tdBL;
    public string machineCode;
    public Control mainForm;
    private IContainer components;
    private TableLayoutPanel tlCardTransaction;
    private TextBox txtCardIn;
    private Label lblCardIn;
    private Label lblCardOut;
    private Label lblCardBox;
    private TextBox txtCardOut;
    private TextBox txtCardBox;
    private Button btnCancel;
    private Button btnUpdate;

    public UpdateTransactionStatus()
    {
      this.InitializeComponent();
      this.tsBL = new CardTransactionSummaryBL();
      this.tdBL = new CardTransactionDetailBL();
    }

    public UpdateTransactionStatus(Control mainForm)
    {
      this.InitializeComponent();
      this.tsBL = new CardTransactionSummaryBL();
      this.tdBL = new CardTransactionDetailBL();
      this.mainForm = mainForm;
    }

    public UpdateTransactionStatus(Control mainForm, string machineCode)
    {
      this.InitializeComponent();
      this.tsBL = new CardTransactionSummaryBL();
      this.tdBL = new CardTransactionDetailBL();
      this.mainForm = mainForm;
      this.machineCode = machineCode;
    }

    private void btnUpdate_Click(object sender, EventArgs e)
    {
      try
      {
        CardTransactionSummary ts = new CardTransactionSummary();
        ts.machineCode = this.machineCode;
        ts.cardInQuantity = Convert.ToInt32(this.txtCardIn.Text);
        ts.cardOutQuantity = Convert.ToInt32(this.txtCardOut.Text);
        ts.cardBoxQuantity = Convert.ToInt32(this.txtCardBox.Text);
        if (this.tsBL.InsertOrUpdate(ts).id <= 0)
          throw new Exception("Update summarry failed.");
        if (this.tdBL.Insert(new CardTransactionDetail()
        {
          actionName = "Update",
          actionType = "Update",
          machineCode = ts.machineCode,
          errorMessage = string.Empty
        }).id > 0)
        {
          int num = (int) MessageBox.Show("Update successfully.");
          this.Close();
          ((CardManagement) this.mainForm).RefreshLog();
        }
        else
        {
          Exception exception = new Exception("Update detail failed.");
        }
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void ExceptionHandler(Exception ex)
    {
      int num = (int) MessageBox.Show(ex.Message);
    }

    private void UpdateTransactionStatus_Load(object sender, EventArgs e)
    {
      try
      {
        CardTransactionSummary transactionSummary = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = this.machineCode
        });
        this.txtCardIn.Text = transactionSummary.cardInQuantity.ToString();
        this.txtCardOut.Text = transactionSummary.cardOutQuantity.ToString();
        this.txtCardBox.Text = transactionSummary.cardBoxQuantity.ToString();
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tlCardTransaction = new TableLayoutPanel();
      this.txtCardIn = new TextBox();
      this.lblCardIn = new Label();
      this.lblCardOut = new Label();
      this.lblCardBox = new Label();
      this.txtCardOut = new TextBox();
      this.txtCardBox = new TextBox();
      this.btnCancel = new Button();
      this.btnUpdate = new Button();
      this.tlCardTransaction.SuspendLayout();
      this.SuspendLayout();
      this.tlCardTransaction.ColumnCount = 2;
      this.tlCardTransaction.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tlCardTransaction.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tlCardTransaction.Controls.Add((Control) this.txtCardIn, 1, 0);
      this.tlCardTransaction.Controls.Add((Control) this.lblCardIn, 0, 0);
      this.tlCardTransaction.Controls.Add((Control) this.lblCardOut, 0, 1);
      this.tlCardTransaction.Controls.Add((Control) this.lblCardBox, 0, 2);
      this.tlCardTransaction.Controls.Add((Control) this.txtCardOut, 1, 1);
      this.tlCardTransaction.Controls.Add((Control) this.txtCardBox, 1, 2);
      this.tlCardTransaction.Controls.Add((Control) this.btnCancel, 1, 3);
      this.tlCardTransaction.Controls.Add((Control) this.btnUpdate, 0, 3);
      this.tlCardTransaction.Dock = DockStyle.Fill;
      this.tlCardTransaction.Location = new Point(0, 0);
      this.tlCardTransaction.Name = "tlCardTransaction";
      this.tlCardTransaction.RowCount = 4;
      this.tlCardTransaction.RowStyles.Add(new RowStyle(SizeType.Percent, 25f));
      this.tlCardTransaction.RowStyles.Add(new RowStyle(SizeType.Percent, 25f));
      this.tlCardTransaction.RowStyles.Add(new RowStyle(SizeType.Percent, 25f));
      this.tlCardTransaction.RowStyles.Add(new RowStyle(SizeType.Percent, 25f));
      this.tlCardTransaction.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
      this.tlCardTransaction.Size = new Size(284, 161);
      this.tlCardTransaction.TabIndex = 0;
      this.txtCardIn.Anchor = AnchorStyles.Left | AnchorStyles.Right;
      this.txtCardIn.Location = new Point(145, 10);
      this.txtCardIn.Name = "txtCardIn";
      this.txtCardIn.Size = new Size(136, 20);
      this.txtCardIn.TabIndex = 14;
      this.lblCardIn.AutoSize = true;
      this.lblCardIn.Dock = DockStyle.Fill;
      this.lblCardIn.Location = new Point(3, 0);
      this.lblCardIn.Name = "lblCardIn";
      this.lblCardIn.Size = new Size(136, 40);
      this.lblCardIn.TabIndex = 15;
      this.lblCardIn.Text = "Card In";
      this.lblCardIn.TextAlign = ContentAlignment.MiddleLeft;
      this.lblCardOut.AutoSize = true;
      this.lblCardOut.Dock = DockStyle.Fill;
      this.lblCardOut.Location = new Point(3, 40);
      this.lblCardOut.Name = "lblCardOut";
      this.lblCardOut.Size = new Size(136, 40);
      this.lblCardOut.TabIndex = 16;
      this.lblCardOut.Text = "Card Out";
      this.lblCardOut.TextAlign = ContentAlignment.MiddleLeft;
      this.lblCardBox.AutoSize = true;
      this.lblCardBox.Dock = DockStyle.Fill;
      this.lblCardBox.Location = new Point(3, 80);
      this.lblCardBox.Name = "lblCardBox";
      this.lblCardBox.Size = new Size(136, 40);
      this.lblCardBox.TabIndex = 17;
      this.lblCardBox.Text = "Card Box";
      this.lblCardBox.TextAlign = ContentAlignment.MiddleLeft;
      this.txtCardOut.Anchor = AnchorStyles.Left | AnchorStyles.Right;
      this.txtCardOut.Location = new Point(145, 50);
      this.txtCardOut.Name = "txtCardOut";
      this.txtCardOut.Size = new Size(136, 20);
      this.txtCardOut.TabIndex = 18;
      this.txtCardBox.Anchor = AnchorStyles.Left | AnchorStyles.Right;
      this.txtCardBox.Location = new Point(145, 90);
      this.txtCardBox.Name = "txtCardBox";
      this.txtCardBox.Size = new Size(136, 20);
      this.txtCardBox.TabIndex = 19;
      this.btnCancel.Anchor = AnchorStyles.None;
      this.btnCancel.CausesValidation = false;
//      this.btnCancel.DialogResult = DialogResult.Cancel;
      this.btnCancel.Location = new Point(175, 129);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 20;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
      this.btnUpdate.Anchor = AnchorStyles.None;
//      this.btnUpdate.DialogResult = DialogResult.OK;
      this.btnUpdate.Location = new Point(33, 129);
      this.btnUpdate.Name = "btnUpdate";
      this.btnUpdate.Size = new Size(75, 23);
      this.btnUpdate.TabIndex = 21;
      this.btnUpdate.Text = "Update";
      this.btnUpdate.UseVisualStyleBackColor = true;
      this.btnUpdate.Click += new EventHandler(this.btnUpdate_Click);
      this.AcceptButton = (IButtonControl) this.btnUpdate;
 //     this.AutoScaleDimensions = new SizeF(6f, 13f);
//      this.AutoScaleMode = AutoScaleMode.Font;
      this.CancelButton = (IButtonControl) this.btnCancel;
      this.ClientSize = new Size(284, 161);
      this.Controls.Add((Control) this.tlCardTransaction);
  //    this.Name = nameof (UpdateTransactionStatus);
      this.StartPosition = FormStartPosition.CenterScreen;
  //    this.Text = nameof (UpdateTransactionStatus);
      this.Load += new EventHandler(this.UpdateTransactionStatus_Load);
      this.tlCardTransaction.ResumeLayout(false);
      this.tlCardTransaction.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
