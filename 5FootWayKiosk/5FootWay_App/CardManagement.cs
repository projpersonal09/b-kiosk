﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.CardManagement
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using CardTransactionBusinessEntity;
using CardTransactionBusinessLayer;
using Helper;
using KioskBusinessEntity.Kiosk_Entity;
using MQuest.HardwareInterface.CardDispenser;
using MQuest.HardwareInterface.CardDispenser.Creator.CRT_591;
using MQuest.HardwareInterface.CardReader;
using MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class CardManagement : Form
  {
    private CardTransactionSummaryBL tsBL;
    private CardTransactionDetailBL tdBL;
    private ICardReaderManager cardReader;
    private ICardDispenser cardDispenser;
    private static bool startFill;
    private KioskData kioskData;
    private int cardToEmpty;
    private IContainer components;
    private TabControl tabManagement;
    private TabPage TransactionManagement;
    private TableLayoutPanel ManagementLayout;
    private DataGridView dgvSummary;
    private DataGridView dgvDetail;
    private CheckedListBox clbMachine;
    private DateTimePicker dtpStart;
    private DateTimePicker dtpEnd;
    private Button btnRefreshLog;
    private Label lblMachine;
    private Label lblTransactionSummary;
    private Label lblTransactionDetail;
    private TabPage DispenserReaderManagement;
    private Label lblStartDate;
    private Label lblEndDate;
    private CheckBox cbErrorOnly;
    private TableLayoutPanel tlDispenserReader;
    private Button btnDispense;
    private CheckBox cbReset;
    private Button btnReturn;
    private ListView lvMessage;
    private Label lblMessage;
    private Button btnClearMessage;
    private Button btnFill;
    private Label lblKioskNameLabel;
    private Label lblKioskName;
    private Button btnAdjust;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private GroupBox groupBox3;
    private GroupBox groupBox4;
    private TextBox txtFillQty;
    private TextBox txtAdjustBoxQty;
    private Label label1;
    private Label label3;
    private Label label2;
    private TextBox txtAdjustBinQty;
    private Label label4;
    private TextBox txtReturnQty;
    private Label label5;
    private TextBox txtDispenseQty;

    public CardManagement(KioskData kioskData)
    {
      this.kioskData = kioskData;
      this.InitializeComponent();
    }

    public CardManagement(ICardDispenser cardDispenser, ICardReaderManager cardReader)
    {
      this.InitializeComponent();
      this.cardDispenser = cardDispenser;
      this.cardReader = cardReader;
    }

    private void btnRefreshLog_Click(object sender, EventArgs e)
    {
      this.RefreshLog();
    }

    public void RefreshLog()
    {
      this.ChangeControlEnable((Control) this.btnRefreshLog, false);
      DateTime startDate = new DateTime(this.dtpStart.Value.Year, this.dtpStart.Value.Month, this.dtpStart.Value.Day, 0, 0, 0);
      DateTime endDate = new DateTime(this.dtpEnd.Value.Year, this.dtpEnd.Value.Month, this.dtpEnd.Value.Day, 23, 59, 59);
      new Thread((ThreadStart) (() => this.UpdateTransaction((Control) this, new Action<List<CardTransactionDetail>>(this.UpdateTransactionDetail), new Action<List<CardTransactionSummary>>(this.UpdateTransactionSummary), new Action<Exception, bool>(this.GlobalExceptionHandler), new Action<Button, bool>(this.ChangeControlEnable), this.btnRefreshLog, this.clbMachine.CheckedItems.Cast<string>().ToList<string>(), startDate, endDate)))
      {
        IsBackground = true
      }.Start();
    }

    private void UpdateTransaction(Control control, Action<List<CardTransactionDetail>> updateDetail, Action<List<CardTransactionSummary>> updateSummary, Action<Exception, bool> exception, Action<Button, bool> changeButtonEnable, Button btn, List<string> selectedMachine, DateTime startDate, DateTime endDate)
    {
      try
      {
        string actionType = string.Empty;
        if (this.cbErrorOnly.Checked)
          actionType = "Error";
        List<CardTransactionDetail> list = this.tdBL.GetList(selectedMachine, startDate, endDate, actionType);
        if (updateDetail != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) updateDetail, (object) list);
          else
            updateDetail(list);
        }
      }
      catch (Exception ex)
      {
        if (exception != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
          else
            exception(ex, false);
        }
      }
      try
      {
        List<CardTransactionSummary> list = this.tsBL.GetList(selectedMachine, startDate, endDate);
        if (updateSummary != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) updateSummary, (object) list);
          else
            updateSummary(list);
        }
      }
      catch (Exception ex)
      {
        if (exception != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
          else
            exception(ex, false);
        }
      }
      if (changeButtonEnable == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) changeButtonEnable, (object) btn, (object) true);
      else
        changeButtonEnable(btn, true);
    }

    private void UpdateTransactionDetail(List<CardTransactionDetail> dataList)
    {
      this.dgvDetail.DataSource = (object) dataList;
    }

    private void UpdateTransactionSummary(List<CardTransactionSummary> dataList)
    {
      this.dgvSummary.DataSource = (object) dataList;
    }

    private void ChangeControlEnable(Control control, bool enable)
    {
      if (control == null)
        return;
      control.Enabled = enable;
    }

    private void CardManagement_Load(object sender, EventArgs e)
    {
      try
      {
        this.tsBL = new CardTransactionSummaryBL();
        this.tdBL = new CardTransactionDetailBL();
        if (this.cardDispenser == null)
        {
          this.cardDispenser = (ICardDispenser) new CardDispenserManager(false);
          this.cardDispenser.StartInitDispenserThread((Control) this, (Action<string>) null, new Action<Exception, bool>(this.GlobalExceptionHandler), ConstantVariable_App.cardDispenserPortName, ConstantVariable_App.cardDispenserBaudRate, ConstantVariable_App.cardDispenserAddress, InitializeMode.INIT_CAPTURE_TO_BOX, RFCardReaderStatus.Deactivate);
        }
        if (this.cardReader == null)
          this.cardReader = (ICardReaderManager) new CardReaderManager(this.kioskData.customerCode, ConstantVariable_App.cardReaderLibraryDll);
        this.dtpEnd.Value = DateTime.Now;
        this.dtpStart.Value = DateTime.Now.AddMonths(-1);
        this.clbMachine.Items.Clear();
        this.lblKioskName.Text = ConstantVariable_App.terminalCode;
        if (!string.IsNullOrEmpty(this.lblKioskName.Text))
        {
          this.CardManagementEnable(true);
          this.clbMachine.Items.Add((object) this.lblKioskName.Text, true);
        }
        this.btnRefreshLog.PerformClick();
      }
      catch (Exception ex)
      {
        this.GlobalExceptionHandler(ex, false);
      }
    }

    private void ExceptionHandler(Exception ex, bool showDetail)
    {
      if (ex == null)
        return;
      try
      {
        this.UpdateMessage(new ListViewItem()
        {
          ForeColor = Color.Red,
          Text = ex.Message
        });
        if (showDetail)
          return;
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        if (this.tdBL.Insert(new CardTransactionDetail()
        {
          machineCode = this.lblKioskName.Text,
          actionName = "Error",
          actionType = "Error",
          errorMessage = ExceptionHelper.GetFullExceptionMessage(ex, ", ")
        }).id == 0)
          throw new Exception("failed to insert transaction detail at exception handler.");
      }
      catch (Exception ex1)
      {
        this.ExceptionHandler(ex1, true);
      }
    }

    private void GlobalExceptionHandler(Exception ex, bool showDetail)
    {
      if (ex == null)
        return;
      try
      {
        if (!showDetail)
        {
          if (this.tdBL.Insert(new CardTransactionDetail()
          {
            machineCode = this.lblKioskName.Text,
            actionName = "Error",
            actionType = "Error",
            errorMessage = ExceptionHelper.GetFullExceptionMessage(ex, "\n")
          }).id == 0)
            throw new Exception("failed to insert transaction detail at exception handler.");
        }
        int num = (int) MessageBox.Show(ex.Message);
      }
      catch (Exception ex1)
      {
        this.GlobalExceptionHandler(ex1, true);
      }
    }

    private void DisplayExceptionMessage(Exception ex)
    {
      this.GlobalExceptionHandler(ex, false);
    }

    private void ExceptionHandler(Exception ex, string machineCode)
    {
      this.tdBL.Insert(new CardTransactionDetail()
      {
        machineCode = string.IsNullOrEmpty(machineCode) ? "Others" : machineCode,
        actionName = "Error",
        actionType = "Error",
        errorMessage = ExceptionHelper.GetFullExceptionMessage(ex, "\n")
      });
      int num = (int) MessageBox.Show(ex.Message);
    }

    private void tabManagement_SelectedIndexChanged(object sender, EventArgs e)
    {
      TabControl tabControl = (TabControl) sender;
      if (tabControl.SelectedIndex == 0)
        return;
      int selectedIndex = tabControl.SelectedIndex;
    }

    private void UpdateMessage(ListViewItem item)
    {
      this.lvMessage.Items.Add(item);
    }

    private void UpdateMessage(string text)
    {
      this.lvMessage.Items.Add(text);
    }

    private void LogExceptionMessage(Exception ex)
    {
      this.ExceptionHandler(ex, false);
    }

    private void EmptyCard()
    {
      List<int> process = new List<int>();
      process.Add(1);
      if (this.cbReset.Checked)
        process.Add(2);
      process.Add(3);
      List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
      if (this.cbReset.Checked)
        additionalFunctionList.Add(new Func<Control, string>(this.ResetCard));
      new List<CardDispensePosition>()
      {
        CardDispensePosition.FROM_BOX
      };
      this.cardDispenser.StartDispenseCardThread((Control) this, additionalFunctionList, process, new Action<string>(this.EmptyComplete), new Action<string, int>(this.EmptyProgress), new Action<string>(this.EmptyFailed), new Action<Exception>(this.DisplayExceptionMessage), new Action<Exception, bool>(this.GlobalExceptionHandler), "", CardPositionCommand.MM_RETURN_TO_FRONT);
    }

    private void EmptyProgress(string cardId, int step)
    {
      if (step.Equals(1))
        this.UpdateMessage("Start");
      else if (step.Equals(2))
      {
        this.UpdateMessage("Processing");
      }
      else
      {
        if (!step.Equals(3))
          return;
        this.UpdateMessage("Done");
      }
    }

    private void EmptyComplete(string cardId)
    {
      --this.cardToEmpty;
      try
      {
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Empty";
        td.actionType = "Empty";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail after done return card.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        --ts.cardInQuantity;
        ++ts.cardOutQuantity;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary after done return card.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
      if (this.cardToEmpty > 0)
        this.EmptyCard();
      else
        this.CardManagementEnable(true);
    }

    private void EmptyFailed(string cardId)
    {
      this.CardManagementEnable(true);
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      if (!this.ValidateTxtBoxEmpty(new List<TextBox>()
      {
        this.txtReturnQty
      }))
        return;
      try
      {
        if (this.cardDispenser.IsCardBinFull())
          throw CardDispenserException.cardBinFull;
        this.CardManagementEnable(false);
        List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
        if (this.cbReset.Checked)
          additionalFunctionList.Add(new Func<Control, string>(this.ResetCard));
        List<int> process = new List<int>();
        process.Add(1);
        if (this.cbReset.Checked)
          process.Add(2);
        process.Add(3);
        new List<CardReturnPosition>()
        {
          CardReturnPosition.FROM_OUT
        };
        int int32 = Convert.ToInt32(this.txtReturnQty.Text);
        this.cardDispenser.StartReturnCardThread((Control) this, additionalFunctionList, process, new Action(this.ReturnComplete), new Action(this.ReturnAllComplete), new Action<int>(this.ReturnProgress), new Action(this.ReturnFailed), CardPositionCommand.MM_RETURN_TO_FRONT, CardPositionCommand.MM_CAPTURE_TO_BOX, new Action<Exception>(this.LogExceptionMessage), new Action<Exception, bool>(this.ReturnException), int32);
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
        this.CardManagementEnable(true);
      }
    }

    private void ReturnException(Exception ex, bool showDetail)
    {
      this.ExceptionHandler(ex, showDetail);
      this.CardManagementEnable(true);
    }

    private void ReturnComplete()
    {
      try
      {
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Return";
        td.actionType = "Return";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail after done return card.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        ++ts.cardBinQuantity;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary after done return card.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
      finally
      {
        this.CardManagementEnable(true);
      }
    }

    private void ReturnAllComplete()
    {
      this.CardManagementEnable(true);
    }

    private void ReturnProgress(int step)
    {
      if (step.Equals(1))
        this.UpdateMessage("Start");
      else if (step.Equals(2))
      {
        this.UpdateMessage("Processing");
      }
      else
      {
        if (!step.Equals(3))
          return;
        this.UpdateMessage("Done");
      }
    }

    private void ReturnFailed()
    {
      this.UpdateMessage("Failed");
      this.CardManagementEnable(true);
    }

    private void btnDispense_Click(object sender, EventArgs e)
    {
      if (!this.ValidateTxtBoxEmpty(new List<TextBox>()
      {
        this.txtDispenseQty
      }))
        return;
      try
      {
        this.CardManagementEnable(false);
        List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
        if (this.cbReset.Checked)
          additionalFunctionList.Add(new Func<Control, string>(this.ResetCard));
        List<int> process = new List<int>();
        process.Add(1);
        if (this.cbReset.Checked)
          process.Add(2);
        process.Add(3);
        new List<CardDispensePosition>()
        {
          CardDispensePosition.FROM_BOX
        };
        int int32 = Convert.ToInt32(this.txtDispenseQty.Text);
        for (int index = 1; index <= int32; ++index)
          this.cardDispenser.StartDispenseCardThread((Control) this, additionalFunctionList, process, new Action<string>(this.DispenseComplete), new Action<string, int>(this.DispenseProgress), new Action<string>(this.DispenseFailed), new Action<Exception>(this.LogExceptionMessage), new Action<Exception, bool>(this.DispenseException), index.ToString(), CardPositionCommand.MM_RETURN_TO_FRONT);
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
        this.CardManagementEnable(true);
      }
    }

    private void DispenseException(Exception ex, bool showDetail)
    {
      this.ExceptionHandler(ex, showDetail);
      this.CardManagementEnable(true);
    }

    private void DispenseComplete(string cardId)
    {
      try
      {
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Dispense";
        td.actionType = "Dispense";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail after done dispense card.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        --ts.cardBoxQuantity;
        ++ts.cardOutQuantity;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary after done dispense card.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
      finally
      {
        this.CardManagementEnable(true);
      }
    }

    private void DispenseProgress(string cardId, int step)
    {
      if (step.Equals(1))
        this.UpdateMessage("Start");
      else if (step.Equals(2))
      {
        this.UpdateMessage("Processing");
      }
      else
      {
        if (!step.Equals(3))
          return;
        this.UpdateMessage("Done");
      }
    }

    private void DispenseFailed(string cardId)
    {
      this.UpdateMessage("Failed");
      this.CardManagementEnable(true);
      this.DispenseFailed();
    }

    private void DispenseFailed()
    {
      try
      {
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Dispense";
        td.actionType = "Dispense";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail when dispense card failed.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        --ts.cardBoxQuantity;
        ++ts.cardBinQuantity;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary when dispense card failed.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
      finally
      {
        this.CardManagementEnable(true);
      }
    }

    private string ResetCard(Control control)
    {
      int num = 0;
      string str1 = string.Empty;
      for (; num < 3; ++num)
      {
        try
        {
          this.cardReader.ResetCard();
          str1 = string.Empty;
        }
        catch (Exception ex)
        {
          string str2 = str1;
          string str3;
          switch (num)
          {
            case 0:
              str3 = "1st try : ";
              break;
            case 1:
              str3 = ",2nd try : ";
              break;
            default:
              str3 = ",3rd try : ";
              break;
          }
          string message = ex.Message;
          str1 = str2 + str3 + message;
        }
        if (string.IsNullOrEmpty(str1))
          break;
      }
      return str1;
    }

    private void CardManagementEnable(bool status)
    {
      this.ChangeControlEnable((Control) this.btnReturn, status);
      this.ChangeControlEnable((Control) this.btnDispense, status);
      this.ChangeControlEnable((Control) this.btnAdjust, status);
      this.ChangeControlEnable((Control) this.cbReset, status);
      this.ChangeControlEnable((Control) this.btnFill, status);
    }

    private void dgvSummary_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      this.ShowCardTransactionEditor(e.RowIndex);
    }

    private void ShowCardTransactionEditor(int rowIndex)
    {
      string machineCode = this.dgvSummary.Rows[rowIndex].Cells[1].Value.ToString();
      try
      {
        new UpdateTransactionStatus((Control) this, machineCode).Show();
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, machineCode);
      }
    }

    private void cbSelectedMachine_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (!string.IsNullOrEmpty(this.lblKioskName.Text))
      {
        bool enable = true;
        this.ChangeControlEnable((Control) this.btnDispense, enable);
        this.ChangeControlEnable((Control) this.btnReturn, enable);
        this.ChangeControlEnable((Control) this.btnAdjust, enable);
        this.ChangeControlEnable((Control) this.cbReset, enable);
        this.ChangeControlEnable((Control) this.btnFill, enable);
      }
      else
      {
        bool enable = false;
        this.ChangeControlEnable((Control) this.btnReturn, enable);
        this.ChangeControlEnable((Control) this.btnDispense, enable);
        this.ChangeControlEnable((Control) this.btnAdjust, enable);
        this.ChangeControlEnable((Control) this.cbReset, enable);
        this.ChangeControlEnable((Control) this.btnFill, enable);
      }
    }

    private void btnClearMessage_Click(object sender, EventArgs e)
    {
      this.lvMessage.Clear();
    }

    private void btnFill_Click(object sender, EventArgs e)
    {
      if (!this.ValidateTxtBoxEmpty(new List<TextBox>()
      {
        this.txtFillQty
      }))
        return;
      try
      {
        this.FillComplete(Convert.ToInt32(this.txtFillQty.Text));
        int num = (int) MessageBox.Show("Filled Card Success");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
    }

    private void FillCardException(Exception ex, bool showDetail)
    {
      this.ExceptionHandler(ex, showDetail);
      this.CardManagementEnable(true);
      this.btnFill.Text = "Start Fill Card";
    }

    private void FillProgress(int step)
    {
      if (step.Equals(1))
        this.UpdateMessage("Start");
      else if (step.Equals(2))
      {
        this.UpdateMessage("Processing");
      }
      else
      {
        if (!step.Equals(3))
          return;
        this.UpdateMessage("Done");
      }
    }

    private void FillComplete(int qty)
    {
      try
      {
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Fill";
        td.actionType = "Fill";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail after done return card.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        ts.cardBoxQuantity += qty;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary after done return card.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
    }

    private void FillAllComplete()
    {
      this.CardManagementEnable(true);
      this.btnFill.Text = "Start Fill Card";
    }

    private void FillFailed()
    {
      this.UpdateMessage("Failed");
      this.CardManagementEnable(true);
      this.btnFill.Text = "Start Fill Card";
    }

    private void CardManagement_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (this.cardDispenser != null)
        this.cardDispenser.Dispose();
      if (this.cardReader != null)
        this.cardReader.Dispose();
      this.DialogResult = DialogResult.OK;
    }

    private void btnAdjust_Click(object sender, EventArgs e)
    {
      if (!this.ValidateTxtBoxEmpty(new List<TextBox>()
      {
        this.txtAdjustBoxQty,
        this.txtAdjustBinQty
      }))
        return;
      try
      {
        this.AdjustComplete(Convert.ToInt32(this.txtAdjustBoxQty.Text), Convert.ToInt32(this.txtAdjustBinQty.Text));
        int num = (int) MessageBox.Show("Adjust Card Success");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
    }

    private void AdjustComplete(int boxQty, int binQty)
    {
      try
      {
        if (string.IsNullOrEmpty(this.lblKioskName.Text))
          throw new Exception("Kiosk name cannot be empty.");
        CardTransactionDetail td = new CardTransactionDetail();
        td.machineCode = this.lblKioskName.Text;
        td.actionName = "Adjust";
        td.actionType = "Adjust";
        td.errorMessage = "";
        if (this.tdBL.Insert(td).id <= 0)
          throw new Exception("failed to insert transaction detail when adjust card.");
        CardTransactionSummary ts = this.tsBL.Get(new CardTransactionSummary()
        {
          machineCode = td.machineCode
        });
        ts.cardBoxQuantity = boxQty;
        ts.cardBinQuantity = binQty;
        if (this.tsBL.InsertOrUpdate(ts).id == 0)
          throw new Exception("failed to insert or update transaction summary when adjust card.");
      }
      catch (Exception ex)
      {
        this.ExceptionHandler(ex, false);
      }
    }

    private void txtIsNumberOnly_KeyPress(object sender, KeyPressEventArgs e)
    {
      e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
    }

    private bool ValidateTxtBoxEmpty(List<TextBox> txtBoxList)
    {
      bool flag = true;
      foreach (TextBox txtBox in txtBoxList)
      {
        if (string.IsNullOrWhiteSpace(txtBox.Text))
        {
          flag = false;
          int num = (int) MessageBox.Show(string.Format("Please input {0}", txtBox.Tag));
          txtBox.Focus();
          WindowHelper.showKeyboard();
          break;
        }
      }
      return flag;
    }

    private void txtShowKeyboard_Click(object sender, EventArgs e)
    {
      WindowHelper.showKeyboard();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabManagement = new System.Windows.Forms.TabControl();
            this.TransactionManagement = new System.Windows.Forms.TabPage();
            this.ManagementLayout = new System.Windows.Forms.TableLayoutPanel();
            this.dgvSummary = new System.Windows.Forms.DataGridView();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.clbMachine = new System.Windows.Forms.CheckedListBox();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblMachine = new System.Windows.Forms.Label();
            this.lblTransactionSummary = new System.Windows.Forms.Label();
            this.lblTransactionDetail = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.cbErrorOnly = new System.Windows.Forms.CheckBox();
            this.btnRefreshLog = new System.Windows.Forms.Button();
            this.DispenserReaderManagement = new System.Windows.Forms.TabPage();
            this.tlDispenserReader = new System.Windows.Forms.TableLayoutPanel();
            this.cbReset = new System.Windows.Forms.CheckBox();
            this.lvMessage = new System.Windows.Forms.ListView();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnClearMessage = new System.Windows.Forms.Button();
            this.lblKioskNameLabel = new System.Windows.Forms.Label();
            this.lblKioskName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFillQty = new System.Windows.Forms.TextBox();
            this.btnFill = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAdjustBinQty = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAdjustBoxQty = new System.Windows.Forms.TextBox();
            this.btnAdjust = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtReturnQty = new System.Windows.Forms.TextBox();
            this.btnReturn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDispenseQty = new System.Windows.Forms.TextBox();
            this.btnDispense = new System.Windows.Forms.Button();
            this.tabManagement.SuspendLayout();
            this.TransactionManagement.SuspendLayout();
            this.ManagementLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.DispenserReaderManagement.SuspendLayout();
            this.tlDispenserReader.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabManagement
            // 
            this.tabManagement.Controls.Add(this.TransactionManagement);
            this.tabManagement.Controls.Add(this.DispenserReaderManagement);
            this.tabManagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabManagement.Location = new System.Drawing.Point(0, 0);
            this.tabManagement.Margin = new System.Windows.Forms.Padding(4);
            this.tabManagement.Name = "tabManagement";
            this.tabManagement.SelectedIndex = 0;
            this.tabManagement.Size = new System.Drawing.Size(1212, 741);
            this.tabManagement.TabIndex = 2;
            this.tabManagement.SelectedIndexChanged += new System.EventHandler(this.tabManagement_SelectedIndexChanged);
            // 
            // TransactionManagement
            // 
            this.TransactionManagement.Controls.Add(this.ManagementLayout);
            this.TransactionManagement.Location = new System.Drawing.Point(4, 25);
            this.TransactionManagement.Margin = new System.Windows.Forms.Padding(4);
            this.TransactionManagement.Name = "TransactionManagement";
            this.TransactionManagement.Padding = new System.Windows.Forms.Padding(4);
            this.TransactionManagement.Size = new System.Drawing.Size(1204, 712);
            this.TransactionManagement.TabIndex = 1;
            this.TransactionManagement.Text = "Transaction Management";
            this.TransactionManagement.UseVisualStyleBackColor = true;
            // 
            // ManagementLayout
            // 
            this.ManagementLayout.ColumnCount = 5;
            this.ManagementLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.ManagementLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ManagementLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ManagementLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.ManagementLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.ManagementLayout.Controls.Add(this.dgvSummary, 0, 5);
            this.ManagementLayout.Controls.Add(this.dgvDetail, 0, 7);
            this.ManagementLayout.Controls.Add(this.clbMachine, 4, 1);
            this.ManagementLayout.Controls.Add(this.dtpStart, 2, 0);
            this.ManagementLayout.Controls.Add(this.dtpEnd, 2, 1);
            this.ManagementLayout.Controls.Add(this.lblMachine, 4, 0);
            this.ManagementLayout.Controls.Add(this.lblTransactionSummary, 0, 4);
            this.ManagementLayout.Controls.Add(this.lblTransactionDetail, 0, 6);
            this.ManagementLayout.Controls.Add(this.lblStartDate, 0, 0);
            this.ManagementLayout.Controls.Add(this.lblEndDate, 0, 1);
            this.ManagementLayout.Controls.Add(this.cbErrorOnly, 0, 2);
            this.ManagementLayout.Controls.Add(this.btnRefreshLog, 4, 3);
            this.ManagementLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ManagementLayout.Location = new System.Drawing.Point(4, 4);
            this.ManagementLayout.Margin = new System.Windows.Forms.Padding(4);
            this.ManagementLayout.Name = "ManagementLayout";
            this.ManagementLayout.RowCount = 8;
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.761906F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.761906F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.761906F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.52381F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.761906F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.761906F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ManagementLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ManagementLayout.Size = new System.Drawing.Size(1196, 704);
            this.ManagementLayout.TabIndex = 0;
            this.ManagementLayout.Paint += new System.Windows.Forms.PaintEventHandler(this.ManagementLayout_Paint);
            // 
            // dgvSummary
            // 
            this.dgvSummary.AllowUserToAddRows = false;
            this.dgvSummary.AllowUserToDeleteRows = false;
            this.dgvSummary.AllowUserToOrderColumns = true;
            this.dgvSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ManagementLayout.SetColumnSpan(this.dgvSummary, 5);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSummary.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSummary.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSummary.Location = new System.Drawing.Point(4, 203);
            this.dgvSummary.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSummary.Name = "dgvSummary";
            this.dgvSummary.ReadOnly = true;
            this.dgvSummary.ShowEditingIcon = false;
            this.dgvSummary.Size = new System.Drawing.Size(1188, 226);
            this.dgvSummary.TabIndex = 0;
            this.dgvSummary.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSummary_RowHeaderMouseDoubleClick);
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AllowUserToDeleteRows = false;
            this.dgvDetail.AllowUserToOrderColumns = true;
            this.dgvDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ManagementLayout.SetColumnSpan(this.dgvDetail, 5);
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetail.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetail.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDetail.Location = new System.Drawing.Point(4, 470);
            this.dgvDetail.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnly = true;
            this.dgvDetail.ShowEditingIcon = false;
            this.dgvDetail.Size = new System.Drawing.Size(1188, 230);
            this.dgvDetail.TabIndex = 1;
            // 
            // clbMachine
            // 
            this.clbMachine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clbMachine.FormattingEnabled = true;
            this.clbMachine.Location = new System.Drawing.Point(839, 37);
            this.clbMachine.Margin = new System.Windows.Forms.Padding(4);
            this.clbMachine.Name = "clbMachine";
            this.ManagementLayout.SetRowSpan(this.clbMachine, 2);
            this.clbMachine.Size = new System.Drawing.Size(353, 58);
            this.clbMachine.TabIndex = 2;
            // 
            // dtpStart
            // 
            this.dtpStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpStart.Location = new System.Drawing.Point(362, 4);
            this.dtpStart.Margin = new System.Windows.Forms.Padding(4);
            this.dtpStart.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(350, 22);
            this.dtpStart.TabIndex = 3;
            this.dtpStart.Value = new System.DateTime(2016, 4, 13, 0, 0, 0, 0);
            // 
            // dtpEnd
            // 
            this.dtpEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpEnd.Location = new System.Drawing.Point(362, 37);
            this.dtpEnd.Margin = new System.Windows.Forms.Padding(4);
            this.dtpEnd.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(350, 22);
            this.dtpEnd.TabIndex = 4;
            this.dtpEnd.Value = new System.DateTime(2016, 4, 13, 0, 0, 0, 0);
            // 
            // lblMachine
            // 
            this.lblMachine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMachine.AutoSize = true;
            this.lblMachine.Location = new System.Drawing.Point(839, 8);
            this.lblMachine.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMachine.Name = "lblMachine";
            this.lblMachine.Size = new System.Drawing.Size(353, 17);
            this.lblMachine.TabIndex = 22;
            this.lblMachine.Text = "Machine Result Filter List";
            // 
            // lblTransactionSummary
            // 
            this.lblTransactionSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTransactionSummary.AutoSize = true;
            this.ManagementLayout.SetColumnSpan(this.lblTransactionSummary, 5);
            this.lblTransactionSummary.Location = new System.Drawing.Point(4, 174);
            this.lblTransactionSummary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTransactionSummary.Name = "lblTransactionSummary";
            this.lblTransactionSummary.Size = new System.Drawing.Size(1188, 17);
            this.lblTransactionSummary.TabIndex = 6;
            this.lblTransactionSummary.Text = "Transaction Summary";
            this.lblTransactionSummary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTransactionDetail
            // 
            this.lblTransactionDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTransactionDetail.AutoSize = true;
            this.ManagementLayout.SetColumnSpan(this.lblTransactionDetail, 5);
            this.lblTransactionDetail.Location = new System.Drawing.Point(4, 441);
            this.lblTransactionDetail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTransactionDetail.Name = "lblTransactionDetail";
            this.lblTransactionDetail.Size = new System.Drawing.Size(1188, 17);
            this.lblTransactionDetail.TabIndex = 7;
            this.lblTransactionDetail.Text = "Transaction Detail";
            this.lblTransactionDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStartDate
            // 
            this.lblStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(4, 8);
            this.lblStartDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(231, 17);
            this.lblStartDate.TabIndex = 23;
            this.lblStartDate.Text = "Start Date";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(4, 41);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(231, 17);
            this.lblEndDate.TabIndex = 24;
            this.lblEndDate.Text = "End Date";
            // 
            // cbErrorOnly
            // 
            this.cbErrorOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbErrorOnly.AutoSize = true;
            this.ManagementLayout.SetColumnSpan(this.cbErrorOnly, 3);
            this.cbErrorOnly.Location = new System.Drawing.Point(4, 72);
            this.cbErrorOnly.Margin = new System.Windows.Forms.Padding(4);
            this.cbErrorOnly.Name = "cbErrorOnly";
            this.cbErrorOnly.Size = new System.Drawing.Size(708, 21);
            this.cbErrorOnly.TabIndex = 25;
            this.cbErrorOnly.Text = "Show Error Only";
            this.cbErrorOnly.UseVisualStyleBackColor = true;
            // 
            // btnRefreshLog
            // 
            this.btnRefreshLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshLog.Location = new System.Drawing.Point(1032, 103);
            this.btnRefreshLog.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshLog.Name = "btnRefreshLog";
            this.btnRefreshLog.Size = new System.Drawing.Size(160, 59);
            this.btnRefreshLog.TabIndex = 5;
            this.btnRefreshLog.Text = "Refresh Log";
            this.btnRefreshLog.UseVisualStyleBackColor = true;
            this.btnRefreshLog.Click += new System.EventHandler(this.btnRefreshLog_Click);
            // 
            // DispenserReaderManagement
            // 
            this.DispenserReaderManagement.Controls.Add(this.tlDispenserReader);
            this.DispenserReaderManagement.Location = new System.Drawing.Point(4, 25);
            this.DispenserReaderManagement.Margin = new System.Windows.Forms.Padding(4);
            this.DispenserReaderManagement.Name = "DispenserReaderManagement";
            this.DispenserReaderManagement.Padding = new System.Windows.Forms.Padding(4);
            this.DispenserReaderManagement.Size = new System.Drawing.Size(1204, 712);
            this.DispenserReaderManagement.TabIndex = 2;
            this.DispenserReaderManagement.Text = "Dispenser and Reader Management";
            this.DispenserReaderManagement.UseVisualStyleBackColor = true;
            // 
            // tlDispenserReader
            // 
            this.tlDispenserReader.ColumnCount = 4;
            this.tlDispenserReader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlDispenserReader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlDispenserReader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlDispenserReader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlDispenserReader.Controls.Add(this.cbReset, 0, 2);
            this.tlDispenserReader.Controls.Add(this.lvMessage, 0, 4);
            this.tlDispenserReader.Controls.Add(this.lblMessage, 0, 3);
            this.tlDispenserReader.Controls.Add(this.btnClearMessage, 3, 3);
            this.tlDispenserReader.Controls.Add(this.lblKioskNameLabel, 0, 0);
            this.tlDispenserReader.Controls.Add(this.lblKioskName, 1, 0);
            this.tlDispenserReader.Controls.Add(this.groupBox1, 2, 0);
            this.tlDispenserReader.Controls.Add(this.groupBox2, 3, 0);
            this.tlDispenserReader.Controls.Add(this.groupBox3, 2, 1);
            this.tlDispenserReader.Controls.Add(this.groupBox4, 3, 1);
            this.tlDispenserReader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlDispenserReader.Location = new System.Drawing.Point(4, 4);
            this.tlDispenserReader.Margin = new System.Windows.Forms.Padding(4);
            this.tlDispenserReader.Name = "tlDispenserReader";
            this.tlDispenserReader.RowCount = 5;
            this.tlDispenserReader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.86443F));
            this.tlDispenserReader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.86443F));
            this.tlDispenserReader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.423087F));
            this.tlDispenserReader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.846177F));
            this.tlDispenserReader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.00188F));
            this.tlDispenserReader.Size = new System.Drawing.Size(1196, 704);
            this.tlDispenserReader.TabIndex = 0;
            // 
            // cbReset
            // 
            this.cbReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbReset.AutoSize = true;
            this.cbReset.Enabled = false;
            this.cbReset.Location = new System.Drawing.Point(4, 324);
            this.cbReset.Margin = new System.Windows.Forms.Padding(4);
            this.cbReset.Name = "cbReset";
            this.cbReset.Size = new System.Drawing.Size(291, 16);
            this.cbReset.TabIndex = 25;
            this.cbReset.Text = "Reset Card";
            this.cbReset.UseVisualStyleBackColor = true;
            // 
            // lvMessage
            // 
            this.tlDispenserReader.SetColumnSpan(this.lvMessage, 4);
            this.lvMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMessage.Location = new System.Drawing.Point(4, 396);
            this.lvMessage.Margin = new System.Windows.Forms.Padding(4);
            this.lvMessage.Name = "lvMessage";
            this.lvMessage.Size = new System.Drawing.Size(1188, 304);
            this.lvMessage.TabIndex = 28;
            this.lvMessage.UseCompatibleStateImageBehavior = false;
            this.lvMessage.View = System.Windows.Forms.View.List;
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.AutoSize = true;
            this.tlDispenserReader.SetColumnSpan(this.lblMessage, 3);
            this.lblMessage.Location = new System.Drawing.Point(4, 359);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(889, 17);
            this.lblMessage.TabIndex = 29;
            this.lblMessage.Text = "Message";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClearMessage
            // 
            this.btnClearMessage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClearMessage.BackColor = System.Drawing.Color.Transparent;
            this.btnClearMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearMessage.Location = new System.Drawing.Point(1032, 348);
            this.btnClearMessage.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearMessage.Name = "btnClearMessage";
            this.btnClearMessage.Size = new System.Drawing.Size(160, 40);
            this.btnClearMessage.TabIndex = 30;
            this.btnClearMessage.Text = "Clear";
            this.btnClearMessage.UseVisualStyleBackColor = false;
            this.btnClearMessage.Click += new System.EventHandler(this.btnClearMessage_Click);
            // 
            // lblKioskNameLabel
            // 
            this.lblKioskNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKioskNameLabel.AutoSize = true;
            this.lblKioskNameLabel.Location = new System.Drawing.Point(102, 71);
            this.lblKioskNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKioskNameLabel.Name = "lblKioskNameLabel";
            this.lblKioskNameLabel.Size = new System.Drawing.Size(95, 17);
            this.lblKioskNameLabel.TabIndex = 32;
            this.lblKioskNameLabel.Text = "Kiosk Name : ";
            // 
            // lblKioskName
            // 
            this.lblKioskName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKioskName.AutoSize = true;
            this.lblKioskName.Location = new System.Drawing.Point(448, 71);
            this.lblKioskName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKioskName.Name = "lblKioskName";
            this.lblKioskName.Size = new System.Drawing.Size(0, 17);
            this.lblKioskName.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFillQty);
            this.groupBox1.Controls.Add(this.btnFill);
            this.groupBox1.Location = new System.Drawing.Point(602, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(291, 152);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fill Card";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 33;
            this.label1.Text = "Quantity :";
            // 
            // txtFillQty
            // 
            this.txtFillQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtFillQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFillQty.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtFillQty.Location = new System.Drawing.Point(121, 34);
            this.txtFillQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtFillQty.Name = "txtFillQty";
            this.txtFillQty.Size = new System.Drawing.Size(125, 30);
            this.txtFillQty.TabIndex = 32;
            this.txtFillQty.Tag = "Quantity";
            this.txtFillQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFillQty.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            this.txtFillQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIsNumberOnly_KeyPress);
            // 
            // btnFill
            // 
            this.btnFill.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnFill.Enabled = false;
            this.btnFill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFill.Location = new System.Drawing.Point(48, 73);
            this.btnFill.Margin = new System.Windows.Forms.Padding(4);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(200, 49);
            this.btnFill.TabIndex = 31;
            this.btnFill.Text = "Fill Card";
            this.btnFill.UseVisualStyleBackColor = true;
            this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtAdjustBinQty);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtAdjustBoxQty);
            this.groupBox2.Controls.Add(this.btnAdjust);
            this.groupBox2.Location = new System.Drawing.Point(901, 4);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(291, 152);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adjust Card";
            // 
            // txtAdjustBinQty
            // 
            this.txtAdjustBinQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAdjustBinQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdjustBinQty.Location = new System.Drawing.Point(150, 54);
            this.txtAdjustBinQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtAdjustBinQty.Name = "txtAdjustBinQty";
            this.txtAdjustBinQty.Size = new System.Drawing.Size(95, 30);
            this.txtAdjustBinQty.TabIndex = 28;
            this.txtAdjustBinQty.Tag = "Card Bin Quantity";
            this.txtAdjustBinQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAdjustBinQty.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            this.txtAdjustBinQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIsNumberOnly_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 27;
            this.label2.Text = "Card Bin Qty :";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "Card Box Qty :";
            // 
            // txtAdjustBoxQty
            // 
            this.txtAdjustBoxQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAdjustBoxQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdjustBoxQty.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtAdjustBoxQty.Location = new System.Drawing.Point(150, 14);
            this.txtAdjustBoxQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtAdjustBoxQty.Name = "txtAdjustBoxQty";
            this.txtAdjustBoxQty.Size = new System.Drawing.Size(95, 30);
            this.txtAdjustBoxQty.TabIndex = 25;
            this.txtAdjustBoxQty.Tag = "Card Box Quantity";
            this.txtAdjustBoxQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAdjustBoxQty.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            this.txtAdjustBoxQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIsNumberOnly_KeyPress);
            // 
            // btnAdjust
            // 
            this.btnAdjust.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdjust.Enabled = false;
            this.btnAdjust.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdjust.Location = new System.Drawing.Point(46, 93);
            this.btnAdjust.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdjust.Name = "btnAdjust";
            this.btnAdjust.Size = new System.Drawing.Size(200, 49);
            this.btnAdjust.TabIndex = 24;
            this.btnAdjust.Text = "Adjust Card";
            this.btnAdjust.UseVisualStyleBackColor = true;
            this.btnAdjust.Click += new System.EventHandler(this.btnAdjust_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtReturnQty);
            this.groupBox3.Controls.Add(this.btnReturn);
            this.groupBox3.Location = new System.Drawing.Point(602, 164);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(291, 152);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Return Card";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 44);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Quantity :";
            // 
            // txtReturnQty
            // 
            this.txtReturnQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtReturnQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReturnQty.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtReturnQty.Location = new System.Drawing.Point(121, 34);
            this.txtReturnQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtReturnQty.Name = "txtReturnQty";
            this.txtReturnQty.Size = new System.Drawing.Size(125, 30);
            this.txtReturnQty.TabIndex = 34;
            this.txtReturnQty.Tag = "Quantity";
            this.txtReturnQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtReturnQty.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            this.txtReturnQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIsNumberOnly_KeyPress);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReturn.Enabled = false;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(48, 73);
            this.btnReturn.Margin = new System.Windows.Forms.Padding(4);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(200, 49);
            this.btnReturn.TabIndex = 23;
            this.btnReturn.Text = "Return Card";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtDispenseQty);
            this.groupBox4.Controls.Add(this.btnDispense);
            this.groupBox4.Location = new System.Drawing.Point(901, 164);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(291, 152);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dispense Card";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 44);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 35;
            this.label5.Text = "Quantity :";
            // 
            // txtDispenseQty
            // 
            this.txtDispenseQty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDispenseQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDispenseQty.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtDispenseQty.Location = new System.Drawing.Point(119, 34);
            this.txtDispenseQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtDispenseQty.Name = "txtDispenseQty";
            this.txtDispenseQty.Size = new System.Drawing.Size(125, 30);
            this.txtDispenseQty.TabIndex = 34;
            this.txtDispenseQty.Tag = "Quantity";
            this.txtDispenseQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDispenseQty.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            this.txtDispenseQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIsNumberOnly_KeyPress);
            // 
            // btnDispense
            // 
            this.btnDispense.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDispense.Enabled = false;
            this.btnDispense.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDispense.Location = new System.Drawing.Point(46, 73);
            this.btnDispense.Margin = new System.Windows.Forms.Padding(4);
            this.btnDispense.Name = "btnDispense";
            this.btnDispense.Size = new System.Drawing.Size(200, 49);
            this.btnDispense.TabIndex = 22;
            this.btnDispense.Text = "Dispense Card";
            this.btnDispense.UseVisualStyleBackColor = true;
            this.btnDispense.Click += new System.EventHandler(this.btnDispense_Click);
            // 
            // CardManagement
            // 
            this.ClientSize = new System.Drawing.Size(1212, 741);
            this.Controls.Add(this.tabManagement);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CardManagement";
            this.Text = "Card Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CardManagement_FormClosing);
            this.Load += new System.EventHandler(this.CardManagement_Load);
            this.tabManagement.ResumeLayout(false);
            this.TransactionManagement.ResumeLayout(false);
            this.ManagementLayout.ResumeLayout(false);
            this.ManagementLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.DispenserReaderManagement.ResumeLayout(false);
            this.tlDispenserReader.ResumeLayout(false);
            this.tlDispenserReader.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

    }

    private void ManagementLayout_Paint(object sender, PaintEventArgs e)
    {

    }
  }
}
