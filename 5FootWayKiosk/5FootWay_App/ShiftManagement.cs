﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.ShiftManagement
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using KioskBusinessLayer;
using System;
using System.ComponentModel;
using System.Drawing;
using System.ServiceProcess;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class ShiftManagement : Form
  {
    private SettingBL sBL;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btnReturn;
    private Button btnStart;
    private Label label1;
    private TextBox tbShiftId;

    public ShiftManagement()
    {
      this.InitializeComponent();
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      try
      {
        if (this.sBL == null)
          throw new Exception("Cannot access database.");
        if (!this.sBL.Update(new KioskBusinessEntity.Database_Entity.Setting()
        {
          code = "ForceStartShift",
          value = "true"
        }))
        {
          this.LogException(new Exception("failed to update setting, ForceStartShift."));
        }
        else
        {
          ServiceController serviceController = new ServiceController("_5FootWaySchedulingWindowService");
          serviceController.Stop();
          serviceController.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 10));
          serviceController.Start();
          serviceController.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 30));
          this.SetShiftId();
          int num = (int) MessageBox.Show("Started a new shift.");
        }
      }
      catch (Exception ex)
      {
        this.ExceptionHandling(ex, false);
      }
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void ShiftManagement_Load(object sender, EventArgs e)
    {
      this.tbShiftId.Text = "-";
      this.btnStart.Enabled = false;
      try
      {
        this.sBL = new SettingBL();
        DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
        this.SetShiftId();
      }
      catch (Exception ex)
      {
        this.ExceptionHandling(ex, false);
        this.DialogResult = DialogResult.OK;
        this.Close();
      }
    }

    private void SetShiftId()
    {
      try
      {
        if (this.sBL == null)
          throw new Exception("Cannot access database.");
        this.tbShiftId.Text = Convert.ToInt32(this.sBL.Get(new KioskBusinessEntity.Database_Entity.Setting()
        {
          code = "ShiftId"
        }).value).ToString();
      }
      catch (Exception ex)
      {
        this.ExceptionHandling(ex, false);
      }
    }

    private void ExceptionHandling(Exception ex, bool showDetail)
    {
      int num = (int) MessageBox.Show(ex.Message);
      this.LogException(ex);
    }

    private void LogException(Exception ex)
    {
      new MyLog().LogException(ex);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.btnReturn = new Button();
      this.btnStart = new Button();
      this.label1 = new Label();
      this.tbShiftId = new TextBox();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel1.Controls.Add((Control) this.btnReturn, 1, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.btnStart, 0, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add((Control) this.tbShiftId, 1, 0);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
      this.tableLayoutPanel1.Size = new Size(584, 441);
      this.tableLayoutPanel1.TabIndex = 0;
      this.btnReturn.Anchor = AnchorStyles.None;
      this.btnReturn.Font = new Font("Microsoft Sans Serif", 14f, FontStyle.Regular, GraphicsUnit.Point, (byte) 134);
      this.btnReturn.Location = new Point(338, 280);
      this.btnReturn.Name = "btnReturn";
      this.btnReturn.Size = new Size(200, 100);
      this.btnReturn.TabIndex = 0;
      this.btnReturn.Text = "Return";
      this.btnReturn.UseVisualStyleBackColor = true;
      this.btnReturn.Click += new EventHandler(this.btnReturn_Click);
      this.btnStart.Anchor = AnchorStyles.None;
      this.btnStart.Font = new Font("Microsoft Sans Serif", 14f, FontStyle.Regular, GraphicsUnit.Point, (byte) 134);
      this.btnStart.Location = new Point(46, 280);
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new Size(200, 100);
      this.btnStart.TabIndex = 1;
      this.btnStart.Text = "Start New Shift";
      this.btnStart.UseVisualStyleBackColor = true;
      this.btnStart.Click += new EventHandler(this.btnStart_Click);
      this.label1.Anchor = AnchorStyles.None;
      this.label1.AutoSize = true;
      this.label1.Font = new Font("Microsoft Sans Serif", 14f, FontStyle.Regular, GraphicsUnit.Point, (byte) 134);
      this.label1.Location = new Point(107, 98);
      this.label1.Name = "label1";
      this.label1.Size = new Size(77, 24);
      this.label1.TabIndex = 2;
      this.label1.Text = "Shift ID :";
      this.tbShiftId.Anchor = AnchorStyles.None;
      this.tbShiftId.Font = new Font("Microsoft Sans Serif", 14f, FontStyle.Regular, GraphicsUnit.Point, (byte) 134);
      this.tbShiftId.Location = new Point(388, 95);
      this.tbShiftId.Name = "tbShiftId";
      this.tbShiftId.ReadOnly = true;
      this.tbShiftId.Size = new Size(100, 29);
      this.tbShiftId.TabIndex = 3;
 //     this.AutoScaleDimensions = new SizeF(6f, 13f);
//      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(584, 441);
      this.Controls.Add((Control) this.tableLayoutPanel1);
  //    this.Name = nameof (ShiftManagement);
      this.StartPosition = FormStartPosition.CenterScreen;
  //    this.Text = nameof (ShiftManagement);
      this.Load += new EventHandler(this.ShiftManagement_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
