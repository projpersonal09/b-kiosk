﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.UserManagement.UserManagement
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using KioskBusinessLayer;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace _5FootWay_App.UserManagement
{
  public class UserManagement : Form
  {
    private UserBL uBL;
    private KioskBusinessEntity.Database_Entity.User user;
    private string kioskUserName;
    private IContainer components;
    private Button btnSave;
    private Button btnReturn;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private TextBox txtUserName;
    private TextBox txtName;
    private TextBox txtOldPassword;
    private TextBox txtNewPassword;
    private TextBox txtConfNewPassword;

    public UserManagement()
    {
      this.InitializeComponent();
    }

    public UserManagement(string _kioskUserName)
    {
      this.kioskUserName = _kioskUserName;
      this.InitializeComponent();
      this.BindUserData();
      this.BindInputFormValue();
    }

    private void BindUserData()
    {
      DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
      this.uBL = new UserBL(ConstantVariable_App.connStr);
      this.user = new KioskBusinessEntity.Database_Entity.User();
      this.user.username = this.kioskUserName;
      this.user = this.uBL.Get(this.user);
    }

    private void BindInputFormValue()
    {
      if (this.user != null)
      {
        this.txtUserName.Text = this.user.username;
        this.txtName.Text = this.user.name;
      }
      this.txtOldPassword.Select();
    }

    private void UserManagement_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.DialogResult = DialogResult.OK;
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      try
      {
        if (!this.FormValidation())
          return;
        if (this.uBL.Update(new KioskBusinessEntity.Database_Entity.User()
        {
          username = this.txtUserName.Text,
          name = this.txtName.Text,
          password = new PasswordHasher().HashPassword(this.txtNewPassword.Text)
        }))
        {
          int num = (int) MessageBox.Show("Save Successfully");
          this.DialogResult = DialogResult.OK;
          this.Close();
        }
        else
        {
          int num1 = (int) MessageBox.Show("Save Failed");
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
      }
    }

    private bool FormValidation()
    {
      bool flag = true;
      if (this.txtUserName.Text == "")
      {
        int num = (int) MessageBox.Show("UserName empty");
        return false;
      }
      if (this.txtOldPassword.Text == "")
      {
        int num = (int) MessageBox.Show("Please input old password");
        return false;
      }
      if (this.txtNewPassword.Text == "")
      {
        int num = (int) MessageBox.Show("Please input new password");
        return false;
      }
      if (this.txtConfNewPassword.Text == "")
      {
        int num = (int) MessageBox.Show("Please input confirm new password");
        return false;
      }
      if (!this.OldPasswordValidation())
      {
        int num = (int) MessageBox.Show("Old password not match");
        return false;
      }
      if (this.txtNewPassword.Text.Equals(this.txtConfNewPassword.Text))
        return flag;
      int num1 = (int) MessageBox.Show("New password and confirm password not match");
      return false;
    }

    private bool OldPasswordValidation()
    {
      bool flag = false;
      IPasswordHasher passwordHasher = (IPasswordHasher) new PasswordHasher();
      PasswordVerificationResult verificationResult = passwordHasher.VerifyHashedPassword(this.user.password, this.txtOldPassword.Text);
      passwordHasher.HashPassword(this.txtOldPassword.Text);
      if (verificationResult.Equals((object) PasswordVerificationResult.Success))
        flag = true;
      return flag;
    }

    private void txtShowKeyboard_Click(object sender, EventArgs e)
    {
      WindowHelper.showKeyboard();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtOldPassword = new System.Windows.Forms.TextBox();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.txtConfNewPassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(140, 266);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(160, 70);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(310, 266);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(160, 70);
            this.btnReturn.TabIndex = 2;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(158, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "UserName :";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(197, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name :";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(131, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Old Password :";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(122, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "New Password :";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(52, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 24);
            this.label5.TabIndex = 7;
            this.label5.Text = "Confirm New Password :";
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtUserName.Enabled = false;
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(292, 28);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(250, 29);
            this.txtUserName.TabIndex = 8;
            // 
            // txtName
            // 
            this.txtName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtName.Enabled = false;
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(292, 68);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(250, 29);
            this.txtName.TabIndex = 9;
            // 
            // txtOldPassword
            // 
            this.txtOldPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtOldPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOldPassword.Location = new System.Drawing.Point(292, 108);
            this.txtOldPassword.Name = "txtOldPassword";
            this.txtOldPassword.PasswordChar = '*';
            this.txtOldPassword.Size = new System.Drawing.Size(250, 29);
            this.txtOldPassword.TabIndex = 10;
            this.txtOldPassword.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtNewPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewPassword.Location = new System.Drawing.Point(292, 148);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(250, 29);
            this.txtNewPassword.TabIndex = 11;
            this.txtNewPassword.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            // 
            // txtConfNewPassword
            // 
            this.txtConfNewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtConfNewPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfNewPassword.Location = new System.Drawing.Point(292, 188);
            this.txtConfNewPassword.Name = "txtConfNewPassword";
            this.txtConfNewPassword.PasswordChar = '*';
            this.txtConfNewPassword.Size = new System.Drawing.Size(250, 29);
            this.txtConfNewPassword.TabIndex = 12;
            this.txtConfNewPassword.Click += new System.EventHandler(this.txtShowKeyboard_Click);
            // 
            // UserManagement
            // 
            this.ClientSize = new System.Drawing.Size(615, 384);
            this.Controls.Add(this.txtConfNewPassword);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.txtOldPassword);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnSave);
            this.Name = "UserManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserManagement_FormClosing);
            this.Load += new System.EventHandler(this.UserManagement_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    private void UserManagement_Load(object sender, EventArgs e)
    {

    }
  }
}
