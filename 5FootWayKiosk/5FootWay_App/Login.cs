﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.Login
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using Helper;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessLayer;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WebService;

namespace _5FootWay_App
{
  public class Login : Form
  {
    private SettingBL sBL;
    private UserBL uBL;
    private Action confirmEvent;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btn3;
    private Button btn2;
    private Button btn8;
    private Button btn9;
    private Button btn5;
    private Button btn6;
    private Button btnConfirm;
    private Button btnClear;
    private Button btn0;
    private Button btn7;
    private Button btn4;
    private Button btn1;
    private Button btnCancel;
    private TextBox tbPassword;

    public bool isReplenish { get; set; }

    public int numOfReplenish { get; set; }

    private string kioskUserName { get; set; }

    private string propertyCode { get; set; }

    public Login()
    {
      this.confirmEvent = new Action(this.LoginEvent);
      this.InitializeComponent();
    }

    public void SetConfirmEvent(Action confirmEvent)
    {
      this.confirmEvent = confirmEvent;
    }

    private void btnConfirm_Click(object sender, EventArgs e)
    {
      try
      {
        this.LoginValidation();
        if (this.confirmEvent == null)
          return;
        this.confirmEvent();
      }
      catch (Exception ex)
      {
        this.LogInfo(ex.Message);
      }
    }

    private string LoginValidation()
    {
      try
      {
        //  String connStr = "Data Source=DQPHIAPS0362\\SQLEXPRESS;Initial Catalog=ServiceKiosk_5FW;Integrated Security=True;MultipleActiveResultSets=True";
       DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
      // DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
       this.uBL = new UserBL(ConstantVariable_App.connStr);
        KioskBusinessEntity.Database_Entity.User user = this.uBL.Get(new KioskBusinessEntity.Database_Entity.User()
        {
          username = this.kioskUserName
        });
        if (user == null)
          throw new Exception("This user is not available");
        IPasswordHasher passwordHasher = (IPasswordHasher) new PasswordHasher();
        PasswordVerificationResult verificationResult = passwordHasher.VerifyHashedPassword(user.password, this.tbPassword.Text);
        passwordHasher.HashPassword(this.tbPassword.Text);
        if (verificationResult.Equals((object) PasswordVerificationResult.Success))
          return user.name;
        if (verificationResult.Equals((object) PasswordVerificationResult.SuccessRehashNeeded))
          throw new Exception("Rehash required");
        throw new Exception("Username or password is inccorrect");
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void LoginEvent()
    {
      KioskData kioskData1 = new KioskData();
      try
      {
        KioskData kioskData2 = WebService.GetService.InitializeKioskData(this.kioskUserName, this.propertyCode);
        this.Hide();
        if (new ManagementMenu(kioskData2).ShowDialog() != DialogResult.OK)
          return;
        this.DialogResult = DialogResult.OK;
        this.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
        this.Close();
      }
    }

    private void btn1_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn1.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn2_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn2.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn3_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn3.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn4_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn4.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn5_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn5.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn6_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn6.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn7_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn7.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn8_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn8.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btn9_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text += this.btn9.Text;
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    private void btnClear_Click(object sender, EventArgs e)
    {
      this.tbPassword.Text = "";
    }

    private void Keypad_Load(object sender, EventArgs e)
    {
      try
      {
       DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
        // string connStr;
       //  connStr = "Data Source=DQPHIAPS0362\\SQLEXPRESS;Initial Catalog=ServiceKiosk_5FW;Integrated Security=True;MultipleActiveResultSets=True";
       DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
       this.sBL = new SettingBL(ConstantVariable_App.connStr);
        KioskBusinessEntity.Database_Entity.Setting s = new KioskBusinessEntity.Database_Entity.Setting();
        s.code = "KioskUser";
        this.kioskUserName = this.sBL.Get(s).value;
        s.code = "PropertyCode";
        this.propertyCode = this.sBL.Get(s).value;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message);
        this.Close();
      }
    }

    private void txtValue_TextChanged(object sender, EventArgs e)
    {
    }

    private void btn0_Click(object sender, EventArgs e)
    {
      this.tbPassword.Focus();
      int selectionStart = this.tbPassword.SelectionStart;
      this.tbPassword.Text = this.tbPassword.Text.Insert(selectionStart, this.btn0.Text);
      this.tbPassword.SelectionStart = selectionStart + 1;
    }

    private void LogInfo(string message)
    {
      int num = (int) MessageBox.Show(message);
      new MyLog().Log(MyLog.LogType.Info, message);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.btn3, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.btn2, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbPassword, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn8, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn9, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.btn4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn5, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn6, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.btn1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnClear, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnConfirm, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.btn0, 2, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.18182F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.10101F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34344F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.34344F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(330, 307);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btn3
            // 
            this.btn3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn3.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(233, 177);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(94, 38);
            this.btn3.TabIndex = 3;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn2
            // 
            this.btn2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn2.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(118, 177);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(93, 38);
            this.btn2.TabIndex = 3;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // tbPassword
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tbPassword, 5);
            this.tbPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPassword.Font = new System.Drawing.Font("Microsoft YaHei", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPassword.Location = new System.Drawing.Point(3, 3);
            this.tbPassword.MaxLength = 20;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(324, 51);
            this.tbPassword.TabIndex = 0;
            this.tbPassword.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // btn7
            // 
            this.btn7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn7.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(3, 89);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(93, 38);
            this.btn7.TabIndex = 1;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn8.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(118, 89);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(93, 38);
            this.btn8.TabIndex = 2;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn9.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(233, 89);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(94, 38);
            this.btn9.TabIndex = 3;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn4
            // 
            this.btn4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn4.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(3, 133);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(93, 38);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn5.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(118, 133);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(93, 38);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn6.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(233, 133);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(94, 38);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn1
            // 
            this.btn1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(3, 177);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(93, 38);
            this.btn1.TabIndex = 7;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(3, 265);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 39);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(118, 265);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(93, 39);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.Location = new System.Drawing.Point(233, 265);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(94, 39);
            this.btnConfirm.TabIndex = 8;
            this.btnConfirm.Text = "OK";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(118, 221);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(93, 38);
            this.btn0.TabIndex = 11;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // Login
            // 
            this.AcceptButton = this.btnConfirm;
            this.ClientSize = new System.Drawing.Size(330, 307);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Keypad";
            this.Load += new System.EventHandler(this.Keypad_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

    }
  }
}
