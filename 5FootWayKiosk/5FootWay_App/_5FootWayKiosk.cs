﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App._5FootWayKiosk
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe


using CardTransactionBusinessLayer;
using Helper;
using KioskBusinessEntity;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using KioskBusinessLayer;
using MQuest.FaceRecognition.Betaface_Company;
using MQuest.FaceRecognition.Betaface_Company.Web_Service;
using MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532;
using MQuest.HardwareInterface.CardDispenser;
using MQuest.HardwareInterface.CardDispenser.Creator.CRT_591;
using MQuest.HardwareInterface.CardReader;
using MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay;
using MQuest.HardwareInterface.CashDispenser;
using MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using MQuest.HardwareInterface.DocumentReader;
using MQuest.HardwareInterface.DocumentReader._3M.KR9000;
using MQuest.Printing;
using MQuest.Printing.Fujitsu;
using Newtonsoft.Json;
using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using WebService;

namespace _5FootWay_App
{
    [ComVisible(true)]
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class _5FootWayKiosk : Form
    {
        private static bool CHECK_MACHINE_ON_START = true;
        private static bool REFRESH_ON_START = true;
        private static readonly object lockAction = new object();
        private static readonly object lockState = new object();
        private int currentRoomGuestSelected = -1;
        private int creditCardRunningNumber = 1;
        private int securityDepositRunningNumber = 1;
        private bool navigation = true;
#if !DEBUG
         private const string host = "http://localhost:8800/";
#endif
#if DEBUG
        private const string host = "http://localhost:5140/";
#endif
        private KioskData kioskData;
        private string terminalCode;
        private string kioskLocation;
        private KioskCheckInData kioskGuestData;
        private BookingData booking;
        private GuestDetail mainGuest;
        private GuestDetail currGuestDetail;
        private FaceData owner;
        private List<RoomStayData> roomStayList;
        private List<BookingChargeData> chargeList;
        private List<SecurityDepositData> depositList;
        private KioskBusinessEntity.Database_Entity.BookingPayment bookingPayment;
        private MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail transactionDetail;
        private Decimal securityDepositAmount;
        private string currentCardUID;
        private bool skipFaceRecognition;
        private int currentRoomSelected;
        private int retryNumber;
        private CreditCardReaderManager creditCardReaderManager;
        private ICashDispenser cashDispenserManager;
        private IDocumentReader documentReaderManager;
        private ICardDispenser cardDispenserManager;
        private ICardReaderManager cardReaderManager;
        private IPrinter printerManager;
        private BarcodeScannerManager barcodeManager;
        private ConstantVariable_CashNote cashNoteManager;
        private CashNoteTransactionBL cntBL;
        private BookingPaymentBL bpBL;
        private SecurityDepositBL sdBL;
        private GuestBL gBL;
        private RoomGuestCardBL rgcBL;
        private BookingBL bBL;
        private SettingBL sBL;
        private CashInOutBL cashInOutBL;
        private CardTransactionSummaryBL cardBL;
        private SurveyResponseBL surveyResponseBL;
        private string page;
        private string sessionId;
        private string culture;
        private bool invalidCard;
        private bool isCancelled;
        private bool isOffline;
        private bool isOfflineEnabled;
        private bool machineStatus;
        private bool stopCollectDeposit;
        private bool _doCollectSecurityDeposit;
        private bool _doReturnSecurityDepositChange;
        private bool _doReturnSecurityDeposit;
        private bool _doSaveSignature;
        private bool _doGenerateReceipt;
        private bool _doVerifyGuest;
        private bool _doCheckInGuest;
        private IContainer components;
        private WebBrowser browser;
        private System.Windows.Forms.Timer tmrHardwareCheck;

        private bool DoCollectSecurityDeposit
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doCollectSecurityDeposit;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doCollectSecurityDeposit = value;
            }
        }

        private bool DoReturnSecurityDepositChange
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doReturnSecurityDepositChange;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doReturnSecurityDepositChange = value;
            }
        }

        private bool DoReturnSecurityDeposit
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doReturnSecurityDeposit;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doReturnSecurityDeposit = value;
            }
        }

        private bool DoSaveSignature
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doSaveSignature;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doSaveSignature = value;
            }
        }

        private bool DoGenerateReceipt
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doGenerateReceipt;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doGenerateReceipt = value;
            }
        }

        private bool DoVerifyGuest
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doVerifyGuest;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doVerifyGuest = value;
            }
        }

        private bool DoCheckInGuest
        {
            get
            {
                lock (_5FootWayKiosk.lockState)
                    return this._doCheckInGuest;
            }
            set
            {
                lock (_5FootWayKiosk.lockState)
                    this._doCheckInGuest = value;
            }
        }

        public _5FootWayKiosk()
        {
            this.InitializeComponent();
            new RegistryHelper().SetupRegistry(Process.GetCurrentProcess().ProcessName);
            this.terminalCode = ConstantVariable_App.terminalCode;
            this.kioskLocation = ConstantVariable_App.kioskLocation;
        }

        private void _5FootWayKiosk_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            this.MaximizeBox = false;

            this.browser.ScrollBarsEnabled = false;
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;




            this.machineStatus = false;
            this.browser.Url = new Uri(host);
            this.browser.ObjectForScripting = (object)this;

        }

        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                browser.Document.Body.Style = "overflow:hidden";
                if (_5FootWayKiosk.REFRESH_ON_START)
                {
                    _5FootWayKiosk.REFRESH_ON_START = false;
                    this.browser.Refresh(WebBrowserRefreshOption.Completely);
                    this.browser.Url = new Uri(host);
                }
                else
                {
                    this.disableHardwareCheck();
                    this.disableSelection();
                    this.AllowNavigation(false);
                    this.onBrowserDocumentCompleted();
                    this.page = string.Empty;
                    if (this.browser.Url.AbsolutePath.Length > 34)
                        this.page = this.browser.Url.AbsolutePath.Substring(34);
                    if (this.page.Equals("CheckIn/CheckIn"))
                    {
                        this.resetApplicationState();
                        this.clearBookingData();
                        this.browser.Document.GetElementById("btnCheckIn").Click += new HtmlElementEventHandler(this.btnCheckInClick);
                        this.StartBarcodeScanner();
                    }
                    if (this.page.Equals("CheckIn/ScanPassport"))
                    {
                        this.documentReaderManager.SetAvailableDocument(new List<DocumentType>()
            {
              DocumentType.PASSPORT
            });
                        this.documentReaderManager.SetScanState(true);
                    }
                    else if (this.page.Equals("CheckIn/ScanChop"))
                    {
                        this.documentReaderManager.SetAvailableDocument(new List<DocumentType>()
            {
              DocumentType.IMMIGRATION_CHOP
            });
                        this.documentReaderManager.SetScanState(true);
                    }
                    else if (this.page.Equals("CheckIn/ScanVisa"))
                    {
                        this.documentReaderManager.SetAvailableDocument(new List<DocumentType>()
            {
              DocumentType.VISA
            });
                        this.documentReaderManager.SetScanState(true);
                    }
                    else if (this.page.Equals("CheckIn/PersonalParticular"))
                    {
                        HtmlElement elementById = this.browser.Document.GetElementById("btnNext");
                        elementById.Click -= new HtmlElementEventHandler(this.btnNavigationClick);
                        elementById.Click += new HtmlElementEventHandler(this.btnPersonalParticularClick);
                    }
                    else if (this.page.Equals("CheckIn/Payment"))
                    {
                        this.retryNumber = 0;
                        this.StartCreditCardReader(true);
                    }
                    else if (this.page.Equals("CheckIn/SecurityDeposit"))
                    {
                        this.isCancelled = false;
                        this.securityDepositAmount = new Decimal(0);
                        this.StartCashDispenser(true);
                    }
                    else if (this.page.Equals("CheckIn/CollectCard"))
                    {
                        foreach (HtmlElement htmlElement in this.browser.Document.All.GetElementsByName("dispense"))
                            htmlElement.Click += new HtmlElementEventHandler(this.btnDispenseCardClick);
                        this.CheckDispensedCard();
                    }
                    else if (this.page.Equals("CheckIn/Signature"))
                    {
                        this.browser.ScrollBarsEnabled = false;
                        this.browser.Document.GetElementById("btnNext").Click += new HtmlElementEventHandler(this.btnSignatureClick);
                    }
                    else if (this.page.Equals("CheckOut/CheckOut"))
                    {
                        this.resetApplicationState();
                        this.clearBookingData();
                        this.UpdateCheckOutInsertCardStatus(1);
                        this.browser.Document.GetElementById("btnScanCard").Click += new HtmlElementEventHandler(this.btnScanCardClick);
                        HtmlElement elementById1 = this.browser.Document.GetElementById("btnCheckOut");
                        HtmlElement elementById2 = this.browser.Document.GetElementById("btnBack");
                        elementById1.Click += new HtmlElementEventHandler(this.btnCheckOutClick);
                        elementById2.Click += new HtmlElementEventHandler(this.btnCheckOutBackClick);
                    }
                    else if (this.page.Equals("CheckOut/ReturnCard"))
                    {
                        this.isCancelled = false;
                        this.retryNumber = 0;
                        this.browser.Document.GetElementById("btnNext").Click += new HtmlElementEventHandler(this.btnReturnCardClick);
                        this.StartReturnCard(this.kioskGuestData.roomList.Count, true);
                    }
                    else if (this.page.Equals("CheckOut/ReturnDeposit"))
                    {
                        this.securityDepositAmount = new Decimal(0);
                        this.StartReturnSecurityDeposit();
                    }
                    else if (this.page.Equals("CheckIn/CapturePhoto"))
                    {
                        this.startLoading();
                        this.retryNumber = 0;
                        this.UpdateRetryAttempts(this.retryNumber, ConstantVariable_App.maximumRecognitionRetryAttempts);
                        new Thread((ThreadStart)(() => this.StartFaceDetection(this.currGuestDetail)))
                        {
                            IsBackground = true
                        }.Start();
                    }
                    else if (this.page.Equals("CheckIn/RoomBedList"))
                    {
                        for (int index = 0; index < this.roomStayList.Count; ++index)
                        {
                            HtmlElement elementById = this.browser.Document.GetElementById("btnMoreGuest-" + (object)index);
                            if (elementById != (HtmlElement)null)
                                elementById.Click += new HtmlElementEventHandler(this.btnMoreGuestClick);
                        }
                    }
                    else if (this.page.Equals("CheckIn/VerifyDetail"))
                    {
                        HtmlElement elementById = this.browser.Document.GetElementById("btnNext");
                        elementById.Click -= new HtmlElementEventHandler(this.btnNavigationClick);
                        elementById.Click += new HtmlElementEventHandler(this.btnVerifyDetailClick);
                    }
                    else if (this.page.Equals("") || this.page.Equals("Home/Index"))
                    {
                        HtmlElement elementById = this.browser.Document.GetElementById("ExitBtn");
                        if (elementById != (HtmlElement)null)
                            elementById.Click += new HtmlElementEventHandler(this.exitClick);
                        if (_5FootWayKiosk.CHECK_MACHINE_ON_START)
                        {
                            _5FootWayKiosk.CHECK_MACHINE_ON_START = false;
                            this.LoadMachineHardware(true);
                        }
                        else if (this.machineStatus)
                        {
                            this.ValidateMachineDevice();
                        }
                        else
                        {
                            this.updateCheckMachineStep(-1);
                            this.enableHardwareCheck();
                        }
                        this.isOffline = false;
                    }
                    else if (this.page.Equals("CheckIn/CheckInComplete"))
                    {
                        HtmlElement elementById = this.browser.Document.GetElementById("btnNext");
                        elementById.Click -= new HtmlElementEventHandler(this.btnNavigationClick);
                        elementById.Click += new HtmlElementEventHandler(this.btnCheckInCompleteClick);
                    }
                    else if (this.page.Equals("CheckOut/CheckOutComplete"))
                    {
                        HtmlElement elementById = this.browser.Document.GetElementById("btnNext");
                        elementById.Click -= new HtmlElementEventHandler(this.btnNavigationClick);
                        elementById.Click += new HtmlElementEventHandler(this.btnCheckOutCompleteClick);
                    }
                    else
                    {
                        if (this.page.Equals("CheckIn/CheckInIncomplete") || this.page.Equals("CheckOut/CheckOutIncomplete") || !this.page.Equals("CheckIn/RateExperience"))
                            return;
                        this.browser.Document.InvokeScript("regRateOptEvents", new object[1]
                        {
              (object) true
                        });
                        foreach (HtmlElement htmlElement in this.browser.Document.GetElementsByTagName("a"))
                        {
                            if (!string.IsNullOrWhiteSpace(htmlElement.Name) && htmlElement.Name.Contains("link_rate_opt"))
                                htmlElement.Click += new HtmlElementEventHandler(this.anchorRateExperienceClick);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            if (this.navigation)
                return;
            e.Cancel = true;
        }

        private void _5FootWayKiosk_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.ReleaseMachineDevice();
            Thread.Sleep(500);
        }

        private void resetApplicationState()
        {
            this.DoCollectSecurityDeposit = false;
            this.DoReturnSecurityDepositChange = false;
            this.DoReturnSecurityDeposit = false;
            this.DoSaveSignature = false;
            this.DoGenerateReceipt = false;
            this.DoVerifyGuest = false;
            this.DoCheckInGuest = false;
        }

        private void btnCheckInClick(object sender, HtmlElementEventArgs e)
        {
            this.startLoading();
            this.booking = (BookingData)null;
            this.mainGuest = (GuestDetail)null;
            HtmlElement elementById1 = this.browser.Document.GetElementById("bookingNumber");
            HtmlElement elementById2 = this.browser.Document.GetElementById("guestName");
            string bookingNumber = elementById1.GetAttribute("Value");
            string guestName = elementById2.GetAttribute("Value");
            new Thread((ThreadStart)(() => this.getBookingData(bookingNumber, guestName)))
            {
                IsBackground = true
            }.Start();
        }

        private void btnNavigationClick(object sender, HtmlElementEventArgs e)
        {
            this.AllowNavigation(true);
        }

        private void btnScanCardClick(object sender, HtmlElementEventArgs e)
        {
            this.browser.Document.InvokeScript("ShowCheckOutTips");
            this.StartCheckOutInsertCard();
        }

        private void btnDispenseCardClick(object sender, HtmlElementEventArgs e)
        {
            foreach (HtmlElement htmlElement in this.browser.Document.All.GetElementsByName("dispense"))
                htmlElement.Enabled = false;
            this.DispenseCard((sender as HtmlElement).GetAttribute("value"));
        }

        private void btnCheckOutClick(object sender, HtmlElementEventArgs e)
        {
            this.cardDispenserManager.stopThreadLoop = true;
            this.setKioskCheckOutData();
        }

        private void btnCheckOutBackClick(object sender, HtmlElementEventArgs e)
        {
            if (this.cardDispenserManager == null)
                return;
            this.cardDispenserManager.stopThreadLoop = true;
        }

        private void btnScanPassportClick(object sender, HtmlElementEventArgs e)
        {
            if (this.documentReaderManager.CheckDocumentDocked())
            {
                this.SetPassportDetail((Passport)null);
                this.documentReaderManager.startScanDocumentThread((Control)this, new Action<bool>(this.ScanPassportFailed), new Action<Exception, bool>(this.ScanPassportException));
            }
            else
                this.DocumentNoDocked();
        }

        private void btnScanPassportNextClick(object sender, HtmlElementEventArgs e)
        {
        }

        private void btnScanVisaClick(object sender, HtmlElementEventArgs e)
        {
            if (this.documentReaderManager.CheckDocumentDocked())
            {
                this.SetVisaDetail((Visa)null);
                this.startLoading();
                this.documentReaderManager.startScanDocumentThread((Control)this, new Action<bool>(this.ScanFailed), new Action<Exception, bool>(this.exceptionHandling));
            }
            else
                this.DocumentNoDocked();
        }

        private void btnScanChopClick(object sender, HtmlElementEventArgs e)
        {
            if (this.documentReaderManager.CheckDocumentDocked())
            {
                this.SetChopDetail((ImmigrationChop)null);
                this.startLoading();
                this.documentReaderManager.startScanDocumentThread((Control)this, new Action<bool>(this.ScanFailed), new Action<Exception, bool>(this.exceptionHandling));
            }
            else
                this.DocumentNoDocked();
        }

        private void btnPersonalParticularClick(object sender, HtmlElementEventArgs e)
        {
            try
            {
                _5FootWayKiosk.OtherParticularData otherParticularData = JsonConvert.DeserializeObject<_5FootWayKiosk.OtherParticularData>(Convert.ToString(this.browser.Document.InvokeScript("getOtherParticularData")));
                if (otherParticularData != null)
                {
                    this.currGuestDetail.email = otherParticularData.email;
                    this.currGuestDetail.phone = otherParticularData.contactNumber;
                    this.currGuestDetail.address = otherParticularData.address1 + (string.IsNullOrEmpty(otherParticularData.address2) ? "" : " " + otherParticularData.address2);
                    this.currGuestDetail.postalCode = otherParticularData.postalCode;
                    this.currGuestDetail.company = otherParticularData.company;
                    this.currGuestDetail.occupation = otherParticularData.occupation;
                    this.currGuestDetail.prevDestination = otherParticularData.prevDestination;
                    this.currGuestDetail.nextDestination = otherParticularData.nextDestination;
                }
                this.currGuestDetail.guestCategoryCode = "FIT";
                this.AllowNavigation(true);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void btnVerifyDetailClick(object sender, HtmlElementEventArgs e)
        {
            try
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoVerifyGuest)
                        return;
                    this.DoVerifyGuest = true;
                }
                if (this.currentRoomGuestSelected >= 0)
                    this.currGuestDetail.guestId = this.kioskGuestData.roomList[this.currentRoomSelected].guestList[this.currentRoomGuestSelected].guestId;
                this.saveGuest(this.currGuestDetail, this.roomStayList[this.currentRoomSelected], this.booking);
                this.setMainGuestOnVerifyDetail(this.currGuestDetail);
                this.addGuestDetaiToRoomList(this.currGuestDetail);
                this.AllowNavigation(true);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
            finally
            {
                this.DoVerifyGuest = false;
            }
        }

        private void exitClick(object sender, HtmlElementEventArgs e)
        {
            if (!this.page.Equals("") && !this.page.Equals("Home/Index"))
                return;
            Login form = new Login();
            form.SetConfirmEvent((Action)(() => this.LoginEvent((Form)form)));
            int num = (int)form.ShowDialog();
        }

        private void btnCheckInCompleteClick(object sender, HtmlElementEventArgs e)
        {
            try
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoCheckInGuest)
                        return;
                    this.DoCheckInGuest = true;
                }
                this.AllowNavigation(true);
                if (!this.updateBookingStatus(BookingStatus.CHIN, this.booking.bookingId))
                    throw new Exception(string.Format("Failed to update booking status to Check In, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
                this.AllowNavigation(true);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void btnCheckOutCompleteClick(object sender, HtmlElementEventArgs e)
        {
            try
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoCheckInGuest)
                        return;
                    this.DoCheckInGuest = true;
                }
                if (!this.updateBookingStatus(BookingStatus.CHOUT, this.booking.bookingId))
                    throw new Exception(string.Format("Failed to update booking status to Check Out, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
                this.AllowNavigation(true);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void btnReturnCardClick(object sender, HtmlElementEventArgs e)
        {
            this.cardDispenserManager.stopThreadLoop = true;
        }

        private void anchorRateExperienceClick(object sender, HtmlElementEventArgs e)
        {
            this.startLoading();
            new Thread((ThreadStart)(() => this.updateRateExperience(sender as HtmlElement)))
            {
                IsBackground = true
            }.Start();
        }

        private void LoginEvent(Form form)
        {
            form.DialogResult = DialogResult.OK;
            form.Close();
            this.ApplicationExit();
        }

        public void LoadMachineHardware(bool status)
        {
            if (!status)
                return;
            this.ReleaseMachineDevice();
            new Thread(new ThreadStart(this.initMachineDevice))
            {
                IsBackground = true
            }.Start();
        }

        public void ReleaseMachineDevice()
        {
            try
            {
                if (this.cashDispenserManager != null)
                    this.cashDispenserManager.Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                if (this.creditCardReaderManager != null)
                    this.creditCardReaderManager.Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                if (this.cardDispenserManager != null)
                    this.cardDispenserManager.Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                ICardReaderManager cardReaderManager = this.cardReaderManager;
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                if (this.documentReaderManager != null)
                    this.documentReaderManager.Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                if (this.printerManager != null)
                    this.printerManager.Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            try
            {
                this.barcodeScanner_Dispose();
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
        }

        public void ValidateMachineDevice()
        {
            new Thread((ThreadStart)(() => this.validateDevice((List<int>)null)))
            {
                IsBackground = true
            }.Start();
        }

        private void updateCheckMachineStep(int step)
        {
            if (step.Equals(0))
                this.machineStatus = true;
            else if (step.Equals(-1))
                this.machineStatus = false;
            this.browser.Document.InvokeScript("updateMachineStatus", new object[1]
            {
        (object) step
            });
        }

        private void initMachineDevice()
        {
            List<int> errList = new List<int>();
            this.stopCollectDeposit = this.getStopCollectDeposit(DateTime.Now.TimeOfDay);
            this.initLocalDatabase(errList);
            this.retrieveKioskData(errList);
            this.initPrinterManager(errList);
            this.initFaceRecognitionManager(errList);
            this.initCreditCardReaderManager(errList);
            // this.initCashAcceptorManager(errList);
            // this.initDocumentReaderManager(errList);
            //this.initCardDispenserManager(errList);
            this.initCardReader(errList);
            // this.initBarcodeManager(errList);
            this.validateDevice(errList);
            this.browser.ScrollBarsEnabled = false;
        }

        private void initLocalDatabase(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)9);
            else
                this.updateCheckMachineStep(9);
            try
            {
                DatabaseHelper.ConnectDatabase(ConstantVariable_App.connStr);
                this.cntBL = new CashNoteTransactionBL(ConstantVariable_App.connStr);
                this.bpBL = new BookingPaymentBL(ConstantVariable_App.connStr);
                this.bBL = new BookingBL(ConstantVariable_App.connStr);
                this.gBL = new GuestBL(ConstantVariable_App.connStr);
                this.rgcBL = new RoomGuestCardBL(ConstantVariable_App.connStr);
                this.sdBL = new SecurityDepositBL(ConstantVariable_App.connStr);
                this.sBL = new SettingBL(ConstantVariable_App.connStr);
                this.cashInOutBL = new CashInOutBL(ConstantVariable_App.connStr);
                this.cardBL = new CardTransactionSummaryBL(ConstantVariable_App.connStr);
                this.surveyResponseBL = new SurveyResponseBL(ConstantVariable_App.connStr);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(9);
            }
        }

        private void retrieveKioskData(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)8);
            else
                this.updateCheckMachineStep(8);
            try
            {
                wininet.LoadLibrary("wininet.dll");
                if (!wininet.CheckInternetConnection())
                    throw new Exception("Internet connection is not available.");
                if (this.sBL == null)
                    throw new Exception("Unable to retrieve Property Code and Kiosk Username from local database.");
                KioskBusinessEntity.Database_Entity.Setting s = new KioskBusinessEntity.Database_Entity.Setting();
                s.code = "PropertyCode";
                string propertyCode = this.sBL.Get(s).value;
                s.code = "KioskUser";
                this.kioskData = WebService.GetService.InitializeKioskData(this.sBL.Get(s).value, propertyCode);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(8);
            }
        }

        private void initPrinterManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)1);
            else
                this.updateCheckMachineStep(1);
            try
            {
                this.printerManager = (IPrinter)new FujitsuPrinterManager();
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(1);
            }
        }

        private void initFaceRecognitionManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)2);
            else
                this.updateCheckMachineStep(2);
            try
            {
                wininet.LoadLibrary("wininet.dll");
                if (!wininet.CheckInternetConnection())
                    throw new Exception("Internet connection is not available.");
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(2);
            }
        }

        private void initCreditCardReaderManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)3);
            else
                this.updateCheckMachineStep(3);
            try
            {
                this.creditCardReaderManager = new CreditCardReaderManager((Control)this);
                this.creditCardReaderManager.InitCreditCardReader(ConstantVariable_App.creditCardReaderPort);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(3);
            }
        }

        private void initCashAcceptorManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)4);
            else
                this.updateCheckMachineStep(4);
            try
            {
                this.cashNoteManager = new ConstantVariable_CashNote();
                this.cashDispenserManager = (ICashDispenser)new CashDispenserManager(ConstantVariable_App.cashDispenserPort, this.cashNoteManager.acceptCashNoteList, false);
                this.cashDispenserManager.Init(this.cashNoteManager.appRouteCashNoteList);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                if (this.stopCollectDeposit)
                    return;
                errList.Add(4);
            }
        }

        private void initDocumentReaderManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)5);
            else
                this.updateCheckMachineStep(5);
            try
            {
                this.documentReaderManager = (IDocumentReader)new DocumentReaderManager(ConstantVariable_App.visaFolder, ConstantVariable_App.passportFolder, ConstantVariable_App.passportHeadFolder);
                this.documentReaderManager.initDocumentReaderWithAction((Control)this, new Action<Passport>(this.SetPassportDetail), new Action<Visa>(this.SetVisaDetail), new Func<ImmigrationChop, string>(this.SetChopDetail), new Action<int>(this.UpdatePassportTips), (Action<Exception, bool>)((ex, status) => this.logException(ex)), new Action<string>(this.warningHandling), new Action<bool>(this.ScanFailed), new Action(this.DocumentReadComplete));
                if (!this.documentReaderManager.GetScanState())
                    return;
                this.documentReaderManager.SetScanState(false);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(5);
            }
        }

        private void initCardDispenserManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)6);
            else
                this.updateCheckMachineStep(6);
            try
            {
                this.cardDispenserManager = (ICardDispenser)new CardDispenserManager(false);
                this.cardDispenserManager.InitDispenser(ConstantVariable_App.cardDispenserPortName, ConstantVariable_App.cardDispenserBaudRate, ConstantVariable_App.cardDispenserAddress, InitializeMode.INIT_CAPTURE_TO_BOX, RFCardReaderStatus.Deactivate);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(6);
            }
        }

        private void initCardReader(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)7);
            else
                this.updateCheckMachineStep(7);
            try
            {
                this.cardReaderManager = (ICardReaderManager)new CardReaderManager(this.kioskData.customerCode, ConstantVariable_App.cardReaderLibraryDll);
                this.cardReaderManager.InitCardReader();
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(7);
            }
        }

        private void initBarcodeManager(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)10);
            else
                this.updateCheckMachineStep(10);
            try
            {
                this.barcodeScanner_Init();
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(10);
            }
        }

        private void validateDevice(List<int> errList)
        {
            if (errList == null)
                errList = new List<int>();
            //this.checkCashNoteQuantity(errList);
            this.checkCardQuantity(errList);
            this.validateDeviceResult(errList);
        }

        private void checkCashNoteQuantity(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)4);
            else
                this.updateCheckMachineStep(4);
            if (errList.Contains(4))
                return;
            try
            {
                CashNoteInformation cashNoteInformation = this.cashDispenserManager.GetNoteCount(this.cashNoteManager.acceptCashNoteList.Select<Decimal, int>((Func<Decimal, int>)(x => Convert.ToInt32(x))).ToList<int>()).FirstOrDefault<CashNoteInformation>();
                if (cashNoteInformation == null || this.stopCollectDeposit || cashNoteInformation.cashNoteStoredCount >= ConstantVariable_App.minCashNoteLoaded)
                    return;
                errList.Add(11);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(4);
            }
        }

        private void checkCardQuantity(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)6);
            else
                this.updateCheckMachineStep(6);
            if (errList.Contains(6))
                return;
            try
            {
                if (this.cardDispenserManager.CheckCardBoxStatus().Equals((object)CardBoxStatus.CBS_EMPTY))
                    errList.Add(12);
                else if (this.cardBL.GetSummary(this.terminalCode).cardBoxQuantity < ConstantVariable_App.cardMinQty)
                    errList.Add(13);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(6);
            }
            if (errList.Contains(6))
                return;
            try
            {
                if (!this.cardDispenserManager.IsCardBinFull())
                    return;
                errList.Add(14);
            }
            catch (Exception ex)
            {
                this.logException(ex);
                errList.Add(6);
            }
        }

        private void validateDeviceResult(List<int> errList)
        {
            if (errList.Count > 0)
            {
                this.showErrorForDevice(errList);
                if (this.InvokeRequired)
                    this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)-1);
                else
                    this.updateCheckMachineStep(-1);
            }
            else if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCheckMachineStep), (object)0);
            else
                this.updateCheckMachineStep(0);
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action(this.enableHardwareCheck));
            else
                this.enableHardwareCheck();
        }

        private void showErrorForDevice(List<int> errList)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<List<int>>(this.showErrorForDevice), (object)errList);
            else
                this.browser.Document.InvokeScript("showInitializeErrorMessage", new object[1]
                {
          (object) JsonConvert.SerializeObject((object) errList)
                });
        }

        private void enableHardwareCheck()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action(this.enableHardwareCheck));
            }
            else
            {
                this.tmrHardwareCheck.Interval = ConstantVariable_App.HardwareCheckInterval;
                this.tmrHardwareCheck.Enabled = true;
            }
        }

        private void disableHardwareCheck()
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action(this.disableHardwareCheck));
            else
                this.tmrHardwareCheck.Enabled = false;
        }

        private void tmrHardwareCheck_Tick(object sender, EventArgs e)
        {
            this.tmrHardwareCheck.Enabled = false;
            try
            {
                this.browser.Document.InvokeScript("ReloadMachine");
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
        }

        public void AllowNavigation(bool status)
        {
            this.navigation = status;
        }

        public void RefreshSessionLifetime(bool setTimer, string url)
        {
            new Thread((ThreadStart)(() => this.refreshSessionLifetime_Start(setTimer, url)))
            {
                IsBackground = true
            }.Start();
        }

        public void UpdateMachineStatus(bool status)
        {
            this.machineStatus = status;
        }

        public void CancelProcess(bool status)
        {
            if (!status)
                return;
            this.browser.Document.InvokeScript("Cancelled");
        }

        public void ApplicationExit()
        {
            this.Close();
        }

        public void LogException(string message)
        {
            this.logException(new Exception(message));
        }

        private void onBrowserDocumentCompleted()
        {
            this.sessionId = this.browser.Url.AbsolutePath.Substring(1, 29);
            this.culture = this.browser.Url.AbsolutePath.Length <= 33 ? string.Empty : this.browser.Url.AbsolutePath.Substring(31, 2);
            foreach (HtmlElement htmlElement in this.browser.Document.All.GetElementsByName("command"))
            {
                if (!string.IsNullOrEmpty(htmlElement.GetAttribute("Value")))
                    htmlElement.Click += new HtmlElementEventHandler(this.btnNavigationClick);
            }
        }

        private void startLoading()
        {
            this.browser.Document.InvokeScript("startLoading");
        }

        private void doneLoading()
        {
            this.browser.Document.InvokeScript("doneLoading");
        }

        private void disableSelection()
        {
            this.browser.Document.InvokeScript("disableSelection");
        }

        private void exceptionHandling(Exception ex, bool displayDetail)
        {
            if (ex == null)
                return;
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Exception, bool>(this.exceptionHandling), (object)ex, (object)displayDetail);
            }
            else
            {
                if (displayDetail)
                {
                    this.browser.Document.InvokeScript("errorMessage", new object[3]
                    {
            (object) ex.Message,
            (object) !displayDetail,
            (object) ""
                    });
                }
                else
                {
                    this.machineStatus = false;
                    this.browser.Document.InvokeScript("errorMessage", new object[3]
                    {
            (object) "",
            (object) !displayDetail,
            (object) ""
                    });
                }
                this.logException(ex);
                this.doneLoading();
            }
        }

        private void warningHandling(string message)
        {
            new MyLog().Log(MyLog.LogType.Warning, message);
        }

        private void logException(Exception ex)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Exception>(this.logException), (object)ex);
            }
            else
            {
                this.sendMaintenanceEmail(ex);
                new MyLog().LogException(ex);
            }
        }

        private void logInfo(string message)
        {
            new MyLog().Log(MyLog.LogType.Info, message);
        }

        private void sendMaintenanceEmail(Exception ex)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Exception>(this.sendMaintenanceEmail), (object)ex);
            }
            else
            {
                ConstantVariable_StaffEmail variableStaffEmail = new ConstantVariable_StaffEmail();
                if (ex == null || variableStaffEmail.staffEmailList.Count <= 0)
                    return;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Exception Type : " + ex.GetType().Name);
                stringBuilder.AppendLine("Exception Message : " + ex.Message);
                stringBuilder.AppendLine("Exception Stack Trace : " + ex.StackTrace);
                Exception innerException = ex.InnerException;
                if (innerException != null)
                {
                    stringBuilder.AppendLine("--Inner Exception------------------------------------");
                    for (; innerException != null; innerException = innerException.InnerException)
                    {
                        stringBuilder.AppendLine("Inner Exception Type : " + innerException.GetType().Name);
                        stringBuilder.AppendLine("Inner Exception Message : " + innerException.Message);
                        stringBuilder.AppendLine("Inner Exception Stack Trace : " + innerException.StackTrace);
                    }
                }
                string subject = string.Format("Error from Self Check In Kiosk - {0} ({1})", (object)this.kioskLocation, (object)this.terminalCode);
                try
                {
                    EmailHelper.SendEmail(ConstantVariable_App.mailFrom, variableStaffEmail.staffEmailList, subject, stringBuilder.ToString(), (List<string>)null, false);
                }
                catch (Exception ex1)
                {
                    this.logException(ex1);
                }
            }
        }

        private void refreshSessionLifetime_Start(bool setTimer, string url)
        {
            string data = (string)null;
            try
            {
                data = new StreamReader(WebRequest.Create(host + url).GetResponse().GetResponseStream()).ReadToEnd();
                data = JsonConvert.DeserializeObject(data).ToString();
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
            finally
            {
                this.refreshSessionLifetime_Success(setTimer, data);
            }
        }

        private void refreshSessionLifetime_Success(bool setTimer, string data)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<bool, string>(this.refreshSessionLifetime_Success), (object)setTimer, (object)data);
            else
                this.browser.Document.InvokeScript("ExtendSessionSuccess", new object[2]
                {
          (object) setTimer,
          (object) data
                });
        }

        private void setKioskCheckInData(List<RoomStayData> guestCardList)
        {
            try
            {
                this.kioskGuestData = new KioskCheckInData();
                CheckInData checkInData = new CheckInData();
                checkInData.checkInSubHeader = new CheckInSubHeader();
                checkInData.checkInSubHeader.bookingDate = this.booking.bookingDate.ToString("yyyy-MM-dd");
                checkInData.checkInSubHeader.bookingId = this.booking.bookingOrderCode;
                checkInData.checkInSubHeader.checkInDate = this.booking.checkInDate.ToString("yyyy-MM-dd");
                checkInData.checkInSubHeader.checkOutDate = this.booking.checkOutDate.ToString("yyyy-MM-dd");
                checkInData.currentRoom = 0;
                checkInData.hotelName = this.kioskData.propertyName;
                checkInData.roomBedList = new List<RoomBed>();
                List<RoomBedDetail> roomBedDetailList = new List<RoomBedDetail>();
                foreach (RoomStayData roomStay1 in this.roomStayList)
                {
                    RoomStayData roomStay = roomStay1;
                    RoomBedDetail roomBedDetail1 = new RoomBedDetail();
                    roomBedDetail1.roomBedLabel = roomStay.roomDesc;
                    roomBedDetail1.RoomTypeGender = roomStay.RoomTypeGender;
                    roomBedDetail1.RoomTypeIsPrivateRoom = roomStay.RoomTypeIsPrivateRoom;
                    roomBedDetail1.card = new RoomBedCard();
                    roomBedDetail1.card.UID = guestCardList.Where<RoomStayData>((Func<RoomStayData, bool>)(x => x.roomStayId.Equals(roomStay.roomStayId))).Select<RoomStayData, string>((Func<RoomStayData, string>)(x => x.cardUID)).FirstOrDefault<string>();
                    roomBedDetail1.card.bedId = roomStay.bedId;
                    roomBedDetail1.card.doorId = string.IsNullOrEmpty(roomStay.doorId) ? "0" : roomStay.doorId;
                    roomBedDetail1.card.lockerId = string.IsNullOrEmpty(roomStay.lockerId) ? " 0" : roomStay.lockerId;
                    roomBedDetail1.card.roomId = roomStay.roomId;
                    roomBedDetail1.card.roomStayId = roomStay.roomStayId;
                    RoomBedDetail roomBedDetail2 = roomBedDetail1;
                    roomBedDetail2.roomBedLabel = roomBedDetail2.roomBedLabel + " [" + roomStay.startDate.ToString("yyyy-MM-dd") + " - " + roomStay.endDate.AddDays(1.0).ToString("yyyy-MM-dd") + "]";
                    roomBedDetail1.card.startDate = roomStay.startDate.Add(this.kioskData.timeStart);
                    roomBedDetail1.card.endDate = roomStay.endDate.AddDays(1.0).Add(this.kioskData.timeEnd);
                    List<string> guestNameList = new List<string>();
                    foreach (GuestData guest in roomStay.guestList)
                    {
                        GuestDetail g = new GuestDetail();
                        g.guestId = guest.guestId;
                        g.guestName = guest.guestName;
                        g.passportNumber = guest.passportNumber;
                        g.gender = guest.gender;
                        g.nationality = guest.nationality;
                        g.dateOfBirth = guest.DOB;
                        g.email = guest.guestEmail;
                        roomBedDetail1.guestList.Add(g);
                        if (this.isGuestDetailComplete(g))
                            guestNameList.Add(g.guestName);
                    }
                    if (this.setMainGuestOnSetBookingData(roomBedDetail1.guestList))
                        checkInData.haveGuest = true;
                    checkInData.roomBedList.Add(new RoomBed(guestNameList, roomStay.roomDesc, roomBedDetail1.card.UID, roomStay.capability, roomStay.RoomTypeGender, roomStay.RoomTypeIsPrivateRoom));
                    roomBedDetailList.Add(roomBedDetail1);
                }
                this.kioskGuestData.roomList = roomBedDetailList;
                if (!checkInData.haveGuest)
                    this.setCurrentRoomSelected(0);
                this.calculatePayment();
                foreach (RoomBedDetail room in this.kioskGuestData.roomList)
                    checkInData.paymentList.Add(new Payment()
                    {
                        price = room.roomBedPriceDetail.Where<RoomBedPayment>((Func<RoomBedPayment, bool>)(x => x.paymentName.Equals("SUB TOTAL : "))).Select<RoomBedPayment, Decimal>((Func<RoomBedPayment, Decimal>)(x => x.paymentAmount)).FirstOrDefault<Decimal>(),
                        description = room.roomBedLabel,
                        quantity = 1
                    });
                checkInData.totalPaid = this.kioskGuestData.totalPaid;
                checkInData.totalCharge = this.kioskGuestData.totalPrice;
                checkInData.totalSurcharge = this.kioskGuestData.totalOutstanding == new Decimal(0) ? new Decimal(0) : this.kioskGuestData.totalOutstanding * this.kioskData.creditCardPerc / new Decimal(100);
                checkInData.paidSecurityDeposit = this.depositList.Sum<SecurityDepositData>((Func<SecurityDepositData, Decimal>)(x => x.amount));
                checkInData.securityDepositPerCard = this.kioskData.securityDepositAmount;
                checkInData.totalSecurityDeposit = checkInData.securityDepositPerCard * (Decimal)checkInData.roomBedList.Where<RoomBed>((Func<RoomBed, bool>)(x => x.guestNameList.Count > 0)).Count<RoomBed>();
                this.logInfo("Check In Data: ");
                this.logInfo("Total Charge: " + (object)checkInData.totalCharge + ", Total Paid: " + (object)checkInData.totalPaid);
                this.logInfo("Security Deposit, Total Amount: " + (object)checkInData.totalSecurityDeposit + ", Paid: " + (object)checkInData.paidSecurityDeposit + ", Total Cards: " + (object)checkInData.roomBedList.Where<RoomBed>((Func<RoomBed, bool>)(x => x.guestNameList.Count > 0)).Count<RoomBed>() + ", Deposit per Card: " + (object)checkInData.securityDepositPerCard);
                this.setCheckInDataOnBrowser(JsonConvert.SerializeObject((object)checkInData));
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private bool setMainGuestOnSetBookingData(List<GuestDetail> guestList)
        {
            if (this.mainGuest == null && guestList != null)
            {
                foreach (GuestDetail guest in guestList)
                {
                    if (this.isGuestDetailComplete(guest))
                    {
                        this.mainGuest = guest;
                        break;
                    }
                }
            }
            return this.mainGuest != null;
        }

        private bool setMainGuestOnVerifyDetail(GuestDetail g)
        {
            if (g != null)
            {
                if (this.mainGuest == null)
                    this.mainGuest = g;
                else if (string.IsNullOrEmpty(this.mainGuest.email) && !string.IsNullOrEmpty(g.email))
                    this.mainGuest = g;
            }
            return this.mainGuest != null;
        }

        private void setKioskCheckOutData()
        {
            try
            {
                this.kioskGuestData = new KioskCheckInData();
                CheckInData checkInData = new CheckInData();
                checkInData.checkInSubHeader = new CheckInSubHeader();
                checkInData.checkInSubHeader.bookingDate = this.booking.bookingDate.ToString("yyyy-MM-dd");
                checkInData.checkInSubHeader.bookingId = this.booking.bookingOrderCode;
                checkInData.checkInSubHeader.checkInDate = this.booking.checkInDate.ToString("yyyy-MM-dd");
                checkInData.checkInSubHeader.checkOutDate = this.booking.checkOutDate.ToString("yyyy-MM-dd");
                checkInData.hotelName = this.kioskData.propertyName;
                checkInData.roomBedList = new List<RoomBed>();
                List<RoomBedDetail> roomBedDetailList = new List<RoomBedDetail>();
                foreach (RoomStayData roomStayData in (IEnumerable<RoomStayData>)this.roomStayList.OrderBy<RoomStayData, long>((Func<RoomStayData, long>)(x => x.roomStayId)))
                {
                    checkInData.roomBedList.Add(new RoomBed(roomStayData.roomDesc, roomStayData.cardUID));
                    if (!string.IsNullOrEmpty(roomStayData.cardUID))
                        roomBedDetailList.Add(new RoomBedDetail()
                        {
                            roomBedLabel = roomStayData.roomDesc,
                            card = new RoomBedCard()
                        });
                }
                this.kioskGuestData.roomList = roomBedDetailList;
                this.setCheckInDataOnBrowser(JsonConvert.SerializeObject((object)checkInData));
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void calculatePayment()
        {
            Decimal num1 = new Decimal(0);
            Decimal num2 = new Decimal(0);
            Decimal num3 = new Decimal(0);
            DateTime minValue1 = DateTime.MinValue;
            DateTime minValue2 = DateTime.MinValue;
            long roomStayId = -1;
            Decimal paymentAmount1 = new Decimal(0);
            Decimal paymentAmount2 = new Decimal(0);
            Decimal paymentAmount3 = new Decimal(0);
            Decimal paymentAmount4 = new Decimal(0);
            this.chargeList = this.chargeList.OrderBy<BookingChargeData, long>((Func<BookingChargeData, long>)(x => x.roomStayId)).ToList<BookingChargeData>();
            foreach (BookingChargeData charge in this.chargeList)
            {
                if (!charge.chargeSubCode.Equals("CCSURCH"))
                {
                    if (roomStayId.Equals(-1L))
                    {
                        DateTime chargeDateTime = charge.chargeDateTime;
                        roomStayId = charge.roomStayId;
                    }
                    if (charge.roomStayId.Equals(roomStayId))
                    {
                        DateTime chargeDateTime1 = charge.chargeDateTime;
                    }
                    else if (charge.roomStayId != roomStayId)
                    {
                        this.kioskGuestData.roomList[this.kioskGuestData.roomList.FindIndex((Predicate<RoomBedDetail>)(x => x.card.roomStayId.Equals(roomStayId)))].roomBedPriceDetail.AddRange((IEnumerable<RoomBedPayment>)new List<RoomBedPayment>()
            {
              new RoomBedPayment("PAYMENT TOTAL : ", paymentAmount3),
              new RoomBedPayment("GST : ", paymentAmount1),
              new RoomBedPayment("SERVICE CHARGE : ", paymentAmount2),
              new RoomBedPayment("SUB TOTAL : ", paymentAmount4)
            });
                        paymentAmount3 = new Decimal(0);
                        paymentAmount1 = new Decimal(0);
                        paymentAmount2 = new Decimal(0);
                        paymentAmount4 = new Decimal(0);
                        DateTime chargeDateTime2 = charge.chargeDateTime;
                        DateTime chargeDateTime3 = charge.chargeDateTime;
                        roomStayId = charge.roomStayId;
                    }
                    paymentAmount3 += charge.charge;
                    paymentAmount1 += charge.gst;
                    paymentAmount2 += charge.serviceCharge;
                    paymentAmount4 += charge.totalCharge;
                    num2 += charge.totalCharge;
                }
                num3 += charge.outstanding;
                num1 += charge.paid;
            }
            if (paymentAmount4 > new Decimal(0))
                this.kioskGuestData.roomList[this.kioskGuestData.roomList.FindIndex((Predicate<RoomBedDetail>)(x => x.card.roomStayId.Equals(roomStayId)))].roomBedPriceDetail.AddRange((IEnumerable<RoomBedPayment>)new List<RoomBedPayment>()
        {
          new RoomBedPayment("PAYMENT TOTAL : ", paymentAmount3),
          new RoomBedPayment("GST : ", paymentAmount1),
          new RoomBedPayment("SERVICE CHARGE : ", paymentAmount2),
          new RoomBedPayment("SUB TOTAL : ", paymentAmount4)
        });
            for (int index = 0; index < this.kioskGuestData.roomList.Count; ++index)
            {
                if (this.kioskGuestData.roomList[index].roomBedPriceDetail.Count == 0)
                    this.kioskGuestData.roomList[index].roomBedPriceDetail.AddRange((IEnumerable<RoomBedPayment>)new List<RoomBedPayment>()
          {
            new RoomBedPayment("PAYMENT TOTAL : ", new Decimal(0)),
            new RoomBedPayment("GST : ", new Decimal(0)),
            new RoomBedPayment("SERVICE CHARGE : ", new Decimal(0)),
            new RoomBedPayment("SUB TOTAL : ", new Decimal(0))
          });
            }
            this.kioskGuestData.totalOutstanding = num3;
            this.kioskGuestData.totalPaid = num1;
            this.kioskGuestData.totalPrice = num2;
        }

        private void setCheckInDataOnBrowser(string serializedCheckInData)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<string>(this.setCheckInDataOnBrowser), (object)serializedCheckInData);
            else
                this.browser.Document.InvokeScript("setCheckInData", new object[1]
                {
          (object) serializedCheckInData
                });
        }

        private void barcodeScanner_Init()
        {
            this.barcodeManager = new BarcodeScannerManager(IntPtr.Zero, 10);
            this.barcodeManager.Init(ConstantVariable_App.barcodeScanner_BeepTime, ConstantVariable_App.barcodeScanner_QR_Enable, ConstantVariable_App.barcodeScanner_DM_Enable, ConstantVariable_App.barcodeScanner_Barcode_Enable);
        }

        private void barcodeScanner_Dispose()
        {
            if (this.barcodeManager == null)
                return;
            this.barcodeManager.Dispose();
        }

        private void barcodeScanner_StopProcess()
        {
            if (this.barcodeManager == null)
                return;
            this.barcodeManager.stopThreadLoop = true;
        }

        public void CancelCheckIn(bool status)
        {
            if (!status)
                return;
            this.barcodeScanner_StopProcess();
        }

        private void StartBarcodeScanner()
        {
            try
            {
                if (!ConstantVariable_App.barcodeScanner_Enable)
                    return;
                this.barcodeScanner_Dispose();
                this.barcodeScanner_Init();
                this.barcodeManager.StartOnAndScanThread((Control)this, new Action<string>(this.scanBookingNumber), new Action<BarcodeScannerStatus>(this.updateBarcodeScannerStatus), new Action<Exception, bool>(this.exceptionHandling));
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, true);
            }
        }

        private void updateBarcodeScannerStatus(BarcodeScannerStatus step)
        {
        }

        private void scanBookingNumber(string barcode)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<string>(this.scanBookingNumber), (object)barcode);
            else
                this.browser.Document.InvokeScript("scanBookingNumber", new object[1]
                {
          (object) barcode
                });
        }

        private void getBookingData(string bookingNumber, string guestName)
        {
            try
            {
                List<BookingData> bookingCheckIn = WebService.GetService.GetBookingCheckIn(bookingNumber, guestName, this.kioskData);
                int status = -1;
                if (bookingCheckIn == null || bookingCheckIn.Count <= 0)
                    status = 0;
                else if (bookingCheckIn.Count == 1)
                {
                    if (bookingCheckIn[0].numberOfBed == 0 || bookingCheckIn[0].numberOfRoom == 0)
                        status = 1;
                }
                else
                    status = 2;
                if (status.Equals(-1))
                {
                    BookingData bookingRes = bookingCheckIn[0];
                    long bookingId = bookingRes.bookingId;
                    int propertyId = this.kioskData.propertyId;
                    this.logInfo(string.Format("Get Room Stay Check In... Booking Id: {0}, Property Id: {1}", (object)bookingId, (object)propertyId));
                    List<RoomStayData> roomStayCheckIn = WebService.GetService.GetRoomStayCheckIn(bookingRes, this.kioskData);
                    this.logInfo(string.Format("Get Charges... Booking Id: {0}, Property Id: {1}", (object)bookingId, (object)propertyId));
                    List<BookingChargeData> chargeList = WebService.GetService.GetChargeList(bookingRes, this.kioskData);
                    this.logInfo(string.Format("Security Deposit... Booking Id: {0}, Property Id: {1}", (object)bookingId, (object)propertyId));
                    List<SecurityDepositData> securityDepositList = WebService.GetService.GetSecurityDepositList(bookingRes);
                    this.logInfo(string.Format("Get Room Stay Check Out... Booking Id: {0}, Property Id: {1}", (object)bookingId, (object)propertyId));
                    List<RoomStayData> roomStayCheckOut = WebService.GetService.GetRoomStayCheckOut(bookingRes);
                    if (roomStayCheckIn.Count <= 0 || chargeList.Count <= 0)
                        throw new Exception("Failed to retrieve user booking details.");
                    if (bookingRes.roomStayId == 0L)
                        bookingRes.roomStayId = roomStayCheckIn[0].roomStayId;
                    this.booking = bookingRes;
                    this.roomStayList = roomStayCheckIn;
                    this.chargeList = chargeList;
                    this.depositList = securityDepositList;
                    this.setKioskCheckInData(roomStayCheckOut);
                    this.barcodeScanner_StopProcess();
                }
                else
                    this.displayBookingStatus(status);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void displayBookingStatus(int status)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<int>(this.displayBookingStatus), (object)status);
            }
            else
            {
                this.doneLoading();
                this.browser.Document.InvokeScript("SearchCheckInFailed", new object[1]
                {
          (object) status
                });
            }
        }

        private void clearBookingData()
        {
            this.booking = (BookingData)null;
            this.mainGuest = (GuestDetail)null;
            this.currGuestDetail = (GuestDetail)null;
            this.transactionDetail = (MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail)null;
            this.bookingPayment = (KioskBusinessEntity.Database_Entity.BookingPayment)null;
            this.securityDepositAmount = new Decimal(0);
            this.isCancelled = false;
            this.currentRoomSelected = 0;
            this.currentRoomGuestSelected = -1;
            this.currentCardUID = string.Empty;
            this.skipFaceRecognition = false;
        }

        public void StartScanDocument(bool status)
        {
            this.documentReaderManager.SetScanState(status);
        }

        private void ScanFailed(bool status)
        {
            if (status)
                return;
            if (this.documentReaderManager.CheckDocumentDocked())
                this.documentReaderManager.SetScanState(true);
            this.doneLoading();
        }

        private void DocumentNoDocked()
        {
            this.browser.Document.InvokeScript("showDocumentNoDocked");
            this.doneLoading();
        }

        private void DocumentReadComplete()
        {
            this.doneLoading();
        }

        private void SetPassportDetail(Passport p)
        {
            if (p == null)
                return;
            this.logInfo("Guest Passport is detected..");
            this.currGuestDetail = new GuestDetail();
            this.currGuestDetail.guestName = p.nationalName.ToUpper();
            this.currGuestDetail.nationality = p.nationalityCode;
            this.currGuestDetail.passportNumber = p.passportNumber;
            this.currGuestDetail.dateOfBirth = p.dateOfBirth;
            this.currGuestDetail.chipImage = p.chipImage;
            this.currGuestDetail.firstName = p.englishFirstName;
            this.currGuestDetail.lastName = p.englishSurname;
            if (!string.IsNullOrEmpty(p.gender))
                this.currGuestDetail.gender = p.gender[0].ToString().ToUpper();
            if (!this.validateGuestPassport(this.currGuestDetail))
                return;
            this.logInfo("Populate Guest Passport Details..");
            HtmlElement elementById1 = this.browser.Document.GetElementById("passportDetail_guestName");
            HtmlElement elementById2 = this.browser.Document.GetElementById("passportDetail_gender");
            HtmlElement elementById3 = this.browser.Document.GetElementById("passportDetail_passportNumber");
            this.browser.Document.GetElementById("passportDetail_nationalityCode");
            elementById1.SetAttribute("value", this.currGuestDetail.guestName);
            elementById3.SetAttribute("value", this.currGuestDetail.passportNumber);
            if (!string.IsNullOrEmpty(p.gender))
                elementById2.SetAttribute("value", this.currGuestDetail.gender);
            this.browser.Document.InvokeScript("updateDatePicker", (object[])new string[2]
            {
        "passportDetail_dob",
        this.currGuestDetail.dateOfBirth.ToString("yyyy-MM-dd")
            });
            this.browser.Document.InvokeScript("updateDatePicker", (object[])new string[2]
            {
        "passportDetail_expiryDate",
        p.dateOfExpiry.ToString("yyyy-MM-dd")
            });
            this.browser.Document.InvokeScript("changeNationality", (object[])new string[1]
            {
        this.currGuestDetail.nationality
            });
            this.browser.Document.InvokeScript("showPassportDetail");
        }

        private void UpdatePassportTips(int step)
        {
            if (!this.page.Equals("CheckIn/ScanPassport"))
                return;
            this.browser.Document.InvokeScript("showPasspostTips", new object[1]
            {
        (object) step
            });
        }

        private void ScanPassportFailed(bool status)
        {
            this.browser.Document.InvokeScript("showScanPassportRule");
            this.ScanFailed(status);
        }

        private void ScanPassportException(Exception ex, bool showDetail)
        {
            this.exceptionHandling(ex, showDetail);
        }

        private bool isGuestDetailComplete(GuestDetail g)
        {
            if (g != null && g.HasCompleteGuestDetails())
                return this.isPassportScanned(g);
            return false;
        }

        private bool isPassportScanned(GuestDetail g)
        {
            return System.IO.File.Exists(FileHelper.GetGuestPassportHeadImagePath(g.passportNumber, g.guestName));
        }

        private bool validateGuestPassport(GuestDetail g)
        {
            this.logInfo("Validate Guest Passport Details..");
            return this.validateGuestGender(g);
        }

        private bool validateGuestGender(GuestDetail g)
        {
            if (string.IsNullOrEmpty(g.gender))
                return true;
            RoomBedDetail room = this.kioskGuestData.roomList[this.currentRoomSelected];
            if (room.RoomTypeGender == RoomTypeGender.Mixed || (!(room.RoomTypeGender == RoomTypeGender.Male) || !(g.gender == GuestGender.Female)) && (!(room.RoomTypeGender == RoomTypeGender.Female) || !(g.gender == GuestGender.Male)))
                return true;
            this.logInfo("Guest Gender does not match with the Room Type Gender..");
            this.browser.Document.InvokeScript("showInvalidGenderAlert", new object[1]
            {
        (object) g.gender
            });
            return false;
        }

        private void SetVisaDetail(Visa v)
        {
            if (v != null)
                this.browser.Document.InvokeScript("SetVisaNumber", new object[1]
                {
          (object) v.MRZVisaNumber
                });
            else
                this.browser.Document.InvokeScript("SetVisaNumber", new object[1]
                {
          (object) ""
                });
        }

        private string SetChopDetail(ImmigrationChop c)
        {
            string str = string.Empty;
            if (c != null)
            {
                str = this.currGuestDetail.passportNumber + "_" + this.currGuestDetail.guestName;
                this.browser.Document.InvokeScript("showChop");
            }
            else
                this.browser.Document.InvokeScript("hideChop");
            return str;
        }

        public void DoneCapturePhoto(string imageData)
        {
            try
            {
                this.startLoading();
                if (!string.IsNullOrEmpty(imageData))
                {
                    string imagePath;
                    using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(Regex.Match(imageData, "data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value)))
                    {
                        using (Bitmap bitmap = new Bitmap((Stream)memoryStream))
                        {
                            imagePath = FileHelper.GetGuestPhotoImagePath(this.currGuestDetail.passportNumber, this.currGuestDetail.guestName, this.retryNumber);
                            bitmap.Save(imagePath, ImageFormat.Jpeg);
                        }
                    }
                    new Thread((ThreadStart)(() => this.ProcessFaceRecognition(imagePath)))
                    {
                        IsBackground = true
                    }.Start();
                }
                else
                {
                    this.ImageInvalid();
                    this.doneLoading();
                }
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        public void StartFaceDetection(GuestDetail g)
        {
            try
            {
                this.owner = (FaceData)null;
                if (ConstantVariable_App.faceDetection_Enable)
                {
                    List<FaceData> faceDataList = this.UploadImage(FileHelper.GetGuestPassportHeadImagePath(g.passportNumber, g.guestName));
                    if (this.skipFaceRecognition)
                        return;
                    if (faceDataList.Count == 1)
                        this.owner = faceDataList[0];
                    else if (faceDataList.Count > 1)
                    {
                        this.skipFaceRecognition = true;
                        this.logException(new Exception("Multiple face detected on passport photo."));
                    }
                    else
                    {
                        this.skipFaceRecognition = true;
                        this.logException(new Exception("No face detected on passport photo."));
                    }
                }
                else
                    Thread.Sleep(ConstantVariable_App.faceDetection_DefaultWaitingTime);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
            finally
            {
                this.EndEnroll();
            }
        }

        public void ProcessFaceRecognition(string imagePath)
        {
            try
            {
                FaceData faceData = (FaceData)null;
                Decimal matchScore = new Decimal(0);
                if (ConstantVariable_App.faceDetection_Enable && !this.skipFaceRecognition)
                {
                    List<FaceData> faceDataList = this.UploadImage(imagePath);
                    if (!this.skipFaceRecognition)
                    {
                        if (faceDataList.Count == 1)
                        {
                            faceData = faceDataList[0];
                            if (ConstantVariable_App.faceRecognition_Enable)
                            {
                                FaceRecognitionData faceRecognitionData = this.RecognizeImage(this.owner, faceData);
                                matchScore = this.skipFaceRecognition || faceRecognitionData == null ? ConstantVariable_App.faceRecognition_DefaultRate : faceRecognitionData.recognitionResult;
                            }
                            else
                            {
                                Thread.Sleep(ConstantVariable_App.faceRecognition_DefaultWaitingTime);
                                matchScore = ConstantVariable_App.faceRecognition_DefaultRate;
                            }
                        }
                        else if (faceDataList.Count > 1)
                        {
                            this.skipFaceRecognition = true;
                            this.logException(new Exception("Multiple face detected on captured guest photo."));
                        }
                        else
                        {
                            this.skipFaceRecognition = true;
                            this.logException(new Exception("No face detected on captured guest photo."));
                        }
                    }
                }
                else
                {
                    Thread.Sleep(ConstantVariable_App.faceRecognition_DefaultWaitingTime);
                    faceData = new FaceData();
                    matchScore = ConstantVariable_App.faceRecognition_DefaultRate;
                }
                if (this.skipFaceRecognition)
                {
                    if (faceData == null)
                        faceData = new FaceData();
                    if (matchScore <= ConstantVariable_App.faceRecognition_DefaultRate)
                        matchScore = ConstantVariable_App.faceRecognition_DefaultRate;
                }
                this.SuccessOnVerifyPerson(faceData, matchScore);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
            finally
            {
                this.EndVerify();
            }
        }

        public List<FaceData> UploadImage(string imagePath)
        {
            List<FaceData> faceDataList = new List<FaceData>();
            try
            {
                faceDataList = FaceRecognitionService.UploadImage(imagePath);
            }
            catch (Exception ex)
            {
                this.skipFaceRecognition = true;
                this.logException(ex);
            }
            return faceDataList;
        }

        public FaceRecognitionData RecognizeImage(FaceData owner, FaceData target)
        {
            FaceRecognitionData faceRecognitionData1 = new FaceRecognitionData();
            FaceRecognitionData faceRecognitionData2;
            try
            {
                List<FaceRecognitionData> source = FaceRecognitionService.RecognizeImage(owner, target);
                if (source != null && source.Count > 0)
                {
                    faceRecognitionData2 = source.OrderByDescending<FaceRecognitionData, Decimal>((Func<FaceRecognitionData, Decimal>)(x => x.recognitionResult)).FirstOrDefault<FaceRecognitionData>();
                }
                else
                {
                    this.skipFaceRecognition = true;
                    faceRecognitionData2 = (FaceRecognitionData)null;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex);
                this.skipFaceRecognition = true;
                faceRecognitionData2 = (FaceRecognitionData)null;
            }
            return faceRecognitionData2;
        }

        public void EndEnroll()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action(this.EndEnroll));
            }
            else
            {
                this.doneLoading();
                this.browser.Document.InvokeScript("ReadyToCapture");
            }
        }

        public void StartFaceVerify(Control control, Action<Exception, bool> exception)
        {
        }

        public void ImageInvalid()
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action(this.ImageInvalid));
            else
                this.browser.Document.InvokeScript("ShowFaceRecognitionInvalidMessage");
        }

        public void VerifySuccess(FaceData faceData)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<FaceData>(this.VerifySuccess), (object)faceData);
            }
            else
            {
                faceData.isValid = true;
                this.DrawFace(faceData);
                this.browser.Document.InvokeScript("ShowCapturePhoto");
            }
        }

        public void SuccessOnVerifyPerson(FaceData faceData, Decimal matchScore)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<FaceData, Decimal>(this.SuccessOnVerifyPerson), (object)faceData, (object)matchScore);
            }
            else
            {
                this.logInfo("Match Rate : " + (object)matchScore + ", Accept Rate : " + (object)(this.currGuestDetail.chipImage ? ConstantVariable_App.chipImageRate : ConstantVariable_App.documentImageRate));
                if (matchScore > (this.currGuestDetail.chipImage ? ConstantVariable_App.chipImageRate : ConstantVariable_App.documentImageRate))
                    this.VerifySuccess(faceData);
                else
                    this.FailedOnVerifyPerson(faceData);
            }
        }

        public void FailedOnVerifyPerson(FaceData faceData)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<FaceData>(this.FailedOnVerifyPerson), (object)faceData);
            }
            else
            {
                ++this.retryNumber;
                if (this.retryNumber < ConstantVariable_App.maximumRecognitionRetryAttempts)
                {
                    faceData.isValid = false;
                    this.DrawFace(faceData);
                    this.UpdateRetryAttempts(this.retryNumber, ConstantVariable_App.maximumRecognitionRetryAttempts);
                }
                else
                    this.browser.Document.InvokeScript("ShowFaceRecognitionErrorMessage");
            }
        }

        public void StartVerify()
        {
        }

        private void EndVerify()
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action(this.EndVerify));
            else
                this.doneLoading();
        }

        private void DrawFace(FaceData faceData)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<FaceData>(this.DrawFace), (object)faceData);
            else
                this.browser.Document.InvokeScript("DrawPoint", new object[1]
                {
          (object) JsonConvert.SerializeObject((object) faceData)
                });
        }

        private void UpdateRetryAttempts(int current, int max)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int, int>(this.UpdateRetryAttempts), (object)current, (object)max);
            else
                this.browser.Document.InvokeScript("UpdateRetryAttempts", new object[2]
                {
          (object) current,
          (object) max
                });
        }

        public void StartFaceIdentify(Control control, Action<Exception, bool> exception)
        {
        }

        public void SuccessOnIdentifyPerson(List<string> matchObject, List<Decimal> matchScore)
        {
            Decimal num = new Decimal(0);
            string str1 = string.Empty;
            for (int index = 0; index < matchObject.Count; ++index)
                str1 = str1 + matchObject[index] + ", " + matchScore[index].ToString("0.00");
            string str2;
            if (num > new Decimal(50))
            {
                str2 = str1 + ". success";
                this.browser.Document.InvokeScript("showCapturePhoto");
            }
            else
                str2 = str1 + ". failed";
        }

        public void StartIdentify()
        {
        }

        public void EndIdentify()
        {
            this.doneLoading();
        }

        public void StartCreditCardReader(bool status)
        {
            if (!status)
                return;
            ++this.retryNumber;
            TagGenerator tagGenerator = new TagGenerator();
            Decimal num = this.kioskGuestData.totalOutstanding + this.kioskGuestData.totalOutstanding * this.kioskData.creditCardPerc / new Decimal(100);
            this.creditCardReaderManager.StartTransactionThread(new Action<MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail, string>(this.completeCreditCardTransaction), new Action<string, string>(this.updateCreditCardStatus), new Action<Exception, bool>(this.exceptionHandling), this.creditCardRunningNumber, "C", Convert.ToInt32((object)PaymentCommandCode.CreditCard_Sales), tagGenerator.getTagLengthValue(PaymentTag.transaction_Amount, string.Join<char>("", num.ToString("0.00").Where<char>(new Func<char, bool>(char.IsDigit)))) + tagGenerator.getTagLengthValue(PaymentTag.Command_Identifier, this.creditCardRunningNumber.ToString()));
        }

        private void updateCreditCardStatus(string messageCode, string message)
        {
            this.browser.Document.InvokeScript("showPaymentTip", new object[2]
            {
        (object) messageCode,
        (object) message
            });
        }

        private void completeCreditCardTransaction(MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail transaction, string paymentType)
        {
            this.increaseCreditCardRunningNumber();
            if (transaction.status.Equals("APPROVED"))
            {
                this.startLoading();
                this.browser.Document.InvokeScript("showPaymentTip", new object[2]
                {
          (object) "03",
          (object) ""
                });
                this.transactionDetail = transaction;
                new Thread((ThreadStart)(() => this.UpdatePaymentOnBooking((Control)this, this.transactionDetail, new Action<MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail>(this.DoneUpdatePayment), new Action(this.EndUpdatePayment), new Action<Exception>(this.logException), new Action<Exception, bool>(this.exceptionHandling))))
                {
                    IsBackground = true
                }.Start();
            }
            else if (this.retryNumber < ConstantVariable_App.maximumRetryAttempts)
            {
                this.browser.Document.InvokeScript("showResponseMessage", new object[4]
                {
          (object) paymentType,
          (object) transaction.responseCode,
          (object) this.retryNumber,
          (object) ConstantVariable_App.maximumRetryAttempts
                });
            }
            else
            {
                bool retryable;
                this.logException(new Exception("transaction failed, Payment Code: " + transaction.paymentCommand + ", Response Code: " + transaction.responseCode + ", " + new ResponseMessage().parseErrorCode(paymentType, transaction.responseCode, out retryable)));
                this.browser.Document.InvokeScript("showErrorResponseMessage", new object[1]
                {
          (object) paymentType
                });
            }
        }

        private void UpdatePaymentOnBooking(Control control, MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail transaction, Action<MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail> successAction, Action end, Action<Exception> exceptionLog, Action<Exception, bool> exception)
        {
            try
            {
                long int64 = Convert.ToInt64(this.sBL.Get(new KioskBusinessEntity.Database_Entity.Setting()
                {
                    code = "ShiftId"
                }).value);
                string paymentModeCode = this.parsePaymentModeCode(transaction.type);
                this.updatePayment(this.chargeList, this.booking, transaction.invoice, paymentModeCode, this.kioskData.currency, this.kioskGuestData.totalOutstanding, int64);
                if (this.bookingPayment == null || string.IsNullOrEmpty(this.bookingPayment.invoiceNumber))
                    throw new Exception(string.Format("Failed to update Payment, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
                if (successAction == null)
                    return;
                if (control.InvokeRequired)
                    control.BeginInvoke((Delegate)successAction, (object)transaction);
                else
                    successAction(transaction);
            }
            catch (Exception ex)
            {
                if (exception == null)
                    return;
                if (control.InvokeRequired)
                    control.BeginInvoke((Delegate)exception, (object)ex, (object)false);
                else
                    exception(ex, false);
            }
            finally
            {
                if (end != null)
                {
                    if (control.InvokeRequired)
                        control.BeginInvoke((Delegate)end);
                    else
                        end();
                }
            }
        }

        private void DoneUpdatePayment(MQuest.HardwareInterface.CreditCardReader.Ingenico.TransactionDetail transaction)
        {
            this.browser.Document.InvokeScript("showPaymentTip", new object[2]
            {
        (object) "00",
        (object) (transaction.title + " " + transaction.status)
            });
            this.browser.Document.InvokeScript("showPayment");
        }

        private void EndUpdatePayment()
        {
            this.doneLoading();
        }

        private void increaseCreditCardRunningNumber()
        {
            ++this.creditCardRunningNumber;
            if (this.creditCardRunningNumber <= 999999)
                return;
            this.creditCardRunningNumber = 1;
        }

        private string parsePaymentModeCode(char transactionType)
        {
            string str = "OTHERS";
            if (transactionType.Equals('V'))
                str = "V_KSK";
            else if (transactionType.Equals('M'))
                str = "M_KSK";
            else if (transactionType.Equals('J'))
                str = "J_KSK";
            else if (transactionType.Equals('C'))
                str = "C_KSK";
            else if (transactionType.Equals('A'))
                str = "A_KSK";
            else if (transactionType.Equals('D'))
                str = "D_KSK";
            else if (transactionType.Equals('E'))
                str = "E_KSK";
            else if (transactionType.Equals('F'))
                str = "F_KSK";
            else if (transactionType.Equals('S'))
                str = "S_KSK";
            else if (transactionType.Equals('N'))
                str = "N_KSK";
            return str;
        }

        public void btnMoreGuestClick(object sender, HtmlElementEventArgs e)
        {
            this.setCurrentRoomSelected(Convert.ToInt32((sender as HtmlElement).Id.Replace("btnMoreGuest-", "")));
        }

        private void setCurrentRoomSelected(int currRoomSel)
        {
            this.currentRoomSelected = currRoomSel;
            this.currentRoomGuestSelected = -1;
            for (int index = 0; index < this.kioskGuestData.roomList[this.currentRoomSelected].guestList.Count; ++index)
            {
                if (!this.isGuestDetailComplete(this.kioskGuestData.roomList[this.currentRoomSelected].guestList[index]))
                {
                    this.currentRoomGuestSelected = index;
                    break;
                }
            }
            this.logInfo("Current Room Selected: " + (object)this.currentRoomSelected);
            this.logInfo("Current Room Guest Selected: " + (object)this.currentRoomGuestSelected);
        }

        private void addGuestDetaiToRoomList(GuestDetail g)
        {
            if (this.currentRoomGuestSelected >= 0)
                this.kioskGuestData.roomList[this.currentRoomSelected].guestList[this.currentRoomGuestSelected].SetGuestDetails(g);
            else
                this.kioskGuestData.roomList[this.currentRoomSelected].guestList.Add(g);
            this.logInfo("Guest Detail: " + JsonConvert.SerializeObject((object)g));
        }

        private int countTotalRoomBedOnCheckIn()
        {
            int num = 0;
            if (this.kioskGuestData != null && this.kioskGuestData.roomList != null)
            {
                foreach (RoomBedDetail room in this.kioskGuestData.roomList)
                {
                    if (room.guestList.Count > 0 && room.guestList.Any<GuestDetail>((Func<GuestDetail, bool>)(g => this.isGuestDetailComplete(g))))
                        ++num;
                }
            }
            return num;
        }

        private void StartCashDispenser(bool status)
        {
            if (!status)
                return;
            new Thread(new ThreadStart(this.StartDeposit))
            {
                IsBackground = true
            }.Start();
        }

        private void StartDeposit()
        {
            lock (_5FootWayKiosk.lockAction)
            {
                if (this.DoCollectSecurityDeposit)
                    return;
                this.DoCollectSecurityDeposit = true;
            }
            int num1 = this.countTotalRoomBedOnCheckIn();
            Decimal num2 = this.depositList.Sum<SecurityDepositData>((Func<SecurityDepositData, Decimal>)(x => x.amount));
            Decimal totalAmountDeposit = this.kioskData.securityDepositAmount * (Decimal)num1 - num2 - this.securityDepositAmount;
            if (totalAmountDeposit <= new Decimal(0))
                totalAmountDeposit = new Decimal(0);
            this.logInfo("Current Deposit Amount : " + (object)num2);
            this.logInfo("Payable Deposit Amount : " + (object)totalAmountDeposit);
            this.securityDepositAmount = new Decimal(0);
            this.cashDispenserManager.StartDepositThread((Control)this, new Action<Decimal, Decimal>(this.completeDeposit), new Action<int>(this.updateCashDispenserStatus), new Action<Decimal, Decimal>(this.updateDepositAmount), new Action<Exception, bool>(this.SecurityDepositException), totalAmountDeposit);
        }

        private void updateDepositAmount(Decimal amountPayableValue, Decimal amountPaidValue)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal, Decimal>(this.updateDepositAmount), (object)amountPayableValue, (object)amountPaidValue);
            }
            else
            {
                this.browser.Document.GetElementById("amountPayable");
                HtmlElement elementById1 = this.browser.Document.GetElementById("amountInserted");
                HtmlElement elementById2 = this.browser.Document.GetElementById("amountChange");
                this.securityDepositAmount += amountPaidValue;
                Decimal num = amountPaidValue - amountPayableValue;
                if (elementById1 != (HtmlElement)null)
                    elementById1.InnerText = (Convert.ToDecimal(elementById1.InnerText) + amountPaidValue).ToString("0.00");
                if (num >= new Decimal(0))
                {
                    this.updateCashDispenserStatus(6);
                    if (elementById2 != (HtmlElement)null)
                        elementById2.InnerText = num.ToString("0.00");
                }
                else
                {
                    this.updateCashDispenserStatus(1);
                    if (elementById2 != (HtmlElement)null)
                        elementById2.InnerText = new Decimal(0).ToString("0.00");
                }
                this.logInfo("Total Deposited " + this.securityDepositAmount.ToString("0.00") + " for booking order code " + this.booking.bookingOrderCode);
            }
        }

        private void updateCashDispenserStatus(int step)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.updateCashDispenserStatus), (object)step);
            else
                this.browser.Document.InvokeScript("showSecurityDepositTip", new object[1]
                {
          (object) step
                });
        }

        private void completeDeposit(Decimal amountToReturn, Decimal amountDeposited)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal, Decimal>(this.completeDeposit), (object)amountToReturn, (object)amountDeposited);
            }
            else
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoReturnSecurityDepositChange)
                        return;
                    this.DoReturnSecurityDepositChange = true;
                }
                this.logInfo("Amount to return : " + (object)amountToReturn);
                if (amountToReturn >= new Decimal(0))
                {
                    this.cashDispenserManager.StartWithdrawThread((Control)this, new Action<Decimal>(this.completePaidSecurityDeposit), new Action<int>(this.updateCashDispenserStatus), new Action<Decimal>(this.updateWithdrawAmount), new Action<Exception, bool>(this.SecurityDepositException), amountToReturn);
                }
                else
                {
                    if (!(this.securityDepositAmount > new Decimal(0)))
                        return;
                    this.UpdateSecurityDeposit((Action)null);
                }
            }
        }

        private void updateWithdrawAmount(Decimal accumulateWithdrawedAmount)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal>(this.updateWithdrawAmount), (object)accumulateWithdrawedAmount);
            }
            else
            {
                HtmlElement elementById1 = this.browser.Document.GetElementById("amountChange");
                HtmlElement elementById2 = this.browser.Document.GetElementById("amountInserted");
                HtmlElement elementById3 = this.browser.Document.GetElementById("amountPayable");
                Decimal num1 = Convert.ToDecimal(elementById2.InnerText);
                Decimal num2 = Convert.ToDecimal(elementById3.InnerText);
                Decimal num3 = num1 - num2 - accumulateWithdrawedAmount;
                this.securityDepositAmount = num1 - accumulateWithdrawedAmount;
                elementById1.InnerText = num3.ToString("0.00");
            }
        }

        private void completePaidSecurityDeposit(Decimal totalWithdrawedAmount)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal>(this.completePaidSecurityDeposit), (object)totalWithdrawedAmount);
            }
            else
            {
                this.logInfo("Returned " + totalWithdrawedAmount.ToString("0.00") + " for booking order code " + this.booking.bookingOrderCode);
                this.updateCashDispenserStatus(9);
                this.UpdateSecurityDeposit(new Action(this.CompleteUpdateSecurityDeposit));
            }
        }

        private void SecurityDepositException(Exception ex, bool showDetail)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Exception, bool>(this.SecurityDepositException), (object)ex, (object)showDetail);
            }
            else
            {
                this.updateCashDispenserStatus(-1);
                this.UpdateSecurityDeposit((Action)null);
                this.browser.Document.InvokeScript("showRetryMessage");
                this.logException(ex);
            }
        }

        public void CancelSecurityDeposit(bool status)
        {
            if (!status)
                return;
            this.isCancelled = true;
            this.cashDispenserManager.StopThread();
        }

        private bool UpdateSecurityDeposit(Action action)
        {
            if (!(this.securityDepositAmount > new Decimal(0)))
                return false;
            this.securityDepositRunningNumber.ToString().PadLeft(6, '0');
            new Thread((ThreadStart)(() => this.UpdateSecurityDepositAmount((Control)this, action, new Action<Exception>(this.logException), new Action<Exception, bool>(this.exceptionHandling), this.booking, string.Empty, this.kioskData.currency, this.securityDepositAmount)))
            {
                IsBackground = true
            }.Start();
            this.increaseSecurityDepositRunningNumber();
            return true;
        }

        private void UpdateSecurityDepositAmount(Control control, Action successAction, Action<Exception> exceptionLog, Action<Exception, bool> exception, BookingData bookingData, string invoiceNumber, string currencyCode, Decimal amount)
        {
            if (!(amount > new Decimal(0)))
                return;
            try
            {
                long int64 = Convert.ToInt64(this.sBL.Get(new KioskBusinessEntity.Database_Entity.Setting()
                {
                    code = "ShiftId"
                }).value);
                this.paySecurityDeposit(bookingData, invoiceNumber, currencyCode, amount, int64);
            }
            catch (Exception ex)
            {
                if (exceptionLog != null)
                {
                    if (control.InvokeRequired)
                        control.BeginInvoke((Delegate)exceptionLog, (object)ex);
                    else
                        exceptionLog(ex);
                }
            }
            if (successAction == null)
                return;
            if (control.InvokeRequired)
                control.BeginInvoke((Delegate)successAction);
            else
                successAction();
        }

        private void CompleteUpdateSecurityDeposit()
        {
            this.securityDepositAmount = new Decimal(0);
            this.browser.Document.InvokeScript("showSecurityDeposit");
            this.updateCashDispenserStatus(10);
        }

        private void increaseSecurityDepositRunningNumber()
        {
            ++this.securityDepositRunningNumber;
        }

        private void DispenseCard(string cardId)
        {
            this.currentRoomSelected = Convert.ToInt32(cardId);
            List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
            additionalFunctionList.Add(new Func<Control, string>(this.ResetDispenseCard));
            additionalFunctionList.Add(new Func<Control, string>(this.WriteDispenseCard));
            additionalFunctionList.Add(new Func<Control, string>(this.ReadDispenseCard));
            additionalFunctionList.Add(new Func<Control, string>(this.CheckInDispenseCard));
            List<int> process = new List<int>();
            process.Add(1);
            process.Add(2);
            process.Add(3);
            process.Add(4);
            process.Add(5);
            process.Add(6);
            new List<CardDispensePosition>()
      {
        CardDispensePosition.FROM_BOX
      };
            this.cardDispenserManager.StartDispenseCardThread((Control)this, additionalFunctionList, process, new Action<string>(this.DoneDispenseCard), new Action<string, int>(this.UpdateDispenseCardStatus), (Action<string>)null, new Action<Exception>(this.logException), new Action<Exception, bool>(this.DispenseCardException), cardId, CardPositionCommand.MM_RETURN_TO_FRONT);
            this.browser.Document.InvokeScript("showLabel", new object[1]
            {
        (object) cardId
            });
        }

        private string ResetDispenseCard(Control control)
        {
            int num = 0;
            string str1 = string.Empty;
            while (num < 3)
            {
                try
                {
                    if (this.cardReaderManager.ResetCard())
                    {
                        str1 = string.Empty;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        this.cardReaderManager.InitCardReader();
                    }
                    catch
                    {
                    }
                    string str2 = str1;
                    string str3;
                    switch (num)
                    {
                        case 0:
                            str3 = "1st try : ";
                            break;
                        case 1:
                            str3 = ",2nd try : ";
                            break;
                        default:
                            str3 = ",3rd try : ";
                            break;
                    }
                    string message = ex.Message;
                    str1 = str2 + str3 + message;
                    ++num;
                    Thread.Sleep(500);
                }
            }
            return str1;
        }

        private string WriteDispenseCard(Control control)
        {
            RoomBedCard card = this.kioskGuestData.roomList[this.currentRoomSelected].card;
            MyHotelCard myHotelCard = new MyHotelCard();
            myHotelCard.hotelDoorId = Convert.ToInt32(card.doorId);
            myHotelCard.hotelPropertyId = (long)this.kioskData.doorAccessPropertyId;
            myHotelCard.hotelLockerId = Convert.ToInt32(card.lockerId);
            myHotelCard.startDate = card.startDate;
            myHotelCard.endDate = card.endDate;
            myHotelCard.customerCode = this.kioskData.customerCode;
            myHotelCard.laundryCredit = 0;
            myHotelCard.UID = string.Empty;
            int num = 0;
            string str1 = string.Empty;
            while (num < 3)
            {
                try
                {
                    if (this.cardReaderManager.WriteCard(CardType.Guest, (Card)myHotelCard))
                    {
                        str1 = string.Empty;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        this.cardReaderManager.InitCardReader();
                    }
                    catch
                    {
                    }
                    string str2 = str1;
                    string str3;
                    switch (num)
                    {
                        case 0:
                            str3 = "1st try : ";
                            break;
                        case 1:
                            str3 = ",2nd try : ";
                            break;
                        default:
                            str3 = ",3rd try : ";
                            break;
                    }
                    string message = ex.Message;
                    str1 = str2 + str3 + message;
                    ++num;
                    Thread.Sleep(500);
                }
            }
            return str1;
        }

        private string ReadDispenseCard(Control control)
        {
            this.currentCardUID = (string)null;
            string empty = string.Empty;
            try
            {
                Card card = this.cardReaderManager.ReadCard();
                if (card == null)
                    throw new Exception("Failed to read card data, Card is in different format.");
                this.currentCardUID = card.UID;
                if (string.IsNullOrEmpty(card.UID))
                    throw new Exception("Cannot read card UID.");
                this.logInfo("Card UID: " + this.currentCardUID);
            }
            catch (Exception ex)
            {
                empty += ex.Message;
            }
            return empty;
        }

        private string CheckInDispenseCard(Control control)
        {
            string str1 = string.Empty;
            try
            {
                RoomBedDetail room = this.kioskGuestData.roomList[this.currentRoomSelected];
                if (room.card != null)
                {
                    if (string.IsNullOrEmpty(room.card.UID))
                    {
                        for (int index = 0; index < 3; ++index)
                        {
                            try
                            {
                                room.card.UID = this.currentCardUID;
                                if (this.checkInGuestCard(this.booking.bookingId, room))
                                    break;
                            }
                            catch (Exception ex)
                            {
                                string str2 = str1;
                                string str3;
                                switch (index)
                                {
                                    case 0:
                                        str3 = "1st try : ";
                                        break;
                                    case 1:
                                        str3 = ",2nd try : ";
                                        break;
                                    default:
                                        str3 = ",3rd try : ";
                                        break;
                                }
                                string message = ex.Message;
                                str1 = str2 + str3 + message;
                                room.card.UID = string.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                str1 += ex.Message;
            }
            return str1;
        }

        private void UpdateDispenseCardStatus(string cardId, int step)
        {
            this.browser.Document.InvokeScript("updateCollectCardStatus", new object[2]
            {
        (object) cardId,
        (object) step
            });
        }

        private void FailDispenseCard(string cardId)
        {
            this.browser.Document.InvokeScript("showButton", new object[1]
            {
        (object) cardId
            });
        }

        private void DispenseCardException(Exception ex, bool showDetail)
        {
            this.CompleteDispenseCard();
            this.exceptionHandling(ex, showDetail);
        }

        private void DoneDispenseCard(string cardId)
        {
            this.RecordDispenseCard();
            this.CheckDispensedCard();
        }

        private void CompleteDispenseCard()
        {
            foreach (HtmlElement htmlElement in this.browser.Document.All.GetElementsByName("dispense"))
                htmlElement.Enabled = true;
        }

        private void RecordDispenseCard()
        {
            try
            {
                this.cardBL.DispenseCard(this.terminalCode, 1);
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
        }

        private void CheckDispensedCard()
        {
            try
            {
                bool flag = true;
                foreach (RoomBedDetail room in this.kioskGuestData.roomList)
                {
                    if (room.guestList.Count > 0 && (room.guestList.Any<GuestDetail>((Func<GuestDetail, bool>)(g => this.isGuestDetailComplete(g))) && string.IsNullOrEmpty(room.card.UID)))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    this.browser.Document.InvokeScript("showCollectCard");
                else
                    this.CompleteDispenseCard();
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void StartCheckOutInsertCard()
        {
            List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
            additionalFunctionList.Add(new Func<Control, string>(this.CheckOutInsertCard));
            additionalFunctionList.Add(new Func<Control, string>(this.TrackBookingData));
            List<int> process = new List<int>();
            process.Add(2);
            process.Add(3);
            process.Add(3);
            process.Add(4);
            new List<CardReturnPosition>()
      {
        CardReturnPosition.FROM_OUT
      };
            this.cardDispenserManager.StartReturnCardThread((Control)this, additionalFunctionList, process, new Action(this.DoneCheckOutInsertCard), new Action(this.DoneAllCheckOutInsertCard), new Action<int>(this.UpdateCheckOutInsertCardStatus), new Action(this.CheckOutInsertCardFailed), CardPositionCommand.MM_RETURN_TO_FRONT, CardPositionCommand.MM_RETURN_TO_FRONT, new Action<Exception>(this.logException), new Action<Exception, bool>(this.CheckOutCardException), 1);
        }

        private void SetBookingData()
        {
            try
            {
                HtmlElement elementById1 = this.browser.Document.GetElementById("checkOut_guestName");
                HtmlElement elementById2 = this.browser.Document.GetElementById("checkOut_securityDeposit");
                HtmlElement elementById3 = this.browser.Document.GetElementById("checkOut_checkInDate");
                HtmlElement elementById4 = this.browser.Document.GetElementById("checkOut_checkOutDate");
                HtmlElement elementById5 = this.browser.Document.GetElementById("checkOut_roomBedNumber");
                if (elementById1 != (HtmlElement)null)
                    elementById1.InnerText = this.booking.guestName;
                if (elementById2 != (HtmlElement)null)
                    elementById2.InnerText = this.depositList.Sum<SecurityDepositData>((Func<SecurityDepositData, Decimal>)(x => x.amount)).ToString();
                if (elementById3 != (HtmlElement)null)
                    elementById3.InnerText = this.booking.checkInDate.ToString("yyyy-MM-dd");
                if (elementById4 != (HtmlElement)null)
                    elementById4.InnerText = this.booking.checkOutDate.ToString("yyyy-MM-dd");
                if (!(elementById5 != (HtmlElement)null))
                    return;
                elementById5.InnerText = this.roomStayList[0].roomDesc;
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private string CheckOutInsertCard(Control control)
        {
            Action<Exception, bool> action = new Action<Exception, bool>(this.exceptionHandling);
            int num = 0;
            string str1 = string.Empty;
            for (; num < 3; ++num)
            {
                Card card1 = (Card)new MyHotelCard();
                string empty = string.Empty;
                try
                {
                    Card card2 = this.cardReaderManager.ReadCard();
                    if (card2 == null)
                        throw new Exception("Card is different format.");
                    Func<Control, Action<Exception, bool>, Card, string> func = new Func<Control, Action<Exception, bool>, Card, string>(this.CheckCardDetail);
                    string str2;
                    if (control.InvokeRequired)
                        str2 = (string)control.Invoke((Delegate)func, (object)control, (object)action, (object)card2);
                    else
                        str2 = func(control, action, card2);
                    if (string.IsNullOrEmpty(str2))
                        throw new Exception("Card UID not found.");
                    str1 = string.Empty;
                }
                catch (Exception ex)
                {
                    string str2 = str1;
                    string str3;
                    switch (num)
                    {
                        case 0:
                            str3 = "1st try : ";
                            break;
                        case 1:
                            str3 = ",2nd try : ";
                            break;
                        default:
                            str3 = ",3rd try : ";
                            break;
                    }
                    string message = ex.Message;
                    str1 = str2 + str3 + message;
                }
                if (!string.IsNullOrEmpty(str1))
                    Thread.Sleep(100);
                else
                    break;
            }
            return str1;
        }

        private string CheckCardDetail(Control control, Action<Exception, bool> exception, Card card)
        {
            if (card != null)
            {
                this.currentCardUID = card.UID;
                if (!string.IsNullOrEmpty(this.currentCardUID))
                    return this.currentCardUID;
            }
            return (string)null;
        }

        private string TrackBookingData(Control control)
        {
            try
            {
                long bookingId = WebService.GetService.ValidateGuestCard(this.currentCardUID);
                if (bookingId <= 0L)
                    throw new Exception("Validate card failed, Card UID : " + this.currentCardUID + ".");
                List<BookingData> bookingCheckOut = WebService.GetService.GetBookingCheckOut(bookingId, this.kioskData);
                if (bookingCheckOut == null || bookingCheckOut.Count != 1)
                    throw new Exception("booking record not found or multiple booking record is found");
                this.booking = bookingCheckOut[0];
                this.booking.bookingId = bookingId;
                this.roomStayList = WebService.GetService.GetRoomStayCheckOut(this.booking);
                if (this.roomStayList == null || this.roomStayList.Count <= 0)
                    throw new Exception("RoomStay record not found");
                this.booking.roomStayId = this.roomStayList[0].roomStayId;
                this.depositList = WebService.GetService.GetSecurityDepositList(this.booking);
                return string.Empty;
            }
            catch (Exception ex)
            {
                this.booking = (BookingData)null;
                return ex.Message;
            }
        }

        private void UpdateCheckOutInsertCardStatus(int step)
        {
            this.browser.Document.InvokeScript("updateInsertCardTips", new object[1]
            {
        (object) step
            });
        }

        private void CheckOutInsertCardFailed()
        {
        }

        private void DoneCheckOutInsertCard()
        {
        }

        private void CheckOutCardException(Exception ex, bool showDetail)
        {
            this.CompleteCheckOutCard();
            this.exceptionHandling(ex, false);
        }

        private void DoneAllCheckOutInsertCard()
        {
            this.CompleteCheckOutCard();
        }

        private void CompleteCheckOutCard()
        {
            if (this.booking == null)
            {
                this.UpdateCheckOutInsertCardStatus(1);
                this.browser.Document.InvokeScript("InvalidCheckOutCard");
                this.browser.Document.InvokeScript("ShowScanCardButton");
            }
            else
            {
                this.SetBookingData();
                this.browser.Document.InvokeScript("showBookingDetail");
            }
        }

        public void CancelReturnCard(bool status)
        {
            if (!status)
                return;
            this.isCancelled = true;
            this.cardDispenserManager.stopThreadLoop = true;
        }

        public void StartReturnCard(int totalCard, bool includeGate)
        {
            ++this.retryNumber;
            List<Func<Control, string>> additionalFunctionList = new List<Func<Control, string>>();
            additionalFunctionList.Add(new Func<Control, string>(this.ReadReturnedCard));
            additionalFunctionList.Add(new Func<Control, string>(this.ResetReturnedCard));
            additionalFunctionList.Add(new Func<Control, string>(this.CheckOutReturnCard));
            List<int> process = new List<int>();
            process.Add(1);
            process.Add(2);
            process.Add(3);
            process.Add(4);
            process.Add(5);
            List<CardReturnPosition> cardReturnPositionList = new List<CardReturnPosition>();
            cardReturnPositionList.Add(CardReturnPosition.FROM_OUT);
            if (includeGate)
                cardReturnPositionList.Add(CardReturnPosition.FROM_GATE);
            this.cardDispenserManager.StartReturnCardThread((Control)this, additionalFunctionList, process, new Action(this.DoneReturnCard), new Action(this.DoneAllReturnCard), new Action<int>(this.updateReturnCardStatus), new Action(this.ReturnCardFail), CardPositionCommand.MM_RETURN_TO_FRONT, CardPositionCommand.MM_CAPTURE_TO_BOX, new Action<Exception>(this.logException), new Action<Exception, bool>(this.ReturnCardException), totalCard);
        }

        private string ReadReturnedCard(Control control)
        {
            this.invalidCard = false;
            string str1 = string.Empty;
            int num = 0;
            this.currentCardUID = (string)null;
            for (; num < 3; ++num)
            {
                try
                {
                    Card card = this.cardReaderManager.ReadCard();
                    if (card == null)
                        throw new Exception("Card is different format.");
                    if (string.IsNullOrEmpty(card.UID))
                        throw new Exception("Unknown card.");
                    Func<Card, bool> func = new Func<Card, bool>(this.ValidateReturnedCardID);
                    bool flag;
                    if (control.InvokeRequired)
                        flag = (bool)control.Invoke((Delegate)func, (object)card);
                    else
                        flag = func(card);
                    if (!flag)
                        throw new Exception("Card not matched.");
                    str1 = string.Empty;
                }
                catch (Exception ex)
                {
                    string str2 = str1;
                    string str3;
                    switch (num)
                    {
                        case 0:
                            str3 = "1st try : ";
                            break;
                        case 1:
                            str3 = ",2nd try : ";
                            break;
                        default:
                            str3 = ",3rd try : ";
                            break;
                    }
                    string message = ex.Message;
                    str1 = str2 + str3 + message;
                }
                if (!string.IsNullOrEmpty(str1))
                    Thread.Sleep(100);
                else
                    break;
            }
            return str1;
        }

        private bool ValidateReturnedCardID(Card card)
        {
            this.currentCardUID = card.UID;
            if (this.kioskGuestData.roomList.Any<RoomBedDetail>((Func<RoomBedDetail, bool>)(x => x.card.UID.Equals(this.currentCardUID))))
                return true;
            this.invalidCard = true;
            return false;
        }

        private string ResetReturnedCard(Control control)
        {
            int num = 0;
            string str1 = string.Empty;
            for (; num < 3; ++num)
            {
                try
                {
                    this.cardReaderManager.ResetCard();
                    str1 = string.Empty;
                }
                catch (Exception ex)
                {
                    string str2 = str1;
                    string str3;
                    switch (num)
                    {
                        case 0:
                            str3 = "1st try : ";
                            break;
                        case 1:
                            str3 = ",2nd try : ";
                            break;
                        default:
                            str3 = ",3rd try : ";
                            break;
                    }
                    string message = ex.Message;
                    str1 = str2 + str3 + message;
                }
                if (string.IsNullOrEmpty(str1))
                    break;
            }
            return str1;
        }

        private string CheckOutReturnCard(Control control)
        {
            try
            {
                if (!this.checkOutGuestCard(this.currentCardUID, this.booking.bookingId))
                    return string.Format("Failed to return Guest Card on Check Out, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private void updateReturnCardStatus(int step)
        {
            if (step == 2)
                this.browser.Document.InvokeScript("hideReturnCard");
            if (this.currentCardUID == null)
                return;
            this.browser.Document.InvokeScript("updateReturnCardStatus", new object[2]
            {
        (object) this.currentCardUID,
        (object) step
            });
        }

        private void ReturnCardFail()
        {
        }

        private void ReturnCardException(Exception ex, bool showDetail)
        {
            this.logException(ex);
            if (!this.invalidCard)
            {
                if (this.retryNumber < ConstantVariable_App.maximumRetryAttempts)
                    this.browser.Document.InvokeScript("ShowReturnCardAlertMessage", new object[3]
                    {
            (object) this.kioskGuestData.roomList.Where<RoomBedDetail>((Func<RoomBedDetail, bool>) (x => x.card != null)).Count<RoomBedDetail>(),
            (object) this.retryNumber,
            (object) ConstantVariable_App.maximumRetryAttempts
                    });
                else
                    this.browser.Document.InvokeScript("ShowReturnCardErrorMessage");
            }
            else
            {
                this.browser.Document.InvokeScript("InvalidReturnCard");
                this.StartReturnCard(this.kioskGuestData.roomList.Where<RoomBedDetail>((Func<RoomBedDetail, bool>)(x => x.card != null)).Count<RoomBedDetail>(), false);
                this.invalidCard = false;
            }
        }

        private void DoneReturnCard()
        {
            for (int index = 0; index < this.kioskGuestData.roomList.Count; ++index)
            {
                if (this.kioskGuestData.roomList[index].card != null && this.kioskGuestData.roomList[index].card.UID.Equals(this.currentCardUID))
                    this.kioskGuestData.roomList[index].card = (RoomBedCard)null;
            }
            this.browser.Document.InvokeScript("showReturnCard");
            try
            {
                this.cardBL.ReturnCard(this.terminalCode, 1);
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
        }

        private void DoneAllReturnCard()
        {
            if (this.isCancelled)
                return;
            try
            {
                bool flag = false;
                Decimal amountToWithdraw = this.depositList.Sum<SecurityDepositData>((Func<SecurityDepositData, Decimal>)(x => x.amount)) / (Decimal)this.kioskGuestData.roomList.Count<RoomBedDetail>() * (Decimal)this.kioskGuestData.roomList.Where<RoomBedDetail>((Func<RoomBedDetail, bool>)(x => x.card == null)).Count<RoomBedDetail>();
                try
                {
                    flag = this.cashDispenserManager.CheckPayoutAvailable(amountToWithdraw);
                }
                catch (Exception ex)
                {
                    if (ex.Equals((object)CashDispenserException.UnknownError))
                        throw ex;
                }
                this.browser.Document.InvokeScript("SetReturnSecurityDepositData", new object[2]
                {
          (object) flag,
          (object) amountToWithdraw
                });
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void StartReturnSecurityDeposit()
        {
            lock (_5FootWayKiosk.lockAction)
            {
                if (this.DoReturnSecurityDeposit)
                    return;
                this.DoReturnSecurityDeposit = true;
            }
            this.securityDepositAmount = new Decimal(0);
            this.cashDispenserManager.StartWithdrawThread((Control)this, new Action<Decimal>(this.DoneReturnSecurityDeposit), new Action<int>(this.UpdateReturnSecurityDepositStatus), new Action<Decimal>(this.UpdateReturnSecurityDepositAmount), new Action<Exception, bool>(this.ReturnSecurityDepositException), this.depositList.Sum<SecurityDepositData>((Func<SecurityDepositData, Decimal>)(x => x.amount)));
        }

        private void DoneReturnSecurityDeposit(Decimal totalReturnedAmount)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal>(this.DoneReturnSecurityDeposit), (object)totalReturnedAmount);
            }
            else
            {
                this.UpdateReturnSecurityDepositStatus(9);
                this.startLoading();
                this.CompleteReturnSecurityDeposit(new Action(this.UpdateCompleteReturnSecurityDeposit));
            }
        }

        private void UpdateReturnSecurityDepositAmount(Decimal accumulateReturnedAmount)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Decimal>(this.UpdateReturnSecurityDepositAmount), (object)accumulateReturnedAmount);
            }
            else
            {
                HtmlElement elementById = this.browser.Document.GetElementById("amountReturned");
                this.securityDepositAmount = accumulateReturnedAmount;
                elementById.InnerText = accumulateReturnedAmount.ToString("0.00");
            }
        }

        private void UpdateReturnSecurityDepositStatus(int step)
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action<int>(this.UpdateReturnSecurityDepositStatus), (object)step);
            else
                this.browser.Document.InvokeScript("showReturnDepositTip", new object[1]
                {
          (object) step
                });
        }

        private void ReturnSecurityDepositException(Exception ex, bool showDetail)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<Exception, bool>(this.ReturnSecurityDepositException), (object)ex, (object)showDetail);
            }
            else
            {
                this.startLoading();
                this.UpdateReturnSecurityDepositStatus(-1);
                this.CompleteReturnSecurityDeposit(new Action(this.doneLoading));
                this.exceptionHandling(ex, showDetail);
            }
        }

        private void CompleteReturnSecurityDeposit(Action success)
        {
            this.logInfo("Returned " + this.securityDepositAmount.ToString("0.00") + " for booking order code " + this.booking.bookingOrderCode);
            this.increaseSecurityDepositRunningNumber();
            if (!(this.securityDepositAmount > new Decimal(0)))
                return;
            new Thread((ThreadStart)(() => this.UpdateReturnSecurityDeposit((Control)this, success, new Action(this.UpdateReturnSecurityDepositStart), new Action(this.UpdateReturnSecurityDepositEnd), new Action<Exception, bool>(this.exceptionHandling), this.depositList, this.booking, string.Empty, this.kioskData.currency, this.securityDepositAmount)))
            {
                IsBackground = true
            }.Start();
        }

        private void UpdateReturnSecurityDeposit(Control control, Action success, Action start, Action end, Action<Exception, bool> exception, List<SecurityDepositData> depositList, BookingData booking, string invoiceNumber, string currencyCode, Decimal securityDepositAmount)
        {
            try
            {
                if (start != null)
                {
                    if (control.InvokeRequired)
                        control.BeginInvoke((Delegate)start);
                    else
                        start();
                }
                long int64 = Convert.ToInt64(this.sBL.Get(new KioskBusinessEntity.Database_Entity.Setting()
                {
                    code = "ShiftId"
                }).value);
                if (!this.returnSecurityDeposit(depositList, booking, invoiceNumber, currencyCode, securityDepositAmount, int64))
                    return;
                securityDepositAmount = new Decimal(0);
                if (success == null)
                    return;
                if (control.InvokeRequired)
                    control.BeginInvoke((Delegate)success);
                else
                    success();
            }
            catch (Exception ex)
            {
                if (control.InvokeRequired)
                    control.BeginInvoke((Delegate)exception, (object)ex, (object)false);
                else
                    exception(ex, false);
            }
            finally
            {
                if (end != null)
                {
                    if (control.InvokeRequired)
                        control.BeginInvoke((Delegate)end);
                    else
                        end();
                }
            }
        }

        private void UpdateCompleteReturnSecurityDeposit()
        {
            this.UpdateReturnSecurityDepositStatus(10);
            this.browser.Document.InvokeScript("showReturnDeposit");
        }

        private void UpdateReturnSecurityDepositStart()
        {
            this.startLoading();
        }

        private void UpdateReturnSecurityDepositEnd()
        {
            this.doneLoading();
        }

        private bool updateBookingStatus(BookingStatus bookingStatus, long bookingId)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.Booking b = new KioskBusinessEntity.Database_Entity.Booking();
            b.bookingId = bookingId;
            b.propertyId = this.kioskData.propertyId;
            b.bookingStatusCode = bookingStatus.ToString();
            b.userId = this.kioskData.kioskUserId;
            try
            {
                if (!this.isOffline)
                    flag = UpdateService.UpdateBookingStatus(b);
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.bBL.Insert(b);
                }
                catch
                {
                    throw;
                }
            }
            return flag;
        }

        private bool saveGuest(GuestDetail guest, RoomStayData currentRoom, BookingData booking)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.Guest g = new KioskBusinessEntity.Database_Entity.Guest();
            g.Id = guest.guestId;
            g.firstName = guest.firstName;
            g.lastName = guest.lastName;
            g.propertyId = this.kioskData.propertyId;
            g.roomStayId = currentRoom.roomStayId;
            g.dateOfBirth = guest.dateOfBirth;
            g.nationality = guest.nationality;
            g.passportNumber = guest.passportNumber;
            g.userId = this.kioskData.kioskUserId;
            g.address = guest.address;
            g.company = guest.company;
            g.email = guest.email;
            g.gender = guest.gender;
            g.guestCategoryCode = guest.guestCategoryCode;
            g.phone = guest.phone;
            g.postalCode = guest.postalCode;
            g.bookingId = booking.bookingId;
            g.occupation = guest.occupation;
            g.prevDestination = guest.prevDestination;
            g.nextDestination = guest.nextDestination;
            try
            {
                if (!this.isOffline)
                {
                    g.Id = UpdateService.GuestCheckIn(g);
                    this.currGuestDetail.guestId = g.Id;
                    flag = g.Id > 0L;
                }
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.currGuestDetail.guestId = -1L;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.gBL.Insert(g);
                }
                catch
                {
                    throw;
                }
            }
            if (!flag)
                throw new Exception(string.Format("Failed to save Guest, Booking Order Code: {0}.", (object)booking.bookingOrderCode));
            return flag;
        }

        private bool checkInGuestCard(long bookingId, RoomBedDetail roomBed)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.RoomGuestCard rgc = new KioskBusinessEntity.Database_Entity.RoomGuestCard();
            rgc.cardUID = roomBed.card.UID;
            rgc.bookingId = bookingId;
            rgc.startDate = roomBed.card.startDate;
            rgc.endDate = roomBed.card.endDate;
            rgc.issueDate = DateTime.Now;
            rgc.propertyId = this.kioskData.propertyId;
            rgc.bedId = roomBed.card.bedId;
            rgc.roomId = roomBed.card.roomId;
            rgc.lockerId = roomBed.card.lockerId;
            rgc.doorId = roomBed.card.doorId;
            rgc.roomStayId = roomBed.card.roomStayId;
            rgc.userId = this.kioskData.kioskUserId;
            if (roomBed.guestList.Count > 0)
                rgc.guestId = roomBed.guestList[0].guestId;
            try
            {
                if (!this.isOffline)
                {
                    if (rgc.guestId > 0L)
                        flag = UpdateService.CheckInGuestCard(rgc);
                }
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.rgcBL.Insert(rgc);
                }
                catch
                {
                    throw;
                }
            }
            if (!flag)
                throw new Exception(string.Format("Failed to save Guest Card on Check In, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
            return flag;
        }

        private bool checkOutGuestCard(string cardUID, long bookingId)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.RoomGuestCard rgc = new KioskBusinessEntity.Database_Entity.RoomGuestCard();
            rgc.cardUID = cardUID;
            rgc.returnDate = DateTime.Now;
            rgc.userId = this.kioskData.kioskUserId;
            rgc.cardNo = string.Empty;
            try
            {
                if (!this.isOffline)
                    flag = UpdateService.CheckOutGuestCard(rgc);
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.rgcBL.Insert(rgc);
                }
                catch
                {
                    throw;
                }
            }
            if (!flag)
                throw new Exception(string.Format("Failed to return Guest Card on Check Out, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
            return flag;
        }

        private bool paySecurityDeposit(BookingData bookingRes, string invoiceNumber, string currencyCode, Decimal amountDeposited, long shiftId)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.SecurityDeposit sd = new KioskBusinessEntity.Database_Entity.SecurityDeposit();
            sd.bookingId = bookingRes.bookingId;
            sd.amount = amountDeposited;
            sd.currencyCode = currencyCode;
            sd.invoiceNumber = invoiceNumber;
            sd.propertyId = this.kioskData.propertyId;
            sd.roomStayId = bookingRes.roomStayId;
            sd.userId = this.kioskData.kioskUserId;
            sd.date = DateTime.Now;
            sd.shiftId = shiftId;
            KioskBusinessEntity.Database_Entity.CashInOut cashInOut = new KioskBusinessEntity.Database_Entity.CashInOut();
            cashInOut.amount = amountDeposited;
            cashInOut.transactionDate = DateTime.Now;
            cashInOut.paymentModeCode = ShiftCash.CASHIN.ToString();
            cashInOut.remarks = "Pay Security Deposit";
            cashInOut.currencyCode = currencyCode;
            cashInOut.shiftId = shiftId;
            cashInOut.propertyId = this.kioskData.propertyId;
            cashInOut.userId = this.kioskData.kioskUserId;
            try
            {
                if (!this.isOffline)
                {
                    flag = UpdateService.PaySecurityDeposit(sd);
                    if (flag)
                    {
                        ShiftService.CashInOut(cashInOut);
                        cashInOut.IsUploaded = true;
                        this.cashInOutBL.Insert(ref cashInOut);
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.sdBL.Insert(sd);
                    if (flag)
                        this.cashInOutBL.Insert(ref cashInOut);
                }
                catch
                {
                    throw;
                }
            }
            if (!flag)
                throw new Exception(string.Format("Failed to save Security Deposit, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
            return flag;
        }

        private bool returnSecurityDeposit(List<SecurityDepositData> despositList, BookingData bookingRes, string invoiceNumber, string currencyCode, Decimal amountReturned, long shiftId)
        {
            bool flag = false;
            KioskBusinessEntity.Database_Entity.SecurityDeposit sd = new KioskBusinessEntity.Database_Entity.SecurityDeposit();
            sd.bookingId = bookingRes.bookingId;
            sd.amount = amountReturned;
            sd.currencyCode = currencyCode;
            sd.invoiceNumber = invoiceNumber;
            sd.propertyId = this.kioskData.propertyId;
            sd.roomStayId = bookingRes.roomStayId;
            sd.userId = this.kioskData.kioskUserId;
            sd.date = DateTime.Now;
            sd.shiftId = shiftId;
            KioskBusinessEntity.Database_Entity.CashInOut cashInOut = new KioskBusinessEntity.Database_Entity.CashInOut();
            cashInOut.amount = amountReturned;
            cashInOut.transactionDate = DateTime.Now;
            cashInOut.paymentModeCode = ShiftCash.CASHOUT.ToString();
            cashInOut.remarks = "Return Security Deposit";
            cashInOut.currencyCode = currencyCode;
            cashInOut.shiftId = shiftId;
            cashInOut.propertyId = this.kioskData.propertyId;
            cashInOut.userId = this.kioskData.kioskUserId;
            try
            {
                if (!this.isOffline)
                {
                    flag = UpdateService.ReturnSecurityDeposit(sd);
                    if (flag)
                    {
                        ShiftService.CashInOut(cashInOut);
                        cashInOut.IsUploaded = true;
                        this.cashInOutBL.Insert(ref cashInOut);
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    flag = this.sdBL.Insert(sd);
                    if (flag)
                        this.cashInOutBL.Insert(ref cashInOut);
                }
                catch
                {
                    throw;
                }
            }
            if (!flag)
                throw new Exception(string.Format("Failed to return Security Deposit, Booking Order Code: {0}.", (object)this.booking.bookingOrderCode));
            return flag;
        }

        private string updatePayment(List<BookingChargeData> chargeListRes, BookingData bookingRes, string invoiceNumber, string paymentMethodCode, string currencyCode, Decimal amountPaid, long shiftId)
        {
            this.bookingPayment = new KioskBusinessEntity.Database_Entity.BookingPayment();
            this.bookingPayment.bookingId = bookingRes.bookingId;
            this.bookingPayment.amountPaid = amountPaid;
            this.bookingPayment.invoiceNumber = invoiceNumber;
            this.bookingPayment.paymentDateTime = DateTime.Now;
            this.bookingPayment.paymentModeCode = paymentMethodCode;
            this.bookingPayment.currencyCode = currencyCode;
            this.bookingPayment.propertyId = this.kioskData.propertyId;
            this.bookingPayment.userId = this.kioskData.kioskUserId;
            this.bookingPayment.surcharge = this.kioskData.creditCardPerc;
            this.bookingPayment.roomStayId = this.booking.roomStayId;
            this.bookingPayment.shiftId = shiftId;
            try
            {
                if (!this.isOffline)
                {
                    this.bookingPayment.invoiceNumber = UpdateService.UpdatePayment(this.bookingPayment);
                    return this.bookingPayment.invoiceNumber;
                }
            }
            catch (Exception ex)
            {
                if (this.isOfflineEnabled)
                {
                    this.isOffline = true;
                    this.logException(ex);
                }
                else
                    throw;
            }
            if (this.isOffline)
            {
                try
                {
                    this.bpBL.Insert(this.bookingPayment);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return this.bookingPayment.invoiceNumber;
        }

        private bool getStopCollectDeposit(TimeSpan currentTime)
        {
            try
            {
                bool result1 = false;
                KioskBusinessEntity.Database_Entity.Setting s = new KioskBusinessEntity.Database_Entity.Setting();
                s.code = "StopCollDep";
                bool.TryParse(this.sBL.Get(s).value, out result1);
                if (result1)
                    return true;
                s.code = "StopCollDepStartTime";
                TimeSpan result2;
                if (!TimeSpan.TryParse(this.sBL.Get(s).value, out result2))
                    return false;
                s.code = "StopCollDepEndTime";
                TimeSpan result3;
                if (!TimeSpan.TryParse(this.sBL.Get(s).value, out result3))
                    return false;
                return currentTime >= result2 && currentTime <= result3;
            }
            catch
            {
                return false;
            }
        }

        public void btnSignatureClick(object sender, HtmlElementEventArgs e)
        {
            try
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoSaveSignature)
                        return;
                    this.DoSaveSignature = true;
                }
                using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(Regex.Match((string)this.browser.Document.InvokeScript("GetSignatureData"), "data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value)))
                {
                    using (Bitmap bitmap = new Bitmap((Stream)memoryStream))
                    {
                        string signatureImagePath = FileHelper.GetGuestSignatureImagePath(this.booking.bookingOrderCode, this.mainGuest.guestName);
                        bitmap.Save(signatureImagePath, ImageFormat.Png);
                    }
                }
                if (this.bookingPayment != null)
                    new Thread(new ThreadStart(this.createPaymentReceiptPDF))
                    {
                        IsBackground = true
                    }.Start();
                else
                    this.logInfo("No Receipt is created for Booking " + this.booking.bookingOrderCode);
            }
            catch (Exception ex)
            {
                this.exceptionHandling(ex, false);
            }
        }

        private void createPaymentReceiptPDF()
        {
            try
            {
                lock (_5FootWayKiosk.lockAction)
                {
                    if (this.DoGenerateReceipt)
                        return;
                    this.DoGenerateReceipt = true;
                }
                ReceiptPrinting receiptPrinting = new ReceiptPrinting(new List<string>()
        {
          this.kioskData.address1,
          this.kioskData.address2
        });
                this.logInfo("Generating Receipt for Booking " + this.booking.bookingOrderCode + "...");
                receiptPrinting.Print((Control)this, new Action<string>(this.onPaymentReceiptCreated), new Action<Exception>(this.logException), this.generateReceiptPrintingParameter());
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            this.createPaymentReceiptPDF_Finished();
        }

        private ReceiptPrintingParameter generateReceiptPrintingParameter()
        {
            return new ReceiptPrintingParameter()
            {
                FileName = FileHelper.GetReceiptFileName(this.booking.bookingOrderCode, this.mainGuest.guestName),
                KioskData = this.kioskData,
                BookingData = this.booking,
                RoomStayData = this.roomStayList[0],
                ChargeData = this.chargeList,
                GuestData = this.kioskGuestData,
                Transaction = this.transactionDetail,
                InvoiceNumber = this.bookingPayment.invoiceNumber,
                Margins = new Margins(ConstantVariable_App.leftMargin, ConstantVariable_App.rightMargin, ConstantVariable_App.topMargin, ConstantVariable_App.bottomMargin),
                LogoImage = new ReceiptImageLine(),
                SignatureImage = new ReceiptImageLine(),
                BankLogoImage = new ReceiptImageLine()
            };
        }

        private void onPaymentReceiptCreated(string filePath)
        {
            new Thread((ThreadStart)(() => this.queueSendBookingReceiptEmail(filePath)))
            {
                IsBackground = true
            }.Start();
        }

        private void queueSendBookingReceiptEmail(string filePath)
        {
            this.logInfo("Sending Receipt for Booking " + this.booking.bookingOrderCode + "...");
            if (!System.IO.File.Exists(filePath))
                this.logInfo("Receipt for Booking " + this.booking.bookingOrderCode + " does not exist");
            if (this.mainGuest != null && !string.IsNullOrEmpty(this.mainGuest.email))
                this.sendBookingReceiptEmail(this.mainGuest, this.booking, filePath);
            if (string.IsNullOrEmpty(this.kioskData.defaultEmail))
                return;
            this.sendBookingReceiptEmail(new GuestDetail()
            {
                guestName = "Reservation",
                email = this.kioskData.defaultEmail
            }, this.booking, filePath);
        }

        private void sendBookingReceiptEmail(GuestDetail guest, BookingData booking, string filePath)
        {
            try
            {
                List<string> recipients = new List<string>();
                recipients.Add(guest.email);
                List<string> attachmentPathList = new List<string>();
                attachmentPathList.Add(filePath);
                string subject = string.Format("5footway.inn e-Receipt Booking Number {0}", (object)booking.bookingOrderCode);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Dear " + guest.guestName + ",");
                stringBuilder.AppendLine("");
                stringBuilder.AppendLine("Please check your receipt attached with this email.");
                EmailHelper.SendEmail(ConstantVariable_App.mailFrom, recipients, subject, stringBuilder.ToString(), attachmentPathList, false);
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
        }

        private void createPaymentReceiptPDF_Finished()
        {
            if (this.InvokeRequired)
                this.BeginInvoke((Delegate)new Action(this.createPaymentReceiptPDF_Finished));
            else
                this.AllowNavigation(true);
        }

        private void updateRateExperience(HtmlElement anchor)
        {
            string selectedRate = string.Empty;
            try
            {
                if (!(anchor != (HtmlElement)null))
                    return;
                if (anchor.Name == "link_rate_opt_1")
                    selectedRate = "1";
                else if (anchor.Name == "link_rate_opt_2")
                    selectedRate = "2";
                else if (anchor.Name == "link_rate_opt_3")
                    selectedRate = "3";
                else if (anchor.Name == "link_rate_opt_4")
                    selectedRate = "4";
                else if (anchor.Name == "link_rate_opt_5")
                    selectedRate = "5";
                if (string.IsNullOrWhiteSpace(selectedRate))
                    return;
                UpdateService.SaveSurveyResponse(this.surveyResponseBL.GenerateRateExperience(selectedRate, this.mainGuest));
            }
            catch (Exception ex)
            {
                this.logException(ex);
            }
            finally
            {
                this.doneUpdateRateExperience(selectedRate);
            }
        }

        private void doneUpdateRateExperience(string selectedRate)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Delegate)new Action<string>(this.doneUpdateRateExperience), (object)selectedRate);
            }
            else
            {
                this.doneLoading();
                this.AllowNavigation(true);
                this.browser.Document.InvokeScript("rateExperienceSelected", new object[1]
                {
          (object) selectedRate
                });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.browser = new System.Windows.Forms.WebBrowser();
            this.tmrHardwareCheck = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.AllowWebBrowserDrop = false;
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.IsWebBrowserContextMenuEnabled = false;
            this.browser.Location = new System.Drawing.Point(0, 0);
            this.browser.MinimumSize = new System.Drawing.Size(800, 600);
            this.browser.Name = "browser";
            this.browser.ScrollBarsEnabled = false;
            this.browser.Size = new System.Drawing.Size(1123, 733);
            this.browser.TabIndex = 0;
            this.browser.WebBrowserShortcutsEnabled = false;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
            this.browser.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.browser_Navigating);
            // 
            // tmrHardwareCheck
            // 
            this.tmrHardwareCheck.Interval = 300000;
            this.tmrHardwareCheck.Tick += new System.EventHandler(this.tmrHardwareCheck_Tick);
            // 
            // _5FootWayKiosk
            // 
            this.ClientSize = new System.Drawing.Size(1123, 733);
            this.Controls.Add(this.browser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "_5FootWayKiosk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "5FootWay Kioskt";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this._5FootWayKiosk_FormClosing);
            this.Load += new System.EventHandler(this._5FootWayKiosk_Load);
            this.ResumeLayout(false);

        }

        private class OtherParticularData
        {
            public string company { get; set; }

            public string email { get; set; }

            public string address1 { get; set; }

            public string address2 { get; set; }

            public string postalCode { get; set; }

            public string contactNumber { get; set; }

            public string occupation { get; set; }

            public string prevDestination { get; set; }

            public string nextDestination { get; set; }
        }
    }
}
