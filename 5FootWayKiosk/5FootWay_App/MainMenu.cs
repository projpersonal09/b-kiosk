﻿// Decompiled with JetBrains decompiler
// Type: _5FootWay_App.MainMenu
// Assembly: 5FootWay_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BD890B35-0F72-48B7-8608-247BBAA2D043
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\5FootWay_App.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace _5FootWay_App
{
  public class MainMenu : Form
  {
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button btnStartApp;
    private Button btnEditConfig;
        private Label label1;
        private Label lblAppVersion;

    public MainMenu()
    {
      this.InitializeComponent();
      this.lblAppVersion.Text = Program.APP_VERSION_LABEL;
    }

    private void btnEditConfig_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (new Login().ShowDialog() != DialogResult.OK)
        return;
      this.Show();
    }

    private void btnStartApp_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnStartApp = new System.Windows.Forms.Button();
            this.btnEditConfig = new System.Windows.Forms.Button();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnStartApp, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEditConfig, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblAppVersion, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(779, 322);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnStartApp
            // 
            this.btnStartApp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartApp.Location = new System.Drawing.Point(450, 62);
            this.btnStartApp.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartApp.Name = "btnStartApp";
            this.btnStartApp.Size = new System.Drawing.Size(267, 123);
            this.btnStartApp.TabIndex = 0;
            this.btnStartApp.Text = "Start";
            this.btnStartApp.UseVisualStyleBackColor = true;
            this.btnStartApp.Click += new System.EventHandler(this.btnStartApp_Click);
            // 
            // btnEditConfig
            // 
            this.btnEditConfig.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEditConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditConfig.Location = new System.Drawing.Point(61, 62);
            this.btnEditConfig.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditConfig.Name = "btnEditConfig";
            this.btnEditConfig.Size = new System.Drawing.Size(267, 123);
            this.btnEditConfig.TabIndex = 1;
            this.btnEditConfig.Text = "Management Menu";
            this.btnEditConfig.UseVisualStyleBackColor = true;
            this.btnEditConfig.Click += new System.EventHandler(this.btnEditConfig_Click);
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblAppVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppVersion.Location = new System.Drawing.Point(598, 247);
            this.lblAppVersion.Margin = new System.Windows.Forms.Padding(3, 0, 40, 0);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(141, 25);
            this.lblAppVersion.TabIndex = 2;
            this.lblAppVersion.Text = "APP_VERSION";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tags Mode v 1.2.5";
            // 
            // MainMenu
            // 
            this.AcceptButton = this.btnStartApp;
            this.ClientSize = new System.Drawing.Size(779, 322);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Self Service Kiosk";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

    }
  }
}
