using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CardReader
{
	public class MyHotelCard : Card
	{
		public string customerCode
		{
			get;
			set;
		}

		public int hotelDoorId
		{
			get;
			set;
		}

		public int hotelLockerId
		{
			get;
			set;
		}

		public long hotelPropertyId
		{
			get;
			set;
		}

		public int laundryCredit
		{
			get;
			set;
		}

		public MyHotelCard()
		{
		}
	}
}