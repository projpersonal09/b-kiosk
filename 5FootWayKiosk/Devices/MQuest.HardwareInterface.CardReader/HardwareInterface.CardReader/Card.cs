using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CardReader
{
	public abstract class Card
	{
		public DateTime endDate
		{
			get;
			set;
		}

		public DateTime startDate
		{
			get;
			set;
		}

		public string UID
		{
			get;
			set;
		}

		public Card()
		{
			this.startDate = new DateTime();
			this.endDate = new DateTime();
		}
	}
}