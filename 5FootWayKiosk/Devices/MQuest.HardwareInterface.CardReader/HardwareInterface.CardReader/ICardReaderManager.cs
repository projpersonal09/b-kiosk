using System;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardReader
{
	public interface ICardReaderManager
	{
		void Dispose();

		bool FormatCard(CardType cardType);

		bool InitCardReader();

		Card ReadCard();

		bool ResetCard();

		void StartFormatCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType);

		void StartInitCardReaderThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception);

		void StartReadCardThread(Control control, Action<Card> actionSuccess, Action<Exception, bool> exception);

		void StartResetCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception);

		void StartWriteCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType, Card card);

		bool WriteCard(CardType cardType, Card card);
	}
}