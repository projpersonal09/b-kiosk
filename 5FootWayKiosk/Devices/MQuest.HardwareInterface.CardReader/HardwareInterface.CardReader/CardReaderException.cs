using System;

namespace MQuest.HardwareInterface.CardReader
{
	public class CardReaderException : Exception
	{
		public bool showDetail;

		public static CardReaderException ControlMissing;

		static CardReaderException()
		{
			CardReaderException.ControlMissing = new CardReaderException("Control cannot be null");
		}

		public CardReaderException()
		{
		}

		public CardReaderException(string message) : base(message)
		{
		}

		public CardReaderException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}