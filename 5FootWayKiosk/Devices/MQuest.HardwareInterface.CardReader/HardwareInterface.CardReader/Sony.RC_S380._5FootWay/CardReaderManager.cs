using Helper;
using MQuest.HardwareInterface.CardReader;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay
{
	public class CardReaderManager : ICardReaderManager, IDisposable
	{
		private MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay.CardReader reader;

		public CardReaderManager(string customerCode, string libDll)
		{
			if (libDll == CardReaderLibraryDllType.FelicaReader)
			{
				this.reader = new CardReader_RCS380(customerCode);
				return;
			}
			this.reader = new MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay.CardReader(customerCode);
		}

		public void Dispose()
		{
			try
			{
				this.reader.Dispose();
			}
			catch
			{
				throw;
			}
		}

		public bool FormatCard(CardType cardType)
		{
			throw new NotSupportedException();
		}

		private void FormatCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess(this.FormatCard(cardType));
					}
					else
					{
						object[] objArray = new object[] { this.FormatCard(cardType) };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
			}
			catch (CardReaderException cardReaderException1)
			{
				CardReaderException cardReaderException = cardReaderException1;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(cardReaderException, cardReaderException.showDetail);
					}
					else
					{
						object[] objArray1 = new object[] { cardReaderException, cardReaderException.showDetail };
						control.BeginInvoke(exception, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public bool InitCardReader()
		{
			bool flag;
			try
			{
				this.reader.InitReader();
				flag = true;
			}
			catch
			{
				throw;
			}
			return flag;
		}

		private void InitCardReaderThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						this.InitCardReader();
					}
					else
					{
						object[] objArray = new object[] { this.InitCardReader() };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
			}
			catch (CardReaderException cardReaderException1)
			{
				CardReaderException cardReaderException = cardReaderException1;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(cardReaderException, cardReaderException.showDetail);
					}
					else
					{
						object[] objArray1 = new object[] { cardReaderException, cardReaderException.showDetail };
						control.BeginInvoke(exception, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public Card ReadCard()
		{
			Card card;
			try
			{
				card = this.reader.ReadCardData();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return card;
		}

		private void ReadCardThread(Control control, Action<Card> actionSuccess, Action<Exception, bool> exception)
		{
			try
			{
				try
				{
					if (actionSuccess != null)
					{
						if (!control.InvokeRequired)
						{
							actionSuccess(this.ReadCard());
						}
						else
						{
							object[] objArray = new object[] { this.ReadCard() };
							control.BeginInvoke(actionSuccess, objArray);
						}
					}
				}
				catch (CardReaderException cardReaderException1)
				{
					CardReaderException cardReaderException = cardReaderException1;
					if (exception != null)
					{
						if (!control.InvokeRequired)
						{
							exception(cardReaderException, cardReaderException.showDetail);
						}
						else
						{
							object[] objArray1 = new object[] { cardReaderException, cardReaderException.showDetail };
							control.BeginInvoke(exception, objArray1);
						}
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public bool ResetCard()
		{
			bool flag;
			try
			{
				flag = this.reader.ClearCard();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return flag;
		}

		private void ResetCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess(this.ResetCard());
					}
					else
					{
						object[] objArray = new object[] { this.ResetCard() };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
			}
			catch (CardReaderException cardReaderException1)
			{
				CardReaderException cardReaderException = cardReaderException1;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(cardReaderException, cardReaderException.showDetail);
					}
					else
					{
						object[] objArray1 = new object[] { cardReaderException, cardReaderException.showDetail };
						control.BeginInvoke(exception, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public void StartFormatCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType)
		{
			Thread thread = new Thread(() => this.FormatCardThread(control, actionSuccess, exception, cardType))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartInitCardReaderThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception)
		{
			Thread thread = new Thread(() => this.InitCardReaderThread(control, actionSuccess, exception))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartReadCardThread(Control control, Action<Card> actionSuccess, Action<Exception, bool> exception)
		{
			Thread thread = new Thread(() => this.ReadCardThread(control, actionSuccess, exception))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartResetCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception)
		{
			Thread thread = new Thread(() => this.ResetCardThread(control, actionSuccess, exception))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartWriteCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType, Card card)
		{
			Thread thread = new Thread(() => this.WriteCardThread(control, actionSuccess, exception, cardType, card))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public bool WriteCard(CardType cardType, Card card)
		{
			bool flag;
			try
			{
				flag = (!cardType.Equals(CardType.Guest) ? false : this.reader.WriteGuestCard(card));
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return flag;
		}

		private void WriteCardThread(Control control, Action<bool> actionSuccess, Action<Exception, bool> exception, CardType cardType, Card card)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess(this.WriteCard(cardType, card));
					}
					else
					{
						object[] objArray = new object[] { this.WriteCard(cardType, card) };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
			}
			catch (CardReaderException cardReaderException1)
			{
				CardReaderException cardReaderException = cardReaderException1;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(cardReaderException, cardReaderException.showDetail);
					}
					else
					{
						object[] objArray1 = new object[] { cardReaderException, cardReaderException.showDetail };
						control.BeginInvoke(exception, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}
	}
}