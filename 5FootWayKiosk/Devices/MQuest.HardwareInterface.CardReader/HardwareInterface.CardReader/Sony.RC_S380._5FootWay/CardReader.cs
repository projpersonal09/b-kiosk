using MQuest.Devices.SmartCard;
using MQuest.HardwareInterface.CardReader;
using MQuest.Security.CardAccess;
using System;

namespace MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay
{
	public class CardReader : IDisposable
	{
		private HotelReader reader;

		protected CardReader()
		{
		}

		public CardReader(string customerCode)
		{
			if (string.IsNullOrEmpty(customerCode))
			{
				throw new CardReaderException("Customer Code cannot be empty.");
			}
			this.reader = new HotelReader(customerCode);
		}

		public virtual bool ClearCard()
		{
			bool flag = false;
			try
			{
				this.readCard().Return();
				flag = true;
			}
			catch
			{
				throw;
			}
			return flag;
		}

		private MyHotelCard convertHotelCard(HotelCard hotelCard)
		{
			MyHotelCard myHotelCard = new MyHotelCard();
			if (hotelCard != null)
			{
				myHotelCard.UID = BitConverter.ToString(hotelCard.UID);
				if (!string.IsNullOrEmpty(myHotelCard.UID))
				{
					myHotelCard.UID = myHotelCard.UID.Replace("-", "").Replace(" ", "").ToUpper();
				}
				myHotelCard.startDate = hotelCard.ValidityStart;
				myHotelCard.endDate = hotelCard.ValidityEnd;
				myHotelCard.hotelPropertyId = Convert.ToInt64(hotelCard.PropertyID);
				myHotelCard.hotelDoorId = Convert.ToInt32(hotelCard.DoorID);
				myHotelCard.hotelLockerId = Convert.ToInt32(hotelCard.LockerID);
				myHotelCard.laundryCredit = Convert.ToInt32(hotelCard.LaundryCredits);
				myHotelCard.customerCode = hotelCard.CustomerCode;
			}
			return myHotelCard;
		}

		public virtual void Dispose()
		{
			try
			{
				this.reader.Disconnect();
			}
			catch
			{
				throw;
			}
		}

		public virtual void InitReader()
		{
			try
			{
				this.Dispose();
				this.reader.Connect();
				this.reader.PollForCard();
			}
			catch (Exception exception)
			{
				throw new CardReaderException("Failed to connect card reader.", exception);
			}
		}

		private HotelCard readCard()
		{
			HotelCard hotelCard;
			try
			{
				HotelCard hotelCard1 = this.reader.PollForCard();
				if (hotelCard1 == null)
				{
					throw new ReaderException.NoCardDetected();
				}
				hotelCard = hotelCard1;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CardReaderException cardReaderException = new CardReaderException(string.Concat("Failed to read card, ", exception.Message, "."), exception);
				throw cardReaderException;
			}
			return hotelCard;
		}

		public virtual MyHotelCard ReadCardData()
		{
			MyHotelCard myHotelCard;
			try
			{
				myHotelCard = this.convertHotelCard(this.readCard());
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CardReaderException cardReaderException = new CardReaderException(string.Concat("Failed to read card data, ", exception.Message, "."), exception);
				throw cardReaderException;
			}
			return myHotelCard;
		}

		public virtual bool WriteGuestCard(Card card)
		{
			bool flag = false;
			try
			{
				HotelCard hotelCard = this.readCard();
				MyHotelCard myHotelCard = (MyHotelCard)card;
				hotelCard.CardType =Convert.ToByte(3);
				hotelCard.LockerID = (Convert.ToByte(myHotelCard.hotelLockerId));
				hotelCard.DoorID = (Convert.ToByte(myHotelCard.hotelDoorId));
				hotelCard.PropertyID = (Convert.ToByte(myHotelCard.hotelPropertyId));
				hotelCard.LaundryCredits = (Convert.ToByte(myHotelCard.laundryCredit));
				hotelCard.ValidityStart = myHotelCard.startDate;
				hotelCard.ValidityEnd = myHotelCard.endDate;
				hotelCard.Issue();
				flag = true;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new CardReaderException(string.Concat("Failed to write card, ", exception.Message, "."), exception);
			}
			return flag;
		}
	}
}