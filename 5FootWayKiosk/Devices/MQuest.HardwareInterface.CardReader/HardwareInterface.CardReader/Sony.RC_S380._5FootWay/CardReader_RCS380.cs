using FelicaReader.RCS380;
using FiveFootWay.AccessControl;
using FiveFootWay.ActiveX;
using MQuest.Devices.SmartCard;
using MQuest.HardwareInterface.CardReader;
using System;

namespace MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay
{
	public class CardReader_RCS380 : MQuest.HardwareInterface.CardReader.Sony.RC_S380._5FootWay.CardReader, IDisposable
	{
		private string _customerCode;

		private RCS380Reader reader;

		public CardReader_RCS380(string customerCode)
		{
			if (string.IsNullOrEmpty(customerCode))
			{
				throw new CardReaderException("Customer Code cannot be empty.");
			}
			this._customerCode = customerCode;
			this.reader = new RCS380Reader();
		}

		public override bool ClearCard()
		{
			bool flag;
			try
			{
				this.readCard();
				flag = (new CardIssuer()).ReturnCard();
			}
			catch
			{
				throw;
			}
			return flag;
		}

		private MyHotelCard convertHotelCard(byte[] cardUID, FiveFootWay.AccessControl.Card hotelCard)
		{
			MyHotelCard myHotelCard = new MyHotelCard();
			if (hotelCard != null)
			{
				if (cardUID != null && (int)cardUID.Length > 0)
				{
					myHotelCard.UID = BitConverter.ToString(cardUID);
				}
				if (!string.IsNullOrEmpty(myHotelCard.UID))
				{
					myHotelCard.UID = myHotelCard.UID.Replace("-", "").Replace(" ", "").ToUpper();
				}
				myHotelCard.startDate = hotelCard.TimeStart;
				myHotelCard.endDate = hotelCard.TimeEnd;
				myHotelCard.hotelPropertyId = Convert.ToInt64(hotelCard.PropertyID);
				myHotelCard.laundryCredit = Convert.ToInt32(hotelCard.LaundryCredits);
				myHotelCard.customerCode = this._customerCode;
			}
			return myHotelCard;
		}

		public override void Dispose()
		{
			try
			{
				this.reader.Disconnect();
			}
			catch
			{
				throw;
			}
		}

		public override void InitReader()
		{
			byte[] numArray = null;
			try
			{
				this.Dispose();
				this.reader.Connect();
				this.reader.Poll(out numArray);
				if (!this.reader.IsConnected)
				{
					throw new CardReaderException("card reader is not connected.");
				}
			}
			catch (Exception exception)
			{
				throw new CardReaderException("Failed to connect card reader.", exception);
			}
		}

		private FiveFootWay.AccessControl.Card readCard()
		{
			FiveFootWay.AccessControl.Card card;
			try
			{
				FiveFootWay.AccessControl.Card card1 = (new CardIssuer()).ReadCard();
				if (card1 == null)
				{
					throw new ReaderException.NoCardDetected();
				}
				card = card1;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CardReaderException cardReaderException = new CardReaderException(string.Concat("Failed to read card, ", exception.Message, "."), exception);
				throw cardReaderException;
			}
			return card;
		}

		public override MyHotelCard ReadCardData()
		{
			MyHotelCard myHotelCard;
			try
			{
				byte[] numArray = this.readCardUID();
				myHotelCard = this.convertHotelCard(numArray, this.readCard());
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CardReaderException cardReaderException = new CardReaderException(string.Concat("Failed to read card data, ", exception.Message, "."), exception);
				throw cardReaderException;
			}
			return myHotelCard;
		}

		private byte[] readCardUID()
		{
			byte[] uID;
			try
			{
				MifareClassic card = (new CardService()).GetCard();
				if (card == null)
				{
					throw new ServiceException.NoCardDetected();
				}
				uID = card.GetUID();
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				CardReaderException cardReaderException = new CardReaderException(string.Concat("Failed to read card UID, ", exception.Message, "."), exception);
				throw cardReaderException;
			}
			return uID;
		}

		public override bool WriteGuestCard(MQuest.HardwareInterface.CardReader.Card card)
		{
			bool flag;
			try
			{
				this.readCard();
				MyHotelCard myHotelCard = (MyHotelCard)card;
				CardIssuer cardIssuer = new CardIssuer();
				flag = cardIssuer.IssueCustomerCard(Convert.ToByte(myHotelCard.hotelPropertyId), Convert.ToByte(myHotelCard.hotelDoorId), Convert.ToByte(myHotelCard.hotelLockerId), myHotelCard.startDate, myHotelCard.endDate);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new CardReaderException(string.Concat("Failed to write card, ", exception.Message, "."), exception);
			}
			return flag;
		}
	}
}