using System;

namespace MQuest.FaceRecognition
{
	public class FaceRecognitionException : Exception
	{
		public bool showDetail;

		public bool navigateRequired;

		public FaceRecognitionException()
		{
		}

		public FaceRecognitionException(string message) : base(message)
		{
		}

		public FaceRecognitionException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}