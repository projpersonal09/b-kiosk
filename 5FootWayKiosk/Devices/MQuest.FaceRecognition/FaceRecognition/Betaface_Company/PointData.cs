using System;
using System.Runtime.CompilerServices;

namespace MQuest.FaceRecognition.Betaface_Company
{
	public class PointData
	{
		public string pointName
		{
			get;
			set;
		}

		public double positionX
		{
			get;
			set;
		}

		public double positionY
		{
			get;
			set;
		}

		public int type
		{
			get;
			set;
		}

		public PointData()
		{
		}
	}
}