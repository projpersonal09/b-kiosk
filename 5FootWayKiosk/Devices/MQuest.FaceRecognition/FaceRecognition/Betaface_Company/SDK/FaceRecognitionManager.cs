using MQuest.FaceRecognition.Betaface_Company;
using System;
using System.Collections.Generic;

namespace MQuest.FaceRecognition.Betaface_Company.SDK
{
	public class FaceRecognitionManager
	{
		private MQuest.FaceRecognition.Betaface_Company.SDK.FaceRecognition faceRec;

		private List<FaceData> faceList;

		public FaceRecognitionManager()
		{
			try
			{
				this.faceRec = new MQuest.FaceRecognition.Betaface_Company.SDK.FaceRecognition();
				this.ResetFaceList();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public FaceRecognitionManager(string corePath)
		{
			try
			{
				this.faceRec = new MQuest.FaceRecognition.Betaface_Company.SDK.FaceRecognition(corePath);
				this.ResetFaceList();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Dispose()
		{
			try
			{
				this.faceRec.Dispose();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Init()
		{
			try
			{
				this.faceRec.Init(true, true);
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public List<FaceData> LoadImage(string filePath)
		{
			List<FaceData> faceDatas;
			try
			{
				this.faceList.AddRange(this.faceRec.LoadImage(filePath));
				faceDatas = this.faceRec.LoadImage(filePath);
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return faceDatas;
		}

		public List<FaceRecognitionData> RecognizeFace()
		{
			List<FaceRecognitionData> faceRecognitionDatas;
			try
			{
				List<FaceRecognitionData> faceRecognitionDatas1 = new List<FaceRecognitionData>();
				this.faceRec.RecognizeFace(null, null);
				faceRecognitionDatas = faceRecognitionDatas1;
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return faceRecognitionDatas;
		}

		public void ResetFaceList()
		{
			this.faceList = new List<FaceData>();
		}
	}
}