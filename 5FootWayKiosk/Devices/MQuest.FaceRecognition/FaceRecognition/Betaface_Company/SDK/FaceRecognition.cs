using Betaface;
using MQuest.FaceRecognition;
using MQuest.FaceRecognition.Betaface_Company;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MQuest.FaceRecognition.Betaface_Company.SDK
{
	public class FaceRecognition : IDisposable
	{
		private BetafaceFDRE faceSDK;

		private long sdkState;

		private bool bestFace;

		private bool cropFace;

		public FaceRecognition()
		{
			try
			{
				this.faceSDK = new BetafaceFDRE();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public FaceRecognition(string corePath)
		{
			try
			{
				this.faceSDK = new BetafaceFDRE(corePath);
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Dispose()
		{
			try
			{
				if (!this.faceSDK.IsInitialized())
				{
					throw new FaceRecognitionException("Not yet initialized.");
				}
				this.faceSDK.Deinit();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public Guid GenerateFaceUID(long faceId)
		{
			string str = BitConverter.ToString(BitConverter.GetBytes(faceId));
			str = str.Replace("-", "");
			str = str.Insert(8, "-");
			str = str.Insert(13, "-");
			return new Guid(str.Insert(18, "-0000-000000000000"));
		}

		public void Init(bool bestFace, bool cropFace)
		{
			try
			{
				if (!this.faceSDK.IsInitialized())
				{
					if (this.faceSDK.Init())
					{
						throw new FaceRecognitionException("Unable to initialize betaface sdk, please specify CoreDll path or ensure it is within same folder.");
					}
					this.bestFace = bestFace;
					this.cropFace = cropFace;
					this.sdkState = this.faceSDK.GetState();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public List<FaceData> LoadImage(string filePath)
		{
            try
            {
                if (!this.faceSDK.IsInitialized())
                    throw new FaceRecognitionException("Not yet initialized.");
                BetafaceDetectionSettings detectionSettings = new BetafaceDetectionSettings();
                detectionSettings.flags = 4;
                if (this.bestFace)
                {
                    ref BetafaceDetectionSettings local = ref detectionSettings;
                    local.flags = (local.flags | 256);
                }
                detectionSettings.iMaxImageWidthPix = 640;
                detectionSettings.iMaxImageHeightPix = 480;
                detectionSettings.dMinFaceSizeOnImage = 0.05;
                detectionSettings.iMinFaceSizePix = 20;
                detectionSettings.dAngleDegrees = 0.0;
                detectionSettings.dAngleToleranceDegrees = 30.0;
                detectionSettings.dMinDetectionScore = 0.25;
                long faceId;
                int num1 = ((BetafaceFDRE.Delegate_Betaface_LoadImage)this.faceSDK.Betaface_LoadImage).Invoke(this.sdkState, filePath, out faceId);
                int num2;
                try
                {
                    if (num1 != 0)
                        throw new FaceRecognitionException("Failed to load image, error code " + (object)num1);
                    int num3;
                    long num4;
                    int num5 = ((BetafaceFDRE.Delegate_Betaface_DetectFaces)this.faceSDK.Betaface_DetectFaces).Invoke(this.sdkState, faceId, detectionSettings, out num3, out num4);
                    try
                    {
                        if (num5 != 0)
                            throw new FaceRecognitionException("Failed to detect face, error code " + (object)num5);
                        if (num3 == 0)
                            throw new FaceRecognitionException("No face detected.");
                        List<FaceData> faceDataList = new List<FaceData>();
                        for (int index = 0; index < num3; ++index)
                        {
                            long num6;
                            int num7 = ((BetafaceFDRE.Delegate_Betaface_GetFaceInfo)this.faceSDK.Betaface_GetFaceInfo).Invoke(this.sdkState, num4, num3, out num6);
                            try
                            {
                                if (num7 != 0)
                                    throw new FaceRecognitionException("Failed on get face information, error code " + (object)num7);
                                double num8 = 0.0;
                                int num9 = ((BetafaceFDRE.Delegate_Betaface_GetFaceInfoDoubleParam)this.faceSDK.Betaface_GetFaceInfoDoubleParam).Invoke(this.sdkState, num6, 257, out num8);
                                if (num9 != 0)
                                    throw new FaceRecognitionException("Failed to get detection score, error code " + (object)num9);
                                FaceData faceData = new FaceData()
                                {
                                    points = new List<PointData>(),
                                    UID = this.GenerateFaceUID(faceId),
                                    faceQuality = num8
                                };
                                int length = 0;
                                IntPtr zero = IntPtr.Zero;
                                int num10 = ((BetafaceFDRE.Delegate_Betaface_GenerateFaceKey)this.faceSDK.Betaface_GenerateFaceKey).Invoke(this.sdkState, faceId, num6, (int)byte.MaxValue, out zero, out length);
                                if (num10 != 0)
                                    throw new FaceRecognitionException("Failed on generate face keys, error code " + (object)num10);
                                byte[] destination = new byte[length];
                                Marshal.Copy(zero, destination, 0, length);
                                int num11 = ((BetafaceFDRE.Delegate_Betaface_ReleaseFaceKey)this.faceSDK.Betaface_ReleaseFaceKey).Invoke(this.sdkState, ref zero);
                                if (num11 != 0)
                                    throw new FaceRecognitionException("Failed on release face keys, error code " + (object)num11);
                                if (this.cropFace)
                                {
                                    long num12;
                                    long num13;
                                    int num14 = ((BetafaceFDRE.Delegate_Betaface_CropFaceImage)this.faceSDK.Betaface_CropFaceImage).Invoke(this.sdkState, faceId, num6, 0.3, 0.45, true, 100, 135, 1.0, 0, out num12, out num13);
                                    try
                                    {
                                        if (num14 != 0)
                                            throw new FaceRecognitionException("Failed on crop face image, error code " + (object)num14);
                                    }
                                    catch (Exception ex)
                                    {
                                        num2 = ((BetafaceFDRE.Delegate_Betaface_ReleaseFaceInfo)this.faceSDK.Betaface_ReleaseFaceInfo).Invoke(this.sdkState, ref num13);
                                        throw ex;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                num2 = ((BetafaceFDRE.Delegate_Betaface_ReleaseFaceInfo)this.faceSDK.Betaface_ReleaseFaceInfo).Invoke(this.sdkState, ref num6);
                                throw ex;
                            }
                        }
                        return faceDataList;
                    }
                    catch (Exception ex)
                    {
                        num2 = ((BetafaceFDRE.Delegate_Betaface_ReleaseDetectionResult)this.faceSDK.Betaface_ReleaseDetectionResult).Invoke(this.sdkState, ref num4);
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    num2 = ((BetafaceFDRE.Delegate_Betaface_ReleaseImage)this.faceSDK.Betaface_ReleaseImage).Invoke(this.sdkState, ref faceId);
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

		public decimal RecognizeFace(FaceData owner, FaceData target)
		{
			decimal num;
			try
			{
				double num1 = 0;
				if (!this.faceSDK.IsInitialized())
				{
					throw new FaceRecognitionException("Not yet initialized.");
				}
				num = Convert.ToDecimal(num1);
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return num;
		}
	}
}