using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace MQuest.FaceRecognition.Betaface_Company
{
	public class FaceData
	{
		public double angle
		{
			get;
			set;
		}

		public string category
		{
			get;
			set;
		}

		public double faceQuality
		{
			get;
			set;
		}

		public double height
		{
			get;
			set;
		}

		public bool isValid
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		public List<PointData> points
		{
			get;
			set;
		}

		public double positionX
		{
			get;
			set;
		}

		public double positionY
		{
			get;
			set;
		}

		public Guid UID
		{
			get;
			set;
		}

		public double width
		{
			get;
			set;
		}

		public FaceData()
		{
			this.points = new List<PointData>();
		}
	}
}