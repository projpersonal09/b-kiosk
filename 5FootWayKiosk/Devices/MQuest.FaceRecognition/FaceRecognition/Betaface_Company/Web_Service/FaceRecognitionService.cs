using MQuest.FaceRecognition.Betaface_Company;
using MQuest.FaceRecognition.FaceRecognitionWS;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Threading;

namespace MQuest.FaceRecognition.Betaface_Company.Web_Service
{
	public static class FaceRecognitionService
	{
		private const string api_key = "d45fd466-51e2-4701-8da8-04351c872236";

		private const string api_secret = "171e8465-f548-401d-b63b-caf0dc28df5f";

		private const int GET_IMAGE_INFO_MAX_CALL = 65;

		private const int GET_IMAGE_INFO_WAIT_TIME = 1000;

		private const int GET_RECOGNIZE_RESULT_MAX_CALL = 65;

		private const int GET_RECOGNIZE_RESULT_WAIT_TIME = 1000;

		public static void CheckBetaFaceResponseStatus(int responseCode, string responseString)
		{
			if (responseCode.CompareTo(0) < 0)
			{
				throw new Exception(string.Concat(responseString, ", Error Code : ", responseCode));
			}
		}

		public static List<FaceRecognitionData> RecognizeImage(FaceData owner_face, FaceData target_face)
		{
			List<FaceRecognitionData> faceRecognitionDatas = new List<FaceRecognitionData>();
			try
			{
				using (Service1Client service1Client = new Service1Client("FaceRecognitionWSSoapSSL"))
				{
					service1Client.Open();
					if (!service1Client.State.Equals(CommunicationState.Opened))
					{
						throw new Exception("BetaFace recognition service is not available.");
					}
					RecognizeFacesRequest recognizeFacesRequest = new RecognizeFacesRequest()
					{
						api_key = "d45fd466-51e2-4701-8da8-04351c872236",
						api_secret = "171e8465-f548-401d-b63b-caf0dc28df5f",
						faces_uids = owner_face.UID.ToString(),
						targets = target_face.UID.ToString()
					};
					BetafaceRecognizeRequestResponse betafaceRecognizeRequestResponse = service1Client.RecognizeFaces(recognizeFacesRequest);
					FaceRecognitionService.CheckBetaFaceResponseStatus(betafaceRecognizeRequestResponse.int_response, betafaceRecognizeRequestResponse.string_response);
					bool flag = false;
					int num = 1;
					RecognizeResultRequest recognizeResultRequest = new RecognizeResultRequest()
					{
						api_key = "d45fd466-51e2-4701-8da8-04351c872236",
						api_secret = "171e8465-f548-401d-b63b-caf0dc28df5f",
						recognize_uid = betafaceRecognizeRequestResponse.recognize_uid
					};
					do
					{
						BetafaceRecognizeResponse recognizeResult = service1Client.GetRecognizeResult(recognizeResultRequest);
						FaceRecognitionService.CheckBetaFaceResponseStatus(recognizeResult.int_response, recognizeResult.string_response);
						if (!recognizeResult.int_response.Equals(0))
						{
							Thread.Sleep(1000);
							num++;
						}
						else
						{
							FaceRecognizeInfo[] facesMatches = recognizeResult.faces_matches;
							for (int i = 0; i < (int)facesMatches.Length; i++)
							{
								PersonMatchInfo[] personMatchInfoArray = facesMatches[i].matches;
								for (int j = 0; j < (int)personMatchInfoArray.Length; j++)
								{
									PersonMatchInfo personMatchInfo = personMatchInfoArray[j];
									FaceRecognitionData faceRecognitionDatum = new FaceRecognitionData()
									{
										recognitionResult = Convert.ToDecimal(personMatchInfo.confidence) * new decimal(100),
										UID = personMatchInfo.face_uid,
										isMatch = personMatchInfo.is_match
									};
									faceRecognitionDatas.Add(faceRecognitionDatum);
								}
							}
							flag = true;
						}
					}
					while (!flag && num <= 65);
					if (!flag && num > 65)
					{
						throw new Exception("Recognize image timeout.");
					}
				}
			}
			catch
			{
				throw;
			}
			return faceRecognitionDatas;
		}

		public static List<FaceData> UploadImage(string imagePath)
		{
			List<FaceData> faceDatas = new List<FaceData>();
			try
			{
				using (Service1Client service1Client = new Service1Client("FaceRecognitionWSSoapSSL"))
				{
					service1Client.Open();
					if (!service1Client.State.Equals(CommunicationState.Opened))
					{
						throw new Exception("BetaFace recognition service is not available.");
					}
					ImageRequestBinary imageRequestBinary = new ImageRequestBinary()
					{
						api_key = "d45fd466-51e2-4701-8da8-04351c872236",
						api_secret = "171e8465-f548-401d-b63b-caf0dc28df5f",
						detection_flags = "cropface,recognition,propoints,bestface",
						original_filename = Path.GetFileName(imagePath),
						imagefile_data = File.ReadAllBytes(imagePath)
					};
					BetafaceImageResponse betafaceImageResponse = service1Client.UploadNewImage_File(imageRequestBinary);
					FaceRecognitionService.CheckBetaFaceResponseStatus(betafaceImageResponse.int_response, betafaceImageResponse.string_response);
					bool flag = false;
					int num = 1;
					ImageInfoRequestUid imageInfoRequestUid = new ImageInfoRequestUid()
					{
						api_key = "d45fd466-51e2-4701-8da8-04351c872236",
						api_secret = "171e8465-f548-401d-b63b-caf0dc28df5f",
						img_uid = betafaceImageResponse.img_uid
					};
					do
					{
						BetafaceImageInfoResponse imageInfo = service1Client.GetImageInfo(imageInfoRequestUid);
						FaceRecognitionService.CheckBetaFaceResponseStatus(imageInfo.int_response, imageInfo.string_response);
						if (!imageInfo.int_response.Equals(0))
						{
							Thread.Sleep(1000);
							num++;
						}
						else
						{
							FaceInfo[] faceInfoArray = imageInfo.faces;
							for (int i = 0; i < (int)faceInfoArray.Length; i++)
							{
								FaceInfo faceInfo = faceInfoArray[i];
								FaceData faceDatum = new FaceData()
								{
									UID = faceInfo.uid,
									positionX = faceInfo.x,
									positionY = faceInfo.y,
									width = faceInfo.width,
									height = faceInfo.height,
									angle = faceInfo.angle
								};
								PointInfo[] pointInfoArray = faceInfo.points;
								for (int j = 0; j < (int)pointInfoArray.Length; j++)
								{
									PointInfo pointInfo = pointInfoArray[j];
									PointData pointDatum = new PointData()
									{
										pointName = pointInfo.name,
										positionX = pointInfo.x,
										positionY = pointInfo.y,
										type = pointInfo.type
									};
									faceDatum.points.Add(pointDatum);
								}
								faceDatas.Add(faceDatum);
							}
							flag = true;
						}
					}
					while (!flag && num <= 65);
					if (!flag && num > 65)
					{
						throw new Exception("Upload image timeout.");
					}
				}
			}
			catch
			{
				throw;
			}
			return faceDatas;
		}
	}
}