using System;
using System.Runtime.CompilerServices;

namespace MQuest.FaceRecognition.Betaface_Company
{
	public class FaceRecognitionData : FaceData
	{
		public bool isMatch
		{
			get;
			set;
		}

		public decimal recognitionResult
		{
			get;
			set;
		}

		public FaceRecognitionData()
		{
		}
	}
}