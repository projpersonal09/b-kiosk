using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceRecognizeResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceRecognizeResponse : BetafaceResponse
	{
		[OptionalField]
		private FaceRecognizeInfo[] faces_matchesField;

		[OptionalField]
		private Guid recognize_uidField;

		[DataMember]
		public FaceRecognizeInfo[] faces_matches
		{
			get
			{
				return this.faces_matchesField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faces_matchesField, value))
				{
					this.faces_matchesField = value;
					base.RaisePropertyChanged("faces_matches");
				}
			}
		}

		[DataMember]
		public Guid recognize_uid
		{
			get
			{
				return this.recognize_uidField;
			}
			set
			{
				if (!this.recognize_uidField.Equals(value))
				{
					this.recognize_uidField = value;
					base.RaisePropertyChanged("recognize_uid");
				}
			}
		}

		public BetafaceRecognizeResponse()
		{
		}
	}
}