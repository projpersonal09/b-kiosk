using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="ImageRequestBinary", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class ImageRequestBinary : ImageRequestBase
	{
		[OptionalField]
		private byte[] imagefile_dataField;

		[OptionalField]
		private string original_filenameField;

		[DataMember]
		public byte[] imagefile_data
		{
			get
			{
				return this.imagefile_dataField;
			}
			set
			{
				if (!object.ReferenceEquals(this.imagefile_dataField, value))
				{
					this.imagefile_dataField = value;
					base.RaisePropertyChanged("imagefile_data");
				}
			}
		}

		[DataMember]
		public string original_filename
		{
			get
			{
				return this.original_filenameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.original_filenameField, value))
				{
					this.original_filenameField = value;
					base.RaisePropertyChanged("original_filename");
				}
			}
		}

		public ImageRequestBinary()
		{
		}
	}
}