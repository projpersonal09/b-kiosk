using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="PointInfo", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class PointInfo : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private string nameField;

		[OptionalField]
		private int typeField;

		[OptionalField]
		private double xField;

		[OptionalField]
		private double yField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.nameField, value))
				{
					this.nameField = value;
					this.RaisePropertyChanged("name");
				}
			}
		}

		[DataMember]
		public int type
		{
			get
			{
				return this.typeField;
			}
			set
			{
				if (!this.typeField.Equals(value))
				{
					this.typeField = value;
					this.RaisePropertyChanged("type");
				}
			}
		}

		[DataMember]
		public double x
		{
			get
			{
				return this.xField;
			}
			set
			{
				if (!this.xField.Equals(value))
				{
					this.xField = value;
					this.RaisePropertyChanged("x");
				}
			}
		}

		[DataMember]
		public double y
		{
			get
			{
				return this.yField;
			}
			set
			{
				if (!this.yField.Equals(value))
				{
					this.yField = value;
					this.RaisePropertyChanged("y");
				}
			}
		}

		public PointInfo()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}