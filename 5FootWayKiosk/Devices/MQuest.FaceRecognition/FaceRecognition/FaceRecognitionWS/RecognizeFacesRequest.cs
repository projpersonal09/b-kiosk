using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="RecognizeFacesRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class RecognizeFacesRequest : BetafaceRequest
	{
		[OptionalField]
		private string faces_uidsField;

		[OptionalField]
		private string parametersField;

		[OptionalField]
		private string targetsField;

		[DataMember]
		public string faces_uids
		{
			get
			{
				return this.faces_uidsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faces_uidsField, value))
				{
					this.faces_uidsField = value;
					base.RaisePropertyChanged("faces_uids");
				}
			}
		}

		[DataMember]
		public string parameters
		{
			get
			{
				return this.parametersField;
			}
			set
			{
				if (!object.ReferenceEquals(this.parametersField, value))
				{
					this.parametersField = value;
					base.RaisePropertyChanged("parameters");
				}
			}
		}

		[DataMember]
		public string targets
		{
			get
			{
				return this.targetsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.targetsField, value))
				{
					this.targetsField = value;
					base.RaisePropertyChanged("targets");
				}
			}
		}

		public RecognizeFacesRequest()
		{
		}
	}
}