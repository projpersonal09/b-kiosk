using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="SetPersonRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class SetPersonRequest : BetafaceRequest
	{
		[OptionalField]
		private string faces_uidsField;

		[OptionalField]
		private string person_idField;

		[DataMember]
		public string faces_uids
		{
			get
			{
				return this.faces_uidsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faces_uidsField, value))
				{
					this.faces_uidsField = value;
					base.RaisePropertyChanged("faces_uids");
				}
			}
		}

		[DataMember]
		public string person_id
		{
			get
			{
				return this.person_idField;
			}
			set
			{
				if (!object.ReferenceEquals(this.person_idField, value))
				{
					this.person_idField = value;
					base.RaisePropertyChanged("person_id");
				}
			}
		}

		public SetPersonRequest()
		{
		}
	}
}