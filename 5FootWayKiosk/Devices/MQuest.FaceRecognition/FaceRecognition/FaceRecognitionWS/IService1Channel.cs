using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	public interface IService1Channel : IService1, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
	{

	}
}