using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceTransformRequestResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceTransformRequestResponse : BetafaceResponse
	{
		[OptionalField]
		private Guid transform_uidField;

		[DataMember]
		public Guid transform_uid
		{
			get
			{
				return this.transform_uidField;
			}
			set
			{
				if (!this.transform_uidField.Equals(value))
				{
					this.transform_uidField = value;
					base.RaisePropertyChanged("transform_uid");
				}
			}
		}

		public BetafaceTransformRequestResponse()
		{
		}
	}
}