using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="TransformFacesRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class TransformFacesRequest : BetafaceRequest
	{
		[OptionalField]
		private string actionField;

		[OptionalField]
		private string faces_pointsField;

		[OptionalField]
		private string faces_uidsField;

		[OptionalField]
		private string parametersField;

		[DataMember]
		public string action
		{
			get
			{
				return this.actionField;
			}
			set
			{
				if (!object.ReferenceEquals(this.actionField, value))
				{
					this.actionField = value;
					base.RaisePropertyChanged("action");
				}
			}
		}

		[DataMember]
		public string faces_points
		{
			get
			{
				return this.faces_pointsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faces_pointsField, value))
				{
					this.faces_pointsField = value;
					base.RaisePropertyChanged("faces_points");
				}
			}
		}

		[DataMember]
		public string faces_uids
		{
			get
			{
				return this.faces_uidsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faces_uidsField, value))
				{
					this.faces_uidsField = value;
					base.RaisePropertyChanged("faces_uids");
				}
			}
		}

		[DataMember]
		public string parameters
		{
			get
			{
				return this.parametersField;
			}
			set
			{
				if (!object.ReferenceEquals(this.parametersField, value))
				{
					this.parametersField = value;
					base.RaisePropertyChanged("parameters");
				}
			}
		}

		public TransformFacesRequest()
		{
		}
	}
}