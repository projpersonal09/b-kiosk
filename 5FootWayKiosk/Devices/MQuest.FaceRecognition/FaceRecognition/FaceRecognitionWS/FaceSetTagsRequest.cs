using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceSetTagsRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceSetTagsRequest : FaceRequestId
	{
		[OptionalField]
		private TagInfo[] tagsField;

		[DataMember]
		public TagInfo[] tags
		{
			get
			{
				return this.tagsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.tagsField, value))
				{
					this.tagsField = value;
					base.RaisePropertyChanged("tags");
				}
			}
		}

		public FaceSetTagsRequest()
		{
		}
	}
}