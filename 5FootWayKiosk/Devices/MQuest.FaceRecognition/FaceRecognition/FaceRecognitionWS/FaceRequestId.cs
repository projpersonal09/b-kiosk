using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceRequestId", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[KnownType(typeof(FaceSetPointsRequest))]
	[KnownType(typeof(FaceSetTagsRequest))]
	[KnownType(typeof(FaceUpdateInfoRequest))]
	[Serializable]
	public class FaceRequestId : BetafaceRequest
	{
		[OptionalField]
		private Guid face_uidField;

		[DataMember]
		public Guid face_uid
		{
			get
			{
				return this.face_uidField;
			}
			set
			{
				if (!this.face_uidField.Equals(value))
				{
					this.face_uidField = value;
					base.RaisePropertyChanged("face_uid");
				}
			}
		}

		public FaceRequestId()
		{
		}
	}
}