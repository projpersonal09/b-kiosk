using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[KnownType(typeof(BetafaceFaceImageResponse))]
	[KnownType(typeof(BetafaceGetVerifyResultResponse))]
	[KnownType(typeof(BetafaceImageInfoResponse))]
	[KnownType(typeof(BetafaceImageResponse))]
	[KnownType(typeof(BetafaceNewFaceResponse))]
	[KnownType(typeof(BetafaceRecognizeRequestResponse))]
	[KnownType(typeof(BetafaceRecognizeResponse))]
	[KnownType(typeof(BetafaceTransformRequestResponse))]
	[KnownType(typeof(BetafaceTransformResponse))]
	[Serializable]
	public class BetafaceResponse : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private int int_responseField;

		[OptionalField]
		private string string_responseField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public int int_response
		{
			get
			{
				return this.int_responseField;
			}
			set
			{
				if (!this.int_responseField.Equals(value))
				{
					this.int_responseField = value;
					this.RaisePropertyChanged("int_response");
				}
			}
		}

		[DataMember]
		public string string_response
		{
			get
			{
				return this.string_responseField;
			}
			set
			{
				if (!object.ReferenceEquals(this.string_responseField, value))
				{
					this.string_responseField = value;
					this.RaisePropertyChanged("string_response");
				}
			}
		}

		public BetafaceResponse()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}