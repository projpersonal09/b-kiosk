using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceFaceImageResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceFaceImageResponse : BetafaceResponse
	{
		[OptionalField]
		private byte[] face_imageField;

		[OptionalField]
		private FaceInfo face_infoField;

		[OptionalField]
		private Guid uidField;

		[DataMember]
		public byte[] face_image
		{
			get
			{
				return this.face_imageField;
			}
			set
			{
				if (!object.ReferenceEquals(this.face_imageField, value))
				{
					this.face_imageField = value;
					base.RaisePropertyChanged("face_image");
				}
			}
		}

		[DataMember]
		public FaceInfo face_info
		{
			get
			{
				return this.face_infoField;
			}
			set
			{
				if (!object.ReferenceEquals(this.face_infoField, value))
				{
					this.face_infoField = value;
					base.RaisePropertyChanged("face_info");
				}
			}
		}

		[DataMember]
		public Guid uid
		{
			get
			{
				return this.uidField;
			}
			set
			{
				if (!this.uidField.Equals(value))
				{
					this.uidField = value;
					base.RaisePropertyChanged("uid");
				}
			}
		}

		public BetafaceFaceImageResponse()
		{
		}
	}
}