using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="GetVerifyResultRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class GetVerifyResultRequest : BetafaceRequest
	{
		[OptionalField]
		private string verify_stringField;

		[DataMember]
		public string verify_string
		{
			get
			{
				return this.verify_stringField;
			}
			set
			{
				if (!object.ReferenceEquals(this.verify_stringField, value))
				{
					this.verify_stringField = value;
					base.RaisePropertyChanged("verify_string");
				}
			}
		}

		public GetVerifyResultRequest()
		{
		}
	}
}