using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="ImageRequestUrl", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class ImageRequestUrl : ImageRequestBase
	{
		[OptionalField]
		private string image_urlField;

		[OptionalField]
		private string original_filenameField;

		[DataMember]
		public string image_url
		{
			get
			{
				return this.image_urlField;
			}
			set
			{
				if (!object.ReferenceEquals(this.image_urlField, value))
				{
					this.image_urlField = value;
					base.RaisePropertyChanged("image_url");
				}
			}
		}

		[DataMember]
		public string original_filename
		{
			get
			{
				return this.original_filenameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.original_filenameField, value))
				{
					this.original_filenameField = value;
					base.RaisePropertyChanged("original_filename");
				}
			}
		}

		public ImageRequestUrl()
		{
		}
	}
}