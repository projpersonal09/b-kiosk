using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DebuggerStepThrough]
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	public class Service1Client : ClientBase<IService1>, IService1
	{
		public Service1Client()
		{
		}

		public Service1Client(string endpointConfigurationName) : base(endpointConfigurationName)
		{
		}

		public Service1Client(string endpointConfigurationName, string remoteAddress) : base(endpointConfigurationName, remoteAddress)
		{
		}

		public Service1Client(string endpointConfigurationName, EndpointAddress remoteAddress) : base(endpointConfigurationName, remoteAddress)
		{
		}

		public Service1Client(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress)
		{
		}

		public BetafaceResponse FaceInfo_Delete(FaceRequestId face)
		{
			return base.Channel.FaceInfo_Delete(face);
		}

		public Task<BetafaceResponse> FaceInfo_DeleteAsync(FaceRequestId face)
		{
			return base.Channel.FaceInfo_DeleteAsync(face);
		}

		public BetafaceNewFaceResponse FaceInfo_New(FaceNewInfoRequest face_new)
		{
			return base.Channel.FaceInfo_New(face_new);
		}

		public Task<BetafaceNewFaceResponse> FaceInfo_NewAsync(FaceNewInfoRequest face_new)
		{
			return base.Channel.FaceInfo_NewAsync(face_new);
		}

		public BetafaceResponse FaceInfo_SetFaceImagePoints(FaceSetPointsRequest face_points)
		{
			return base.Channel.FaceInfo_SetFaceImagePoints(face_points);
		}

		public Task<BetafaceResponse> FaceInfo_SetFaceImagePointsAsync(FaceSetPointsRequest face_points)
		{
			return base.Channel.FaceInfo_SetFaceImagePointsAsync(face_points);
		}

		public BetafaceResponse FaceInfo_SetPoints(FaceSetPointsRequest face_points)
		{
			return base.Channel.FaceInfo_SetPoints(face_points);
		}

		public Task<BetafaceResponse> FaceInfo_SetPointsAsync(FaceSetPointsRequest face_points)
		{
			return base.Channel.FaceInfo_SetPointsAsync(face_points);
		}

		public BetafaceResponse FaceInfo_SetTags(FaceSetTagsRequest face_tags)
		{
			return base.Channel.FaceInfo_SetTags(face_tags);
		}

		public Task<BetafaceResponse> FaceInfo_SetTagsAsync(FaceSetTagsRequest face_tags)
		{
			return base.Channel.FaceInfo_SetTagsAsync(face_tags);
		}

		public BetafaceResponse FaceInfo_Update(FaceUpdateInfoRequest face_info)
		{
			return base.Channel.FaceInfo_Update(face_info);
		}

		public Task<BetafaceResponse> FaceInfo_UpdateAsync(FaceUpdateInfoRequest face_info)
		{
			return base.Channel.FaceInfo_UpdateAsync(face_info);
		}

		public BetafaceFaceImageResponse GetFaceImage(FaceRequestId face)
		{
			return base.Channel.GetFaceImage(face);
		}

		public Task<BetafaceFaceImageResponse> GetFaceImageAsync(FaceRequestId face)
		{
			return base.Channel.GetFaceImageAsync(face);
		}

		public BetafaceImageInfoResponse GetImageFileInfo(ImageInfoRequestChecksum info)
		{
			return base.Channel.GetImageFileInfo(info);
		}

		public Task<BetafaceImageInfoResponse> GetImageFileInfoAsync(ImageInfoRequestChecksum info)
		{
			return base.Channel.GetImageFileInfoAsync(info);
		}

		public BetafaceImageInfoResponse GetImageInfo(ImageInfoRequestUid info)
		{
			return base.Channel.GetImageInfo(info);
		}

		public Task<BetafaceImageInfoResponse> GetImageInfoAsync(ImageInfoRequestUid info)
		{
			return base.Channel.GetImageInfoAsync(info);
		}

		public void GetOptions()
		{
			base.Channel.GetOptions();
		}

		public Task GetOptionsAsync()
		{
			return base.Channel.GetOptionsAsync();
		}

		public BetafaceRecognizeResponse GetRecognizeResult(RecognizeResultRequest recognize)
		{
			return base.Channel.GetRecognizeResult(recognize);
		}

		public Task<BetafaceRecognizeResponse> GetRecognizeResultAsync(RecognizeResultRequest recognize)
		{
			return base.Channel.GetRecognizeResultAsync(recognize);
		}

		public BetafaceTransformResponse GetTransformResult(TransformResultRequest compare)
		{
			return base.Channel.GetTransformResult(compare);
		}

		public Task<BetafaceTransformResponse> GetTransformResultAsync(TransformResultRequest compare)
		{
			return base.Channel.GetTransformResultAsync(compare);
		}

		public BetafaceGetVerifyResultResponse GetVerifyResult(GetVerifyResultRequest request)
		{
			return base.Channel.GetVerifyResult(request);
		}

		public Task<BetafaceGetVerifyResultResponse> GetVerifyResultAsync(GetVerifyResultRequest request)
		{
			return base.Channel.GetVerifyResultAsync(request);
		}

		public BetafaceRecognizeRequestResponse RecognizeFaces(RecognizeFacesRequest request)
		{
			return base.Channel.RecognizeFaces(request);
		}

		public Task<BetafaceRecognizeRequestResponse> RecognizeFacesAsync(RecognizeFacesRequest request)
		{
			return base.Channel.RecognizeFacesAsync(request);
		}

		public BetafaceResponse SetPerson(SetPersonRequest request)
		{
			return base.Channel.SetPerson(request);
		}

		public Task<BetafaceResponse> SetPersonAsync(SetPersonRequest request)
		{
			return base.Channel.SetPersonAsync(request);
		}

		public BetafaceTransformRequestResponse TransformFaces(TransformFacesRequest request)
		{
			return base.Channel.TransformFaces(request);
		}

		public Task<BetafaceTransformRequestResponse> TransformFacesAsync(TransformFacesRequest request)
		{
			return base.Channel.TransformFacesAsync(request);
		}

		public BetafaceImageResponse UploadImage(ImageRequest img)
		{
			return base.Channel.UploadImage(img);
		}

		public Task<BetafaceImageResponse> UploadImageAsync(ImageRequest img)
		{
			return base.Channel.UploadImageAsync(img);
		}

		public BetafaceImageResponse UploadNewImage_File(ImageRequestBinary img)
		{
			return base.Channel.UploadNewImage_File(img);
		}

		public Task<BetafaceImageResponse> UploadNewImage_FileAsync(ImageRequestBinary img)
		{
			return base.Channel.UploadNewImage_FileAsync(img);
		}

		public BetafaceImageResponse UploadNewImage_Url(ImageRequestUrl img)
		{
			return base.Channel.UploadNewImage_Url(img);
		}

		public Task<BetafaceImageResponse> UploadNewImage_UrlAsync(ImageRequestUrl img)
		{
			return base.Channel.UploadNewImage_UrlAsync(img);
		}
	}
}