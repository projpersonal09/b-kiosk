using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="TagInfo", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class TagInfo : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private double confidenceField;

		[OptionalField]
		private string nameField;

		[OptionalField]
		private string valueField;

		[DataMember]
		public double confidence
		{
			get
			{
				return this.confidenceField;
			}
			set
			{
				if (!this.confidenceField.Equals(value))
				{
					this.confidenceField = value;
					this.RaisePropertyChanged("confidence");
				}
			}
		}

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public string name
		{
			get
			{
				return this.nameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.nameField, value))
				{
					this.nameField = value;
					this.RaisePropertyChanged("name");
				}
			}
		}

		[DataMember]
		public string @value
		{
			get
			{
				return this.valueField;
			}
			set
			{
				if (!object.ReferenceEquals(this.valueField, value))
				{
					this.valueField = value;
					this.RaisePropertyChanged("value");
				}
			}
		}

		public TagInfo()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}