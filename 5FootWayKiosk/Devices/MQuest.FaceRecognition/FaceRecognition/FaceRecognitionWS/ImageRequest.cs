using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="ImageRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class ImageRequest : ImageRequestBase
	{
		[OptionalField]
		private byte[] imageField;

		[OptionalField]
		private string image_base64Field;

		[OptionalField]
		private string original_filenameField;

		[OptionalField]
		private string urlField;

		[DataMember]
		public byte[] image
		{
			get
			{
				return this.imageField;
			}
			set
			{
				if (!object.ReferenceEquals(this.imageField, value))
				{
					this.imageField = value;
					base.RaisePropertyChanged("image");
				}
			}
		}

		[DataMember]
		public string image_base64
		{
			get
			{
				return this.image_base64Field;
			}
			set
			{
				if (!object.ReferenceEquals(this.image_base64Field, value))
				{
					this.image_base64Field = value;
					base.RaisePropertyChanged("image_base64");
				}
			}
		}

		[DataMember]
		public string original_filename
		{
			get
			{
				return this.original_filenameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.original_filenameField, value))
				{
					this.original_filenameField = value;
					base.RaisePropertyChanged("original_filename");
				}
			}
		}

		[DataMember]
		public string url
		{
			get
			{
				return this.urlField;
			}
			set
			{
				if (!object.ReferenceEquals(this.urlField, value))
				{
					this.urlField = value;
					base.RaisePropertyChanged("url");
				}
			}
		}

		public ImageRequest()
		{
		}
	}
}