using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceNewFaceResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceNewFaceResponse : BetafaceResponse
	{
		[OptionalField]
		private Guid uidField;

		[DataMember]
		public Guid uid
		{
			get
			{
				return this.uidField;
			}
			set
			{
				if (!this.uidField.Equals(value))
				{
					this.uidField = value;
					base.RaisePropertyChanged("uid");
				}
			}
		}

		public BetafaceNewFaceResponse()
		{
		}
	}
}