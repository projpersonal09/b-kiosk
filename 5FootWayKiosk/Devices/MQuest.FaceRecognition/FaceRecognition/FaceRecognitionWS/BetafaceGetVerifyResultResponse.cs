using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceGetVerifyResultResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceGetVerifyResultResponse : BetafaceResponse
	{
		[OptionalField]
		private string resultField;

		[OptionalField]
		private string result_dateField;

		[OptionalField]
		private string result_dbgField;

		[OptionalField]
		private string result_stringField;

		[OptionalField]
		private string result_userField;

		[DataMember]
		public string result
		{
			get
			{
				return this.resultField;
			}
			set
			{
				if (!object.ReferenceEquals(this.resultField, value))
				{
					this.resultField = value;
					base.RaisePropertyChanged("result");
				}
			}
		}

		[DataMember]
		public string result_date
		{
			get
			{
				return this.result_dateField;
			}
			set
			{
				if (!object.ReferenceEquals(this.result_dateField, value))
				{
					this.result_dateField = value;
					base.RaisePropertyChanged("result_date");
				}
			}
		}

		[DataMember]
		public string result_dbg
		{
			get
			{
				return this.result_dbgField;
			}
			set
			{
				if (!object.ReferenceEquals(this.result_dbgField, value))
				{
					this.result_dbgField = value;
					base.RaisePropertyChanged("result_dbg");
				}
			}
		}

		[DataMember]
		public string result_string
		{
			get
			{
				return this.result_stringField;
			}
			set
			{
				if (!object.ReferenceEquals(this.result_stringField, value))
				{
					this.result_stringField = value;
					base.RaisePropertyChanged("result_string");
				}
			}
		}

		[DataMember]
		public string result_user
		{
			get
			{
				return this.result_userField;
			}
			set
			{
				if (!object.ReferenceEquals(this.result_userField, value))
				{
					this.result_userField = value;
					base.RaisePropertyChanged("result_user");
				}
			}
		}

		public BetafaceGetVerifyResultResponse()
		{
		}
	}
}