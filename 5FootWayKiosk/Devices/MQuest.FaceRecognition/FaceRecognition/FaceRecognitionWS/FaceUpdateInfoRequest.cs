using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceUpdateInfoRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceUpdateInfoRequest : FaceRequestId
	{
		[OptionalField]
		private FaceInfo faceinfoField;

		[DataMember]
		public FaceInfo faceinfo
		{
			get
			{
				return this.faceinfoField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faceinfoField, value))
				{
					this.faceinfoField = value;
					base.RaisePropertyChanged("faceinfo");
				}
			}
		}

		public FaceUpdateInfoRequest()
		{
		}
	}
}