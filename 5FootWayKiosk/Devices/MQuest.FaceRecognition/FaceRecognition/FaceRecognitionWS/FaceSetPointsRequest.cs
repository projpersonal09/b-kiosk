using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceSetPointsRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceSetPointsRequest : FaceRequestId
	{
		[OptionalField]
		private PointInfo[] pointsField;

		[DataMember]
		public PointInfo[] points
		{
			get
			{
				return this.pointsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.pointsField, value))
				{
					this.pointsField = value;
					base.RaisePropertyChanged("points");
				}
			}
		}

		public FaceSetPointsRequest()
		{
		}
	}
}