using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceInfo", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceInfo : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private double angleField;

		[OptionalField]
		private double heightField;

		[OptionalField]
		private Guid image_uidField;

		[OptionalField]
		private string person_nameField;

		[OptionalField]
		private PointInfo[] pointsField;

		[OptionalField]
		private double scoreField;

		[OptionalField]
		private TagInfo[] tagsField;

		[OptionalField]
		private Guid uidField;

		[OptionalField]
		private PointInfo[] user_pointsField;

		[OptionalField]
		private double widthField;

		[OptionalField]
		private double xField;

		[OptionalField]
		private double yField;

		[DataMember]
		public double angle
		{
			get
			{
				return this.angleField;
			}
			set
			{
				if (!this.angleField.Equals(value))
				{
					this.angleField = value;
					this.RaisePropertyChanged("angle");
				}
			}
		}

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public double height
		{
			get
			{
				return this.heightField;
			}
			set
			{
				if (!this.heightField.Equals(value))
				{
					this.heightField = value;
					this.RaisePropertyChanged("height");
				}
			}
		}

		[DataMember]
		public Guid image_uid
		{
			get
			{
				return this.image_uidField;
			}
			set
			{
				if (!this.image_uidField.Equals(value))
				{
					this.image_uidField = value;
					this.RaisePropertyChanged("image_uid");
				}
			}
		}

		[DataMember]
		public string person_name
		{
			get
			{
				return this.person_nameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.person_nameField, value))
				{
					this.person_nameField = value;
					this.RaisePropertyChanged("person_name");
				}
			}
		}

		[DataMember]
		public PointInfo[] points
		{
			get
			{
				return this.pointsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.pointsField, value))
				{
					this.pointsField = value;
					this.RaisePropertyChanged("points");
				}
			}
		}

		[DataMember]
		public double score
		{
			get
			{
				return this.scoreField;
			}
			set
			{
				if (!this.scoreField.Equals(value))
				{
					this.scoreField = value;
					this.RaisePropertyChanged("score");
				}
			}
		}

		[DataMember]
		public TagInfo[] tags
		{
			get
			{
				return this.tagsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.tagsField, value))
				{
					this.tagsField = value;
					this.RaisePropertyChanged("tags");
				}
			}
		}

		[DataMember]
		public Guid uid
		{
			get
			{
				return this.uidField;
			}
			set
			{
				if (!this.uidField.Equals(value))
				{
					this.uidField = value;
					this.RaisePropertyChanged("uid");
				}
			}
		}

		[DataMember]
		public PointInfo[] user_points
		{
			get
			{
				return this.user_pointsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.user_pointsField, value))
				{
					this.user_pointsField = value;
					this.RaisePropertyChanged("user_points");
				}
			}
		}

		[DataMember]
		public double width
		{
			get
			{
				return this.widthField;
			}
			set
			{
				if (!this.widthField.Equals(value))
				{
					this.widthField = value;
					this.RaisePropertyChanged("width");
				}
			}
		}

		[DataMember]
		public double x
		{
			get
			{
				return this.xField;
			}
			set
			{
				if (!this.xField.Equals(value))
				{
					this.xField = value;
					this.RaisePropertyChanged("x");
				}
			}
		}

		[DataMember]
		public double y
		{
			get
			{
				return this.yField;
			}
			set
			{
				if (!this.yField.Equals(value))
				{
					this.yField = value;
					this.RaisePropertyChanged("y");
				}
			}
		}

		public FaceInfo()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}