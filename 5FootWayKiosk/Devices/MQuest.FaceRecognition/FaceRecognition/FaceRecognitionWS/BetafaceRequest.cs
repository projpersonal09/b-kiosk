using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[KnownType(typeof(FaceNewInfoRequest))]
	[KnownType(typeof(FaceRequestId))]
	[KnownType(typeof(FaceSetPointsRequest))]
	[KnownType(typeof(FaceSetTagsRequest))]
	[KnownType(typeof(FaceUpdateInfoRequest))]
	[KnownType(typeof(GetVerifyResultRequest))]
	[KnownType(typeof(ImageInfoRequestChecksum))]
	[KnownType(typeof(ImageInfoRequestUid))]
	[KnownType(typeof(ImageRequest))]
	[KnownType(typeof(ImageRequestBase))]
	[KnownType(typeof(ImageRequestBinary))]
	[KnownType(typeof(ImageRequestUrl))]
	[KnownType(typeof(RecognizeFacesRequest))]
	[KnownType(typeof(RecognizeResultRequest))]
	[KnownType(typeof(SetPersonRequest))]
	[KnownType(typeof(TransformFacesRequest))]
	[KnownType(typeof(TransformResultRequest))]
	[Serializable]
	public class BetafaceRequest : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private string api_keyField;

		[OptionalField]
		private string api_secretField;

		[DataMember]
		public string api_key
		{
			get
			{
				return this.api_keyField;
			}
			set
			{
				if (!object.ReferenceEquals(this.api_keyField, value))
				{
					this.api_keyField = value;
					this.RaisePropertyChanged("api_key");
				}
			}
		}

		[DataMember]
		public string api_secret
		{
			get
			{
				return this.api_secretField;
			}
			set
			{
				if (!object.ReferenceEquals(this.api_secretField, value))
				{
					this.api_secretField = value;
					this.RaisePropertyChanged("api_secret");
				}
			}
		}

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		public BetafaceRequest()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}