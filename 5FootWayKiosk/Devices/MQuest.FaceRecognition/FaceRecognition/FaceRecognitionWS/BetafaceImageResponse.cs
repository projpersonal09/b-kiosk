using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceImageResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceImageResponse : BetafaceResponse
	{
		[OptionalField]
		private Guid img_uidField;

		[DataMember]
		public Guid img_uid
		{
			get
			{
				return this.img_uidField;
			}
			set
			{
				if (!this.img_uidField.Equals(value))
				{
					this.img_uidField = value;
					base.RaisePropertyChanged("img_uid");
				}
			}
		}

		public BetafaceImageResponse()
		{
		}
	}
}