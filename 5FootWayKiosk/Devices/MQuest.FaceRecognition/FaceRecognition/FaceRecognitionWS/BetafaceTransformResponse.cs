using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceTransformResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceTransformResponse : BetafaceResponse
	{
		[OptionalField]
		private byte[] result_imageField;

		[OptionalField]
		private Guid transform_uidField;

		[DataMember]
		public byte[] result_image
		{
			get
			{
				return this.result_imageField;
			}
			set
			{
				if (!object.ReferenceEquals(this.result_imageField, value))
				{
					this.result_imageField = value;
					base.RaisePropertyChanged("result_image");
				}
			}
		}

		[DataMember]
		public Guid transform_uid
		{
			get
			{
				return this.transform_uidField;
			}
			set
			{
				if (!this.transform_uidField.Equals(value))
				{
					this.transform_uidField = value;
					base.RaisePropertyChanged("transform_uid");
				}
			}
		}

		public BetafaceTransformResponse()
		{
		}
	}
}