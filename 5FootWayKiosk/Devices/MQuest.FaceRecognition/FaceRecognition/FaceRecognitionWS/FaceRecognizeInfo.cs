using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceRecognizeInfo", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceRecognizeInfo : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private Guid face_uidField;

		[OptionalField]
		private PersonMatchInfo[] matchesField;

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public Guid face_uid
		{
			get
			{
				return this.face_uidField;
			}
			set
			{
				if (!this.face_uidField.Equals(value))
				{
					this.face_uidField = value;
					this.RaisePropertyChanged("face_uid");
				}
			}
		}

		[DataMember]
		public PersonMatchInfo[] matches
		{
			get
			{
				return this.matchesField;
			}
			set
			{
				if (!object.ReferenceEquals(this.matchesField, value))
				{
					this.matchesField = value;
					this.RaisePropertyChanged("matches");
				}
			}
		}

		public FaceRecognizeInfo()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}