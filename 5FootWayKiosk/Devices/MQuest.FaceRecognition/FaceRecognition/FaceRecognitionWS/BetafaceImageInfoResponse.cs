using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceImageInfoResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceImageInfoResponse : BetafaceResponse
	{
		[OptionalField]
		private string checksumField;

		[OptionalField]
		private FaceInfo[] facesField;

		[OptionalField]
		private string original_filenameField;

		[OptionalField]
		private Guid uidField;

		[DataMember]
		public string checksum
		{
			get
			{
				return this.checksumField;
			}
			set
			{
				if (!object.ReferenceEquals(this.checksumField, value))
				{
					this.checksumField = value;
					base.RaisePropertyChanged("checksum");
				}
			}
		}

		[DataMember]
		public FaceInfo[] faces
		{
			get
			{
				return this.facesField;
			}
			set
			{
				if (!object.ReferenceEquals(this.facesField, value))
				{
					this.facesField = value;
					base.RaisePropertyChanged("faces");
				}
			}
		}

		[DataMember]
		public string original_filename
		{
			get
			{
				return this.original_filenameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.original_filenameField, value))
				{
					this.original_filenameField = value;
					base.RaisePropertyChanged("original_filename");
				}
			}
		}

		[DataMember]
		public Guid uid
		{
			get
			{
				return this.uidField;
			}
			set
			{
				if (!this.uidField.Equals(value))
				{
					this.uidField = value;
					base.RaisePropertyChanged("uid");
				}
			}
		}

		public BetafaceImageInfoResponse()
		{
		}
	}
}