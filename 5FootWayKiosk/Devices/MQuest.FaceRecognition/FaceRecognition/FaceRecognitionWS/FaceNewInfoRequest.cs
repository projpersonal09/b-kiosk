using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="FaceNewInfoRequest", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class FaceNewInfoRequest : BetafaceRequest
	{
		[OptionalField]
		private FaceInfo faceinfoField;

		[OptionalField]
		private Guid img_uidField;

		[DataMember]
		public FaceInfo faceinfo
		{
			get
			{
				return this.faceinfoField;
			}
			set
			{
				if (!object.ReferenceEquals(this.faceinfoField, value))
				{
					this.faceinfoField = value;
					base.RaisePropertyChanged("faceinfo");
				}
			}
		}

		[DataMember]
		public Guid img_uid
		{
			get
			{
				return this.img_uidField;
			}
			set
			{
				if (!this.img_uidField.Equals(value))
				{
					this.img_uidField = value;
					base.RaisePropertyChanged("img_uid");
				}
			}
		}

		public FaceNewInfoRequest()
		{
		}
	}
}