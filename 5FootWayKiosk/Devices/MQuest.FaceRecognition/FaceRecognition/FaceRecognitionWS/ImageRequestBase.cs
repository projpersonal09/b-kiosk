using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="ImageRequestBase", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[KnownType(typeof(ImageRequest))]
	[KnownType(typeof(ImageRequestBinary))]
	[KnownType(typeof(ImageRequestUrl))]
	[Serializable]
	public class ImageRequestBase : BetafaceRequest
	{
		[OptionalField]
		private string detection_flagsField;

		[DataMember]
		public string detection_flags
		{
			get
			{
				return this.detection_flagsField;
			}
			set
			{
				if (!object.ReferenceEquals(this.detection_flagsField, value))
				{
					this.detection_flagsField = value;
					base.RaisePropertyChanged("detection_flags");
				}
			}
		}

		public ImageRequestBase()
		{
		}
	}
}