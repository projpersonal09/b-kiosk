using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="ImageInfoRequestChecksum", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class ImageInfoRequestChecksum : BetafaceRequest
	{
		[OptionalField]
		private string img_checksumField;

		[DataMember]
		public string img_checksum
		{
			get
			{
				return this.img_checksumField;
			}
			set
			{
				if (!object.ReferenceEquals(this.img_checksumField, value))
				{
					this.img_checksumField = value;
					base.RaisePropertyChanged("img_checksum");
				}
			}
		}

		public ImageInfoRequestChecksum()
		{
		}
	}
}