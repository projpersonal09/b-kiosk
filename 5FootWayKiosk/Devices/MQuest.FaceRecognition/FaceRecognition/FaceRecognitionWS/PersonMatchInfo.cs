using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="PersonMatchInfo", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class PersonMatchInfo : IExtensibleDataObject, INotifyPropertyChanged
	{
		[NonSerialized]
		private ExtensionDataObject extensionDataField;

		[OptionalField]
		private double confidenceField;

		[OptionalField]
		private Guid face_uidField;

		[OptionalField]
		private bool is_matchField;

		[OptionalField]
		private string person_nameField;

		[DataMember]
		public double confidence
		{
			get
			{
				return this.confidenceField;
			}
			set
			{
				if (!this.confidenceField.Equals(value))
				{
					this.confidenceField = value;
					this.RaisePropertyChanged("confidence");
				}
			}
		}

		[Browsable(false)]
		public ExtensionDataObject ExtensionData
		{
			get
			{
				return this.extensionDataField;
			}
			set
			{
				this.extensionDataField = value;
			}
		}

		[DataMember]
		public Guid face_uid
		{
			get
			{
				return this.face_uidField;
			}
			set
			{
				if (!this.face_uidField.Equals(value))
				{
					this.face_uidField = value;
					this.RaisePropertyChanged("face_uid");
				}
			}
		}

		[DataMember]
		public bool is_match
		{
			get
			{
				return this.is_matchField;
			}
			set
			{
				if (!this.is_matchField.Equals(value))
				{
					this.is_matchField = value;
					this.RaisePropertyChanged("is_match");
				}
			}
		}

		[DataMember]
		public string person_name
		{
			get
			{
				return this.person_nameField;
			}
			set
			{
				if (!object.ReferenceEquals(this.person_nameField, value))
				{
					this.person_nameField = value;
					this.RaisePropertyChanged("person_name");
				}
			}
		}

		public PersonMatchInfo()
		{
		}

		protected void RaisePropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
	}
}