using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MQuest.FaceRecognition.FaceRecognitionWS
{
	[DataContract(Name="BetafaceRecognizeRequestResponse", Namespace="")]
	[DebuggerStepThrough]
	[GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
	[Serializable]
	public class BetafaceRecognizeRequestResponse : BetafaceResponse
	{
		[OptionalField]
		private Guid recognize_uidField;

		[DataMember]
		public Guid recognize_uid
		{
			get
			{
				return this.recognize_uidField;
			}
			set
			{
				if (!this.recognize_uidField.Equals(value))
				{
					this.recognize_uidField = value;
					base.RaisePropertyChanged("recognize_uid");
				}
			}
		}

		public BetafaceRecognizeRequestResponse()
		{
		}
	}
}