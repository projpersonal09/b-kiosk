﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.CardPositionCommand
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser
{
  public enum CardPositionCommand
  {
    DONOTHING = 0,
    MM_RETURN_TO_FRONT = 48, // 0x00000030
    MM_RETURN_TO_IC_POS = 49, // 0x00000031
    MM_RETURN_TO_RF_POS = 50, // 0x00000032
    MM_CAPTURE_TO_BOX = 51, // 0x00000033
    MM_EJECT_TO_FRONT = 57, // 0x00000039
  }
}
