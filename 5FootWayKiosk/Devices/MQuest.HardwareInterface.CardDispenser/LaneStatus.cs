﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.LaneStatus
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser
{
  public enum LaneStatus
  {
    CARD_AT_UNSPECIFIC = 0,
    NO_CARD = 48, // 0x00000030
    CARD_AT_GATE = 49, // 0x00000031
    CARD_AT_RF_IC_POS = 50, // 0x00000032
  }
}
