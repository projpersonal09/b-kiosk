using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum RFCardReaderStatus
	{
		Activate = 48,
		Deactivate = 49
	}
}