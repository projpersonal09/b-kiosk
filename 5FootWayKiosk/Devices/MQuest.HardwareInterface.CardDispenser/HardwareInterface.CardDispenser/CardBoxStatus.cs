using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardBoxStatus
	{
		CBS_EMPTY = 48,
		CBS_INSUFFICIENT = 49,
		CBS_ENOUGH = 50
	}
}