using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardPositionCommand
	{
		DONOTHING = 0,
		MM_RETURN_TO_FRONT = 48,
		MM_RETURN_TO_IC_POS = 49,
		MM_RETURN_TO_RF_POS = 50,
		MM_CAPTURE_TO_BOX = 51,
		MM_EJECT_TO_FRONT = 57
	}
}