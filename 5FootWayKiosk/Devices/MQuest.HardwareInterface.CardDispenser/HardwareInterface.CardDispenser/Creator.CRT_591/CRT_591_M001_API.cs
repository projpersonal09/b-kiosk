using MQuest.HardwareInterface.CardDispenser;
using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public class CRT_591_M001_API
	{
		private IntPtr Hdle = IntPtr.Zero;

		private byte address;

		public CRStatus status;

		public SensorDetail sensorDetail;

		public RFCardData rfCardData;

		public CRT_591_M001_API()
		{
			this.address = 0;
			this.status = new CRStatus();
			this.sensorDetail = new SensorDetail();
		}

		public void CloseComm()
		{
			if (this.Hdle != IntPtr.Zero)
			{
				CRT_591_M001.CRT591MRClose(this.Hdle);
				this.Hdle = IntPtr.Zero;
			}
		}

		public bool FreeLibrary(int libraryHandler)
		{
			return CRT_591_M001.FreeLibrary(libraryHandler);
		}

		public void GetCardStatus()
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on GetCardStatus.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 49;
			byte num3 = 48;
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on GetCardStatus, value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "GetCardStatus");
				return;
			}
			this.GetErrorMessage(num6, num7, "GetCardStatus");
		}

		public void GetErrorMessage(byte PmE0, byte PmE1, string from)
		{
			char pmE0 = (char)PmE0;
			char pmE1 = (char)PmE1;
			string str = string.Concat(pmE0.ToString(), pmE1.ToString());
			ErrorCode num = (ErrorCode)Convert.ToInt32(str, 16);
			string[] strArrays = new string[] { "Error : 0x", str, ", ", num.ToString(), " on ", from, "." };
			throw new CardDispenserException(string.Concat(strArrays));
		}

		public void GetErrorMessage(byte PmReplyType, string from)
		{
			CRT_591_M001.RxReplyType pmReplyType = (CRT_591_M001.RxReplyType)PmReplyType;
			object[] objArray = new object[] { "Reply Type : ", PmReplyType, ", ", pmReplyType.ToString(), " on ", from, "." };
			throw new CardDispenserException(string.Concat(objArray));
		}

		public void GetSensorDetail()
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on GetSensorDetail.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 49;
			byte num3 = 49;
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on GetSensorDetail, value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				this.SetSensorDetail(num1, numArray1);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "GetSensorDetail");
				return;
			}
			this.GetErrorMessage(num6, num7, "GetSensorDetail");
		}

		public void Initialize(InitializeMode mode)
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on Initialize.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 48;
			byte num3 = Convert.ToByte(mode);
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on Initialize. value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "Initialize");
				return;
			}
			this.GetErrorMessage(num6, num7, "Initialize");
		}

		public int LoadLibrary(string libraryName)
		{
			return CRT_591_M001.LoadLibrary(libraryName);
		}

		public void MoveCard(CardPositionCommand position)
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on MoveCard.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 50;
			byte num3 = Convert.ToByte(position);
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on MoveCard, value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "MoveCard");
				return;
			}
			this.GetErrorMessage(num6, num7, "MoveCard");
		}

		public void OpenComm(string portName, byte address, BaudRate baudRate = BaudRate.BAUD_RATE_38400)
		{
			this.CloseComm();
			if (!baudRate.Equals(BaudRate.BAUD_RATE_38400))
			{
				this.Hdle = CRT_591_M001.CRT591MROpenWithBaut(portName, Convert.ToUInt16(baudRate));
			}
			else
			{
				this.Hdle = CRT_591_M001.CRT591MROpen(portName);
			}
			if (this.Hdle == IntPtr.Zero)
			{
				object[] objArray = new object[] { portName, " Port with baud rate ", Convert.ToUInt32(baudRate), " is Unavailable." };
				throw new CardDispenserException(string.Concat(objArray));
			}
		}

		public void RFCardOperationControl(RFCardReaderStatus status)
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on ChangeRFCardReaderStatus.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 96;
			byte num3 = Convert.ToByte(status);
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on ChangeRFCardReaderStatus, value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				this.SetContaclessCardData(numArray1);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "RFCardOperationControl");
				return;
			}
			this.GetErrorMessage(num6, num7, "RFCardOperationControl");
		}

		public void SetCardIn(CardInsertPermission permission)
		{
			if (this.Hdle == IntPtr.Zero)
			{
				throw new CardDispenserException("Invalid Handle on SetCardIn.");
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			short num = 0;
			short num1 = 0;
			byte num2 = 51;
			byte num3 = Convert.ToByte(permission);
			byte num4 = 0;
			byte num5 = 0;
			byte num6 = 0;
			byte num7 = 0;
			int num8 = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, num2, num3, num, numArray, ref num4, ref num5, ref num6, ref num7, ref num1, numArray1);
			if (num8 != 0)
			{
				throw new CardDispenserException(string.Concat("Communication Error on SetCardIn, value : ", num8, "."));
			}
			if (num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Success)))
			{
				this.SetCRStatus(num5, num6, num7);
				return;
			}
			if (!num4.Equals(Convert.ToByte(CRT_591_M001.RxReplyType.Fail)))
			{
				this.GetErrorMessage(num4, "SetCardIn");
				return;
			}
			this.GetErrorMessage(num6, num7, "SetCardIn");
		}

		private void SetContaclessCardData(byte[] PmData)
		{
			try
			{
				this.rfCardData = new RFCardData(PmData);
			}
			catch
			{
				this.rfCardData = null;
			}
		}

		public void SetCRStatus(byte PmSt0, byte PmSt1, byte PmSt2)
		{
			this.status.bLaneStatus = PmSt0;
			this.status.bCardBoxStatus = PmSt1;
			this.status.boolCardBinFull = PmSt2 != 48;
		}

		private void SetSensorDetail(short PmDataLen, byte[] PmData)
		{
			this.sensorDetail = new SensorDetail(PmDataLen, PmData);
		}
	}
}