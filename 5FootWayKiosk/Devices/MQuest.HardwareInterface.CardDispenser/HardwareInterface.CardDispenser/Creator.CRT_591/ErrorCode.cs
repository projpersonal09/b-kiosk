using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public enum ErrorCode
	{
		CRT_591_M001_Reception_of_Undefined_Command = 0,
		CRT_591_M001_Command_Parameter_Error = 1,
		CRT_591_M001_Command_Sequence_Error = 2,
		CRT_591_M001_Out_of_Hardware_Support_Command = 3,
		CRT_591_M001_Command_Data_Error = 4,
		CRT_591_M001_IC_Card_Contact_Not_Release = 5,
		CRT_591_M001_Card_Jam = 16,
		CRT_591_M001_sensor_error = 17,
		CRT_591_M001_Too_Long_Card = 18,
		CRT_591_M001_Too_Short_Card = 19,
		CRT_591_M001_Disability_of_Recycling_card = 64,
		CRT_591_M001_Magnet_of_IC_Card_Error = 65,
		CRT_591_M001_Disable_To_Move_Card_To_IC_Card_Position = 67,
		CRT_591_M001_Manually_Move_Card = 69,
		CRT_591_M001_Received_Card_Counter_Overflow = 80,
		CRT_591_M001_Motor_error = 81,
		CRT_591_M001_Short_Circuit_of_IC_Card_Supply_Power = 96,
		CRT_591_M001_Activiation_of_Card_failure = 97,
		CRT_591_M001_Command_Out_of_IC_Card_Support = 98,
		CRT_591_M001_Disablity_of_IC_Card = 101,
		CRT_591_M001_Command_Out_of_IC_Current_Card_Support = 102,
		CRT_591_M001_IC_Card_Transmittion_Error = 103,
		CRT_591_M001_IC_Card_Transmittion_Overtime = 104,
		CRT_591_M001_CPU_SAM_Non_Compliance_To_EMV_Standard = 105,
		CRT_591_M001_Empty_Stacker = 160,
		CRT_591_M001_Error_card_bin_full = 161,
		CRT_591_M001_Not_Reset = 176
	}
}