using Helper;
using MQuest.HardwareInterface.CardDispenser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public class CardDispenserManager : ICardDispenser, IDisposable
	{
		private readonly static int DISPENSE_CARD_TIMEOUT;

		private readonly static int WAIT_TIME;

		private MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser _cardDispenser;

		private MQuest.HardwareInterface.CardDispenser.RFCardData _rfCard;

		private bool _gateIsOpened;

		private MyLog _logFile;

		private readonly static object lockInstance;

		public bool LogOutput
		{
			get;
			set;
		}

		private MQuest.HardwareInterface.CardDispenser.RFCardData RFCardData
		{
			get
			{
				MQuest.HardwareInterface.CardDispenser.RFCardData rFCardDatum;
				lock (CardDispenserManager.lockInstance)
				{
					rFCardDatum = this._rfCard;
				}
				return rFCardDatum;
			}
			set
			{
				lock (CardDispenserManager.lockInstance)
				{
					this._rfCard = value;
				}
			}
		}

		public bool stopThreadLoop
		{
			get;
			set;
		}

		static CardDispenserManager()
		{
			CardDispenserManager.DISPENSE_CARD_TIMEOUT = 60000;
			CardDispenserManager.WAIT_TIME = 1000;
			CardDispenserManager.lockInstance = new object();
		}

		public CardDispenserManager(bool logOutput)
		{
			this._cardDispenser = new MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser();
			this.RFCardData = null;
			this._gateIsOpened = false;
			this.stopThreadLoop = false;
			this.LogOutput = logOutput;
			this._logFile = new MyLog();
		}

		private bool _activateRFCard()
		{
			bool rFCardData;
			try
			{
				this.RFCardData = this._cardDispenser.ActivateRFCard();
				rFCardData = this.RFCardData != null;
			}
			catch
			{
				rFCardData = false;
			}
			return rFCardData;
		}

		public void CardPermission(CardInsertPermission permission)
		{
			if (this._gateIsOpened && permission.Equals(CardInsertPermission.DENIED_INSERTION) || !this._gateIsOpened && permission.Equals(CardInsertPermission.PERMIT_INSERTION))
			{
				this._cardDispenser.InsertCard(permission);
				this._gateIsOpened = !this._gateIsOpened;
			}
		}

		public CardBoxStatus CheckCardBoxStatus()
		{
			return this._cardDispenser.CheckCardBoxStatus();
		}

		public bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition)
		{
			throw new NotSupportedException("Function not tested.");
		}

		private void DispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
		{
			this.RFCardData = null;
			System.Timers.Timer timer = null;
			LaneStatus laneStatus = LaneStatus.CARD_AT_UNSPECIFIC;
			Exception exception1 = null;
			try
			{
				try
				{
					laneStatus = this.GetLaneStatus();
					if (laneStatus == LaneStatus.CARD_AT_GATE)
					{
						this.EjectCard();
					}
					if (this.IsCardBoxEmpty() && this.IsNoCardAtLane())
					{
						throw CardDispenserException.cardBoxEmpty;
					}
					this.stopThreadLoop = false;
					bool flag = false;
					bool flag1 = false;
					timer = new System.Timers.Timer((double)CardDispenserManager.DISPENSE_CARD_TIMEOUT);
					timer.Elapsed += new ElapsedEventHandler((object param0, ElapsedEventArgs param1) => this.dispenseTimerElapsed(ref flag1));
					if (process.Count > 0)
					{
						this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[0]);
					}
					this.logInfo("Card Dispenser: Processing Dispense Card...", true);
					timer.Enabled = true;
					while (!flag && !flag1 && !this.stopThreadLoop)
					{
						if (exception1 != null)
						{
							try
							{
								this._cardDispenser.ResetMachine();
							}
							catch (Exception exception2)
							{
								this.triggerLogException(control, logException, exception2);
							}
							exception1 = null;
						}
						if (this.IsCardBoxEmpty() && this.IsNoCardAtLane())
						{
							throw CardDispenserException.cardBoxEmpty;
						}
						try
						{
							laneStatus = this.GetLaneStatus();
							if (laneStatus != LaneStatus.CARD_AT_RF_IC_POS)
							{
								this.MoveCardToRFPosition();
								Thread.Sleep(CardDispenserManager.WAIT_TIME);
							}
							else if (laneStatus == LaneStatus.CARD_AT_RF_IC_POS)
							{
								for (int i = 0; i < additionalFunctionList.Count; i++)
								{
									this.logInfo(string.Concat("Card Dispenser: Performing function ", additionalFunctionList[i].Method.Name, "..."), false);
									string item = additionalFunctionList[i](control);
									if (!string.IsNullOrEmpty(item))
									{
										this.triggerActionFailureOnDispense(control, actionFailure, cardId);
										throw new CardDispenserException(string.Concat("Process name: ", additionalFunctionList[i].Method.Name, ". Error: ", item));
									}
									if (process.Count > i + 1)
									{
										this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[i + 1]);
									}
								}
								this.MoveCard(ejectPosition);
								if (process.Count > additionalFunctionList.Count + 1)
								{
									this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[additionalFunctionList.Count + 1]);
								}
								this.logInfo("Card Dispenser: Dispense Card is successful", true);
								flag = true;
							}
						}
						catch (Exception exception5)
						{
							Exception exception3 = exception5;
							this.logInfo("Card Dispenser: Dispense Card is failed..", true);
							exception1 = exception3;
							this.triggerLogException(control, logException, exception3);
							if (this.IsCardBinFull())
							{
								throw CardDispenserException.cardBinFull;
							}
							try
							{
								this.MoveCardToBin();
							}
							catch (Exception exception4)
							{
								this.triggerLogException(control, logException, exception4);
							}
							this.logInfo("Card Dispenser: Retry Dispensing Card..", true);
						}
					}
					if (!flag)
					{
						this.logInfo("Card Dispenser: Dispensing is not completed.", true);
						laneStatus = this.GetLaneStatus();
						if (laneStatus.Equals(LaneStatus.CARD_AT_RF_IC_POS))
						{
							this.MoveCardToBin();
						}
						if (flag1)
						{
							throw new Exception("Unable to dispense card. 1 minute Timeout reached.");
						}
					}
					else if (actionComplete != null)
					{
						if (!control.InvokeRequired)
						{
							actionComplete(cardId);
						}
						else
						{
							object[] objArray = new object[] { cardId };
							control.BeginInvoke(actionComplete, objArray);
						}
					}
				}
				catch (Exception exception7)
				{
					Exception exception6 = exception7;
					if (exception != null)
					{
						if (!control.InvokeRequired)
						{
							exception(exception6, false);
						}
						else
						{
							object[] objArray1 = new object[] { exception6, false };
							control.BeginInvoke(exception, objArray1);
						}
					}
				}
			}
			finally
			{
				if (timer != null)
				{
					timer.Enabled = false;
					timer.Dispose();
				}
			}
		}

		private void dispenseTimerElapsed(ref bool timeoutFlag)
		{
			timeoutFlag = true;
		}

		public void Dispose()
		{
			try
			{
				this.stopThreadLoop = true;
				this._cardDispenser.Dispose();
				Thread.Sleep(100);
			}
			catch
			{
				throw;
			}
		}

		public bool EjectCard()
		{
			return this.MoveCard(CardPositionCommand.MM_EJECT_TO_FRONT);
		}

		public LaneStatus GetLaneStatus()
		{
			LaneStatus laneStatus = this._cardDispenser.GetLaneStatus();
			this.logInfo(string.Concat("Card Dispenser: Card Position: ", laneStatus.ToString()), false);
			return laneStatus;
		}

		public MQuest.HardwareInterface.CardDispenser.RFCardData GetRFCardData()
		{
			return MQuest.HardwareInterface.CardDispenser.RFCardData.Clone(this.RFCardData);
		}

		public bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			BaudRate baudRate1 = (BaudRate)Enum.Parse(typeof(BaudRate), string.Concat("BAUD_RATE_", baudRate));
			this._cardDispenser.Connect(portName, baudRate1, address);
			if (!mode.Equals(InitializeMode.INIT_CAPTURE_TO_BOX))
			{
				this._cardDispenser.Init(mode);
			}
			else if (!this.IsCardBinFull())
			{
				this._cardDispenser.Init(mode);
			}
			this._cardDispenser.RFCardOperation(RFReaderStatus);
			return true;
		}

		private void InitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess("Initializing");
					}
					else
					{
						object[] objArray = new object[] { "Initializing" };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
				this.InitDispenser(portName, baudRate, address, mode, RFReaderStatus);
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess("Initialized");
					}
					else
					{
						object[] objArray1 = new object[] { "Initialized" };
						control.BeginInvoke(actionSuccess, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public bool IsCardAt_RF_IC_Pos()
		{
			return this.GetLaneStatus() == LaneStatus.CARD_AT_RF_IC_POS;
		}

		public bool IsCardAtGate()
		{
			return this.GetLaneStatus() == LaneStatus.CARD_AT_GATE;
		}

		public bool IsCardBinFull()
		{
			return this._cardDispenser.IsCardBinFull();
		}

		public bool IsCardBoxEmpty()
		{
			return this._cardDispenser.CheckCardBoxStatus().Equals(CardBoxStatus.CBS_EMPTY);
		}

		public bool IsNoCardAtLane()
		{
			return this.GetLaneStatus() == LaneStatus.NO_CARD;
		}

		private void logInfo(string message, bool force)
		{
			if (force || this.LogOutput)
			{
				this._logFile.Log(MyLog.LogType.Info, message);
			}
		}

		public bool MoveCard(CardPositionCommand position)
		{
			bool flag;
			try
			{
				if (position != CardPositionCommand.DONOTHING)
				{
					this._cardDispenser.MoveCard(position);
				}
				flag = true;
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		public bool MoveCardToBin()
		{
			return this.MoveCard(CardPositionCommand.MM_CAPTURE_TO_BOX);
		}

		public bool MoveCardToFront()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_FRONT);
		}

		public bool MoveCardToICPosition()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_IC_POS);
		}

		public bool MoveCardToRFPosition()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_RF_POS);
		}

		public bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard)
		{
			currentCard = 0;
			throw new NotSupportedException("Function not tested.");
		}

		private void ReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
		{
			Exception exception1 = null;
			LaneStatus laneStatus = LaneStatus.CARD_AT_UNSPECIFIC;
			try
			{
				try
				{
					if (this.IsCardBinFull())
					{
						throw CardDispenserException.cardBinFull;
					}
					this.stopThreadLoop = false;
					int num = 0;
					this.CardPermission(CardInsertPermission.PERMIT_INSERTION);
					if (process.Count > 0)
					{
						this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[0]);
					}
					this.logInfo("Card Dispenser: Processing Return Card...", true);
					while (totalCard > num && !this.stopThreadLoop)
					{
						bool flag = false;
						Exception exception2 = null;
						while (!flag && !this.stopThreadLoop)
						{
							if (exception2 != null)
							{
								try
								{
									this._cardDispenser.ResetMachine();
								}
								catch (Exception exception3)
								{
									this.triggerLogException(control, logException, exception3);
								}
								exception2 = null;
							}
							if (this.IsCardBinFull())
							{
								throw CardDispenserException.cardBinFull;
							}
							try
							{
								laneStatus = this.GetLaneStatus();
								if (laneStatus == LaneStatus.NO_CARD || laneStatus == LaneStatus.CARD_AT_UNSPECIFIC)
								{
									Thread.Sleep(CardDispenserManager.WAIT_TIME);
								}
								else if (laneStatus == LaneStatus.CARD_AT_GATE)
								{
									this.MoveCardToRFPosition();
									Thread.Sleep(CardDispenserManager.WAIT_TIME);
								}
								else if (laneStatus == LaneStatus.CARD_AT_RF_IC_POS)
								{
									for (int i = 0; i < additionalFunctionList.Count; i++)
									{
										this.logInfo(string.Concat("Card Dispenser: Performing function ", additionalFunctionList[i].Method.Name, "..."), false);
										string item = additionalFunctionList[i](control);
										if (!string.IsNullOrEmpty(item))
										{
											this.triggerActionFailureOnReturnCard(control, actionFailure);
											throw new CardDispenserException(string.Concat("Process name: ", additionalFunctionList[i].Method.Name, ". Error: ", item));
										}
										if (process.Count > i + 1)
										{
											this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[i + 1]);
										}
									}
									num++;
									this.MoveCard(successAction);
									if (process.Count > additionalFunctionList.Count + 1)
									{
										this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[additionalFunctionList.Count + 1]);
									}
									this.logInfo("Card Dispenser: Return Card is successful", true);
									flag = true;
								}
							}
							catch (CardDispenserException cardDispenserException)
							{
								this.logInfo("Card Dispenser: Return Card is failed..", true);
								try
								{
									this.MoveCard(failedAction);
								}
								catch (Exception exception4)
								{
									this.triggerLogException(control, logException, exception4);
								}
								throw;
							}
							catch (Exception exception6)
							{
								Exception exception5 = exception6;
								this.logInfo("Card Dispenser: Return Card is failed..", true);
								exception2 = exception5;
								this.logInfo("Card Dispenser: Retry Returning Card..", true);
							}
						}
						if (flag)
						{
							if (actionComplete == null)
							{
								continue;
							}
							if (!control.InvokeRequired)
							{
								actionComplete();
							}
							else
							{
								control.BeginInvoke(actionComplete);
							}
						}
						else
						{
							this.logInfo("Card Dispenser: Returning is not completed.", true);
							laneStatus = this.GetLaneStatus();
							if (!laneStatus.Equals(LaneStatus.CARD_AT_RF_IC_POS))
							{
								continue;
							}
							this.MoveCard(failedAction);
						}
					}
					if (actionAllComplete != null)
					{
						if (!control.InvokeRequired)
						{
							actionAllComplete();
						}
						else
						{
							control.BeginInvoke(actionAllComplete);
						}
					}
				}
				catch (Exception exception7)
				{
					exception1 = exception7;
				}
			}
			finally
			{
				try
				{
					this.CardPermission(CardInsertPermission.DENIED_INSERTION);
				}
				catch (Exception exception9)
				{
					Exception exception8 = exception9;
					exception1 = (exception1 == null ? exception8 : new Exception(exception1.Message, exception1));
					exception1 = new Exception("Failed to close card dispenser gate.", exception1);
				}
				if (exception != null && exception1 != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray);
					}
				}
			}
		}

		public void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
		{
			Thread thread = new Thread(() => this.DispenseCardThread(control, additionalFunctionList, process, actionComplete, actionSuccess, actionFailure, logException, exception, cardId, ejectPosition))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			Thread thread = new Thread(() => this.InitDispenserThread(control, actionSuccess, exception, portName, baudRate, address, mode, RFReaderStatus))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
		{
			Thread thread = new Thread(() => this.ReturnCardThread(control, additionalFunctionList, process, actionComplete, actionAllComplete, actionSuccess, actionFailure, failedAction, successAction, logException, exception, totalCard))
			{
				IsBackground = true
			};
			thread.Start();
		}

		private void triggerActionFailureOnDispense(Control control, Action<string> actionFailure, string cardId)
		{
			if (control != null && actionFailure != null)
			{
				if (control.InvokeRequired)
				{
					object[] objArray = new object[] { cardId };
					control.BeginInvoke(actionFailure, objArray);
					return;
				}
				actionFailure(cardId);
			}
		}

		private void triggerActionFailureOnReturnCard(Control control, Action actionFailure)
		{
			if (control != null && actionFailure != null)
			{
				if (control.InvokeRequired)
				{
					control.BeginInvoke(actionFailure);
					return;
				}
				actionFailure();
			}
		}

		private void triggerActionSuccessOnDispense(Control control, Action<string, int> actionSuccess, string cardId, int processNo)
		{
			if (control != null && actionSuccess != null)
			{
				if (control.InvokeRequired)
				{
					object[] objArray = new object[] { cardId, processNo };
					control.BeginInvoke(actionSuccess, objArray);
					return;
				}
				actionSuccess(cardId, processNo);
			}
		}

		private void triggerActionSuccessOnReturnCard(Control control, Action<int> actionSuccess, int processNo)
		{
			if (control != null && actionSuccess != null)
			{
				if (control.InvokeRequired)
				{
					object[] objArray = new object[] { processNo };
					control.BeginInvoke(actionSuccess, objArray);
					return;
				}
				actionSuccess(processNo);
			}
		}

		private void triggerLogException(Control control, Action<Exception> logException, Exception ex)
		{
			if (control != null && logException != null)
			{
				if (control.InvokeRequired)
				{
					object[] objArray = new object[] { ex };
					control.BeginInvoke(logException, objArray);
					return;
				}
				logException(ex);
			}
		}
	}
}