using System;
using System.Runtime.InteropServices;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public class CRT_591_M001
	{
		public const int PAC_ADDRESS = 1021;

		public CRT_591_M001()
		{
		}

		[DllImport("CRT_591_M001.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int CRT591MRClose(IntPtr ComHandle);

		[DllImport("CRT_591_M001.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern IntPtr CRT591MROpen(string port);

		[DllImport("CRT_591_M001.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern IntPtr CRT591MROpenWithBaut(string port, uint Baudrate);

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern bool FreeLibrary(int libraryHandler);

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int LoadLibrary(string strDllName);

		[DllImport("CRT_591_M001.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int RS232_ExeCommand(IntPtr ComHandle, byte TxAddr, byte TxCmCode, byte TxPmCode, short TxDataLen, byte[] TxData, ref byte RxReplyType, ref byte RxStCode0, ref byte RxStCode1, ref byte RxStCode2, ref short RxDataLen, byte[] RxData);

		public enum RxReplyType
		{
			Cancel_by_Machine = 16,
			Communication_Error = 32,
			Cancel_By_HOST = 48,
			Fail = 78,
			Success = 80
		}

		public enum RxType
		{
			ENQ = 5,
			ACK = 6,
			NAK = 21
		}
	}
}