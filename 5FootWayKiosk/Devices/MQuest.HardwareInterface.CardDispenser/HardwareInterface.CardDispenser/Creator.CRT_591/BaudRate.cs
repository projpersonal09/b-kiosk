using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public enum BaudRate
	{
		BAUD_RATE_9600 = 9600,
		BAUD_RATE_19200 = 19200,
		BAUD_RATE_38400 = 38400,
		BAUD_RATE_57600 = 57600,
		BAUD_RATE_115200 = 115200
	}
}