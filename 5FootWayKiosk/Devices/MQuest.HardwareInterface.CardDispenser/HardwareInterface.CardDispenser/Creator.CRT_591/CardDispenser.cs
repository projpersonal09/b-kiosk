using MQuest.HardwareInterface.CardDispenser;
using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
	public class CardDispenser : IDisposable
	{
		public CRT_591_M001_API api;

		private int libraryHandler;

		private readonly static object lockAction;

		static CardDispenser()
		{
			MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction = new object();
		}

		public CardDispenser()
		{
			this.api = new CRT_591_M001_API();
			this.libraryHandler = this.api.LoadLibrary("CRT_591_M001");
			if (this.libraryHandler == 0)
			{
				throw new CardDispenserException(string.Concat("Failed to load CRT_591_M001.dll, please check in ", Environment.CurrentDirectory));
			}
		}

		public RFCardData ActivateRFCard()
		{
			RFCardData rFCardDatum;
			try
			{
				this.RFCardOperation(RFCardReaderStatus.Activate);
				rFCardDatum = this.api.rfCardData;
			}
			catch
			{
				rFCardDatum = null;
			}
			return rFCardDatum;
		}

		public CardBoxStatus CheckCardBoxStatus()
		{
			CardBoxStatus cardBoxStatu;
			try
			{
				CRStatus status = this.GetStatus();
				if (!status.bCardBoxStatus.Equals(Convert.ToByte(CardBoxStatus.CBS_EMPTY)))
				{
					cardBoxStatu = (!status.bCardBoxStatus.Equals(Convert.ToByte(CardBoxStatus.CBS_ENOUGH)) ? CardBoxStatus.CBS_INSUFFICIENT : CardBoxStatus.CBS_ENOUGH);
				}
				else
				{
					cardBoxStatu = CardBoxStatus.CBS_EMPTY;
				}
			}
			catch
			{
				cardBoxStatu = CardBoxStatus.CBS_INSUFFICIENT;
			}
			return cardBoxStatu;
		}

		public void Connect(string portName, BaudRate baudRate, byte address)
		{
			lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
			{
				this.api.OpenComm(portName, address, baudRate);
			}
		}

		public void Dispose()
		{
			try
			{
				this.api.CloseComm();
				if (this.libraryHandler != 0)
				{
					this.api.FreeLibrary(this.libraryHandler);
				}
			}
			catch
			{
				throw;
			}
		}

		public LaneStatus GetLaneStatus()
		{
			CRStatus status = this.GetStatus();
			if (status.bLaneStatus.Equals(Convert.ToByte(LaneStatus.CARD_AT_GATE)))
			{
				return LaneStatus.CARD_AT_GATE;
			}
			if (status.bLaneStatus.Equals(Convert.ToByte(LaneStatus.NO_CARD)))
			{
				return LaneStatus.NO_CARD;
			}
			if (status.bLaneStatus.Equals(Convert.ToByte(LaneStatus.CARD_AT_RF_IC_POS)))
			{
				return LaneStatus.CARD_AT_RF_IC_POS;
			}
			return LaneStatus.CARD_AT_UNSPECIFIC;
		}

		private SensorDetail GetSensorDetail()
		{
			SensorDetail sensorDetail;
			lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
			{
				this.api.GetSensorDetail();
				sensorDetail = this.api.sensorDetail;
			}
			return sensorDetail;
		}

		private CRStatus GetStatus()
		{
			CRStatus cRStatu;
			lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
			{
				this.api.GetCardStatus();
				cRStatu = this.api.status;
			}
			return cRStatu;
		}

		public void Init(InitializeMode initializeMode)
		{
			lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
			{
				this.api.Initialize(initializeMode);
			}
		}

		public void InsertCard(CardInsertPermission permission)
		{
			try
			{
				lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
				{
					this.api.SetCardIn(permission);
				}
			}
			catch
			{
			}
		}

		public bool IsCardBinFull()
		{
			bool status;
			try
			{
				status = this.GetStatus().boolCardBinFull;
			}
			catch
			{
				status = false;
			}
			return status;
		}

		public void MoveCard(CardPositionCommand cardPosition)
		{
			try
			{
				lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
				{
					this.api.MoveCard(cardPosition);
				}
			}
			catch
			{
			}
		}

		public void ResetMachine()
		{
			this.Init(InitializeMode.INIT_WITHOUT_MOVEMENT);
		}

		public void RFCardOperation(RFCardReaderStatus status)
		{
			try
			{
				lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
				{
					this.api.RFCardOperationControl(status);
				}
			}
			catch
			{
			}
		}
	}
}