using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardReturnPosition
	{
		FROM_OUT = 0,
		FROM_GATE = 49,
		FROM_LANE = 50
	}
}