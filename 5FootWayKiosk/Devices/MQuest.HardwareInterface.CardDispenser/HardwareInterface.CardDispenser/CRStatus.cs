using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CardDispenser
{
	[Serializable]
	public struct CRStatus
	{
		public byte bCardBoxStatus
		{
			get;
			set;
		}

		public byte bLaneStatus
		{
			get;
			set;
		}

		public bool boolCardBinFull
		{
			get;
			set;
		}
	}
}