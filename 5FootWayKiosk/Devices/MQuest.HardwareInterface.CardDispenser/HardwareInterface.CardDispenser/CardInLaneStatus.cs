using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardInLaneStatus
	{
		NO_CARD_INSIDE = 48,
		CARD_INSIDE = 49
	}
}