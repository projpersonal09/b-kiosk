using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public class SensorDetail
	{
		public static int NUM_SENSOR;

		public bool[] isCardExist;

		static SensorDetail()
		{
			SensorDetail.NUM_SENSOR = 7;
		}

		public SensorDetail()
		{
			this.isCardExist = new bool[0];
		}

		public SensorDetail(int numSensor, byte[] sensors)
		{
			this.isCardExist = new bool[numSensor];
			for (int i = 0; i < numSensor; i++)
			{
				this.isCardExist[i] = sensors[i].Equals(48);
			}
		}

		public void Set(byte[] RxData)
		{
		}
	}
}