using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public class CardDispenserException : Exception
	{
		public bool showDetail;

		public static CardDispenserException cardBoxEmpty;

		public static CardDispenserException cardBinFull;

		static CardDispenserException()
		{
			CardDispenserException.cardBoxEmpty = new CardDispenserException("Card Box Empty.");
			CardDispenserException.cardBinFull = new CardDispenserException("Card Bin Full.");
		}

		public CardDispenserException()
		{
		}

		public CardDispenserException(string message) : base(message)
		{
		}

		public CardDispenserException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}