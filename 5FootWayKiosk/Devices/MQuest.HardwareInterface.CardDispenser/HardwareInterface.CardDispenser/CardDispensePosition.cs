using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardDispensePosition
	{
		FROM_BOX = 48,
		FROM_LANE = 50
	}
}