using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser
{
	public interface ICardDispenser
	{
		bool LogOutput
		{
			get;
			set;
		}

		bool stopThreadLoop
		{
			get;
			set;
		}

		void CardPermission(CardInsertPermission permission);

		CardBoxStatus CheckCardBoxStatus();

		bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition);

		void Dispose();

		bool EjectCard();

		LaneStatus GetLaneStatus();

		RFCardData GetRFCardData();

		bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFStatus);

		bool IsCardAt_RF_IC_Pos();

		bool IsCardAtGate();

		bool IsCardBinFull();

		bool IsCardBoxEmpty();

		bool IsNoCardAtLane();

		bool MoveCard(CardPositionCommand position);

		bool MoveCardToBin();

		bool MoveCardToFront();

		bool MoveCardToICPosition();

		bool MoveCardToRFPosition();

		bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard);

		void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition);

		void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus);

		void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard);
	}
}