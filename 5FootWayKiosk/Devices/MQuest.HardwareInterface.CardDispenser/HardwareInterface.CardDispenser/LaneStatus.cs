using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum LaneStatus
	{
		CARD_AT_UNSPECIFIC = 0,
		NO_CARD = 48,
		CARD_AT_GATE = 49,
		CARD_AT_RF_IC_POS = 50
	}
}