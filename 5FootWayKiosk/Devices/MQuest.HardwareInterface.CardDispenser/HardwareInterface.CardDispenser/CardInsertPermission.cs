using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum CardInsertPermission
	{
		PERMIT_INSERTION = 48,
		DENIED_INSERTION = 49
	}
}