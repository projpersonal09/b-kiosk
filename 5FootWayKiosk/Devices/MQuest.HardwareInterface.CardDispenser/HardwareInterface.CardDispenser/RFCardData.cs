using MQuest.EnterpriseLibrary.Utility;
using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CardDispenser
{
	public class RFCardData
	{
		public byte[] ATQA
		{
			get;
			set;
		}

		public byte[] CardUID
		{
			get;
			set;
		}

		public byte RType
		{
			get;
			set;
		}

		public string sCardUID
		{
			get;
			set;
		}

		private RFCardData(byte rType, byte[] atqa, byte[] cardUID)
		{
			this.RType = rType;
			this.ATQA = new byte[(int)atqa.Length];
			Array.Copy(atqa, this.ATQA, (int)atqa.Length);
			this.CardUID = new byte[(int)cardUID.Length];
			Array.Copy(cardUID, this.CardUID, (int)cardUID.Length);
			this.setCardUIDString();
		}

		public RFCardData(byte[] data)
		{
			this.RType = data[0];
			this.ATQA = new byte[2];
			Array.Copy(data, 1, this.ATQA, 0, 2);
			this.CardUID = new byte[data[3]];
			Array.Copy(data, 4, this.CardUID, 0, (int)this.CardUID.Length);
			this.setCardUIDString();
		}

		internal static RFCardData Clone(RFCardData rfCardData)
		{
			if (rfCardData == null)
			{
				return null;
			}
			return new RFCardData(rfCardData.RType, rfCardData.ATQA, rfCardData.CardUID);
		}

		private void setCardUIDString()
		{
			if (this.CardUID == null || (int)this.CardUID.Length == 0)
			{
				this.sCardUID = string.Empty;
				return;
			}
			this.sCardUID = Conversion.ConvertByteArrayToHexString(this.CardUID).Trim().Replace(" ", "");
		}
	}
}