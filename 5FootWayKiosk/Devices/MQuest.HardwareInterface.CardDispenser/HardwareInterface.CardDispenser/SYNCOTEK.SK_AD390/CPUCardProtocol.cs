using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class CPUCardProtocol
	{
		public const byte ICC_PROTOCOL_T0 = 48;

		public const byte ICC_PROTOCOL_T1 = 52;

		public const byte ICC_PROTOCOL_AUTO = 57;

		public CPUCardProtocol()
		{
		}
	}
}