using MQuest.HardwareInterface.CardDispenser;
using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class CardDispenser
	{
		public AD390_API_v2 api;

		public CardDispenser()
		{
			this.api = new AD390_API_v2();
			if (this.api.LoadLibrary("AD_390") == 0)
			{
				throw new CardDispenserException(string.Concat("Failed to load AD_390.dll, please check in ", Environment.CurrentDirectory));
			}
		}

		public CardBoxStatus CheckCardBoxStatus()
		{
			CRStatus status = this.GetStatus();
			if (status.bCardBoxStatus.Equals(Convert.ToByte(CardBoxStatus.CBS_EMPTY)))
			{
				return CardBoxStatus.CBS_EMPTY;
			}
			if (status.bCardBoxStatus.Equals(Convert.ToByte(CardBoxStatus.CBS_ENOUGH)))
			{
				return CardBoxStatus.CBS_ENOUGH;
			}
			return CardBoxStatus.CBS_INSUFFICIENT;
		}

		public CardInLaneStatus CheckCardInLane()
		{
			for (int i = 0; i < SensorDetail.NUM_SENSOR; i++)
			{
				if (i != 4 && this.api.sensorDetail.isCardExist[i])
				{
					return CardInLaneStatus.CARD_INSIDE;
				}
			}
			return CardInLaneStatus.NO_CARD_INSIDE;
		}

		public LaneStatus CheckLaneStatus()
		{
			CRStatus status = this.GetStatus();
			if (status.bLaneStatus.Equals(Convert.ToByte(LaneStatus.CARD_AT_GATE)))
			{
				return LaneStatus.CARD_AT_GATE;
			}
			if (status.bLaneStatus.Equals(Convert.ToByte(LaneStatus.NO_CARD)))
			{
				return LaneStatus.NO_CARD;
			}
			return LaneStatus.CARD_AT_RF_IC_POS;
		}

		public bool Connect(string portName, BaudRate baudRate, byte address)
		{
			ErrorCode errorCode = this.api.OpenComm(portName, baudRate, address);
			bool flag = this.ParseError(errorCode, true);
			if (!flag)
			{
				errorCode = this.api.OpenComm(portName, baudRate, address);
				flag = this.ParseError(errorCode, false);
			}
			return flag;
		}

		public bool Dispose()
		{
			bool flag = this.ParseError(this.api.CloseComm(), true);
			if (!flag)
			{
				flag = this.ParseError(this.api.CloseComm(), false);
			}
			return flag;
		}

		private CRStatus GetStatus()
		{
			if (!this.ParseError(this.api.GetCardStatus(), true))
			{
				this.ParseError(this.api.GetCardStatus(), false);
			}
			return this.api.status;
		}

		public bool Init(InitializeMode initializeMode)
		{
			return this.ParseError(this.api.Initialize(initializeMode), false);
		}

		public bool InsertCard(CardInsertPermission permission)
		{
			ErrorCode errorCode = this.api.SetCardIn(permission);
			bool flag = this.ParseError(errorCode, true);
			if (!flag)
			{
				errorCode = this.api.SetCardIn(permission);
				flag = this.ParseError(errorCode, false);
			}
			return flag;
		}

		public bool IsCardBinFull()
		{
			return this.GetStatus().boolCardBinFull;
		}

		public bool MoveCard(CardPositionCommand cardPosition)
		{
			ErrorCode errorCode = this.api.MoveCard(cardPosition);
			bool flag = this.ParseError(errorCode, true);
			if (!flag)
			{
				errorCode = this.api.MoveCard(cardPosition);
				flag = this.ParseError(errorCode, false);
			}
			return flag;
		}

		public bool ParseError(ErrorCode value, bool isFirst)
		{
			if (value.Equals(ErrorCode.AD390_S_SUCCESS))
			{
				return true;
			}
			if (isFirst)
			{
				if (!value.Equals(ErrorCode.AD390_E_CARD_JAMMED))
				{
					this.Init(InitializeMode.INIT_WITHOUT_MOVEMENT);
				}
				else
				{
					this.Init(InitializeMode.INIT_CAPTURE_TO_BOX);
				}
			}
			if (this.api.status.bCardBoxStatus.Equals(CardBoxStatus.CBS_EMPTY))
			{
				throw CardDispenserException.cardBoxEmpty;
			}
			if (this.api.status.boolCardBinFull)
			{
				throw CardDispenserException.cardBinFull;
			}
			if (!isFirst)
			{
				CardDispenserException cardDispenserException = new CardDispenserException(string.Concat("Card Dispenser Error: ", value.ToString()));
				throw new CardDispenserException("Card Dispenser Error.", cardDispenserException);
			}
			return false;
		}
	}
}