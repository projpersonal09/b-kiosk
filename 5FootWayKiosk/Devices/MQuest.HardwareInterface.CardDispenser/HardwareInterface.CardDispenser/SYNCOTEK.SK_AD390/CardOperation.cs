using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public enum CardOperation
	{
		OPERATION_LENGTH_OVERFLOW = 26368,
		OPERATION_ADDRESS_OVERFLOW = 27392,
		OPERATION_FAILED = 28416,
		OPERATION_SUCCESS = 36864
	}
}