using System;
using System.Runtime.InteropServices;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class AD390
	{
		public AD390()
		{
		}

		[DllImport("AD_390.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int CommClose(uint ComHandle);

		[DllImport("AD_390.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint CommOpen(string port);

		[DllImport("AD_390.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint CommOpenWithBaut(string port, byte Baudrate);

		[DllImport("AD_390.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int ExecuteCommand(uint ComHandle, byte TxAddr, ushort TxDataLen, byte[] TxData, ref ushort RxDataLen, byte[] RxData);

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int LoadLibrary(string strDllName);
	}
}