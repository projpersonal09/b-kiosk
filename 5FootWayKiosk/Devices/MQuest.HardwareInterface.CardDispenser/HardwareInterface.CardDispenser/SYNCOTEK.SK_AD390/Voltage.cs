using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public enum Voltage
	{
		VCC_5V_EMV = 48,
		VCC_5V_ISO7816 = 51,
		VCC_3V_ISO7816 = 53
	}
}