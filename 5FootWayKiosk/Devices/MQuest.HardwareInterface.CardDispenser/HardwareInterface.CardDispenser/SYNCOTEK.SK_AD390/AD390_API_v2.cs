using MQuest.HardwareInterface.CardDispenser;
using System;
using System.Globalization;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class AD390_API_v2
	{
		private uint Hdle;

		private byte address;

		public CRStatus status;

		public SensorDetail sensorDetail;

		public AD390_API_v2()
		{
			this.address = 0;
			this.status = new CRStatus();
			this.sensorDetail = new SensorDetail();
		}

		public ErrorCode CloseComm()
		{
			if (this.Hdle != 0)
			{
				AD390.CommClose(this.Hdle);
				this.Hdle = 0;
			}
			return ErrorCode.AD390_S_SUCCESS;
		}

		public ErrorCode CPUColdReset(Voltage voltage)
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] num = new byte[1024];
			byte[] numArray = new byte[1024];
			num[0] = 67;
			num[1] = 81;
			num[2] = 48;
			ushort num1 = 4;
			ushort num2 = 0;
			num[3] = Convert.ToByte(voltage);
			if (AD390.ExecuteCommand(this.Hdle, this.address, num1, num, ref num2, numArray) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray[0] == 80)
			{
				this.SetCRStatus(numArray);
				return ErrorCode.AD390_S_SUCCESS;
			}
			if (numArray[0] == 78 && num2 > 7)
			{
				return ErrorCode.AD390_S_SUCCESS;
			}
			return this.GetErrorCode(numArray);
		}

		public ErrorCode CPUWarmReset()
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			numArray[0] = 67;
			numArray[1] = 81;
			numArray[2] = 48;
			ushort num = 3;
			ushort num1 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num, numArray, ref num1, numArray1) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray1[0] == 80)
			{
				this.SetCRStatus(numArray1);
				return ErrorCode.AD390_S_SUCCESS;
			}
			if (numArray1[0] == 78 && num1 > 7)
			{
				return ErrorCode.AD390_S_SUCCESS;
			}
			return this.GetErrorCode(numArray1);
		}

		public ErrorCode GetCardStatus()
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			numArray[0] = 67;
			numArray[1] = 49;
			numArray[2] = 48;
			ushort num = 3;
			ushort num1 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num, numArray, ref num1, numArray1) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray1[0] != 80)
			{
				return this.GetErrorCode(numArray1);
			}
			this.SetCRStatus(numArray1);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public ErrorCode GetErrorCode(byte[] RxData)
		{
			ErrorCode errorCode;
			string str = ((char)RxData[3]).ToString();
			char rxData = (char)RxData[4];
			string str1 = string.Concat(str, rxData.ToString());
			if (Enum.TryParse<ErrorCode>(Convert.ToInt32(string.Concat("4E", str1), 16).ToString(), out errorCode))
			{
				return errorCode;
			}
			return (ErrorCode)Convert.ToInt32(str1);
		}

		public ErrorCode GetSensorDetail()
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			numArray[0] = 67;
			numArray[1] = 49;
			numArray[2] = 49;
			ushort num = 3;
			ushort num1 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num, numArray, ref num1, numArray1) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray1[0] != 80)
			{
				return this.GetErrorCode(numArray1);
			}
			this.SetCRStatus(numArray1);
			this.sensorDetail.Set(numArray1);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public ErrorCode Initialize(InitializeMode mode)
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] num = new byte[1024];
			byte[] numArray = new byte[1024];
			num[0] = 67;
			num[1] = 48;
			num[2] = Convert.ToByte(mode);
			ushort num1 = 3;
			ushort num2 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num1, num, ref num2, numArray) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray[0] != 80)
			{
				return this.GetErrorCode(numArray);
			}
			this.SetCRStatus(numArray);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public int LoadLibrary(string libraryName)
		{
			return AD390.LoadLibrary(libraryName);
		}

		public ErrorCode MoveCard(CardPositionCommand position)
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] num = new byte[1024];
			byte[] numArray = new byte[1024];
			num[0] = 67;
			num[1] = 50;
			num[2] = Convert.ToByte(position);
			ushort num1 = 3;
			ushort num2 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num1, num, ref num2, numArray) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray[0] != 80)
			{
				return this.GetErrorCode(numArray);
			}
			this.SetCRStatus(numArray);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public ErrorCode OpenComm(string portName, BaudRate baudRate, byte address)
		{
			if (this.Hdle != 0)
			{
				AD390.CommClose(this.Hdle);
				this.Hdle = 0;
			}
			this.Hdle = AD390.CommOpenWithBaut(portName, Convert.ToByte(baudRate));
			if (this.Hdle != 0)
			{
				return ErrorCode.AD390_S_SUCCESS;
			}
			return ErrorCode.AD390_E_PORT_UNAVAILABLE;
		}

		public ErrorCode SendAPDU(string APDU)
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] numArray = new byte[1024];
			byte[] numArray1 = new byte[1024];
			numArray[0] = 67;
			numArray[1] = 81;
			numArray[2] = 57;
			ushort length = (ushort)(APDU.Length / 2 + 3);
			ushort num = 0;
			for (int i = 0; i < APDU.Length / 2; i++)
			{
				numArray[i + 3] = byte.Parse(APDU.Substring(i * 2, 2), NumberStyles.HexNumber);
			}
			if (AD390.ExecuteCommand(this.Hdle, this.address, length, numArray, ref num, numArray1) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray1[0] != 80)
			{
				return this.GetErrorCode(numArray1);
			}
			this.SetCRStatus(numArray1);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public ErrorCode SetCardIn(CardInsertPermission permission)
		{
			if (this.Hdle == 0)
			{
				return ErrorCode.AD390_E_INVALID_HANDLE;
			}
			byte[] num = new byte[1024];
			byte[] numArray = new byte[1024];
			num[0] = 67;
			num[1] = 51;
			num[2] = Convert.ToByte(permission);
			ushort num1 = 3;
			ushort num2 = 0;
			if (AD390.ExecuteCommand(this.Hdle, this.address, num1, num, ref num2, numArray) != 0)
			{
				return ErrorCode.AD390_E_COMM_ERROR;
			}
			if (numArray[0] != 80)
			{
				return this.GetErrorCode(numArray);
			}
			this.SetCRStatus(numArray);
			return ErrorCode.AD390_S_SUCCESS;
		}

		public void SetCRStatus(byte[] RxData)
		{
			this.status.bLaneStatus = RxData[3];
			this.status.bCardBoxStatus = RxData[4];
			this.status.boolCardBinFull = !RxData[5].Equals(48);
		}
	}
}