using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class ContactlessCardProtocol
	{
		public const byte RFC_PROTOCOL_NONE = 48;

		public const byte RFC_PROTOCOL_TYPE_A = 65;

		public const byte RFC_PROTOCOL_TYPE_B = 66;

		public ContactlessCardProtocol()
		{
		}
	}
}