using Helper;
using MQuest.HardwareInterface.CardDispenser;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public class CardDispenserManager : ICardDispenser, IDisposable
	{
		private const int WAIT_TIME = 800;

		private const int PROCESS_TIME = 200;

		private MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenser dispenser;

		private MyLog _logFile;

		public bool LogOutput
		{
			get;
			set;
		}

		public bool stopThreadLoop
		{
			get;
			set;
		}

		public CardDispenserManager(MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenser dispenser, bool logOutput)
		{
			this.dispenser = dispenser;
			this.LogOutput = logOutput;
			this._logFile = new MyLog();
		}

		public void CardPermission(CardInsertPermission permission)
		{
			this.dispenser.InsertCard(permission);
		}

		public CardBoxStatus CheckCardBoxStatus()
		{
			return this.dispenser.CheckCardBoxStatus();
		}

		public bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition)
		{
			throw new NotSupportedException("Function not tested.");
		}

		private void DispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
		{
			try
			{
				if (this.CheckCardBoxStatus().Equals(CardBoxStatus.CBS_EMPTY))
				{
					throw CardDispenserException.cardBoxEmpty;
				}
				if (actionSuccess != null && process.Count > 0)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess(cardId, process[0]);
					}
					else
					{
						object[] objArray = new object[] { cardId, process[0] };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
				bool flag = false;
				this.stopThreadLoop = false;
				while (!flag && !this.stopThreadLoop)
				{
					Thread.Sleep(200);
					this.GetLaneStatus();
					if (additionalFunctionList.Count > 0)
					{
						this.MoveCardToRFPosition();
						Thread.Sleep(800);
					}
					bool flag1 = true;
					for (int i = 0; i < additionalFunctionList.Count; i++)
					{
						string item = additionalFunctionList[i](control);
						if (!string.IsNullOrEmpty(item))
						{
							flag1 = false;
							if (actionFailure != null)
							{
								if (!control.InvokeRequired)
								{
									actionFailure(cardId);
								}
								else
								{
									object[] objArray1 = new object[] { cardId };
									control.BeginInvoke(actionFailure, objArray1);
								}
							}
							throw new CardDispenserException(string.Concat("Process name: ", additionalFunctionList[i].Method.Name, ", ", item));
						}
						if (actionSuccess != null && process.Count > i + 1)
						{
							if (!control.InvokeRequired)
							{
								actionSuccess(cardId, process[i + 1]);
							}
							else
							{
								object[] objArray2 = new object[] { cardId, process[i + 1] };
								control.BeginInvoke(actionSuccess, objArray2);
							}
						}
					}
					if (!flag1)
					{
						continue;
					}
					flag = true;
					this.MoveCard(ejectPosition);
					if (actionSuccess == null)
					{
						continue;
					}
					int count = additionalFunctionList.Count + 1;
					if (process.Count <= count)
					{
						continue;
					}
					if (!control.InvokeRequired)
					{
						actionSuccess(cardId, process[count]);
					}
					else
					{
						object[] objArray3 = new object[] { cardId, process[count] };
						control.BeginInvoke(actionSuccess, objArray3);
					}
				}
				if (actionComplete != null)
				{
					if (!control.InvokeRequired)
					{
						actionComplete(cardId);
					}
					else
					{
						object[] objArray4 = new object[] { cardId };
						control.BeginInvoke(actionComplete, objArray4);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				this.MoveCardToBin();
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray5 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray5);
					}
				}
			}
		}

		public void Dispose()
		{
			try
			{
				this.dispenser.Dispose();
			}
			catch
			{
				throw;
			}
		}

		public bool EjectCard()
		{
			return this.MoveCard(CardPositionCommand.MM_EJECT_TO_FRONT);
		}

		public LaneStatus GetLaneStatus()
		{
			return this.dispenser.CheckLaneStatus();
		}

		public RFCardData GetRFCardData()
		{
			throw new NotImplementedException();
		}

		public bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			BaudRate baudRate1 = (BaudRate)Enum.Parse(typeof(BaudRate), string.Concat("BAUD_RATE_", baudRate));
			this.dispenser.Connect(portName, baudRate1, address);
			this.dispenser.Init(mode);
			return true;
		}

		private void InitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			try
			{
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess("Initializing");
					}
					else
					{
						object[] objArray = new object[] { "Initializing" };
						control.BeginInvoke(actionSuccess, objArray);
					}
				}
				this.InitDispenser(portName, baudRate, address, mode, RFReaderStatus);
				if (actionSuccess != null)
				{
					if (!control.InvokeRequired)
					{
						actionSuccess("Initialized");
					}
					else
					{
						object[] objArray1 = new object[] { "Initialized" };
						control.BeginInvoke(actionSuccess, objArray1);
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray2 = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray2);
					}
				}
			}
		}

		public bool IsCardAt_RF_IC_Pos()
		{
			throw new NotImplementedException();
		}

		public bool IsCardAtGate()
		{
			throw new NotImplementedException();
		}

		public bool IsCardBinFull()
		{
			return this.dispenser.IsCardBinFull();
		}

		public bool IsCardBoxEmpty()
		{
			throw new NotImplementedException();
		}

		public bool IsNoCardAtLane()
		{
			throw new NotImplementedException();
		}

		private void logInfo(string message, bool force)
		{
			if (force || this.LogOutput)
			{
				this._logFile.Log(MyLog.LogType.Info, message);
			}
		}

		public bool MoveCard(CardPositionCommand position)
		{
			bool flag;
			try
			{
				if (position != CardPositionCommand.DONOTHING)
				{
					this.dispenser.MoveCard(position);
				}
				flag = true;
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		public bool MoveCardToBin()
		{
			return this.MoveCard(CardPositionCommand.MM_CAPTURE_TO_BOX);
		}

		public bool MoveCardToFront()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_FRONT);
		}

		public bool MoveCardToICPosition()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_IC_POS);
		}

		public bool MoveCardToRFPosition()
		{
			return this.MoveCard(CardPositionCommand.MM_RETURN_TO_RF_POS);
		}

		public bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard)
		{
			currentCard = 0;
			throw new NotSupportedException("Function not tested.");
		}

		private void ReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
		{
			try
			{
				try
				{
					int num = 0;
					this.stopThreadLoop = false;
					this.CardPermission(CardInsertPermission.PERMIT_INSERTION);
					if (actionSuccess != null && process.Count > 0)
					{
						if (!control.InvokeRequired)
						{
							actionSuccess(process[0]);
						}
						else
						{
							object[] item = new object[] { process[0] };
							control.BeginInvoke(actionSuccess, item);
						}
					}
					while (totalCard > num && !this.stopThreadLoop)
					{
						if (this.IsCardBinFull())
						{
							throw CardDispenserException.cardBinFull;
						}
						bool flag = false;
						while (!flag && !this.stopThreadLoop)
						{
							Thread.Sleep(200);
							this.GetLaneStatus();
							if (additionalFunctionList.Count > 0)
							{
								this.MoveCardToRFPosition();
								Thread.Sleep(800);
							}
							bool flag1 = true;
							for (int i = 0; i < additionalFunctionList.Count; i++)
							{
								string str = additionalFunctionList[i](control);
								if (!string.IsNullOrEmpty(str))
								{
									flag1 = false;
									if (actionFailure != null)
									{
										if (!control.InvokeRequired)
										{
											actionFailure();
										}
										else
										{
											control.BeginInvoke(actionFailure);
										}
									}
									throw new CardDispenserException(string.Concat("Process name: ", additionalFunctionList[i].Method.Name, ", ", str));
								}
								if (actionSuccess != null && process.Count > i + 1)
								{
									if (!control.InvokeRequired)
									{
										actionSuccess(process[i + 1]);
									}
									else
									{
										object[] objArray = new object[] { process[i + 1] };
										control.BeginInvoke(actionSuccess, objArray);
									}
								}
							}
							if (!flag1)
							{
								continue;
							}
							num++;
							flag = true;
							this.MoveCard(successAction);
							if (actionSuccess == null)
							{
								continue;
							}
							int count = additionalFunctionList.Count + 1;
							if (process.Count <= count)
							{
								continue;
							}
							if (!control.InvokeRequired)
							{
								actionSuccess(process[count]);
							}
							else
							{
								object[] item1 = new object[] { process[count] };
								control.BeginInvoke(actionSuccess, item1);
							}
						}
						if (actionComplete == null)
						{
							continue;
						}
						if (!control.InvokeRequired)
						{
							actionComplete();
						}
						else
						{
							control.BeginInvoke(actionComplete);
						}
					}
					if (actionAllComplete != null)
					{
						if (!control.InvokeRequired)
						{
							actionAllComplete();
						}
						else
						{
							control.BeginInvoke(actionAllComplete);
						}
					}
				}
				catch (Exception exception3)
				{
					Exception exception1 = exception3;
					try
					{
						if (this.GetLaneStatus() == LaneStatus.CARD_AT_RF_IC_POS)
						{
							this.MoveCard(failedAction);
						}
						if (exception != null)
						{
							if (!control.InvokeRequired)
							{
								exception(exception1, false);
							}
							else
							{
								object[] objArray1 = new object[] { exception1, false };
								control.BeginInvoke(exception, objArray1);
							}
						}
					}
					catch (Exception exception2)
					{
						CardDispenserException cardDispenserException = new CardDispenserException(exception2.Message, exception1);
						if (exception != null)
						{
							if (!control.InvokeRequired)
							{
								exception(cardDispenserException, false);
							}
							else
							{
								object[] objArray2 = new object[] { cardDispenserException, false };
								control.BeginInvoke(exception, objArray2);
							}
						}
					}
				}
			}
			finally
			{
				try
				{
					this.CardPermission(CardInsertPermission.DENIED_INSERTION);
				}
				catch (Exception exception5)
				{
					Exception exception4 = exception5;
					if (exception != null)
					{
						if (!control.InvokeRequired)
						{
							exception(exception4, false);
						}
						else
						{
							object[] objArray3 = new object[] { exception4, false };
							control.BeginInvoke(exception, objArray3);
						}
					}
				}
			}
		}

		public void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
		{
			Thread thread = new Thread(() => this.DispenseCardThread(control, additionalFunctionList, process, actionComplete, actionSuccess, actionFailure, logException, exception, cardId, ejectPosition))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
		{
			Thread thread = new Thread(() => this.InitDispenserThread(control, actionSuccess, exception, portName, baudRate, address, mode, RFReaderStatus))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
		{
			Thread thread = new Thread(() => this.ReturnCardThread(control, additionalFunctionList, process, actionComplete, actionAllComplete, actionSuccess, actionFailure, failedAction, successAction, logException, exception, totalCard))
			{
				IsBackground = true
			};
			thread.Start();
		}
	}
}