using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public enum BaudRate
	{
		BAUD_RATE_1200,
		BAUD_RATE_2400,
		BAUD_RATE_4800,
		BAUD_RATE_9600,
		BAUD_RATE_19200,
		BAUD_RATE_38400
	}
}