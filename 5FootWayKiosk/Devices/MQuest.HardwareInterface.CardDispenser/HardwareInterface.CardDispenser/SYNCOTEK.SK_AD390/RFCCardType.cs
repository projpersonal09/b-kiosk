using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public enum RFCCardType
	{
		RFCTYPE_MIFARE_S50 = 16,
		RFCTYPE_MIFARE_S70 = 17,
		RFCTYPE_MIFARE_UL = 18,
		RFCTYPE_TYPEA_CPU = 32,
		RFCTYPE_TYPEB_CPU = 48,
		RFCTYPE_UNKNOWN = 255
	}
}