using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
	public enum CPUCardStatus
	{
		STATUS_DEACTIVATION = 48,
		STATUS_CLKFREQ_3_57 = 49,
		STATUS_CLKFREQ_7_16 = 50
	}
}