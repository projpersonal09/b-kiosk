using System;

namespace MQuest.HardwareInterface.CardDispenser
{
	public enum InitializeMode
	{
		INIT_RETURN_TO_FRONT = 48,
		INIT_CAPTURE_TO_BOX = 49,
		INIT_WITHOUT_MOVEMENT = 51
	}
}