﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardOperation
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum CardOperation
  {
    OPERATION_LENGTH_OVERFLOW = 26368, // 0x00006700
    OPERATION_ADDRESS_OVERFLOW = 27392, // 0x00006B00
    OPERATION_FAILED = 28416, // 0x00006F00
    OPERATION_SUCCESS = 36864, // 0x00009000
  }
}
