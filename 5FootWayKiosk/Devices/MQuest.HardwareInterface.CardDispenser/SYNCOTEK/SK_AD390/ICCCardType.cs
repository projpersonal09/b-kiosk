﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.ICCCardType
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum ICCCardType
  {
    ICCTYPE_TO_CPU = 16, // 0x00000010
    ICCTYPE_T1_CPU = 17, // 0x00000011
    ICCTYPE_SLE4442 = 32, // 0x00000020
    ICCTYPE_SLE4428 = 33, // 0x00000021
    ICCTYPE_AT24C01 = 48, // 0x00000030
    ICCTYPE_AT24C02 = 49, // 0x00000031
    ICCTYPE_AT24C04 = 50, // 0x00000032
    ICCTYPE_AT24C08 = 51, // 0x00000033
    ICCTYPE_AT24C16 = 52, // 0x00000034
    ICCTYPE_AT24C32 = 53, // 0x00000035
    ICCTYPE_AT24C64 = 54, // 0x00000036
    ICCTYPE_AT24C128 = 55, // 0x00000037
    ICCTYPE_AT24C256 = 56, // 0x00000038
    ICCTYPE_UNKNOWN = 255, // 0x000000FF
  }
}
