﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenser
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public class CardDispenser
  {
    public AD390_API_v2 api;

    public CardDispenser()
    {
      this.api = new AD390_API_v2();
      if (this.api.LoadLibrary("AD_390") == 0)
        throw new CardDispenserException("Failed to load AD_390.dll, please check in " + Environment.CurrentDirectory);
    }

    public bool Connect(string portName, BaudRate baudRate, byte address)
    {
      bool error = this.ParseError(this.api.OpenComm(portName, baudRate, address), true);
      if (!error)
        error = this.ParseError(this.api.OpenComm(portName, baudRate, address), false);
      return error;
    }

    public bool Init(InitializeMode initializeMode)
    {
      return this.ParseError(this.api.Initialize(initializeMode), false);
    }

    public bool InsertCard(CardInsertPermission permission)
    {
      bool error = this.ParseError(this.api.SetCardIn(permission), true);
      if (!error)
        error = this.ParseError(this.api.SetCardIn(permission), false);
      return error;
    }

    public bool MoveCard(CardPositionCommand cardPosition)
    {
      bool error = this.ParseError(this.api.MoveCard(cardPosition), true);
      if (!error)
        error = this.ParseError(this.api.MoveCard(cardPosition), false);
      return error;
    }

    private CRStatus GetStatus()
    {
      if (!this.ParseError(this.api.GetCardStatus(), true))
        this.ParseError(this.api.GetCardStatus(), false);
      return this.api.status;
    }

    public CardBoxStatus CheckCardBoxStatus()
    {
      CRStatus status = this.GetStatus();
      if (status.bCardBoxStatus.Equals(Convert.ToByte((object) CardBoxStatus.CBS_EMPTY)))
        return CardBoxStatus.CBS_EMPTY;
      return status.bCardBoxStatus.Equals(Convert.ToByte((object) CardBoxStatus.CBS_ENOUGH)) ? CardBoxStatus.CBS_ENOUGH : CardBoxStatus.CBS_INSUFFICIENT;
    }

    public bool IsCardBinFull()
    {
      return this.GetStatus().boolCardBinFull;
    }

    public LaneStatus CheckLaneStatus()
    {
      CRStatus status = this.GetStatus();
      if (status.bLaneStatus.Equals(Convert.ToByte((object) LaneStatus.CARD_AT_GATE)))
        return LaneStatus.CARD_AT_GATE;
      return status.bLaneStatus.Equals(Convert.ToByte((object) LaneStatus.NO_CARD)) ? LaneStatus.NO_CARD : LaneStatus.CARD_AT_RF_IC_POS;
    }

    public CardInLaneStatus CheckCardInLane()
    {
      for (int index = 0; index < SensorDetail.NUM_SENSOR; ++index)
      {
        if (index != 4 && this.api.sensorDetail.isCardExist[index])
          return CardInLaneStatus.CARD_INSIDE;
      }
      return CardInLaneStatus.NO_CARD_INSIDE;
    }

    public bool Dispose()
    {
      bool error = this.ParseError(this.api.CloseComm(), true);
      if (!error)
        error = this.ParseError(this.api.CloseComm(), false);
      return error;
    }

    public bool ParseError(ErrorCode value, bool isFirst)
    {
      if (value.Equals((object) ErrorCode.AD390_S_SUCCESS))
        return true;
      if (isFirst)
      {
        if (value.Equals((object) ErrorCode.AD390_E_CARD_JAMMED))
          this.Init(InitializeMode.INIT_CAPTURE_TO_BOX);
        else
          this.Init(InitializeMode.INIT_WITHOUT_MOVEMENT);
      }
      if (this.api.status.bCardBoxStatus.Equals((object) CardBoxStatus.CBS_EMPTY))
        throw CardDispenserException.cardBoxEmpty;
      if (this.api.status.boolCardBinFull)
        throw CardDispenserException.cardBinFull;
      if (!isFirst)
        throw new CardDispenserException("Card Dispenser Error.", (Exception) new CardDispenserException("Card Dispenser Error: " + value.ToString()));
      return false;
    }
  }
}
