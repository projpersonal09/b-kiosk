﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.IC2CardType
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum IC2CardType
  {
    I2CTYPE_UNKNOWN = 48, // 0x00000030
    I2CTYPE_24C01 = 49, // 0x00000031
    I2CTYPE_24C02 = 50, // 0x00000032
    I2CTYPE_24C04 = 51, // 0x00000033
    I2CTYPE_24C08 = 52, // 0x00000034
    I2CTYPE_24C16 = 53, // 0x00000035
    I2CTYPE_24C32 = 54, // 0x00000036
    I2CTYPE_24C64 = 55, // 0x00000037
    I2CTYPE_24C128 = 56, // 0x00000038
    I2CTYPE_24C256 = 57, // 0x00000039
  }
}
