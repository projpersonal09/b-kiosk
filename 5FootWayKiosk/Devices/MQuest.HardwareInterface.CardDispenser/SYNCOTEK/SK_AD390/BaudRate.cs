﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.BaudRate
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum BaudRate
  {
    BAUD_RATE_1200,
    BAUD_RATE_2400,
    BAUD_RATE_4800,
    BAUD_RATE_9600,
    BAUD_RATE_19200,
    BAUD_RATE_38400,
  }
}
