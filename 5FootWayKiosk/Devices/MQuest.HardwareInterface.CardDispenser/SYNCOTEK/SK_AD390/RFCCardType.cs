﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.RFCCardType
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum RFCCardType
  {
    RFCTYPE_MIFARE_S50 = 16, // 0x00000010
    RFCTYPE_MIFARE_S70 = 17, // 0x00000011
    RFCTYPE_MIFARE_UL = 18, // 0x00000012
    RFCTYPE_TYPEA_CPU = 32, // 0x00000020
    RFCTYPE_TYPEB_CPU = 48, // 0x00000030
    RFCTYPE_UNKNOWN = 255, // 0x000000FF
  }
}
