﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenserManager
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using Helper;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public class CardDispenserManager : ICardDispenser, IDisposable
  {
    private const int WAIT_TIME = 800;
    private const int PROCESS_TIME = 200;
    private MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenser dispenser;
    private MyLog _logFile;

    public bool stopThreadLoop { get; set; }

    public bool LogOutput { get; set; }

    public CardDispenserManager(MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CardDispenser dispenser, bool logOutput)
    {
      this.dispenser = dispenser;
      this.LogOutput = logOutput;
      this._logFile = new MyLog();
    }

    public void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      new Thread((ThreadStart) (() => this.InitDispenserThread(control, actionSuccess, exception, portName, baudRate, address, mode, RFReaderStatus)))
      {
        IsBackground = true
      }.Start();
    }

    private void InitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      try
      {
        if (actionSuccess != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) actionSuccess, (object) "Initializing");
          else
            actionSuccess("Initializing");
        }
        this.InitDispenser(portName, baudRate, address, mode, RFReaderStatus);
        if (actionSuccess == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionSuccess, (object) "Initialized");
        else
          actionSuccess("Initialized");
      }
      catch (Exception ex)
      {
        if (exception == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
    }

    public bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      BaudRate baudRate1 = (BaudRate) Enum.Parse(typeof (BaudRate), "BAUD_RATE_" + (object) baudRate);
      this.dispenser.Connect(portName, baudRate1, address);
      this.dispenser.Init(mode);
      return true;
    }

    public void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
    {
      new Thread((ThreadStart) (() => this.DispenseCardThread(control, additionalFunctionList, process, actionComplete, actionSuccess, actionFailure, logException, exception, cardId, ejectPosition)))
      {
        IsBackground = true
      }.Start();
    }

    private void DispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
    {
      try
      {
        if (this.CheckCardBoxStatus().Equals((object) CardBoxStatus.CBS_EMPTY))
          throw CardDispenserException.cardBoxEmpty;
        if (actionSuccess != null && process.Count > 0)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) actionSuccess, (object) cardId, (object) process[0]);
          else
            actionSuccess(cardId, process[0]);
        }
        bool flag1 = false;
        this.stopThreadLoop = false;
        while (!flag1 && !this.stopThreadLoop)
        {
          Thread.Sleep(200);
          int laneStatus = (int) this.GetLaneStatus();
          if (additionalFunctionList.Count > 0)
          {
            this.MoveCardToRFPosition();
            Thread.Sleep(800);
          }
          bool flag2 = true;
          for (int index = 0; index < additionalFunctionList.Count; ++index)
          {
            string str = additionalFunctionList[index](control);
            if (string.IsNullOrEmpty(str))
            {
              if (actionSuccess != null && process.Count > index + 1)
              {
                if (control.InvokeRequired)
                  control.BeginInvoke((Delegate) actionSuccess, (object) cardId, (object) process[index + 1]);
                else
                  actionSuccess(cardId, process[index + 1]);
              }
            }
            else
            {
              if (actionFailure != null)
              {
                if (control.InvokeRequired)
                  control.BeginInvoke((Delegate) actionFailure, (object) cardId);
                else
                  actionFailure(cardId);
              }
              throw new CardDispenserException("Process name: " + additionalFunctionList[index].Method.Name + ", " + str);
            }
          }
          if (flag2)
          {
            flag1 = true;
            this.MoveCard(ejectPosition);
            if (actionSuccess != null)
            {
              int index = additionalFunctionList.Count + 1;
              if (process.Count > index)
              {
                if (control.InvokeRequired)
                  control.BeginInvoke((Delegate) actionSuccess, (object) cardId, (object) process[index]);
                else
                  actionSuccess(cardId, process[index]);
              }
            }
          }
        }
        if (actionComplete == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionComplete, (object) cardId);
        else
          actionComplete(cardId);
      }
      catch (Exception ex)
      {
        this.MoveCardToBin();
        if (exception == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
    }

    public bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition)
    {
      throw new NotSupportedException("Function not tested.");
    }

    public void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
    {
      new Thread((ThreadStart) (() => this.ReturnCardThread(control, additionalFunctionList, process, actionComplete, actionAllComplete, actionSuccess, actionFailure, failedAction, successAction, logException, exception, totalCard)))
      {
        IsBackground = true
      }.Start();
    }

    private void ReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
    {
      try
      {
        int num = 0;
        this.stopThreadLoop = false;
        this.CardPermission(CardInsertPermission.PERMIT_INSERTION);
        if (actionSuccess != null && process.Count > 0)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) actionSuccess, (object) process[0]);
          else
            actionSuccess(process[0]);
        }
        while (totalCard > num && !this.stopThreadLoop)
        {
          if (this.IsCardBinFull())
            throw CardDispenserException.cardBinFull;
          bool flag1 = false;
          while (!flag1 && !this.stopThreadLoop)
          {
            Thread.Sleep(200);
            int laneStatus = (int) this.GetLaneStatus();
            if (additionalFunctionList.Count > 0)
            {
              this.MoveCardToRFPosition();
              Thread.Sleep(800);
            }
            bool flag2 = true;
            for (int index = 0; index < additionalFunctionList.Count; ++index)
            {
              string str = additionalFunctionList[index](control);
              if (string.IsNullOrEmpty(str))
              {
                if (actionSuccess != null && process.Count > index + 1)
                {
                  if (control.InvokeRequired)
                    control.BeginInvoke((Delegate) actionSuccess, (object) process[index + 1]);
                  else
                    actionSuccess(process[index + 1]);
                }
              }
              else
              {
                if (actionFailure != null)
                {
                  if (control.InvokeRequired)
                    control.BeginInvoke((Delegate) actionFailure);
                  else
                    actionFailure();
                }
                throw new CardDispenserException("Process name: " + additionalFunctionList[index].Method.Name + ", " + str);
              }
            }
            if (flag2)
            {
              ++num;
              flag1 = true;
              this.MoveCard(successAction);
              if (actionSuccess != null)
              {
                int index = additionalFunctionList.Count + 1;
                if (process.Count > index)
                {
                  if (control.InvokeRequired)
                    control.BeginInvoke((Delegate) actionSuccess, (object) process[index]);
                  else
                    actionSuccess(process[index]);
                }
              }
            }
          }
          if (actionComplete != null)
          {
            if (control.InvokeRequired)
              control.BeginInvoke((Delegate) actionComplete);
            else
              actionComplete();
          }
        }
        if (actionAllComplete == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionAllComplete);
        else
          actionAllComplete();
      }
      catch (Exception ex1)
      {
        try
        {
          if (this.GetLaneStatus() == LaneStatus.CARD_AT_RF_IC_POS)
            this.MoveCard(failedAction);
          if (exception == null)
            return;
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) exception, (object) ex1, (object) false);
          else
            exception(ex1, false);
        }
        catch (Exception ex2)
        {
          CardDispenserException dispenserException = new CardDispenserException(ex2.Message, ex1);
          if (exception == null)
            return;
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) exception, (object) dispenserException, (object) false);
          else
            exception((Exception) dispenserException, false);
        }
      }
      finally
      {
        try
        {
          this.CardPermission(CardInsertPermission.DENIED_INSERTION);
        }
        catch (Exception ex)
        {
          if (exception != null)
          {
            if (control.InvokeRequired)
              control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
            else
              exception(ex, false);
          }
        }
      }
    }

    public bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard)
    {
      throw new NotSupportedException("Function not tested.");
    }

    public bool IsCardBinFull()
    {
      return this.dispenser.IsCardBinFull();
    }

    public bool IsCardBoxEmpty()
    {
      throw new NotImplementedException();
    }

    public CardBoxStatus CheckCardBoxStatus()
    {
      return this.dispenser.CheckCardBoxStatus();
    }

    public LaneStatus GetLaneStatus()
    {
      return this.dispenser.CheckLaneStatus();
    }

    public bool IsCardAtGate()
    {
      throw new NotImplementedException();
    }

    public bool IsCardAt_RF_IC_Pos()
    {
      throw new NotImplementedException();
    }

    public bool IsNoCardAtLane()
    {
      throw new NotImplementedException();
    }

    public void CardPermission(CardInsertPermission permission)
    {
      this.dispenser.InsertCard(permission);
    }

    public bool MoveCard(CardPositionCommand position)
    {
      try
      {
        if (position != CardPositionCommand.DONOTHING)
          this.dispenser.MoveCard(position);
        return true;
      }
      catch
      {
        return false;
      }
    }

    public bool MoveCardToBin()
    {
      return this.MoveCard(CardPositionCommand.MM_CAPTURE_TO_BOX);
    }

    public bool MoveCardToRFPosition()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_RF_POS);
    }

    public bool MoveCardToICPosition()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_IC_POS);
    }

    public bool MoveCardToFront()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_FRONT);
    }

    public bool EjectCard()
    {
      return this.MoveCard(CardPositionCommand.MM_EJECT_TO_FRONT);
    }

    public RFCardData GetRFCardData()
    {
      throw new NotImplementedException();
    }

    private void logInfo(string message, bool force)
    {
      if (!force && !this.LogOutput)
        return;
      this._logFile.Log(MyLog.LogType.Info, message);
    }

    public void Dispose()
    {
      try
      {
        this.dispenser.Dispose();
      }
      catch
      {
        throw;
      }
    }
  }
}
