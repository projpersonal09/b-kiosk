﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.AD390_API_v2
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;
using System.Globalization;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public class AD390_API_v2
  {
    private uint Hdle;
    private byte address;
    public CRStatus status;
    public SensorDetail sensorDetail;

    public AD390_API_v2()
    {
      this.address = (byte) 0;
      this.status = new CRStatus();
      this.sensorDetail = new SensorDetail();
    }

    public int LoadLibrary(string libraryName)
    {
      return AD390.LoadLibrary(libraryName);
    }

    public ErrorCode OpenComm(string portName, BaudRate baudRate, byte address)
    {
      if (this.Hdle != 0U)
      {
        AD390.CommClose(this.Hdle);
        this.Hdle = 0U;
      }
      this.Hdle = AD390.CommOpenWithBaut(portName, Convert.ToByte((object) baudRate));
      return this.Hdle != 0U ? ErrorCode.AD390_S_SUCCESS : ErrorCode.AD390_E_PORT_UNAVAILABLE;
    }

    public ErrorCode CloseComm()
    {
      if (this.Hdle != 0U)
      {
        AD390.CommClose(this.Hdle);
        this.Hdle = 0U;
      }
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode Initialize(InitializeMode mode)
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 48;
      TxData[2] = Convert.ToByte((object) mode);
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode GetCardStatus()
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 49;
      TxData[2] = (byte) 48;
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode GetSensorDetail()
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 49;
      TxData[2] = (byte) 49;
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      this.sensorDetail.Set(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode SetCardIn(CardInsertPermission permission)
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 51;
      TxData[2] = Convert.ToByte((object) permission);
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode MoveCard(CardPositionCommand position)
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 50;
      TxData[2] = Convert.ToByte((object) position);
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public ErrorCode CPUColdReset(Voltage voltage)
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 81;
      TxData[2] = (byte) 48;
      ushort TxDataLen = 4;
      ushort RxDataLen = 0;
      TxData[3] = Convert.ToByte((object) voltage);
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] == (byte) 80)
      {
        this.SetCRStatus(RxData);
        return ErrorCode.AD390_S_SUCCESS;
      }
      if (RxData[0] == (byte) 78 && RxDataLen > (ushort) 7)
        return ErrorCode.AD390_S_SUCCESS;
      return this.GetErrorCode(RxData);
    }

    public ErrorCode CPUWarmReset()
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 81;
      TxData[2] = (byte) 48;
      ushort TxDataLen = 3;
      ushort RxDataLen = 0;
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] == (byte) 80)
      {
        this.SetCRStatus(RxData);
        return ErrorCode.AD390_S_SUCCESS;
      }
      if (RxData[0] == (byte) 78 && RxDataLen > (ushort) 7)
        return ErrorCode.AD390_S_SUCCESS;
      return this.GetErrorCode(RxData);
    }

    public ErrorCode SendAPDU(string APDU)
    {
      if (this.Hdle == 0U)
        return ErrorCode.AD390_E_INVALID_HANDLE;
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      TxData[0] = (byte) 67;
      TxData[1] = (byte) 81;
      TxData[2] = (byte) 57;
      ushort TxDataLen = (ushort) (APDU.Length / 2 + 3);
      ushort RxDataLen = 0;
      for (int index = 0; index < APDU.Length / 2; ++index)
        TxData[index + 3] = byte.Parse(APDU.Substring(index * 2, 2), NumberStyles.HexNumber);
      if (AD390.ExecuteCommand(this.Hdle, this.address, TxDataLen, TxData, ref RxDataLen, RxData) != 0)
        return ErrorCode.AD390_E_COMM_ERROR;
      if (RxData[0] != (byte) 80)
        return this.GetErrorCode(RxData);
      this.SetCRStatus(RxData);
      return ErrorCode.AD390_S_SUCCESS;
    }

    public void SetCRStatus(byte[] RxData)
    {
      this.status.bLaneStatus = RxData[3];
      this.status.bCardBoxStatus = RxData[4];
      this.status.boolCardBinFull = !RxData[5].Equals((byte) 48);
    }

    public ErrorCode GetErrorCode(byte[] RxData)
    {
      string str = ((char) RxData[3]).ToString() + ((char) RxData[4]).ToString();
      ErrorCode result;
      if (Enum.TryParse<ErrorCode>(Convert.ToInt32("4E" + str, 16).ToString(), out result))
        return result;
      return (ErrorCode) Convert.ToInt32(str);
    }
  }
}
