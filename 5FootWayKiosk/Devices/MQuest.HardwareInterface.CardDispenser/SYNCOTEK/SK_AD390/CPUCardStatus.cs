﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.CPUCardStatus
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum CPUCardStatus
  {
    STATUS_DEACTIVATION = 48, // 0x00000030
    STATUS_CLKFREQ_3_57 = 49, // 0x00000031
    STATUS_CLKFREQ_7_16 = 50, // 0x00000032
  }
}
