﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.Voltage
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public enum Voltage
  {
    VCC_5V_EMV = 48, // 0x00000030
    VCC_5V_ISO7816 = 51, // 0x00000033
    VCC_3V_ISO7816 = 53, // 0x00000035
  }
}
