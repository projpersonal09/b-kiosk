﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390.AD390
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System.Runtime.InteropServices;

namespace MQuest.HardwareInterface.CardDispenser.SYNCOTEK.SK_AD390
{
  public class AD390
  {
    [DllImport("kernel32")]
    public static extern int LoadLibrary(string strDllName);

    [DllImport("AD_390.dll")]
    public static extern uint CommOpen(string port);

    [DllImport("AD_390.dll")]
    public static extern uint CommOpenWithBaut(string port, byte Baudrate);

    [DllImport("AD_390.dll")]
    public static extern int CommClose(uint ComHandle);

    [DllImport("AD_390.dll")]
    public static extern int ExecuteCommand(uint ComHandle, byte TxAddr, ushort TxDataLen, byte[] TxData, ref ushort RxDataLen, byte[] RxData);
  }
}
