﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.SensorDetail
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser
{
  public class SensorDetail
  {
    public static int NUM_SENSOR = 7;
    public bool[] isCardExist;

    public SensorDetail()
    {
      this.isCardExist = new bool[0];
    }

    public SensorDetail(int numSensor, byte[] sensors)
    {
      this.isCardExist = new bool[numSensor];
      for (int index = 0; index < numSensor; ++index)
        this.isCardExist[index] = sensors[index].Equals((byte) 48);
    }

    public void Set(byte[] RxData)
    {
    }
  }
}
