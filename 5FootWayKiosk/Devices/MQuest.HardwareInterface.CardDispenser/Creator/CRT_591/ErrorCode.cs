﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.ErrorCode
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public enum ErrorCode
  {
    CRT_591_M001_Reception_of_Undefined_Command = 0,
    CRT_591_M001_Command_Parameter_Error = 1,
    CRT_591_M001_Command_Sequence_Error = 2,
    CRT_591_M001_Out_of_Hardware_Support_Command = 3,
    CRT_591_M001_Command_Data_Error = 4,
    CRT_591_M001_IC_Card_Contact_Not_Release = 5,
    CRT_591_M001_Card_Jam = 16, // 0x00000010
    CRT_591_M001_sensor_error = 17, // 0x00000011
    CRT_591_M001_Too_Long_Card = 18, // 0x00000012
    CRT_591_M001_Too_Short_Card = 19, // 0x00000013
    CRT_591_M001_Disability_of_Recycling_card = 64, // 0x00000040
    CRT_591_M001_Magnet_of_IC_Card_Error = 65, // 0x00000041
    CRT_591_M001_Disable_To_Move_Card_To_IC_Card_Position = 67, // 0x00000043
    CRT_591_M001_Manually_Move_Card = 69, // 0x00000045
    CRT_591_M001_Received_Card_Counter_Overflow = 80, // 0x00000050
    CRT_591_M001_Motor_error = 81, // 0x00000051
    CRT_591_M001_Short_Circuit_of_IC_Card_Supply_Power = 96, // 0x00000060
    CRT_591_M001_Activiation_of_Card_failure = 97, // 0x00000061
    CRT_591_M001_Command_Out_of_IC_Card_Support = 98, // 0x00000062
    CRT_591_M001_Disablity_of_IC_Card = 101, // 0x00000065
    CRT_591_M001_Command_Out_of_IC_Current_Card_Support = 102, // 0x00000066
    CRT_591_M001_IC_Card_Transmittion_Error = 103, // 0x00000067
    CRT_591_M001_IC_Card_Transmittion_Overtime = 104, // 0x00000068
    CRT_591_M001_CPU_SAM_Non_Compliance_To_EMV_Standard = 105, // 0x00000069
    CRT_591_M001_Empty_Stacker = 160, // 0x000000A0
    CRT_591_M001_Error_card_bin_full = 161, // 0x000000A1
    CRT_591_M001_Not_Reset = 176, // 0x000000B0
  }
}
