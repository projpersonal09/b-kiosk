﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.BaudRate
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public enum BaudRate
  {
    BAUD_RATE_9600 = 9600, // 0x00002580
    BAUD_RATE_19200 = 19200, // 0x00004B00
    BAUD_RATE_38400 = 38400, // 0x00009600
    BAUD_RATE_57600 = 57600, // 0x0000E100
    BAUD_RATE_115200 = 115200, // 0x0001C200
  }
}
