﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public class CardDispenser : IDisposable
  {
    private static readonly object lockAction = new object();
    public CRT_591_M001_API api;
    private int libraryHandler;

    public CardDispenser()
    {
      this.api = new CRT_591_M001_API();
      this.libraryHandler = this.api.LoadLibrary("CRT_591_M001");
      if (this.libraryHandler == 0)
        throw new CardDispenserException("Failed to load CRT_591_M001.dll, please check in " + Environment.CurrentDirectory);
    }

    public void Connect(string portName, BaudRate baudRate, byte address)
    {
      lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
        this.api.OpenComm(portName, address, baudRate);
    }

    public void Init(InitializeMode initializeMode)
    {
      lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
        this.api.Initialize(initializeMode);
    }

    public void ResetMachine()
    {
      this.Init(InitializeMode.INIT_WITHOUT_MOVEMENT);
    }

    public void RFCardOperation(RFCardReaderStatus status)
    {
      try
      {
        lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
          this.api.RFCardOperationControl(status);
      }
      catch
      {
      }
    }

    public void InsertCard(CardInsertPermission permission)
    {
      try
      {
        lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
          this.api.SetCardIn(permission);
      }
      catch
      {
      }
    }

    public void MoveCard(CardPositionCommand cardPosition)
    {
      try
      {
        lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
          this.api.MoveCard(cardPosition);
      }
      catch
      {
      }
    }

    public LaneStatus GetLaneStatus()
    {
      CRStatus status = this.GetStatus();
      if (status.bLaneStatus.Equals(Convert.ToByte((object) LaneStatus.CARD_AT_GATE)))
        return LaneStatus.CARD_AT_GATE;
      if (status.bLaneStatus.Equals(Convert.ToByte((object) LaneStatus.NO_CARD)))
        return LaneStatus.NO_CARD;
      return status.bLaneStatus.Equals(Convert.ToByte((object) LaneStatus.CARD_AT_RF_IC_POS)) ? LaneStatus.CARD_AT_RF_IC_POS : LaneStatus.CARD_AT_UNSPECIFIC;
    }

    public bool IsCardBinFull()
    {
      try
      {
        return this.GetStatus().boolCardBinFull;
      }
      catch
      {
        return false;
      }
    }

    public CardBoxStatus CheckCardBoxStatus()
    {
      try
      {
        CRStatus status = this.GetStatus();
        if (status.bCardBoxStatus.Equals(Convert.ToByte((object) CardBoxStatus.CBS_EMPTY)))
          return CardBoxStatus.CBS_EMPTY;
        return status.bCardBoxStatus.Equals(Convert.ToByte((object) CardBoxStatus.CBS_ENOUGH)) ? CardBoxStatus.CBS_ENOUGH : CardBoxStatus.CBS_INSUFFICIENT;
      }
      catch
      {
        return CardBoxStatus.CBS_INSUFFICIENT;
      }
    }

    public RFCardData ActivateRFCard()
    {
      try
      {
        this.RFCardOperation(RFCardReaderStatus.Activate);
        return this.api.rfCardData;
      }
      catch
      {
        return (RFCardData) null;
      }
    }

    public void Dispose()
    {
      try
      {
        this.api.CloseComm();
        if (this.libraryHandler == 0)
          return;
        this.api.FreeLibrary(this.libraryHandler);
      }
      catch
      {
        throw;
      }
    }

    private CRStatus GetStatus()
    {
      lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
      {
        this.api.GetCardStatus();
        return this.api.status;
      }
    }

    private SensorDetail GetSensorDetail()
    {
      lock (MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser.lockAction)
      {
        this.api.GetSensorDetail();
        return this.api.sensorDetail;
      }
    }
  }
}
