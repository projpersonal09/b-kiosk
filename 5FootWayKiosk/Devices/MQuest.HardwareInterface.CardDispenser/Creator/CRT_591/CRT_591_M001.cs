﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CRT_591_M001
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;
using System.Runtime.InteropServices;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public class CRT_591_M001
  {
    public const int PAC_ADDRESS = 1021;

    [DllImport("kernel32")]
    public static extern int LoadLibrary(string strDllName);

    [DllImport("kernel32")]
    public static extern bool FreeLibrary(int libraryHandler);

    [DllImport("CRT_591_M001.dll")]
    public static extern IntPtr CRT591MROpen([MarshalAs(UnmanagedType.LPStr)] string port);

    [DllImport("CRT_591_M001.dll")]
    public static extern IntPtr CRT591MROpenWithBaut([MarshalAs(UnmanagedType.LPStr)] string port, uint Baudrate);

    [DllImport("CRT_591_M001.dll")]
    public static extern int CRT591MRClose(IntPtr ComHandle);

    [DllImport("CRT_591_M001.dll")]
    public static extern int RS232_ExeCommand(IntPtr ComHandle, byte TxAddr, byte TxCmCode, byte TxPmCode, short TxDataLen, byte[] TxData, ref byte RxReplyType, ref byte RxStCode0, ref byte RxStCode1, ref byte RxStCode2, ref short RxDataLen, byte[] RxData);

    public enum RxType
    {
      ENQ = 5,
      ACK = 6,
      NAK = 21, // 0x00000015
    }

    public enum RxReplyType
    {
      Cancel_by_Machine = 16, // 0x00000010
      Communication_Error = 32, // 0x00000020
      Cancel_By_HOST = 48, // 0x00000030
      Fail = 78, // 0x0000004E
      Success = 80, // 0x00000050
    }
  }
}
