﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenserManager
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using Helper;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public class CardDispenserManager : ICardDispenser, IDisposable
  {
    private static readonly int DISPENSE_CARD_TIMEOUT = 60000;
    private static readonly int WAIT_TIME = 1000;
    private static readonly object lockInstance = new object();
    private MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser _cardDispenser;
    private RFCardData _rfCard;
    private bool _gateIsOpened;
    private MyLog _logFile;

    private RFCardData RFCardData
    {
      get
      {
        lock (CardDispenserManager.lockInstance)
          return this._rfCard;
      }
      set
      {
        lock (CardDispenserManager.lockInstance)
          this._rfCard = value;
      }
    }

    public bool stopThreadLoop { get; set; }

    public bool LogOutput { get; set; }

    public CardDispenserManager(bool logOutput)
    {
      this._cardDispenser = new MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CardDispenser();
      this.RFCardData = (RFCardData) null;
      this._gateIsOpened = false;
      this.stopThreadLoop = false;
      this.LogOutput = logOutput;
      this._logFile = new MyLog();
    }

    public void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      new Thread((ThreadStart) (() => this.InitDispenserThread(control, actionSuccess, exception, portName, baudRate, address, mode, RFReaderStatus)))
      {
        IsBackground = true
      }.Start();
    }

    private void InitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      try
      {
        if (actionSuccess != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) actionSuccess, (object) "Initializing");
          else
            actionSuccess("Initializing");
        }
        this.InitDispenser(portName, baudRate, address, mode, RFReaderStatus);
        if (actionSuccess == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionSuccess, (object) "Initialized");
        else
          actionSuccess("Initialized");
      }
      catch (Exception ex)
      {
        if (exception == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
    }

    public bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus)
    {
      BaudRate baudRate1 = (BaudRate) Enum.Parse(typeof (BaudRate), "BAUD_RATE_" + (object) baudRate);
      this._cardDispenser.Connect(portName, baudRate1, address);
      if (mode.Equals((object) InitializeMode.INIT_CAPTURE_TO_BOX))
      {
        if (!this.IsCardBinFull())
          this._cardDispenser.Init(mode);
      }
      else
        this._cardDispenser.Init(mode);
      this._cardDispenser.RFCardOperation(RFReaderStatus);
      return true;
    }

    public void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
    {
      new Thread((ThreadStart) (() => this.DispenseCardThread(control, additionalFunctionList, process, actionComplete, actionSuccess, actionFailure, logException, exception, cardId, ejectPosition)))
      {
        IsBackground = true
      }.Start();
    }

    private void DispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition)
    {
      this.RFCardData = (RFCardData) null;
      System.Timers.Timer timer = (System.Timers.Timer) null;
      Exception exception1 = (Exception) null;
      try
      {
        if (this.GetLaneStatus() == LaneStatus.CARD_AT_GATE)
          this.EjectCard();
        if (this.IsCardBoxEmpty() && this.IsNoCardAtLane())
          throw CardDispenserException.cardBoxEmpty;
        this.stopThreadLoop = false;
        bool flag = false;
        bool dispenseTimeoutFlag = false;
        timer = new System.Timers.Timer((double) CardDispenserManager.DISPENSE_CARD_TIMEOUT);
        timer.Elapsed += (ElapsedEventHandler) ((param0, param1) => this.dispenseTimerElapsed(ref dispenseTimeoutFlag));
        if (process.Count > 0)
          this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[0]);
        this.logInfo("Card Dispenser: Processing Dispense Card...", true);
        timer.Enabled = true;
        while (!flag && !dispenseTimeoutFlag && !this.stopThreadLoop)
        {
          if (exception1 != null)
          {
            try
            {
              this._cardDispenser.ResetMachine();
            }
            catch (Exception ex)
            {
              this.triggerLogException(control, logException, ex);
            }
            exception1 = (Exception) null;
          }
          if (this.IsCardBoxEmpty())
          {
            if (this.IsNoCardAtLane())
              throw CardDispenserException.cardBoxEmpty;
          }
          try
          {
            LaneStatus laneStatus = this.GetLaneStatus();
            if (laneStatus != LaneStatus.CARD_AT_RF_IC_POS)
            {
              this.MoveCardToRFPosition();
              Thread.Sleep(CardDispenserManager.WAIT_TIME);
            }
            else if (laneStatus == LaneStatus.CARD_AT_RF_IC_POS)
            {
              for (int index = 0; index < additionalFunctionList.Count; ++index)
              {
                this.logInfo("Card Dispenser: Performing function " + additionalFunctionList[index].Method.Name + "...", false);
                string str = additionalFunctionList[index](control);
                if (string.IsNullOrEmpty(str))
                {
                  if (process.Count > index + 1)
                    this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[index + 1]);
                }
                else
                {
                  this.triggerActionFailureOnDispense(control, actionFailure, cardId);
                  throw new CardDispenserException("Process name: " + additionalFunctionList[index].Method.Name + ". Error: " + str);
                }
              }
              this.MoveCard(ejectPosition);
              if (process.Count > additionalFunctionList.Count + 1)
                this.triggerActionSuccessOnDispense(control, actionSuccess, cardId, process[additionalFunctionList.Count + 1]);
              this.logInfo("Card Dispenser: Dispense Card is successful", true);
              flag = true;
            }
          }
          catch (Exception ex1)
          {
            this.logInfo("Card Dispenser: Dispense Card is failed..", true);
            exception1 = ex1;
            this.triggerLogException(control, logException, ex1);
            if (this.IsCardBinFull())
              throw CardDispenserException.cardBinFull;
            try
            {
              this.MoveCardToBin();
            }
            catch (Exception ex2)
            {
              this.triggerLogException(control, logException, ex2);
            }
            this.logInfo("Card Dispenser: Retry Dispensing Card..", true);
          }
        }
        if (!flag)
        {
          this.logInfo("Card Dispenser: Dispensing is not completed.", true);
          if (this.GetLaneStatus().Equals((object) LaneStatus.CARD_AT_RF_IC_POS))
            this.MoveCardToBin();
          if (dispenseTimeoutFlag)
            throw new Exception("Unable to dispense card. 1 minute Timeout reached.");
        }
        else
        {
          if (actionComplete == null)
            return;
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) actionComplete, (object) cardId);
          else
            actionComplete(cardId);
        }
      }
      catch (Exception ex)
      {
        if (exception == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) exception, (object) ex, (object) false);
        else
          exception(ex, false);
      }
      finally
      {
        if (timer != null)
        {
          timer.Enabled = false;
          timer.Dispose();
        }
      }
    }

    public bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition)
    {
      throw new NotSupportedException("Function not tested.");
    }

    private void triggerActionSuccessOnDispense(Control control, Action<string, int> actionSuccess, string cardId, int processNo)
    {
      if (control == null || actionSuccess == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) actionSuccess, (object) cardId, (object) processNo);
      else
        actionSuccess(cardId, processNo);
    }

    private void triggerActionFailureOnDispense(Control control, Action<string> actionFailure, string cardId)
    {
      if (control == null || actionFailure == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) actionFailure, (object) cardId);
      else
        actionFailure(cardId);
    }

    private void triggerLogException(Control control, Action<Exception> logException, Exception ex)
    {
      if (control == null || logException == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) logException, (object) ex);
      else
        logException(ex);
    }

    private void dispenseTimerElapsed(ref bool timeoutFlag)
    {
      timeoutFlag = true;
    }

    public void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
    {
      new Thread((ThreadStart) (() => this.ReturnCardThread(control, additionalFunctionList, process, actionComplete, actionAllComplete, actionSuccess, actionFailure, failedAction, successAction, logException, exception, totalCard)))
      {
        IsBackground = true
      }.Start();
    }

    private void ReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard)
    {
      Exception innerException = (Exception) null;
      try
      {
        if (this.IsCardBinFull())
          throw CardDispenserException.cardBinFull;
        this.stopThreadLoop = false;
        int num = 0;
        this.CardPermission(CardInsertPermission.PERMIT_INSERTION);
        if (process.Count > 0)
          this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[0]);
        this.logInfo("Card Dispenser: Processing Return Card...", true);
        while (totalCard > num && !this.stopThreadLoop)
        {
          bool flag = false;
          Exception exception1 = (Exception) null;
          while (!flag && !this.stopThreadLoop)
          {
            if (exception1 != null)
            {
              try
              {
                this._cardDispenser.ResetMachine();
              }
              catch (Exception ex)
              {
                this.triggerLogException(control, logException, ex);
              }
              exception1 = (Exception) null;
            }
            if (this.IsCardBinFull())
              throw CardDispenserException.cardBinFull;
            try
            {
              switch (this.GetLaneStatus())
              {
                case LaneStatus.CARD_AT_UNSPECIFIC:
                case LaneStatus.NO_CARD:
                  Thread.Sleep(CardDispenserManager.WAIT_TIME);
                  continue;
                case LaneStatus.CARD_AT_GATE:
                  this.MoveCardToRFPosition();
                  Thread.Sleep(CardDispenserManager.WAIT_TIME);
                  continue;
                case LaneStatus.CARD_AT_RF_IC_POS:
                  for (int index = 0; index < additionalFunctionList.Count; ++index)
                  {
                    this.logInfo("Card Dispenser: Performing function " + additionalFunctionList[index].Method.Name + "...", false);
                    string str = additionalFunctionList[index](control);
                    if (string.IsNullOrEmpty(str))
                    {
                      if (process.Count > index + 1)
                        this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[index + 1]);
                    }
                    else
                    {
                      this.triggerActionFailureOnReturnCard(control, actionFailure);
                      throw new CardDispenserException("Process name: " + additionalFunctionList[index].Method.Name + ". Error: " + str);
                    }
                  }
                  ++num;
                  this.MoveCard(successAction);
                  if (process.Count > additionalFunctionList.Count + 1)
                    this.triggerActionSuccessOnReturnCard(control, actionSuccess, process[additionalFunctionList.Count + 1]);
                  this.logInfo("Card Dispenser: Return Card is successful", true);
                  flag = true;
                  continue;
                default:
                  continue;
              }
            }
            catch (CardDispenserException ex1)
            {
              this.logInfo("Card Dispenser: Return Card is failed..", true);
              try
              {
                this.MoveCard(failedAction);
              }
              catch (Exception ex2)
              {
                this.triggerLogException(control, logException, ex2);
              }
              throw;
            }
            catch (Exception ex)
            {
              this.logInfo("Card Dispenser: Return Card is failed..", true);
              exception1 = ex;
              this.logInfo("Card Dispenser: Retry Returning Card..", true);
            }
          }
          if (!flag)
          {
            this.logInfo("Card Dispenser: Returning is not completed.", true);
            if (this.GetLaneStatus().Equals((object) LaneStatus.CARD_AT_RF_IC_POS))
              this.MoveCard(failedAction);
          }
          else if (actionComplete != null)
          {
            if (control.InvokeRequired)
              control.BeginInvoke((Delegate) actionComplete);
            else
              actionComplete();
          }
        }
        if (actionAllComplete == null)
          return;
        if (control.InvokeRequired)
          control.BeginInvoke((Delegate) actionAllComplete);
        else
          actionAllComplete();
      }
      catch (Exception ex)
      {
        innerException = ex;
      }
      finally
      {
        try
        {
          this.CardPermission(CardInsertPermission.DENIED_INSERTION);
        }
        catch (Exception ex)
        {
          innerException = new Exception("Failed to close card dispenser gate.", innerException == null ? ex : new Exception(innerException.Message, innerException));
        }
        if (exception != null && innerException != null)
        {
          if (control.InvokeRequired)
            control.BeginInvoke((Delegate) exception, (object) innerException, (object) false);
          else
            exception(innerException, false);
        }
      }
    }

    public bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard)
    {
      throw new NotSupportedException("Function not tested.");
    }

    private void triggerActionSuccessOnReturnCard(Control control, Action<int> actionSuccess, int processNo)
    {
      if (control == null || actionSuccess == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) actionSuccess, (object) processNo);
      else
        actionSuccess(processNo);
    }

    private void triggerActionFailureOnReturnCard(Control control, Action actionFailure)
    {
      if (control == null || actionFailure == null)
        return;
      if (control.InvokeRequired)
        control.BeginInvoke((Delegate) actionFailure);
      else
        actionFailure();
    }

    public bool IsCardBinFull()
    {
      return this._cardDispenser.IsCardBinFull();
    }

    public bool IsCardBoxEmpty()
    {
      return this._cardDispenser.CheckCardBoxStatus().Equals((object) CardBoxStatus.CBS_EMPTY);
    }

    public CardBoxStatus CheckCardBoxStatus()
    {
      return this._cardDispenser.CheckCardBoxStatus();
    }

    public LaneStatus GetLaneStatus()
    {
      LaneStatus laneStatus = this._cardDispenser.GetLaneStatus();
      this.logInfo("Card Dispenser: Card Position: " + laneStatus.ToString(), false);
      return laneStatus;
    }

    public bool IsCardAtGate()
    {
      return this.GetLaneStatus() == LaneStatus.CARD_AT_GATE;
    }

    public bool IsCardAt_RF_IC_Pos()
    {
      return this.GetLaneStatus() == LaneStatus.CARD_AT_RF_IC_POS;
    }

    public bool IsNoCardAtLane()
    {
      return this.GetLaneStatus() == LaneStatus.NO_CARD;
    }

    public void CardPermission(CardInsertPermission permission)
    {
      if ((!this._gateIsOpened || !permission.Equals((object) CardInsertPermission.DENIED_INSERTION)) && (this._gateIsOpened || !permission.Equals((object) CardInsertPermission.PERMIT_INSERTION)))
        return;
      this._cardDispenser.InsertCard(permission);
      this._gateIsOpened = !this._gateIsOpened;
    }

    public bool MoveCard(CardPositionCommand position)
    {
      try
      {
        if (position != CardPositionCommand.DONOTHING)
          this._cardDispenser.MoveCard(position);
        return true;
      }
      catch
      {
        return false;
      }
    }

    public bool MoveCardToBin()
    {
      return this.MoveCard(CardPositionCommand.MM_CAPTURE_TO_BOX);
    }

    public bool MoveCardToRFPosition()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_RF_POS);
    }

    public bool MoveCardToICPosition()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_IC_POS);
    }

    public bool MoveCardToFront()
    {
      return this.MoveCard(CardPositionCommand.MM_RETURN_TO_FRONT);
    }

    public bool EjectCard()
    {
      return this.MoveCard(CardPositionCommand.MM_EJECT_TO_FRONT);
    }

    public RFCardData GetRFCardData()
    {
      return RFCardData.Clone(this.RFCardData);
    }

    private bool _activateRFCard()
    {
      try
      {
        this.RFCardData = this._cardDispenser.ActivateRFCard();
        return this.RFCardData != null;
      }
      catch
      {
        return false;
      }
    }

    private void logInfo(string message, bool force)
    {
      if (!force && !this.LogOutput)
        return;
      this._logFile.Log(MyLog.LogType.Info, message);
    }

    public void Dispose()
    {
      try
      {
        this.stopThreadLoop = true;
        this._cardDispenser.Dispose();
        Thread.Sleep(100);
      }
      catch
      {
        throw;
      }
    }
  }
}
