﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.Creator.CRT_591.CRT_591_M001_API
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;

namespace MQuest.HardwareInterface.CardDispenser.Creator.CRT_591
{
  public class CRT_591_M001_API
  {
    private IntPtr Hdle = IntPtr.Zero;
    private byte address;
    public CRStatus status;
    public SensorDetail sensorDetail;
    public RFCardData rfCardData;

    public CRT_591_M001_API()
    {
      this.address = (byte) 0;
      this.status = new CRStatus();
      this.sensorDetail = new SensorDetail();
    }

    public int LoadLibrary(string libraryName)
    {
      return CRT_591_M001.LoadLibrary(libraryName);
    }

    public bool FreeLibrary(int libraryHandler)
    {
      return CRT_591_M001.FreeLibrary(libraryHandler);
    }

    public void OpenComm(string portName, byte address, BaudRate baudRate = BaudRate.BAUD_RATE_38400)
    {
      this.CloseComm();
      this.Hdle = !baudRate.Equals((object) BaudRate.BAUD_RATE_38400) ? CRT_591_M001.CRT591MROpenWithBaut(portName, (uint) Convert.ToUInt16((object) baudRate)) : CRT_591_M001.CRT591MROpen(portName);
      if (this.Hdle == IntPtr.Zero)
        throw new CardDispenserException(portName + " Port with baud rate " + (object) Convert.ToUInt32((object) baudRate) + " is Unavailable.");
    }

    public void CloseComm()
    {
      if (!(this.Hdle != IntPtr.Zero))
        return;
      CRT_591_M001.CRT591MRClose(this.Hdle);
      this.Hdle = IntPtr.Zero;
    }

    public void Initialize(InitializeMode mode)
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on Initialize.");
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 48;
      byte TxPmCode = Convert.ToByte((object) mode);
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, RxData);
      if (num != 0)
        throw new CardDispenserException("Communication Error on Initialize. value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (Initialize));
      else
        this.GetErrorMessage(RxReplyType, nameof (Initialize));
    }

    public void GetCardStatus()
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on GetCardStatus.");
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 49;
      byte TxPmCode = 48;
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, RxData);
      if (num != 0)
        throw new CardDispenserException("Communication Error on GetCardStatus, value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (GetCardStatus));
      else
        this.GetErrorMessage(RxReplyType, nameof (GetCardStatus));
    }

    public void GetSensorDetail()
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on GetSensorDetail.");
      byte[] TxData = new byte[1024];
      byte[] numArray = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 49;
      byte TxPmCode = 49;
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, numArray);
      if (num != 0)
        throw new CardDispenserException("Communication Error on GetSensorDetail, value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
      {
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
        this.SetSensorDetail(RxDataLen, numArray);
      }
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (GetSensorDetail));
      else
        this.GetErrorMessage(RxReplyType, nameof (GetSensorDetail));
    }

    public void SetCardIn(CardInsertPermission permission)
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on SetCardIn.");
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 51;
      byte TxPmCode = Convert.ToByte((object) permission);
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, RxData);
      if (num != 0)
        throw new CardDispenserException("Communication Error on SetCardIn, value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (SetCardIn));
      else
        this.GetErrorMessage(RxReplyType, nameof (SetCardIn));
    }

    public void MoveCard(CardPositionCommand position)
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on MoveCard.");
      byte[] TxData = new byte[1024];
      byte[] RxData = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 50;
      byte TxPmCode = Convert.ToByte((object) position);
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, RxData);
      if (num != 0)
        throw new CardDispenserException("Communication Error on MoveCard, value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (MoveCard));
      else
        this.GetErrorMessage(RxReplyType, nameof (MoveCard));
    }

    public void RFCardOperationControl(RFCardReaderStatus status)
    {
      if (!(this.Hdle != IntPtr.Zero))
        throw new CardDispenserException("Invalid Handle on ChangeRFCardReaderStatus.");
      byte[] TxData = new byte[1024];
      byte[] numArray = new byte[1024];
      short TxDataLen = 0;
      short RxDataLen = 0;
      byte TxCmCode = 96;
      byte TxPmCode = Convert.ToByte((object) status);
      byte RxReplyType = 0;
      byte RxStCode0 = 0;
      byte RxStCode1 = 0;
      byte RxStCode2 = 0;
      int num = CRT_591_M001.RS232_ExeCommand(this.Hdle, this.address, TxCmCode, TxPmCode, TxDataLen, TxData, ref RxReplyType, ref RxStCode0, ref RxStCode1, ref RxStCode2, ref RxDataLen, numArray);
      if (num != 0)
        throw new CardDispenserException("Communication Error on ChangeRFCardReaderStatus, value : " + (object) num + ".");
      if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Success)))
      {
        this.SetCRStatus(RxStCode0, RxStCode1, RxStCode2);
        this.SetContaclessCardData(numArray);
      }
      else if (RxReplyType.Equals(Convert.ToByte((object) CRT_591_M001.RxReplyType.Fail)))
        this.GetErrorMessage(RxStCode1, RxStCode2, nameof (RFCardOperationControl));
      else
        this.GetErrorMessage(RxReplyType, nameof (RFCardOperationControl));
    }

    private void SetContaclessCardData(byte[] PmData)
    {
      try
      {
        this.rfCardData = new RFCardData(PmData);
      }
      catch
      {
        this.rfCardData = (RFCardData) null;
      }
    }

    private void SetSensorDetail(short PmDataLen, byte[] PmData)
    {
      this.sensorDetail = new SensorDetail((int) PmDataLen, PmData);
    }

    public void SetCRStatus(byte PmSt0, byte PmSt1, byte PmSt2)
    {
      this.status.bLaneStatus = PmSt0;
      this.status.bCardBoxStatus = PmSt1;
      this.status.boolCardBinFull = PmSt2 != (byte) 48;
    }

    public void GetErrorMessage(byte PmE0, byte PmE1, string from)
    {
      string str = ((char) PmE0).ToString() + ((char) PmE1).ToString();
      ErrorCode int32 = (ErrorCode) Convert.ToInt32(str, 16);
      throw new CardDispenserException("Error : 0x" + str + ", " + int32.ToString() + " on " + from + ".");
    }

    public void GetErrorMessage(byte PmReplyType, string from)
    {
      CRT_591_M001.RxReplyType rxReplyType = (CRT_591_M001.RxReplyType) PmReplyType;
      throw new CardDispenserException("Reply Type : " + (object) PmReplyType + ", " + rxReplyType.ToString() + " on " + from + ".");
    }
  }
}
