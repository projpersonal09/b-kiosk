﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.CardInsertPermission
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

namespace MQuest.HardwareInterface.CardDispenser
{
  public enum CardInsertPermission
  {
    PERMIT_INSERTION = 48, // 0x00000030
    DENIED_INSERTION = 49, // 0x00000031
  }
}
