﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.CRStatus
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;

namespace MQuest.HardwareInterface.CardDispenser
{
  [Serializable]
  public struct CRStatus
  {
    public byte bLaneStatus { get; set; }

    public byte bCardBoxStatus { get; set; }

    public bool boolCardBinFull { get; set; }
  }
}
