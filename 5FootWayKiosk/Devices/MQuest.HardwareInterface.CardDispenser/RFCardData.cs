﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.RFCardData
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using MQuest.EnterpriseLibrary.Utility;
using System;

namespace MQuest.HardwareInterface.CardDispenser
{
  public class RFCardData
  {
    public byte RType { get; set; }

    public byte[] ATQA { get; set; }

    public byte[] CardUID { get; set; }

    public string sCardUID { get; set; }

    private RFCardData(byte rType, byte[] atqa, byte[] cardUID)
    {
      this.RType = rType;
      this.ATQA = new byte[atqa.Length];
      Array.Copy((Array) atqa, (Array) this.ATQA, atqa.Length);
      this.CardUID = new byte[cardUID.Length];
      Array.Copy((Array) cardUID, (Array) this.CardUID, cardUID.Length);
      this.setCardUIDString();
    }

    public RFCardData(byte[] data)
    {
      this.RType = data[0];
      this.ATQA = new byte[2];
      Array.Copy((Array) data, 1, (Array) this.ATQA, 0, 2);
      this.CardUID = new byte[(int) data[3]];
      Array.Copy((Array) data, 4, (Array) this.CardUID, 0, this.CardUID.Length);
      this.setCardUIDString();
    }

    private void setCardUIDString()
    {
      if (this.CardUID == null || this.CardUID.Length == 0)
        this.sCardUID = string.Empty;
      else
        this.sCardUID = Conversion.ConvertByteArrayToHexString(this.CardUID).Trim().Replace(" ", "");
    }

    internal static RFCardData Clone(RFCardData rfCardData)
    {
      if (rfCardData == null)
        return (RFCardData) null;
      return new RFCardData(rfCardData.RType, rfCardData.ATQA, rfCardData.CardUID);
    }
  }
}
