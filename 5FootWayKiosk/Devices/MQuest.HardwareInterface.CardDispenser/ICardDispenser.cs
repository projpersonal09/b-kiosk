﻿// Decompiled with JetBrains decompiler
// Type: MQuest.HardwareInterface.CardDispenser.ICardDispenser
// Assembly: MQuest.HardwareInterface.CardDispenser, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0CC29AAC-8199-4151-86AC-77047C26F52C
// Assembly location: C:\Projects\5FootWayIn\KIOSK\KIOSK-Project\5Footway_App\5FootWay_App\bin\Debug\MQuest.HardwareInterface.CardDispenser.dll

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CardDispenser
{
  public interface ICardDispenser
  {
    bool stopThreadLoop { get; set; }

    bool LogOutput { get; set; }

    void StartInitDispenserThread(Control control, Action<string> actionSuccess, Action<Exception, bool> exception, string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFReaderStatus);

    bool InitDispenser(string portName, uint baudRate, byte address, InitializeMode mode, RFCardReaderStatus RFStatus);

    void StartDispenseCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action<string> actionComplete, Action<string, int> actionSuccess, Action<string> actionFailure, Action<Exception> logException, Action<Exception, bool> exception, string cardId, CardPositionCommand ejectPosition);

    bool DispenseCard(List<Func<string>> additionalFunctionList, CardPositionCommand ejectPosition);

    void StartReturnCardThread(Control control, List<Func<Control, string>> additionalFunctionList, List<int> process, Action actionComplete, Action actionAllComplete, Action<int> actionSuccess, Action actionFailure, CardPositionCommand failedAction, CardPositionCommand successAction, Action<Exception> logException, Action<Exception, bool> exception, int totalCard);

    bool ReturnCard(List<Func<string>> additionalFunctionList, CardPositionCommand failedAction, CardPositionCommand successAction, int totalCard, out int currentCard);

    bool IsCardBinFull();

    bool IsCardBoxEmpty();

    CardBoxStatus CheckCardBoxStatus();

    LaneStatus GetLaneStatus();

    bool IsCardAtGate();

    bool IsCardAt_RF_IC_Pos();

    bool IsNoCardAtLane();

    void CardPermission(CardInsertPermission permission);

    bool MoveCard(CardPositionCommand position);

    bool MoveCardToBin();

    bool MoveCardToRFPosition();

    bool MoveCardToICPosition();

    bool MoveCardToFront();

    bool EjectCard();

    RFCardData GetRFCardData();

    void Dispose();
  }
}
