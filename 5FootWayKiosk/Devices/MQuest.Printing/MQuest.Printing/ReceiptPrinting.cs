using Helper;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace MQuest.Printing
{
	public class ReceiptPrinting
	{
		public const float spaceBetweenLine = 2f;

		public ConstantVariable_Receipt receipt;

		public int currentLine;

		public int currentPage;

		public float accumulateHeight;

		public ReceiptPrinting()
		{
			this.receipt = new ConstantVariable_Receipt();
			this.currentLine = 0;
			this.currentPage = 0;
			this.accumulateHeight = 0f;
		}

		public ReceiptPrinting(List<string> addressList)
		{
			this.receipt = new ConstantVariable_Receipt(addressList);
			this.currentLine = 0;
			this.currentPage = 0;
			this.accumulateHeight = 0f;
		}

		private float DrawString(Graphics g, string message, ReceiptFont font, Brush brush, float xPos, float yPos, float pageWidth, float additionalOffsetAfter, StringFormat format, bool print)
		{
			List<string> strs = new List<string>()
			{
				message
			};
			List<float> singles = new List<float>()
			{
				1f
			};
			List<ReceiptFont> receiptFonts = new List<ReceiptFont>()
			{
				font
			};
			List<Brush> brushes = new List<Brush>()
			{
				brush
			};
			List<StringFormat> stringFormats = new List<StringFormat>()
			{
				format
			};
			yPos = this.DrawString(g, strs, singles, receiptFonts, brushes, xPos, 0f, yPos, pageWidth, additionalOffsetAfter, stringFormats, print);
			return yPos;
		}

		private float DrawString(Graphics g, List<string> messageList, List<float> percentageList, List<ReceiptFont> fontList, List<Brush> brushList, float xPos, float spaceBetweenColumn, float yPos, float pageWidth, float additionalOffsetAfter, List<StringFormat> formatList, bool print)
		{
			try
			{
				if (messageList.Count > 0)
				{
					float single = pageWidth - spaceBetweenColumn * (float)(messageList.Count - 1);
					float height = 0f;
					List<float> singles = new List<float>();
					List<SizeF> sizeFs = new List<SizeF>();
					for (int i = 0; i < messageList.Count; i++)
					{
						if (percentageList[i] > 1f)
						{
							percentageList[i] = percentageList[i] / 100f;
						}
						singles.Add(single * percentageList[i]);
						int num = Convert.ToInt32(Math.Ceiling((double)singles[i]));
						if (messageList[i] == null)
						{
							messageList[i] = string.Empty;
						}
						using (Font font = new Font(fontList[i].font, fontList[i].style))
						{
							sizeFs.Add(new SizeF(g.MeasureString(messageList[i], font, num)));
							if (height < sizeFs[i].Height)
							{
								height = sizeFs[i].Height;
							}
						}
					}
					if (print)
					{
						for (int j = 0; j < messageList.Count; j++)
						{
							float item = xPos;
							for (int k = 0; k < j; k++)
							{
								item = item + (singles[k] + spaceBetweenColumn);
							}
							if (sizeFs[j].Width <= singles[j])
							{
								float item1 = singles[j] - sizeFs[j].Width;
								if (formatList[j].Alignment.Equals(StringAlignment.Center))
								{
									item = item + item1 / 2f;
								}
								else if (formatList[j].Alignment.Equals(StringAlignment.Far))
								{
									item += item1;
								}
							}
							float single1 = yPos;
							if (height >= sizeFs[j].Height)
							{
								float height1 = height - sizeFs[j].Height;
								if (formatList[j].LineAlignment.Equals(StringAlignment.Center))
								{
									single1 = single1 + height1 / 2f;
								}
								else if (formatList[j].LineAlignment.Equals(StringAlignment.Far))
								{
									single1 += height1;
								}
							}
							float width = sizeFs[j].Width;
							SizeF sizeF = sizeFs[j];
							RectangleF rectangleF = new RectangleF(item, single1, width, sizeF.Height);
							using (Font font1 = new Font(fontList[j].font, fontList[j].style))
							{
								g.DrawString(messageList[j], font1, brushList[j], rectangleF);
							}
						}
					}
					yPos = yPos + (height + 2f + additionalOffsetAfter);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return yPos;
		}

		public List<ReceiptLine> InitailizePrintItem(KioskData kioskData, BookingData bookingData, RoomStayData roomStayData, KioskCheckInData guestData, List<BookingChargeData> chargeData, TransactionDetail transaction, Graphics g, PageSettings ps, string invoiceNumber, ReceiptImageLine logoImage, ReceiptImageLine signatureImage, Font largeFont, Font mediumFont, Font smallFont, Brush brush, Pen dashPen, Pen normalPen)
		{
			decimal num;
			List<ReceiptLine> receiptLines = new List<ReceiptLine>();
			try
			{
				using (g)
				{
					float width = (float)(ps.PaperSize.Width - ps.Margins.Left - ps.Margins.Right);
					ReceiptImageLine top = logoImage;
					top.printOffsetAfter = 2f;
					top.donAddHeight = true;
					top.startX = width * 0.8f;
					top.width = width - top.startX;
					top.startY = (float)ps.Margins.Top;
					List<ReceiptLine> receiptLines1 = new List<ReceiptLine>();
					foreach (string str in this.receipt.addressList)
					{
						ReceiptStringLine receiptStringLine = new ReceiptStringLine();
						receiptStringLine.printContentList.Add(str);
						receiptStringLine.printFontList.Add(new ReceiptFont(smallFont, FontStyle.Bold));
						receiptStringLine.printBrushList.Add(brush);
						receiptStringLine.printStringFormatList.Add(new StringFormat()
						{
							Alignment = StringAlignment.Near
						});
						receiptStringLine.printOffsetAfter = 0f;
						receiptStringLine.percentageOfPageWidthList.Add(80f);
						receiptStringLine.printHeight = this.DrawString(g, receiptStringLine.printContentList[0], receiptStringLine.printFontList[0], receiptStringLine.printBrushList[0], (float)ps.Margins.Left, 0f, width, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList[0], false);
						receiptLines1.Add(receiptStringLine);
					}
					for (int i = 0; i < kioskData.registrationNumber.Count; i++)
					{
						ReceiptStringLine receiptStringLine1 = new ReceiptStringLine();
						receiptStringLine1.printContentList.Add(kioskData.registrationNumber[i]);
						receiptStringLine1.printFontList.Add(new ReceiptFont(smallFont, FontStyle.Bold));
						receiptStringLine1.printBrushList.Add(brush);
						receiptStringLine1.printStringFormatList.Add(new StringFormat()
						{
							Alignment = StringAlignment.Near
						});
						receiptStringLine1.printOffsetAfter = (i + 1 == kioskData.registrationNumber.Count ? 40f : 0f);
						receiptStringLine1.percentageOfPageWidthList.Add(80f);
						receiptStringLine1.printHeight = this.DrawString(g, receiptStringLine1.printContentList[0], receiptStringLine1.printFontList[0], receiptStringLine1.printBrushList[0], (float)ps.Margins.Left, 0f, width, receiptStringLine1.printOffsetAfter, receiptStringLine1.printStringFormatList[0], false);
						receiptLines1.Add(receiptStringLine1);
					}
					int num1 = 0;
					string empty = string.Empty;
					string empty1 = string.Empty;
					if (guestData.roomList.Count > 0)
					{
						while (guestData.roomList[num1].guestList.Count.Equals(0))
						{
							num1++;
						}
						empty = guestData.roomList[num1].guestList[0].guestName;
						string item = guestData.roomList[num1].roomBedLabel;
					}
					ReceiptStringLine brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("GUEST NAME : ", empty));
					brushes.printFontList.Add(new ReceiptFont(mediumFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 0f;
					brushes.percentageOfPageWidthList.Add(80f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList[0], brushes.printFontList[0], brushes.printBrushList[0], (float)ps.Margins.Left, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList[0], false);
					receiptLines1.Add(brushes);
					brushes = new ReceiptStringLine();
					List<string> strs = brushes.printContentList;
					object[] objArray = new object[] { "Room : ", roomStayData.roomName, "Arrival : ", bookingData.checkInDate, "Departure : ", bookingData.checkOutDate };
					strs.Add(string.Concat(objArray));
					brushes.printFontList.Add(new ReceiptFont(mediumFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 0f;
					brushes.percentageOfPageWidthList.Add(80f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList[0], brushes.printFontList[0], brushes.printBrushList[0], (float)ps.Margins.Left, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList[0], false);
					receiptLines1.Add(brushes);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("Booking Source : ", bookingData.sourceName, "Order Code : ", bookingData.bookingOrderCode));
					brushes.printFontList.Add(new ReceiptFont(mediumFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 0f;
					brushes.percentageOfPageWidthList.Add(80f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList[0], brushes.printFontList[0], brushes.printBrushList[0], (float)ps.Margins.Left, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList[0], false);
					receiptLines1.Add(brushes);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("Kiosk Name : ", kioskData.kioskUserName));
					brushes.printFontList.Add(new ReceiptFont(mediumFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 2f;
					brushes.percentageOfPageWidthList.Add(80f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList[0], brushes.printFontList[0], brushes.printBrushList[0], (float)ps.Margins.Left, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList[0], false);
					receiptLines1.Add(brushes);
					top.height = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines1) - top.startY;
					top.printOffsetAfter = 10f;
					receiptLines.AddRange(receiptLines1);
					receiptLines.Add(top);
					float receiptItemTotalHeight = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines);
					float single = 5f;
					float[] singleArray = new float[] { 0.06f, 0.14f, 0.55f, 0.25f };
					List<ReceiptLine> receiptLines2 = new List<ReceiptLine>(receiptLines);
					ReceiptPenLine receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 10f,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width
					};
					receiptLines2.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					List<string> strs1 = brushes.printContentList;
					DateTime now = DateTime.Now;
					strs1.Add(string.Concat("Printed Date : ", now.ToString("dd/MM/yyyy HH:mm")));
					brushes.printFontList.Add(new ReceiptFont(smallFont, FontStyle.Italic));
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats = brushes.printStringFormatList;
					StringFormat stringFormat = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats.Add(stringFormat);
					brushes.printOffsetAfter = 0f;
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left + brushes.paddingLeft, brushes.spaceBetweenCell, 0f, width - brushes.paddingRight, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 10f,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width,
						paddingLeft = single,
						paddingRight = single
					};
					receiptLines2.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("Receipt Number : ", invoiceNumber));
					List<string> strs2 = brushes.printContentList;
					now = transaction.transactionDateTime;
					strs2.Add(now.ToString("dd/MM/yyyy HH:mm"));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats1 = brushes.printStringFormatList;
					StringFormat stringFormat1 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats1.Add(stringFormat1);
					List<StringFormat> stringFormats2 = brushes.printStringFormatList;
					StringFormat stringFormat2 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats2.Add(stringFormat2);
					brushes.printOffsetAfter = 10f;
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.percentageOfPageWidthList.Add(0.6f);
					brushes.percentageOfPageWidthList.Add(0.4f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left + brushes.paddingLeft, brushes.spaceBetweenCell, 0f, width - brushes.paddingRight, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 5f,
						paddingLeft = single,
						paddingRight = single,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width
					};
					receiptLines2.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add("S/N");
					brushes.printContentList.Add("Date");
					brushes.printContentList.Add("Description");
					brushes.printContentList.Add("Amount");
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats3 = brushes.printStringFormatList;
					StringFormat stringFormat3 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats3.Add(stringFormat3);
					List<StringFormat> stringFormats4 = brushes.printStringFormatList;
					StringFormat stringFormat4 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats4.Add(stringFormat4);
					List<StringFormat> stringFormats5 = brushes.printStringFormatList;
					StringFormat stringFormat5 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats5.Add(stringFormat5);
					List<StringFormat> stringFormats6 = brushes.printStringFormatList;
					StringFormat stringFormat6 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats6.Add(stringFormat6);
					brushes.spaceBetweenCell = 5f;
					brushes.printOffsetAfter = 5f;
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left + brushes.paddingLeft, brushes.spaceBetweenCell, 0f, width - brushes.paddingRight, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 5f,
						paddingLeft = single,
						paddingRight = single,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width
					};
					receiptLines2.Add(receiptPenLine);
					num1 = 1;
					foreach (BookingChargeData chargeDatum in chargeData)
					{
						brushes = new ReceiptStringLine();
						List<string> strs3 = new List<string>()
						{
							num1.ToString()
						};
						now = chargeDatum.chargeDateTime;
						strs3.Add(now.ToString("dd/MM/yyyy"));
						strs3.Add(chargeDatum.chargeName);
						num = chargeDatum.outstanding;
						strs3.Add(num.ToString("0.00"));
						brushes.printContentList = strs3;
						List<ReceiptFont> receiptFonts = new List<ReceiptFont>()
						{
							new ReceiptFont(mediumFont),
							new ReceiptFont(mediumFont),
							new ReceiptFont(mediumFont),
							new ReceiptFont(mediumFont)
						};
						brushes.printFontList = receiptFonts;
						brushes.printBrushList = new List<Brush>()
						{
							brush,
							brush,
							brush,
							brush
						};
						List<StringFormat> stringFormats7 = new List<StringFormat>();
						StringFormat stringFormat7 = new StringFormat()
						{
							Alignment = StringAlignment.Near,
							LineAlignment = StringAlignment.Near
						};
						stringFormats7.Add(stringFormat7);
						StringFormat stringFormat8 = new StringFormat()
						{
							Alignment = StringAlignment.Near,
							LineAlignment = StringAlignment.Near
						};
						stringFormats7.Add(stringFormat8);
						StringFormat stringFormat9 = new StringFormat()
						{
							Alignment = StringAlignment.Near,
							LineAlignment = StringAlignment.Near
						};
						stringFormats7.Add(stringFormat9);
						StringFormat stringFormat10 = new StringFormat()
						{
							Alignment = StringAlignment.Far,
							LineAlignment = StringAlignment.Near
						};
						stringFormats7.Add(stringFormat10);
						brushes.printStringFormatList = stringFormats7;
						brushes.printOffsetAfter = 5f;
						brushes.spaceBetweenCell = 5f;
						brushes.paddingLeft = single;
						brushes.paddingRight = single;
						brushes.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
						brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
						receiptLines2.Add(brushes);
						receiptPenLine = new ReceiptPenLine()
						{
							printPen = normalPen,
							printOffsetAfter = 5f,
							paddingLeft = single,
							paddingRight = single,
							startX = (float)ps.Margins.Left,
							startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
							height = 1f,
							width = width
						};
						receiptLines2.Add(receiptPenLine);
						num1++;
					}
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("Currency : ", kioskData.currency));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats8 = brushes.printStringFormatList;
					StringFormat stringFormat11 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats8.Add(stringFormat11);
					brushes.printOffsetAfter = 0f;
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.percentageOfPageWidthList.Add(0.35f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left + brushes.paddingLeft, brushes.spaceBetweenCell, 0f, width - brushes.paddingRight, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 10f,
						paddingLeft = single,
						paddingRight = single,
						startX = (float)ps.Margins.Left + width * 0.35f + 5f + 5f,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width * 0.65f - 5f - 5f
					};
					receiptLines2.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add("Total");
					List<string> strs4 = brushes.printContentList;
					string str1 = kioskData.currency;
					num = chargeData.Sum<BookingChargeData>((BookingChargeData x) => x.outstanding);
					strs4.Add(string.Concat(str1, " ", num.ToString("0.00")));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats9 = brushes.printStringFormatList;
					StringFormat stringFormat12 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats9.Add(stringFormat12);
					List<StringFormat> stringFormats10 = brushes.printStringFormatList;
					StringFormat stringFormat13 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats10.Add(stringFormat13);
					brushes.percentageOfPageWidthList.Add(0.65f);
					brushes.percentageOfPageWidthList.Add(0.35f);
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.printOffsetAfter = 10f;
					brushes.spaceBetweenCell = 5f;
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 5f,
						paddingLeft = single,
						paddingRight = single,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width
					};
					receiptLines2.Add(receiptPenLine);
					num1 = 1;
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(num1.ToString());
					List<string> strs5 = brushes.printContentList;
					now = transaction.transactionDateTime;
					strs5.Add(now.ToString("dd/MM/yyyy"));
					List<string> strs6 = brushes.printContentList;
					string[] eMV = new string[] { invoiceNumber, " Payment [", transaction.cardLabel, ": ", transaction.approvalCode, "]" };
					strs6.Add(string.Concat(eMV));
					List<string> strs7 = brushes.printContentList;
					num = transaction.transactionAmount;
					strs7.Add(num.ToString("0.00"));
					List<ReceiptFont> receiptFonts1 = new List<ReceiptFont>()
					{
						new ReceiptFont(mediumFont),
						new ReceiptFont(mediumFont),
						new ReceiptFont(mediumFont),
						new ReceiptFont(mediumFont)
					};
					brushes.printFontList = receiptFonts1;
					brushes.printBrushList = new List<Brush>()
					{
						brush,
						brush,
						brush,
						brush
					};
					List<StringFormat> stringFormats11 = new List<StringFormat>();
					StringFormat stringFormat14 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats11.Add(stringFormat14);
					StringFormat stringFormat15 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats11.Add(stringFormat15);
					StringFormat stringFormat16 = new StringFormat()
					{
						Alignment = StringAlignment.Near,
						LineAlignment = StringAlignment.Near
					};
					stringFormats11.Add(stringFormat16);
					StringFormat stringFormat17 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats11.Add(stringFormat17);
					brushes.printStringFormatList = stringFormats11;
					brushes.printOffsetAfter = 5f;
					brushes.spaceBetweenCell = 5f;
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					brushes.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
					brushes.printOffsetAfter = 5f;
					brushes.spaceBetweenCell = 5f;
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 10f,
						paddingLeft = single,
						paddingRight = single,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2),
						height = 1f,
						width = width
					};
					receiptLines2.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add("Balance");
					List<string> strs8 = brushes.printContentList;
					string str2 = kioskData.currency;
					num = chargeData.Sum<BookingChargeData>((BookingChargeData x) => x.outstanding) - transaction.transactionAmount;
					strs8.Add(string.Concat(str2, " ", num.ToString("0.00")));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printFontList.Add(new ReceiptFont(largeFont, FontStyle.Bold));
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					brushes.paddingLeft = single;
					brushes.paddingRight = single;
					List<StringFormat> stringFormats12 = brushes.printStringFormatList;
					StringFormat stringFormat18 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats12.Add(stringFormat18);
					List<StringFormat> stringFormats13 = brushes.printStringFormatList;
					StringFormat stringFormat19 = new StringFormat()
					{
						Alignment = StringAlignment.Far,
						LineAlignment = StringAlignment.Near
					};
					stringFormats13.Add(stringFormat19);
					brushes.percentageOfPageWidthList.Add(0.65f);
					brushes.percentageOfPageWidthList.Add(0.35f);
					brushes.printOffsetAfter = 10f;
					brushes.spaceBetweenCell = 5f;
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines2.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 0f,
						donAddHeight = true,
						startY = receiptItemTotalHeight,
						startX = (float)ps.Margins.Left,
						width = 1f,
						height = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2) - ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines)
					};
					receiptLines2.Insert(0, receiptPenLine);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 0f,
						donAddHeight = true,
						startY = receiptItemTotalHeight,
						startX = (float)ps.Margins.Left + width,
						width = 1f,
						height = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines2) - ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines)
					};
					receiptLines2.Insert(0, receiptPenLine);
					receiptLines = receiptLines2;
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = normalPen,
						printOffsetAfter = 25f,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines),
						height = 1f,
						width = width
					};
					receiptLines.Add(receiptPenLine);
					top = signatureImage;
					top.donAddHeight = true;
					top.startX = width * 0.75f;
					top.width = width - top.startX;
					top.startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add("GUEST SIGN : ");
					brushes.printContentList.Add("");
					brushes.printFontList.Add(new ReceiptFont(largeFont));
					brushes.printFontList.Add(new ReceiptFont(largeFont));
					brushes.printBrushList.Add(brush);
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Far
					});
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Far
					});
					brushes.printOffsetAfter = 10f;
					brushes.spaceBetweenCell = 5f;
					brushes.percentageOfPageWidthList.Add(0.75f);
					brushes.percentageOfPageWidthList.Add(0.25f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					top.height = brushes.printHeight;
					top.printOffsetAfter = (this.receipt.TandCList.Count > 0 ? 0f : 25f);
					receiptLines.Add(top);
					for (int j = 0; j < this.receipt.TandCList.Count; j++)
					{
						brushes = new ReceiptStringLine();
						brushes.printContentList.Add(this.receipt.TandCList[j]);
						brushes.printFontList.Add(new ReceiptFont(mediumFont));
						brushes.printBrushList.Add(brush);
						brushes.printStringFormatList.Add(new StringFormat()
						{
							Alignment = StringAlignment.Near
						});
						brushes.printOffsetAfter = (j + 1 == this.receipt.TandCList.Count ? 25f : 0f);
						brushes.percentageOfPageWidthList.Add(1f);
						brushes.printHeight = this.DrawString(g, brushes.printContentList[0], brushes.printFontList[0], brushes.printBrushList[0], (float)ps.Margins.Left, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList[0], false);
						receiptLines.Add(brushes);
					}
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(this.receipt.appreciateTerm);
					brushes.printFontList.Add(new ReceiptFont(mediumFont, FontStyle.Italic));
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats14 = brushes.printStringFormatList;
					StringFormat stringFormat20 = new StringFormat()
					{
						Alignment = StringAlignment.Center,
						LineAlignment = StringAlignment.Center
					};
					stringFormats14.Add(stringFormat20);
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printOffsetAfter = 0f;
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(kioskData.webSite);
					brushes.printFontList.Add(new ReceiptFont(mediumFont));
					brushes.printBrushList.Add(brush);
					List<StringFormat> stringFormats15 = brushes.printStringFormatList;
					StringFormat stringFormat21 = new StringFormat()
					{
						Alignment = StringAlignment.Center,
						LineAlignment = StringAlignment.Center
					};
					stringFormats15.Add(stringFormat21);
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printOffsetAfter = 20f;
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					receiptPenLine = new ReceiptPenLine()
					{
						printPen = dashPen,
						printOffsetAfter = 10f,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines),
						height = 1f,
						width = width
					};
					receiptLines.Add(receiptPenLine);
					brushes = new ReceiptStringLine();
					List<string> strs9 = brushes.printContentList;
					eMV = new string[] { "Date & Time : ", null, null, null, null, null };
					now = transaction.transactionDateTime;
					eMV[1] = now.ToString("yyyy-MM-dd hh:mm:ss");
					eMV[2] = " INVOICE# : ";
					eMV[3] = transaction.invoice;
					eMV[4] = " BATCH# : ";
					eMV[5] = transaction.batch;
					strs9.Add(string.Concat(eMV));
					brushes.printFontList.Add(new ReceiptFont(smallFont));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 5f;
					brushes.spaceBetweenCell = 5f;
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					brushes = new ReceiptStringLine();
					brushes.printContentList.Add(string.Concat("TID : ", transaction.terminalId, " MID : ", transaction.mechantId));
					brushes.printFontList.Add(new ReceiptFont(smallFont));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 5f;
					brushes.spaceBetweenCell = 5f;
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					brushes = new ReceiptStringLine();
					List<string> strs10 = brushes.printContentList;
					eMV = new string[] { transaction.entryMode, " ", transaction.cardLabel, " ", transaction.cardNumber, " APPR CODE : ", transaction.approvalCode, " REF# : ", transaction.retrievalReference };
					strs10.Add(string.Concat(eMV));
					brushes.printFontList.Add(new ReceiptFont(smallFont));
					brushes.printBrushList.Add(brush);
					brushes.printStringFormatList.Add(new StringFormat()
					{
						Alignment = StringAlignment.Near
					});
					brushes.printOffsetAfter = 5f;
					brushes.spaceBetweenCell = 5f;
					brushes.percentageOfPageWidthList.Add(1f);
					brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
					receiptLines.Add(brushes);
					if (transaction.EMV != null)
					{
						brushes = new ReceiptStringLine();
						List<string> strs11 = brushes.printContentList;
						eMV = new string[] { "EMV CHIP* : ", transaction.EMV.appLabel, " TC : ", transaction.EMV.TC, " AID : ", transaction.EMV.AID };
						strs11.Add(string.Concat(eMV));
						brushes.printFontList.Add(new ReceiptFont(smallFont));
						brushes.printBrushList.Add(brush);
						brushes.printStringFormatList.Add(new StringFormat()
						{
							Alignment = StringAlignment.Near
						});
						brushes.printOffsetAfter = 5f;
						brushes.spaceBetweenCell = 5f;
						brushes.percentageOfPageWidthList.Add(1f);
						brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
						receiptLines.Add(brushes);
						brushes = new ReceiptStringLine();
						brushes.printContentList.Add(string.Concat("TVR : ", transaction.EMV.TVR, " TSI : ", transaction.EMV.TSI));
						brushes.printFontList.Add(new ReceiptFont(smallFont));
						brushes.printBrushList.Add(brush);
						brushes.printStringFormatList.Add(new StringFormat()
						{
							Alignment = StringAlignment.Near
						});
						brushes.printOffsetAfter = 10f;
						brushes.spaceBetweenCell = 5f;
						brushes.percentageOfPageWidthList.Add(1f);
						brushes.printHeight = this.DrawString(g, brushes.printContentList, brushes.percentageOfPageWidthList, brushes.printFontList, brushes.printBrushList, (float)ps.Margins.Left, brushes.spaceBetweenCell, 0f, width, brushes.printOffsetAfter, brushes.printStringFormatList, false);
						receiptLines.Add(brushes);
					}
					receiptPenLine = new ReceiptPenLine()
					{
						printOffsetAfter = 0f,
						printPen = dashPen,
						startX = (float)ps.Margins.Left,
						startY = ReceiptLine.GetReceiptItemTotalHeight((float)ps.Margins.Top, receiptLines),
						width = width,
						height = 1f
					};
					receiptLines.Add(receiptPenLine);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return receiptLines;
		}

		public void Print(Control control, Action<string> complete, Action<Exception> log, ReceiptPrintingParameter param)
		{
			Font font = null;
			Font font1 = null;
			Font font2 = null;
			Brush solidBrush = null;
			Pen pen = null;
			Pen pen1 = null;
			PDFConvertor pDFConvertor = null;
			try
			{
				try
				{
					font = new Font("Arial", 8f, GraphicsUnit.World);
					font1 = new Font("Arial", 9f, GraphicsUnit.World);
					font2 = new Font("Arial", 10f, GraphicsUnit.World);
					solidBrush = new SolidBrush(Color.Black);
					pen = new Pen(solidBrush)
					{
						Color = Color.Black
					};
					pen1 = new Pen(solidBrush)
					{
						Color = Color.Black,
						DashPattern = new float[] { 5f, 2f, 5f, 2f }
					};
					string receiptFilePath = FileHelper.GetReceiptFilePath(param.FileName);
					pDFConvertor = new PDFConvertor(this.receipt, false);
					ReceiptPDFGenerateParameter receiptPDFGenerateParameter = new ReceiptPDFGenerateParameter(receiptFilePath, PdfSharp.PageSize.A4, param)
					{
						SmallFont = font,
						MediumFont = font1,
						LargeFont = font2,
						Brush = solidBrush,
						NormalPen = pen,
						DashPen = pen1
					};
					pDFConvertor.GenerateReceipt(control, log, receiptPDFGenerateParameter);
					if (!string.IsNullOrEmpty(ConstantVariable_App.printerName) && this.receipt.physicalReceipt)
					{
						using (PrintDocument printDocument = new PrintDocument())
						{
							printDocument.DefaultPageSettings.Margins = param.Margins;
							printDocument.DefaultPageSettings.PaperSize = printDocument.PrinterSettings.PaperSizes.Cast<PaperSize>().FirstOrDefault<PaperSize>((PaperSize e) => e.Kind == PaperKind.A5);
							using (Graphics graphic = printDocument.PrinterSettings.CreateMeasurementGraphics())
							{
								List<ReceiptLine> receiptLines = this.InitailizePrintItem(param.KioskData, param.BookingData, param.RoomStayData, param.GuestData, param.ChargeData, param.Transaction, graphic, printDocument.DefaultPageSettings, param.InvoiceNumber, param.LogoImage, param.SignatureImage, font2, font1, font, solidBrush, pen1, pen);
								printDocument.PrintController = new StandardPrintController();
								printDocument.PrinterSettings.PrinterName = ConstantVariable_App.printerName;
								printDocument.PrinterSettings.PrintToFile = false;
								printDocument.DocumentName = param.FileName;
								printDocument.PrintPage += new PrintPageEventHandler((object sender, PrintPageEventArgs e) => this.PrintReceiptItem(sender, e, control, receiptLines, log));
								printDocument.Print();
							}
						}
					}
					if (complete != null)
					{
						if (!control.InvokeRequired)
						{
							complete(receiptFilePath);
						}
						else
						{
							Control control1 = control;
							object[] objArray = new object[] { receiptFilePath };
							control1.BeginInvoke(complete, objArray);
						}
					}
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					if (log != null)
					{
						if (!control.InvokeRequired)
						{
							log(exception);
						}
						else
						{
							Control control2 = control;
							Action<Exception> action = log;
							object[] objArray1 = new object[] { exception };
							control2.BeginInvoke(action, objArray1);
						}
					}
				}
			}
			finally
			{
				if (font != null)
				{
					font.Dispose();
				}
				if (font1 != null)
				{
					font1.Dispose();
				}
				if (font2 != null)
				{
					font2.Dispose();
				}
				if (solidBrush != null)
				{
					solidBrush.Dispose();
				}
				if (pen != null)
				{
					pen.Dispose();
				}
				if (pen1 != null)
				{
					pen1.Dispose();
				}
			}
		}

		public void PrintReceiptItem(object sender, PrintPageEventArgs ev, Control control, List<ReceiptLine> receiptItemList, Action<Exception> log)
		{
			try
			{
				float top = (float)ev.PageSettings.Margins.Top;
				if (this.currentLine > 0)
				{
					this.accumulateHeight -= (float)ev.PageSettings.Margins.Top;
				}
				float width = (float)(ev.PageSettings.PaperSize.Width - ev.PageSettings.Margins.Left - ev.PageSettings.Margins.Right);
				float height = (float)(ev.PageSettings.PaperSize.Height - ev.PageSettings.Margins.Top - ev.PageSettings.Margins.Bottom);
				while (this.currentLine < receiptItemList.Count)
				{
					if (receiptItemList[this.currentLine] is ReceiptImageLine)
					{
						ReceiptImageLine item = (ReceiptImageLine)receiptItemList[this.currentLine];
						if (!string.IsNullOrEmpty(item.printImagePath))
						{
							bool flag = false;
							if (item.isWeb)
							{
								HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("url");
								httpWebRequest.Method = "HEAD";
								try
								{
									using (WebResponse response = httpWebRequest.GetResponse())
									{
										flag = true;
									}
								}
								catch (Exception exception1)
								{
									Exception exception = exception1;
									if (!control.InvokeRequired)
									{
										log(exception);
									}
									else
									{
										object[] objArray = new object[] { exception };
										control.BeginInvoke(log, objArray);
									}
								}
							}
							else if (File.Exists(item.printImagePath))
							{
								flag = true;
							}
							if (flag)
							{
								using (Image image = Image.FromFile(item.printImagePath))
								{
									if ((float)image.Size.Height + item.printOffsetAfter + top <= height)
									{
										ev.Graphics.DrawImage(image, item.startX + item.paddingLeft, item.startY, item.width - item.paddingRight, item.height);
										if (!item.donAddHeight)
										{
											top += Convert.ToSingle(image.Size.Height);
										}
										top += item.printOffsetAfter;
									}
									else
									{
										break;
									}
								}
							}
						}
						this.currentLine++;
					}
					else if (receiptItemList[this.currentLine] is ReceiptPenLine)
					{
						ReceiptPenLine receiptPenLine = (ReceiptPenLine)receiptItemList[this.currentLine];
						if (receiptPenLine.printOffsetAfter + receiptPenLine.height + top <= height)
						{
							float single = receiptPenLine.startY;
							float single1 = receiptPenLine.startY + receiptPenLine.height;
							if (this.currentPage > 0)
							{
								while (single > this.accumulateHeight)
								{
									single -= this.accumulateHeight;
								}
								while (single1 > this.accumulateHeight)
								{
									single1 -= this.accumulateHeight;
								}
							}
							ev.Graphics.DrawLine(receiptPenLine.printPen, receiptPenLine.startX + receiptPenLine.paddingLeft, single, receiptPenLine.startX + receiptPenLine.width - receiptPenLine.paddingRight, single1);
							top += receiptPenLine.printOffsetAfter;
							if (!receiptPenLine.donAddHeight)
							{
								top += receiptPenLine.height;
							}
						}
						else
						{
							ev.Graphics.DrawLine(receiptPenLine.printPen, receiptPenLine.startX + receiptPenLine.paddingLeft, receiptPenLine.startY, receiptPenLine.startX + receiptPenLine.width - receiptPenLine.paddingRight, height);
							float single2 = receiptPenLine.printOffsetAfter + receiptPenLine.height + top - height;
							ReceiptPenLine receiptPenLine1 = receiptPenLine;
							receiptPenLine1.height = single2;
							ReceiptPenLine receiptPenLine2 = receiptPenLine;
							receiptPenLine2.height = receiptPenLine2.height - single2;
							receiptItemList.Insert(this.currentLine + 1, receiptPenLine1);
							top += receiptPenLine.printOffsetAfter;
							if (!receiptPenLine.donAddHeight)
							{
								top += receiptPenLine.height;
								break;
							}
						}
						this.currentLine++;
					}
					else if (!(receiptItemList[this.currentLine] is ReceiptStringLine))
					{
						top += receiptItemList[this.currentLine].printOffsetAfter;
						this.currentLine++;
					}
					else
					{
						ReceiptStringLine receiptStringLine = (ReceiptStringLine)receiptItemList[this.currentLine];
						if (receiptStringLine.printOffsetAfter + receiptStringLine.printHeight + top > height)
						{
							break;
						}
						float single3 = top;
						if (receiptStringLine.printContentList.Count > 1)
						{
							single3 = this.DrawString(ev.Graphics, receiptStringLine.printContentList, receiptStringLine.percentageOfPageWidthList, receiptStringLine.printFontList, receiptStringLine.printBrushList, (float)ev.PageSettings.Margins.Left + receiptStringLine.paddingLeft, receiptStringLine.spaceBetweenCell, top, width - receiptStringLine.paddingRight, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList, true);
						}
						else if (receiptStringLine.printContentList.Count == 1)
						{
							single3 = this.DrawString(ev.Graphics, receiptStringLine.printContentList[0], receiptStringLine.printFontList[0], receiptStringLine.printBrushList[0], (float)ev.PageSettings.Margins.Left + receiptStringLine.paddingLeft, top, width - receiptStringLine.paddingRight, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList[0], true);
						}
						if (receiptStringLine.donAddHeight)
						{
							top += receiptStringLine.printOffsetAfter;
						}
						else
						{
							top = single3;
						}
						this.currentLine++;
					}
				}
				if (this.currentLine >= receiptItemList.Count)
				{
					ev.HasMorePages = false;
				}
				else
				{
					this.currentLine++;
					this.accumulateHeight += top;
					ev.HasMorePages = true;
				}
			}
			catch (Exception exception3)
			{
				Exception exception2 = exception3;
				if (!control.InvokeRequired)
				{
					log(exception2);
				}
				else
				{
					object[] objArray1 = new object[] { exception2 };
					control.BeginInvoke(log, objArray1);
				}
			}
		}
	}
}