using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using PdfSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptPDFGenerateParameter
	{
		public ReceiptImageLine BankLogoImage
		{
			get;
			set;
		}

		public KioskBusinessEntity.Web_Service_Entity.BookingData BookingData
		{
			get;
			set;
		}

		public System.Drawing.Brush Brush
		{
			get;
			set;
		}

		public List<BookingChargeData> ChargeData
		{
			get;
			set;
		}

		public Pen DashPen
		{
			get;
			set;
		}

		public string FileName
		{
			get;
			set;
		}

		public string FilePath
		{
			get;
			set;
		}

		public KioskCheckInData GuestData
		{
			get;
			set;
		}

		public string InvoiceNumber
		{
			get;
			set;
		}

		public KioskBusinessEntity.Kiosk_Entity.KioskData KioskData
		{
			get;
			set;
		}

		public Font LargeFont
		{
			get;
			set;
		}

		public ReceiptImageLine LogoImage
		{
			get;
			set;
		}

		public System.Drawing.Printing.Margins Margins
		{
			get;
			set;
		}

		public Font MediumFont
		{
			get;
			set;
		}

		public Pen NormalPen
		{
			get;
			set;
		}

		public PageSize PdfPageSize
		{
			get;
			set;
		}

		public KioskBusinessEntity.Web_Service_Entity.RoomStayData RoomStayData
		{
			get;
			set;
		}

		public ReceiptImageLine SignatureImage
		{
			get;
			set;
		}

		public Font SmallFont
		{
			get;
			set;
		}

		public TransactionDetail Transaction
		{
			get;
			set;
		}

		public ReceiptPDFGenerateParameter(string filePath, PageSize pageSize, ReceiptPrintingParameter param)
		{
			this.FilePath = filePath;
			this.FileName = param.FileName;
			this.KioskData = param.KioskData;
			this.BookingData = param.BookingData;
			this.RoomStayData = param.RoomStayData;
			this.ChargeData = param.ChargeData;
			this.GuestData = param.GuestData;
			this.Transaction = param.Transaction;
			this.InvoiceNumber = param.InvoiceNumber;
			this.PdfPageSize = pageSize;
			this.Margins = param.Margins;
			this.LogoImage = param.LogoImage;
			this.SignatureImage = param.SignatureImage;
			this.BankLogoImage = param.BankLogoImage;
			this.SmallFont = null;
			this.MediumFont = null;
			this.LargeFont = null;
			this.Brush = null;
			this.NormalPen = null;
			this.DashPen = null;
		}
	}
}