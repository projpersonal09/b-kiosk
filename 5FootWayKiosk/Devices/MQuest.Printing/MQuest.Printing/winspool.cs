using System;
using System.Runtime.InteropServices;

namespace MQuest.Printing
{
	public class winspool
	{
		public winspool()
		{
		}

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int LoadLibrary(string strDllName);

		[DllImport("winspool.drv", CharSet=CharSet.Auto, ExactSpelling=false, SetLastError=true)]
		public static extern bool SetDefaultPrinter(string Name);
	}
}