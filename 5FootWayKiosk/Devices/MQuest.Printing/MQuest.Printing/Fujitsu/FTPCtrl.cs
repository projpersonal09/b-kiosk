using System;
using System.Runtime.InteropServices;

namespace MQuest.Printing.Fujitsu
{
	public class FTPCtrl
	{
		public FTPCtrl()
		{
		}

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_GetVendorCommand(ref FTPCtrl._VENDOR_COMMAND lpVendorCmd, ref FTPCtrl._RECEIVE_DATA lpRcvData);

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_GetVendorCommand_B(ref FTPCtrl._VENDOR_COMMAND lpVendorCmd, ref FTPCtrl._RECEIVE_DATA lpRcvData);

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_GetVendorCommandEx(string lpPrinterDriverName, ref FTPCtrl._VENDOR_COMMAND lpVendorCmd, ref FTPCtrl._RECEIVE_DATA lpRcvData);

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_GetVendorCommandEx_B(string lpPrinterDriverName, ref FTPCtrl._VENDOR_COMMAND lpVendorCmd, ref FTPCtrl._RECEIVE_DATA lpRcvData);

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_Search_USB();

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_Search_USB_B();

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_Search_USBEx(string lpPrinterDriverName);

		[DllImport("FTPCtrl.dll", CallingConvention=CallingConvention.StdCall, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern uint FclTP_Search_USBEx_B(string lpPrinterDriverName);

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern bool FreeLibrary(int libraryHandler);

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int LoadLibrary(string strDllName);

		public struct _RECEIVE_DATA
		{
			public ushort unitLength;

			public bool DataValid;

			public uint DataLength;

			public byte[] Data;
		}

		public struct _VENDOR_COMMAND
		{
			public ushort unitLength;

			public byte bRequest;

			public byte wValueH;

			public byte wValueL;

			public byte wIndexH;

			public byte wIndexL;

			public byte wLengthH;

			public byte wLengthL;
		}
	}
}