using MQuest.Printing;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace MQuest.Printing.Fujitsu
{
	public class FujitsuPrinterManager : IPrinter, IDisposable
	{
		private string printerName;

		private int libraryHandler;

		private bool requiredPrinterName;

		public FujitsuPrinterManager()
		{
		}

		public void CheckPrinterStatus()
		{
			try
			{
				if (!string.IsNullOrEmpty(this.printerName))
				{
					FTPCtrl._RECEIVE_DATA _RECEIVEDATum = new FTPCtrl._RECEIVE_DATA();
					FTPCtrl._VENDOR_COMMAND _VENDORCOMMAND = new FTPCtrl._VENDOR_COMMAND()
					{
						bRequest = 1,
						wValueH = 0,
						wValueL = 0,
						wIndexH = 0,
						wIndexL = 0,
						wLengthH = 0,
						wLengthL = 1,
						unitLength = Convert.ToUInt16(Marshal.SizeOf(typeof(FTPCtrl._VENDOR_COMMAND)))
					};
					if (this.requiredPrinterName)
					{
						if (FTPCtrl.FclTP_GetVendorCommandEx(this.printerName, ref _VENDORCOMMAND, ref _RECEIVEDATum) != 1)
						{
							throw new Exception("failed to get printer status and vendor information.");
						}
					}
					else if (FTPCtrl.FclTP_GetVendorCommand(ref _VENDORCOMMAND, ref _RECEIVEDATum) != 1)
					{
						throw new Exception("failed to get printer status and vendor information.");
					}
					if (!_RECEIVEDATum.DataValid)
					{
						throw new Exception("Data is not valid.");
					}
					if (_RECEIVEDATum.Data[2].Equals(4))
					{
						throw new Exception("Out of paper.");
					}
					if (_RECEIVEDATum.Data[1].Equals(4))
					{
						throw new Exception("Platen opened.");
					}
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Dispose()
		{
			try
			{
				if (this.libraryHandler != 0)
				{
					FTPCtrl.FreeLibrary(this.libraryHandler);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public List<string> GetPrinterList()
		{
			return PrinterSettings.InstalledPrinters.Cast<string>().ToList<string>();
		}

		public void Init(string printerName)
		{
			try
			{
				if (string.IsNullOrEmpty(printerName))
				{
					throw new Exception("Printer is not selected.");
				}
				this.printerName = printerName;
				this.libraryHandler = FTPCtrl.LoadLibrary("FTPCtrl.dll");
				if (this.libraryHandler == 0)
				{
					throw new FileNotFoundException("Cannot load library FTPCtrl.dll.");
				}
				if (FTPCtrl.FclTP_Search_USB() == 1)
				{
					this.requiredPrinterName = false;
				}
				else
				{
					this.requiredPrinterName = true;
					if (FTPCtrl.FclTP_Search_USBEx(printerName) != 1)
					{
						throw new Exception("Printer driver not normally installed.");
					}
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}
	}
}