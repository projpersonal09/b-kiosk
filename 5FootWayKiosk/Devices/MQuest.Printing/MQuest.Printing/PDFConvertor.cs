using Helper;
using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace MQuest.Printing
{
	public class PDFConvertor
	{
		private const float spaceBetweenLine = 2f;

		private string filePath;

		private string fileName;

		private MyLog _logFile;

		private KioskData kioskData;

		private BookingData bookingData;

		private RoomStayData roomStayData;

		private List<BookingChargeData> chargeData;

		private KioskCheckInData guestData;

		private TransactionDetail transaction;

		private string invoiceNumber;

		private PageSize pageSize;

		private Margins margins;

		private ReceiptImageLine logoImage;

		private ReceiptImageLine signatureImage;

		private ReceiptImageLine bankLogoImage;

		private Font smallFont;

		private Font mediumFont;

		private Font largeFont;

		private Brush brush;

		private Pen dashPen;

		private Pen normalPen;

		public bool LogOutput
		{
			get;
			set;
		}

		public ConstantVariable_Receipt receipt
		{
			get;
			set;
		}

		public PDFConvertor(ConstantVariable_Receipt receipt, bool logOutput)
		{
			this.receipt = receipt;
			this.LogOutput = logOutput;
			this._logFile = new MyLog();
		}

		private void convertToPDF(Control control, Action<Exception> logException, List<ReceiptLine> receiptItemList, List<ReceiptPenLine> tableBorders)
		{
			using (PdfDocument pdfDocument = new PdfDocument())
			{
				int num = 0;
				int num1 = 0;
				float top = 0f;
				bool flag = false;
				int num2 = receiptItemList.Count<ReceiptLine>();
				this.logInfo(string.Concat("Total Receipt Line: ", num2.ToString()), false);
				while (num < receiptItemList.Count<ReceiptLine>())
				{
					PdfPage pdfPage = pdfDocument.AddPage();
					pdfPage.Size = this.pageSize;
					flag = false;
					using (XGraphics xGraphic = XGraphics.FromPdfPage(pdfPage))
					{
						float item = (float)this.margins.Top;
						if (num1 > 0)
						{
							top -= (float)this.margins.Top;
						}
						this.logInfo(string.Concat("Accumulate Height: ", top.ToString()), false);
						float single = Convert.ToSingle((double)pdfPage.Width) - (float)this.margins.Left - (float)this.margins.Right;
						float single1 = Convert.ToSingle((double)pdfPage.Height) - (float)this.margins.Bottom;
						this.printBorderLines(xGraphic, tableBorders, single, single1, num1, ref item, top);
						do
						{
							if (num >= receiptItemList.Count<ReceiptLine>())
							{
								break;
							}
							if (receiptItemList[num] is ReceiptImageLine)
							{
								this.printImageLine(control, logException, xGraphic, receiptItemList[num] as ReceiptImageLine, single, single1, ref num, num1, ref item, top, out flag);
							}
							else if (receiptItemList[num] is ReceiptPenLine)
							{
								this.printPenLine(xGraphic, receiptItemList[num] as ReceiptPenLine, single, single1, receiptItemList, ref num, num1, ref item, top, out flag);
							}
							else if (!(receiptItemList[num] is ReceiptStringLine))
							{
								this.logInfo(string.Concat("Line ", num.ToString().PadLeft(7).PadRight(1), "Nothing to Draw..."), false);
								item += receiptItemList[num].printOffsetAfter;
								num++;
							}
							else
							{
								this.printStringLine(xGraphic, receiptItemList[num] as ReceiptStringLine, single, single1, ref num, ref item, out flag);
							}
							this.logInfo(string.Concat("Y Pos: ", item.ToString()), false);
						}
						while (!flag);
						foreach (ReceiptPenLine tableBorder in tableBorders)
						{
							float single2 = tableBorder.startY;
							if (single2 > top)
							{
								single2 -= top;
							}
							tableBorder.startY = (float)this.margins.Top;
							tableBorder.height = tableBorder.printOffsetAfter + tableBorder.height + single2 - item;
						}
						num1++;
						top += item;
						if (num < receiptItemList.Count<ReceiptLine>())
						{
							int num3 = num1 + 1;
							this.logInfo(string.Concat("Next Page...", num3.ToString()), false);
						}
					}
				}
				this.logInfo("Saving PDF File...", true);
				pdfDocument.Save(this.filePath);
			}
		}

		private void createSignatureLine(XGraphics g, List<ReceiptLine> receiptItemList, float pageWidth)
		{
			ReceiptImageLine receiptItemTotalHeight = this.signatureImage;
			receiptItemTotalHeight.donAddHeight = true;
			receiptItemTotalHeight.startX = pageWidth * 0.75f;
			receiptItemTotalHeight.width = pageWidth - receiptItemTotalHeight.startX;
			receiptItemTotalHeight.startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptItemList);
			ReceiptStringLine receiptStringLine = new ReceiptStringLine();
			receiptStringLine.printContentList.Add("GUEST SIGN : ");
			receiptStringLine.printContentList.Add("");
			receiptStringLine.printFontList.Add(new ReceiptFont(this.largeFont));
			receiptStringLine.printFontList.Add(new ReceiptFont(this.largeFont));
			receiptStringLine.printBrushList.Add(this.brush);
			receiptStringLine.printBrushList.Add(this.brush);
			receiptStringLine.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Far
			});
			receiptStringLine.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Far
			});
			receiptStringLine.printOffsetAfter = 10f;
			receiptStringLine.spaceBetweenCell = 5f;
			receiptStringLine.percentageOfPageWidthList.Add(0.75f);
			receiptStringLine.percentageOfPageWidthList.Add(0.25f);
			receiptStringLine.printHeight = this.DrawString(g, receiptStringLine.printContentList, receiptStringLine.percentageOfPageWidthList, receiptStringLine.printFontList, receiptStringLine.printBrushList, (float)this.margins.Left, receiptStringLine.spaceBetweenCell, 0f, pageWidth, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList, false);
			receiptItemTotalHeight.height = receiptStringLine.printHeight;
			receiptItemTotalHeight.printOffsetAfter = (this.receipt.TandCList.Count > 0 ? 0f : 25f);
			receiptItemList.Add(receiptItemTotalHeight);
			receiptItemList.Add(receiptStringLine);
		}

		private ReceiptStringLine createStringLine(XGraphics g, string text, float pageWidth)
		{
			ReceiptStringLine receiptStringLine = new ReceiptStringLine();
			receiptStringLine.printContentList.Add(text);
			receiptStringLine.printFontList.Add(new ReceiptFont(this.smallFont));
			receiptStringLine.printBrushList.Add(this.brush);
			receiptStringLine.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine.printOffsetAfter = 2f;
			receiptStringLine.percentageOfPageWidthList.Add(1f);
			receiptStringLine.printHeight = this.DrawString(g, receiptStringLine.printContentList[0], receiptStringLine.percentageOfPageWidthList, receiptStringLine.printFontList[0], receiptStringLine.printBrushList[0], (float)this.margins.Left, 0f, pageWidth, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList[0], false);
			return receiptStringLine;
		}

		private float DrawString(XGraphics g, string message, List<float> percentageList, ReceiptFont font, Brush brush, float xPos, float yPos, float pageWidth, float additionalOffsetAfter, StringFormat format, bool print)
		{
			if (percentageList.Count == 0)
			{
				percentageList.Add(1f);
			}
			List<string> strs = new List<string>()
			{
				message
			};
			List<ReceiptFont> receiptFonts = new List<ReceiptFont>()
			{
				font
			};
			List<Brush> brushes = new List<Brush>()
			{
				brush
			};
			List<StringFormat> stringFormats = new List<StringFormat>()
			{
				format
			};
			yPos = this.DrawString(g, strs, percentageList, receiptFonts, brushes, xPos, 0f, yPos, pageWidth, additionalOffsetAfter, stringFormats, print);
			return yPos;
		}

		private float DrawString(XGraphics g, List<string> messageList, List<float> percentageList, List<ReceiptFont> fontList, List<Brush> brushList, float xPos, float spaceBetweenColumn, float yPos, float pageWidth, float additionalOffsetAfter, List<StringFormat> formatList, bool print)
		{
			try
			{
				string empty = string.Empty;
				if (messageList.Count > 0)
				{
					float single = pageWidth - spaceBetweenColumn * (float)(messageList.Count - 1);
					float single1 = 0f;
					List<float> singles = new List<float>();
					List<XSize> xSizes = new List<XSize>();
					for (int i = 0; i < messageList.Count; i++)
					{
						if (percentageList[i] > 1f)
						{
							percentageList[i] = percentageList[i] / 100f;
						}
						singles.Add(single * percentageList[i]);
						int num = Convert.ToInt32(Math.Ceiling((double)singles[i]));
						if (messageList[i] == null)
						{
							messageList[i] = string.Empty;
						}
						using (Font font = new Font(fontList[i].font, fontList[i].style))
						{
							XSize xSize = g.MeasureString(messageList[i], font);
							if (font.Height <= 12)
							{
								xSize.Height = (xSize.Height * 1.14);
							}
							xSizes.Add(xSize);
							if ((double)single1 < xSizes[i].Height)
							{
								XSize item = xSizes[i];
								single1 = Convert.ToSingle(item.Height);
							}
							if (print)
							{
								string str = xSize.Width.ToString();
								double height = xSize.Height;
								empty = string.Format("Width: {0}, Height: {1}, Max Height: {2}", str, height.ToString(), single1.ToString());
								this.logInfo(empty, false);
							}
						}
					}
					if (print)
					{
						XTextFormatter xTextFormatter = new XTextFormatter(g);
						for (int j = 0; j < messageList.Count; j++)
						{
							float item1 = xPos;
							for (int k = 0; k < j; k++)
							{
								item1 = item1 + (singles[k] + spaceBetweenColumn);
							}
							if (xSizes[j].Width <= (double)singles[j])
							{
								float item2 = singles[j];
								XSize xSize1 = xSizes[j];
								float single2 = item2 - Convert.ToSingle(xSize1.Width);
								if (formatList[j].Alignment.Equals(StringAlignment.Center))
								{
									item1 = item1 + single2 / 2f;
									XSize xSize2 = new XSize();
									xSize2.Height=(xSizes[j].Height);
									XSize item3 = xSizes[j];
									xSize2.Width=(item3.Width + (double)(single2 / 2f));
									xSizes[j] = xSize2;
								}
								else if (formatList[j].Alignment.Equals(StringAlignment.Far))
								{
									item1 += single2;
								}
								else if (formatList[j].Alignment.Equals(StringAlignment.Near))
								{
									XSize xSize3 = new XSize();
									xSize3.Height=(xSizes[j].Height);
									xSize3.Width=((double)singles[j]);
									xSizes[j] = xSize3;
								}
							}
							float single3 = yPos;
							if ((double)single1 >= xSizes[j].Height)
							{
								XSize item4 = xSizes[j];
								float single4 = single1 - Convert.ToSingle(item4.Height);
								if (formatList[j].LineAlignment.Equals(StringAlignment.Center))
								{
									single3 = single3 + single4 / 2f;
								}
								else if (formatList[j].LineAlignment.Equals(StringAlignment.Far))
								{
									single3 += single4;
								}
							}
							string str1 = xSizes[j].Width.ToString();
							double height1 = xSizes[j].Height;
							empty = string.Format("Draw Rectangle for Text: Width: {0}, Height: {1}", str1, height1.ToString());
							this.logInfo(empty, false);
							XSize xSize4 = xSizes[j];
							float single5 = Convert.ToSingle(xSize4.Width);
							XSize item5 = xSizes[j];
							RectangleF rectangleF = new RectangleF(item1, single3, single5, Convert.ToSingle(item5.Height));
							using (Font font1 = new Font(fontList[j].font, fontList[j].style))
							{
								xTextFormatter.DrawString(messageList[j], font1, brushList[j], rectangleF);
							}
						}
					}
					yPos = yPos + (single1 + 2f + additionalOffsetAfter);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return yPos;
		}

		private List<ReceiptLine> generatePrintItems(PdfPage pdfPage, XGraphics g, List<ReceiptPenLine> tableBorders)
		{
			decimal num;
			List<ReceiptLine> receiptLines = new List<ReceiptLine>();
			float single = Convert.ToSingle((double)pdfPage.Width) - (float)this.margins.Left - (float)this.margins.Right;
			ReceiptImageLine top = this.logoImage;
			top.printOffsetAfter = 2f;
			top.donAddHeight = true;
			top.startX = single * 0.9f;
			top.width = single - top.startX;
			top.startY = (float)this.margins.Top;
			List<ReceiptLine> receiptLines1 = new List<ReceiptLine>();
			foreach (string str in this.receipt.addressList)
			{
				ReceiptStringLine receiptStringLine = new ReceiptStringLine();
				receiptStringLine.printContentList.Add(str);
				receiptStringLine.printFontList.Add(new ReceiptFont(this.smallFont, FontStyle.Bold));
				receiptStringLine.printBrushList.Add(this.brush);
				receiptStringLine.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine.printOffsetAfter = 5f;
				receiptStringLine.percentageOfPageWidthList.Add(80f);
				receiptStringLine.printHeight = this.DrawString(g, receiptStringLine.printContentList[0], receiptStringLine.percentageOfPageWidthList, receiptStringLine.printFontList[0], receiptStringLine.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine.printOffsetAfter, receiptStringLine.printStringFormatList[0], false);
				receiptLines1.Add(receiptStringLine);
			}
			for (int i = 0; i < this.kioskData.registrationNumber.Count; i++)
			{
				ReceiptStringLine receiptStringLine1 = new ReceiptStringLine();
				receiptStringLine1.printContentList.Add(this.kioskData.registrationNumber[i]);
				receiptStringLine1.printFontList.Add(new ReceiptFont(this.smallFont, FontStyle.Bold));
				receiptStringLine1.printBrushList.Add(this.brush);
				receiptStringLine1.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine1.printOffsetAfter = (i + 1 == this.kioskData.registrationNumber.Count ? 20f : 0f);
				receiptStringLine1.percentageOfPageWidthList.Add(80f);
				receiptStringLine1.printHeight = this.DrawString(g, receiptStringLine1.printContentList[0], receiptStringLine1.percentageOfPageWidthList, receiptStringLine1.printFontList[0], receiptStringLine1.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine1.printOffsetAfter, receiptStringLine1.printStringFormatList[0], false);
				receiptLines1.Add(receiptStringLine1);
			}
			int num1 = 0;
			string empty = string.Empty;
			string empty1 = string.Empty;
			if (this.guestData.roomList.Count > 0)
			{
				while (this.guestData.roomList[num1].guestList.Count.Equals(0))
				{
					num1++;
				}
				empty = this.guestData.roomList[num1].guestList[0].guestName;
				string item = this.guestData.roomList[num1].roomBedLabel;
			}
			ReceiptStringLine receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("GUEST NAME : ", empty));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.percentageOfPageWidthList.Add(80f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList[0], receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList[0], receiptStringLine2.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList[0], false);
			receiptLines1.Add(receiptStringLine2);
			receiptStringLine2 = new ReceiptStringLine();
			List<string> strs = receiptStringLine2.printContentList;
			string[] eMV = new string[] { "Room : ", this.roomStayData.roomName, "   Arrival : ", null, null, null };
			DateTime now = this.bookingData.checkInDate;
			eMV[3] = now.ToString("dd/MM/yyyy");
			eMV[4] = "   Departure : ";
			now = this.bookingData.checkOutDate;
			eMV[5] = now.ToString("dd/MM/yyyy");
			strs.Add(string.Concat(eMV));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.percentageOfPageWidthList.Add(80f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList[0], receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList[0], receiptStringLine2.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList[0], false);
			receiptLines1.Add(receiptStringLine2);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("Booking Source : ", this.bookingData.sourceName, "   Order Code : ", this.bookingData.bookingOrderCode));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.percentageOfPageWidthList.Add(80f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList[0], receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList[0], receiptStringLine2.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList[0], false);
			receiptLines1.Add(receiptStringLine2);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("Kiosk Name : ", this.kioskData.kioskUserName));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.percentageOfPageWidthList.Add(80f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList[0], receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList[0], receiptStringLine2.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList[0], false);
			receiptLines1.Add(receiptStringLine2);
			top.height = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines1) - top.startY - 50f;
			top.printOffsetAfter = 10f;
			if (top.height > this.kioskData.logoHeight && this.kioskData.logoHeight != 0f)
			{
				float single1 = top.height - this.kioskData.logoHeight;
				if (single1 > 0f)
				{
					if (top.printFormat.LineAlignment == StringAlignment.Center)
					{
						ReceiptImageLine receiptImageLine = top;
						receiptImageLine.startY = receiptImageLine.startY + single1 / 2f;
					}
					else if (top.printFormat.LineAlignment == StringAlignment.Far)
					{
						ReceiptImageLine receiptImageLine1 = top;
						receiptImageLine1.startY = receiptImageLine1.startY + single1;
					}
				}
				top.height = this.kioskData.logoHeight;
			}
			if (top.width > this.kioskData.logoWidth && this.kioskData.logoWidth != 0f)
			{
				float single2 = top.width - this.kioskData.logoWidth;
				if (single2 > 0f)
				{
					if (top.printFormat.Alignment == StringAlignment.Center)
					{
						ReceiptImageLine receiptImageLine2 = top;
						receiptImageLine2.startX = receiptImageLine2.startX + single2 / 2f;
					}
					else if (top.printFormat.Alignment == StringAlignment.Far)
					{
						ReceiptImageLine receiptImageLine3 = top;
						receiptImageLine3.startX = receiptImageLine3.startX + single2;
					}
				}
				top.width = this.kioskData.logoWidth;
			}
			receiptLines.AddRange(receiptLines1);
			receiptLines.Add(top);
			float receiptItemTotalHeight = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines);
			float single3 = 5f;
			float[] singleArray = new float[] { 0.05f, 0.12f, 0.6f, 0.23f };
			List<ReceiptLine> receiptLines2 = new List<ReceiptLine>(receiptLines);
			ReceiptPenLine receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single
			};
			receiptLines2.Add(receiptPenLine);
			receiptStringLine2 = new ReceiptStringLine();
			List<string> strs1 = receiptStringLine2.printContentList;
			now = DateTime.Now;
			strs1.Add(string.Concat("Printed Date : ", now.ToString("dd/MM/yyyy HH:mm")));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont, FontStyle.Italic));
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats.Add(stringFormat);
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.percentageOfPageWidthList.Add(1f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left + receiptStringLine2.paddingLeft, receiptStringLine2.spaceBetweenCell, 0f, single - receiptStringLine2.paddingRight, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single,
				paddingLeft = single3,
				paddingRight = single3
			};
			receiptLines2.Add(receiptPenLine);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("Receipt Number : ", this.invoiceNumber));
			List<string> strs2 = receiptStringLine2.printContentList;
			now = this.transaction.transactionDateTime;
			strs2.Add(now.ToString("dd/MM/yyyy HH:mm"));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats1 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat1 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats1.Add(stringFormat1);
			List<StringFormat> stringFormats2 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat2 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats2.Add(stringFormat2);
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.percentageOfPageWidthList.Add(0.6f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.4f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left + receiptStringLine2.paddingLeft, receiptStringLine2.spaceBetweenCell, 0f, single - receiptStringLine2.paddingRight, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				paddingLeft = single3,
				paddingRight = single3,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single
			};
			receiptLines2.Add(receiptPenLine);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add("S/N");
			receiptStringLine2.printContentList.Add("Date");
			receiptStringLine2.printContentList.Add("Description");
			receiptStringLine2.printContentList.Add("Amount");
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats3 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat3 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats3.Add(stringFormat3);
			List<StringFormat> stringFormats4 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat4 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats4.Add(stringFormat4);
			List<StringFormat> stringFormats5 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat5 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats5.Add(stringFormat5);
			List<StringFormat> stringFormats6 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat6 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats6.Add(stringFormat6);
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left + receiptStringLine2.paddingLeft, receiptStringLine2.spaceBetweenCell, 0f, single - receiptStringLine2.paddingRight, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				paddingLeft = single3,
				paddingRight = single3,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single
			};
			receiptLines2.Add(receiptPenLine);
			num1 = 1;
			foreach (BookingChargeData chargeDatum in this.chargeData)
			{
				receiptStringLine2 = new ReceiptStringLine();
				List<string> strs3 = new List<string>()
				{
					num1.ToString()
				};
				now = chargeDatum.chargeDateTime;
				strs3.Add(now.ToString("dd/MM/yyyy"));
				strs3.Add(chargeDatum.chargeName);
				num = chargeDatum.outstanding;
				strs3.Add(num.ToString("0.00"));
				receiptStringLine2.printContentList = strs3;
				List<ReceiptFont> receiptFonts = new List<ReceiptFont>()
				{
					new ReceiptFont(this.mediumFont),
					new ReceiptFont(this.mediumFont),
					new ReceiptFont(this.mediumFont),
					new ReceiptFont(this.mediumFont)
				};
				receiptStringLine2.printFontList = receiptFonts;
				List<Brush> brushes = new List<Brush>()
				{
					this.brush,
					this.brush,
					this.brush,
					this.brush
				};
				receiptStringLine2.printBrushList = brushes;
				List<StringFormat> stringFormats7 = new List<StringFormat>();
				StringFormat stringFormat7 = new StringFormat()
				{
					Alignment = StringAlignment.Near,
					LineAlignment = StringAlignment.Near
				};
				stringFormats7.Add(stringFormat7);
				StringFormat stringFormat8 = new StringFormat()
				{
					Alignment = StringAlignment.Near,
					LineAlignment = StringAlignment.Near
				};
				stringFormats7.Add(stringFormat8);
				StringFormat stringFormat9 = new StringFormat()
				{
					Alignment = StringAlignment.Near,
					LineAlignment = StringAlignment.Near
				};
				stringFormats7.Add(stringFormat9);
				StringFormat stringFormat10 = new StringFormat()
				{
					Alignment = StringAlignment.Far,
					LineAlignment = StringAlignment.Near
				};
				stringFormats7.Add(stringFormat10);
				receiptStringLine2.printStringFormatList = stringFormats7;
				receiptStringLine2.printOffsetAfter = 2f;
				receiptStringLine2.spaceBetweenCell = 5f;
				receiptStringLine2.paddingLeft = single3;
				receiptStringLine2.paddingRight = single3;
				receiptStringLine2.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
				receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
				receiptLines2.Add(receiptStringLine2);
				receiptPenLine = new ReceiptPenLine()
				{
					printPen = this.normalPen,
					printOffsetAfter = 2f,
					paddingLeft = single3,
					paddingRight = single3,
					startX = (float)this.margins.Left,
					startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
					height = 1f,
					width = single
				};
				receiptLines2.Add(receiptPenLine);
				num1++;
			}
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("Currency : ", this.kioskData.currency));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats8 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat11 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats8.Add(stringFormat11);
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.percentageOfPageWidthList.Add(0.35f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left + receiptStringLine2.paddingLeft, receiptStringLine2.spaceBetweenCell, 0f, single - receiptStringLine2.paddingRight, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				paddingLeft = single3,
				paddingRight = single3,
				startX = (float)this.margins.Left + single * 0.35f,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single * 0.65f
			};
			receiptLines2.Add(receiptPenLine);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add("Total");
			List<string> strs4 = receiptStringLine2.printContentList;
			string str1 = this.kioskData.currency;
			num = this.chargeData.Sum<BookingChargeData>((BookingChargeData x) => x.outstanding);
			strs4.Add(string.Concat(str1, " ", num.ToString("0.00")));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats9 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat12 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats9.Add(stringFormat12);
			List<StringFormat> stringFormats10 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat13 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats10.Add(stringFormat13);
			receiptStringLine2.percentageOfPageWidthList.Add(0.65f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.35f);
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				paddingLeft = single3,
				paddingRight = single3,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single
			};
			receiptLines2.Add(receiptPenLine);
			num1 = 1;
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(num1.ToString());
			List<string> strs5 = receiptStringLine2.printContentList;
			now = this.transaction.transactionDateTime;
			strs5.Add(now.ToString("dd/MM/yyyy"));
			List<string> strs6 = receiptStringLine2.printContentList;
			eMV = new string[] { this.invoiceNumber, " Payment [", this.transaction.cardLabel, ": ", this.transaction.approvalCode, "]" };
			strs6.Add(string.Concat(eMV));
			List<string> strs7 = receiptStringLine2.printContentList;
			num = this.transaction.transactionAmount;
			strs7.Add(num.ToString("0.00"));
			List<ReceiptFont> receiptFonts1 = new List<ReceiptFont>()
			{
				new ReceiptFont(this.mediumFont),
				new ReceiptFont(this.mediumFont),
				new ReceiptFont(this.mediumFont),
				new ReceiptFont(this.mediumFont)
			};
			receiptStringLine2.printFontList = receiptFonts1;
			List<Brush> brushes1 = new List<Brush>()
			{
				this.brush,
				this.brush,
				this.brush,
				this.brush
			};
			receiptStringLine2.printBrushList = brushes1;
			List<StringFormat> stringFormats11 = new List<StringFormat>();
			StringFormat stringFormat14 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats11.Add(stringFormat14);
			StringFormat stringFormat15 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats11.Add(stringFormat15);
			StringFormat stringFormat16 = new StringFormat()
			{
				Alignment = StringAlignment.Near,
				LineAlignment = StringAlignment.Near
			};
			stringFormats11.Add(stringFormat16);
			StringFormat stringFormat17 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats11.Add(stringFormat17);
			receiptStringLine2.printStringFormatList = stringFormats11;
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			receiptStringLine2.percentageOfPageWidthList.AddRange(singleArray.ToList<float>());
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 2f,
				paddingLeft = single3,
				paddingRight = single3,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2),
				height = 1f,
				width = single
			};
			receiptLines2.Add(receiptPenLine);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add("Balance");
			List<string> strs8 = receiptStringLine2.printContentList;
			string str2 = this.kioskData.currency;
			num = this.chargeData.Sum<BookingChargeData>((BookingChargeData x) => x.outstanding) - this.transaction.transactionAmount;
			strs8.Add(string.Concat(str2, " ", num.ToString("0.00")));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.largeFont, FontStyle.Bold));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.paddingLeft = single3;
			receiptStringLine2.paddingRight = single3;
			List<StringFormat> stringFormats12 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat18 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats12.Add(stringFormat18);
			List<StringFormat> stringFormats13 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat19 = new StringFormat()
			{
				Alignment = StringAlignment.Far,
				LineAlignment = StringAlignment.Near
			};
			stringFormats13.Add(stringFormat19);
			receiptStringLine2.percentageOfPageWidthList.Add(0.65f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.35f);
			receiptStringLine2.printOffsetAfter = 10f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines2.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 0f,
				donAddHeight = true,
				startY = receiptItemTotalHeight,
				startX = (float)this.margins.Left,
				width = 1f,
				height = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2) - ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines)
			};
			tableBorders.Add(receiptPenLine);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 0f,
				donAddHeight = true,
				startY = receiptItemTotalHeight,
				startX = (float)this.margins.Left + single,
				width = 1f,
				height = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines2) - ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines)
			};
			tableBorders.Add(receiptPenLine);
			receiptLines = receiptLines2;
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.normalPen,
				printOffsetAfter = 30f,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines),
				height = 1f,
				width = single
			};
			receiptLines.Add(receiptPenLine);
			this.createSignatureLine(g, receiptLines, single);
			for (int j = 0; j < this.receipt.TandCList.Count; j++)
			{
				receiptStringLine2 = new ReceiptStringLine();
				receiptStringLine2.printContentList.Add(this.receipt.TandCList[j]);
				receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont));
				receiptStringLine2.printBrushList.Add(this.brush);
				receiptStringLine2.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine2.printOffsetAfter = (j + 1 == this.receipt.TandCList.Count ? 15f : 0f);
				receiptStringLine2.percentageOfPageWidthList.Add(1f);
				receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList[0], receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList[0], receiptStringLine2.printBrushList[0], (float)this.margins.Left, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList[0], false);
				receiptLines.Add(receiptStringLine2);
			}
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(this.receipt.appreciateTerm);
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont, FontStyle.Italic));
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats14 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat20 = new StringFormat()
			{
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			stringFormats14.Add(stringFormat20);
			receiptStringLine2.percentageOfPageWidthList.Add(1f);
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines.Add(receiptStringLine2);
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(this.kioskData.webSite);
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.mediumFont));
			receiptStringLine2.printBrushList.Add(this.brush);
			List<StringFormat> stringFormats15 = receiptStringLine2.printStringFormatList;
			StringFormat stringFormat21 = new StringFormat()
			{
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			stringFormats15.Add(stringFormat21);
			receiptStringLine2.percentageOfPageWidthList.Add(1f);
			receiptStringLine2.printOffsetAfter = 20f;
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines.Add(receiptStringLine2);
			receiptPenLine = new ReceiptPenLine()
			{
				printPen = this.dashPen,
				printOffsetAfter = 2f,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines),
				height = 1f,
				width = single
			};
			receiptLines.Add(receiptPenLine);
			top = this.bankLogoImage;
			top.donAddHeight = true;
			top.startX = single * 0.7f;
			top.width = (single - top.startX >= 384f ? 384f : single - top.startX);
			top.startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines);
			List<ReceiptLine> receiptLines3 = new List<ReceiptLine>();
			receiptStringLine2 = new ReceiptStringLine();
			List<string> strs9 = receiptStringLine2.printContentList;
			eMV = new string[] { "Date & Time : ", null, null, null, null, null };
			now = this.transaction.transactionDateTime;
			eMV[1] = now.ToString("yyyy-MM-dd hh:mm:ss");
			eMV[2] = " INVOICE# : ";
			eMV[3] = this.transaction.invoice;
			eMV[4] = " BATCH# : ";
			eMV[5] = this.transaction.batch;
			strs9.Add(string.Concat(eMV));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines3.Add(receiptStringLine2);
			if (!string.IsNullOrWhiteSpace(this.transaction.cardHolderName))
			{
				receiptStringLine2 = new ReceiptStringLine();
				receiptStringLine2.printContentList.Add(string.Concat("CARD HOLDER : ", this.transaction.cardHolderName));
				receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
				receiptStringLine2.printBrushList.Add(this.brush);
				receiptStringLine2.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine2.printOffsetAfter = 2f;
				receiptStringLine2.spaceBetweenCell = 5f;
				receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
				receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
				receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
				receiptLines3.Add(receiptStringLine2);
			}
			receiptStringLine2 = new ReceiptStringLine();
			receiptStringLine2.printContentList.Add(string.Concat("TID : ", this.transaction.terminalId, " MID : ", this.transaction.mechantId));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines3.Add(receiptStringLine2);
			receiptStringLine2 = new ReceiptStringLine();
			List<string> strs10 = receiptStringLine2.printContentList;
			eMV = new string[] { this.transaction.entryMode, " ", this.transaction.cardLabel, " ", this.transaction.cardNumber, " APPR CODE : ", this.transaction.approvalCode, " REF# : ", this.transaction.retrievalReference };
			strs10.Add(string.Concat(eMV));
			receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
			receiptStringLine2.printBrushList.Add(this.brush);
			receiptStringLine2.printStringFormatList.Add(new StringFormat()
			{
				Alignment = StringAlignment.Near
			});
			receiptStringLine2.printOffsetAfter = 2f;
			receiptStringLine2.spaceBetweenCell = 5f;
			receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
			receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
			receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
			receiptLines3.Add(receiptStringLine2);
			if (this.transaction.EMV != null)
			{
				receiptStringLine2 = new ReceiptStringLine();
				List<string> strs11 = receiptStringLine2.printContentList;
				eMV = new string[] { this.transaction.EMV.appLabel, " TC : ", this.transaction.EMV.TC, " AID : ", this.transaction.EMV.AID };
				strs11.Add(string.Concat(eMV));
				receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
				receiptStringLine2.printBrushList.Add(this.brush);
				receiptStringLine2.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine2.printOffsetAfter = 2f;
				receiptStringLine2.spaceBetweenCell = 5f;
				receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
				receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
				receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
				receiptLines3.Add(receiptStringLine2);
				receiptStringLine2 = new ReceiptStringLine();
				receiptStringLine2.printContentList.Add(string.Concat("TVR : ", this.transaction.EMV.TVR, " TSI : ", this.transaction.EMV.TSI));
				receiptStringLine2.printFontList.Add(new ReceiptFont(this.smallFont));
				receiptStringLine2.printBrushList.Add(this.brush);
				receiptStringLine2.printStringFormatList.Add(new StringFormat()
				{
					Alignment = StringAlignment.Near
				});
				receiptStringLine2.printOffsetAfter = 2f;
				receiptStringLine2.spaceBetweenCell = 5f;
				receiptStringLine2.percentageOfPageWidthList.Add(0.7f);
				receiptStringLine2.percentageOfPageWidthList.Add(0.3f);
				receiptStringLine2.printHeight = this.DrawString(g, receiptStringLine2.printContentList, receiptStringLine2.percentageOfPageWidthList, receiptStringLine2.printFontList, receiptStringLine2.printBrushList, (float)this.margins.Left, receiptStringLine2.spaceBetweenCell, 0f, single, receiptStringLine2.printOffsetAfter, receiptStringLine2.printStringFormatList, false);
				receiptLines3.Add(receiptStringLine2);
			}
			top.height = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines3);
			float single4 = 90f * (top.width / 384f);
			if (top.height > single4)
			{
				top.height = single4;
			}
			top.printOffsetAfter = 2f;
			receiptLines.Add(top);
			receiptLines.AddRange(receiptLines3);
			receiptPenLine = new ReceiptPenLine()
			{
				printOffsetAfter = 2f,
				printPen = this.dashPen,
				startX = (float)this.margins.Left,
				startY = ReceiptLine.GetReceiptItemTotalHeight((float)this.margins.Top, receiptLines),
				width = single,
				height = 1f
			};
			receiptLines.Add(receiptPenLine);
			receiptLines.AddRange(this.printPrintingFlag(g, single));
			return receiptLines;
		}

		public bool GenerateReceipt(Control control, Action<Exception> logException, ReceiptPDFGenerateParameter pdfGenerateParam)
		{
			bool flag = true;
			List<ReceiptLine> receiptLines = new List<ReceiptLine>();
			List<ReceiptPenLine> receiptPenLines = new List<ReceiptPenLine>();
			this.logInfo("Generating Receipt lines...", true);
			try
			{
				this.initVariables(pdfGenerateParam);
				using (PdfDocument pdfDocument = new PdfDocument())
				{
					PdfPage pdfPage = pdfDocument.AddPage();
					pdfPage.Size=(this.pageSize);
					using (XGraphics xGraphic = XGraphics.FromPdfPage(pdfPage))
					{
						receiptLines = this.generatePrintItems(pdfPage, xGraphic, receiptPenLines);
					}
				}
				flag = true;
				this.logInfo("Generating Receipt lines... Successful", true);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				flag = false;
				this.triggerLogException(control, logException, exception);
				this.logInfo("Generating Receipt lines... Failed", true);
			}
			if (flag && receiptLines.Count > 0)
			{
				this.logInfo("Generating PDF Files...", true);
				try
				{
					this.convertToPDF(control, logException, receiptLines, receiptPenLines);
					flag = true;
					this.logInfo("Generating PDF Files... Successful", true);
				}
				catch (Exception exception3)
				{
					Exception exception2 = exception3;
					flag = false;
					this.triggerLogException(control, logException, exception2);
					this.logInfo("Generating PDF Files... Failed", true);
				}
			}
			return flag;
		}

		private void initVariables(ReceiptPDFGenerateParameter pdfGenerateParam)
		{
			this.filePath = pdfGenerateParam.FilePath;
			this.fileName = pdfGenerateParam.FileName;
			this.kioskData = pdfGenerateParam.KioskData;
			this.bookingData = pdfGenerateParam.BookingData;
			this.roomStayData = pdfGenerateParam.RoomStayData;
			this.chargeData = pdfGenerateParam.ChargeData;
			this.guestData = pdfGenerateParam.GuestData;
			this.transaction = pdfGenerateParam.Transaction;
			this.invoiceNumber = pdfGenerateParam.InvoiceNumber;
			this.pageSize = pdfGenerateParam.PdfPageSize;
			this.margins = pdfGenerateParam.Margins;
			this.logoImage = pdfGenerateParam.LogoImage;
			this.signatureImage = pdfGenerateParam.SignatureImage;
			this.bankLogoImage = pdfGenerateParam.BankLogoImage;
			this.smallFont = pdfGenerateParam.SmallFont;
			this.mediumFont = pdfGenerateParam.MediumFont;
			this.largeFont = pdfGenerateParam.LargeFont;
			this.brush = pdfGenerateParam.Brush;
			this.normalPen = pdfGenerateParam.NormalPen;
			this.dashPen = pdfGenerateParam.DashPen;
		}

		private void logInfo(string message, bool force)
		{
			if (force || this.LogOutput)
			{
				this._logFile.Log(MyLog.LogType.Info, message);
			}
		}

		private void printBorderLines(XGraphics graph, List<ReceiptPenLine> tableBorders, float pageWidth, float pageHeight, int currentPage, ref float yPos, float accumulateHeight)
		{
			this.logInfo("Drawing Table Borders...", false);
			List<ReceiptPenLine> receiptPenLines = new List<ReceiptPenLine>();
			foreach (ReceiptPenLine tableBorder in tableBorders)
			{
				float single = tableBorder.startY;
				if (single > accumulateHeight)
				{
					single -= accumulateHeight;
				}
				float single1 = tableBorder.startY + tableBorder.height;
				if (single1 > accumulateHeight)
				{
					single1 -= accumulateHeight;
				}
				object[] str = new object[] { "Border Line Height: ", null, null, null, null, null, null, null, null, null, null };
				str[1] = tableBorder.height.ToString();
				str[2] = ",Print Offset: ";
				str[3] = tableBorder.printOffsetAfter.ToString();
				str[4] = ",Start Y: ";
				str[5] = tableBorder.startY.ToString();
				str[6] = ",Page Height: ";
				str[7] = pageHeight.ToString();
				str[8] = ",Accumulate Height: ";
				str[9] = accumulateHeight;
				str[10] = ".";
				this.logInfo(string.Concat(str), false);
				if (tableBorder.printOffsetAfter + tableBorder.height + single <= pageHeight)
				{
					graph.DrawLine(tableBorder.printPen, (double)(tableBorder.startX + tableBorder.paddingLeft), (double)single, (double)(tableBorder.startX + tableBorder.width - tableBorder.paddingRight), (double)single1);
					yPos += tableBorder.printOffsetAfter;
					if (tableBorder.donAddHeight)
					{
						continue;
					}
					yPos += tableBorder.height;
				}
				else
				{
					graph.DrawLine(tableBorder.printPen, (double)(tableBorder.startX + tableBorder.paddingLeft), (double)single, (double)(tableBorder.startX + tableBorder.width - tableBorder.paddingRight), (double)pageHeight);
					ReceiptPenLine receiptPenLine = new ReceiptPenLine()
					{
						printPen = tableBorder.printPen,
						printOffsetAfter = tableBorder.printOffsetAfter,
						donAddHeight = tableBorder.donAddHeight,
						startY = tableBorder.startY,
						startX = tableBorder.startX,
						width = tableBorder.width,
						height = tableBorder.height
					};
					yPos += tableBorder.printOffsetAfter;
					if (!tableBorder.donAddHeight)
					{
						float single2 = tableBorder.printOffsetAfter + tableBorder.height + single - pageHeight;
						yPos = yPos + (tableBorder.height - single2);
					}
					receiptPenLines.Add(receiptPenLine);
				}
			}
			tableBorders.Clear();
			if (receiptPenLines.Count<ReceiptPenLine>() > 0)
			{
				tableBorders.AddRange(receiptPenLines);
			}
		}

		private void printImageLine(Control control, Action<Exception> logException, XGraphics graph, ReceiptImageLine imageLine, float pageWidth, float pageHeight, ref int currentLine, int currentPage, ref float yPos, float accumulateHeight, out bool newPage)
		{
			newPage = false;
			if (!string.IsNullOrEmpty(imageLine.printImagePath))
			{
				bool flag = false;
				if (imageLine.isWeb)
				{
					try
					{
						WebRequest webRequest = WebRequest.Create(imageLine.printImagePath);
						webRequest.Method = "HEAD";
						using (WebResponse response = webRequest.GetResponse())
						{
							flag = true;
						}
					}
					catch (Exception exception1)
					{
						Exception exception = exception1;
						this.logInfo(string.Concat("Error on getting image file: ", imageLine.printImagePath), true);
						this.triggerLogException(control, logException, exception);
					}
				}
				else if (File.Exists(imageLine.printImagePath))
				{
					flag = true;
				}
				if (!flag)
				{
					string[] strArrays = new string[] { "Line ", currentLine.ToString().PadLeft(7).PadRight(1), "Drawing Image ", imageLine.printImagePath, ". Image Not Found." };
					this.logInfo(string.Concat(strArrays), true);
				}
				else
				{
					this.logInfo(string.Concat("Line ", currentLine.ToString().PadLeft(7).PadRight(1), "Drawing Image ", imageLine.printImagePath), false);
					try
					{
						using (XImage xImage = XImage.FromFile(imageLine.printImagePath))
						{
							float single = imageLine.startY;
							if (single > accumulateHeight)
							{
								single -= accumulateHeight;
							}
							object[] height = new object[] { "Image Height: ", null, null, null, null, null, null, null, null, null, null };
							XSize size = xImage.Size;
							height[1] = size.Height;
							height[2] = ", Print Offset: ";
							height[3] = imageLine.printOffsetAfter;
							height[4] = ", Start Y: ";
							height[5] = single;
							height[6] = ", Page Height: ";
							height[7] = pageHeight;
							height[8] = ", Accumulated Height: ";
							height[9] = accumulateHeight;
							height[10] = ".";
							this.logInfo(string.Concat(height), false);
							if (xImage.Size.Height + (double)imageLine.printOffsetAfter + (double)single <= (double)pageHeight)
							{
								graph.DrawImage(xImage, (double)(imageLine.startX + imageLine.paddingLeft), (double)single, (double)(imageLine.width - imageLine.paddingRight), (double)imageLine.height);
								if (!imageLine.donAddHeight)
								{
									XSize xSize = xImage.Size;
									yPos += Convert.ToSingle(xSize.Height);
								}
								yPos += imageLine.printOffsetAfter;
							}
							else
							{
								this.logInfo("Draw Image on next Page..", false);
								float single1 = imageLine.startY;
								this.logInfo(string.Concat("Current Image Start Y position:", single1.ToString()), false);
								ReceiptImageLine receiptImageLine = imageLine;
								receiptImageLine.startY = receiptImageLine.startY - pageHeight * (float)(currentPage + 1);
								if (imageLine.startY <= (float)this.margins.Top)
								{
									imageLine.startY = (float)this.margins.Top;
								}
								float single2 = imageLine.startY;
								this.logInfo(string.Concat("New Image Start Y position:", single2.ToString()), false);
								newPage = true;
								return;
							}
						}
					}
					catch (Exception exception3)
					{
						Exception exception2 = exception3;
						this.logInfo(string.Concat("Error on getting image file: ", imageLine.printImagePath), true);
						this.triggerLogException(control, logException, exception2);
					}
				}
			}
			currentLine++;
		}

		private void printPenLine(XGraphics graph, ReceiptPenLine penLine, float pageWidth, float pageHeight, List<ReceiptLine> receiptItemList, ref int currentLine, int currentPage, ref float yPos, float accumulateHeight, out bool newPage)
		{
			newPage = false;
			this.logInfo(string.Concat("Line ", currentLine.ToString().PadLeft(7).PadRight(1), "Drawing Pen Line..."), false);
			float single = penLine.startY;
			if (single > accumulateHeight)
			{
				single -= accumulateHeight;
			}
			float single1 = penLine.startY + penLine.height;
			if (single1 > accumulateHeight)
			{
				single1 -= accumulateHeight;
			}
			object[] str = new object[] { "Pen Line Height: ", null, null, null, null, null, null, null, null, null, null };
			str[1] = penLine.height.ToString();
			str[2] = ",Print Offset: ";
			str[3] = penLine.printOffsetAfter.ToString();
			str[4] = ",Start Y: ";
			str[5] = penLine.startY.ToString();
			str[6] = ",Page Height: ";
			str[7] = pageHeight.ToString();
			str[8] = ",Accumulate Height: ";
			str[9] = accumulateHeight;
			str[10] = ".";
			this.logInfo(string.Concat(str), false);
			if (penLine.printOffsetAfter + penLine.height + single <= pageHeight)
			{
				graph.DrawLine(penLine.printPen, (double)(penLine.startX + penLine.paddingLeft), (double)single, (double)(penLine.startX + penLine.width - penLine.paddingRight), (double)single1);
				yPos += penLine.printOffsetAfter;
				if (!penLine.donAddHeight)
				{
					yPos += penLine.height;
				}
				currentLine++;
				return;
			}
			this.logInfo("Draw Pen Line on next Page..", false);
			float single2 = penLine.startY;
			this.logInfo(string.Concat("Current Pen Line Start Y position:", single2.ToString()), false);
			ReceiptPenLine receiptPenLine = penLine;
			receiptPenLine.startY = receiptPenLine.startY - pageHeight * (float)(currentPage + 1);
			if (penLine.startY <= (float)this.margins.Top)
			{
				penLine.startY = (float)this.margins.Top;
			}
			float single3 = penLine.startY;
			this.logInfo(string.Concat("New Pen Line Start Y position:", single3.ToString()), false);
			newPage = true;
		}

		private List<ReceiptLine> printPrintingFlag(XGraphics g, float pageWidth)
		{
			List<ReceiptLine> receiptLines = new List<ReceiptLine>();
			if (this.transaction.PrintingFlag == null)
			{
				return receiptLines;
			}
			if (this.transaction.PrintingFlag.Disclaimer1)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer1, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer2)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer2, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer3)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer3, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer4)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer4, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer5)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer5, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer6)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer6, pageWidth));
			}
			if (this.transaction.PrintingFlag.Disclaimer7)
			{
				receiptLines.Add(this.createStringLine(g, ConstantVariable_Receipt.Disclaimer7, pageWidth));
			}
			return receiptLines;
		}

		private void printStringLine(XGraphics graph, ReceiptStringLine stringLine, float pageWidth, float pageHeight, ref int currentLine, ref float yPos, out bool newPage)
		{
			newPage = false;
			object[] objArray = new object[] { "Line ", currentLine.ToString().PadLeft(7).PadRight(1), "Writing ", stringLine.printContentList.Count<string>(), " Text(s)" };
			this.logInfo(string.Concat(objArray), false);
			if (stringLine.printOffsetAfter + stringLine.printHeight + yPos > pageHeight)
			{
				this.logInfo("Write Text on next Page..", false);
				newPage = true;
				return;
			}
			float single = yPos;
			if (stringLine.printContentList.Count == 1)
			{
				while (stringLine.percentageOfPageWidthList.Count == 0)
				{
					stringLine.percentageOfPageWidthList.Add(1f);
				}
			}
			single = this.DrawString(graph, stringLine.printContentList, stringLine.percentageOfPageWidthList, stringLine.printFontList, stringLine.printBrushList, (float)this.margins.Left + stringLine.paddingLeft, stringLine.spaceBetweenCell, yPos, pageWidth - stringLine.paddingRight, stringLine.printOffsetAfter, stringLine.printStringFormatList, true);
			if (stringLine.donAddHeight)
			{
				yPos += stringLine.printOffsetAfter;
			}
			else
			{
				yPos = single;
			}
			currentLine++;
		}

		private void triggerLogException(Control control, Action<Exception> logException, Exception ex)
		{
			if (control != null && logException != null)
			{
				if (control.InvokeRequired)
				{
					object[] objArray = new object[] { ex };
					control.BeginInvoke(logException, objArray);
					return;
				}
				logException(ex);
			}
		}
	}
}