using System;
using System.Collections.Generic;

namespace MQuest.Printing
{
	public interface IPrinter
	{
		void CheckPrinterStatus();

		void Dispose();

		List<string> GetPrinterList();

		void Init(string printerName);
	}
}