using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public static class ListExtension
	{
		public static void Dispose(this IEnumerable<IDisposable> collection)
		{
			foreach (IDisposable disposable in collection)
			{
				if (disposable == null)
				{
					continue;
				}
				try
				{
					disposable.Dispose();
				}
				catch (Exception exception)
				{
				}
			}
		}
	}
}