using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptStringLine : ReceiptLine
	{
		public List<float> percentageOfPageWidthList
		{
			get;
			set;
		}

		public List<Brush> printBrushList
		{
			get;
			set;
		}

		public List<string> printContentList
		{
			get;
			set;
		}

		public List<ReceiptFont> printFontList
		{
			get;
			set;
		}

		public float printHeight
		{
			get;
			set;
		}

		public List<StringFormat> printStringFormatList
		{
			get;
			set;
		}

		public float spaceBetweenCell
		{
			get;
			set;
		}

		public ReceiptStringLine()
		{
			this.percentageOfPageWidthList = new List<float>();
			this.printContentList = new List<string>();
			this.printStringFormatList = new List<StringFormat>();
			this.printBrushList = new List<Brush>();
			this.printFontList = new List<ReceiptFont>();
		}
	}
}