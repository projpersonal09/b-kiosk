using System;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptPenLine : ReceiptLine
	{
		public float height
		{
			get;
			set;
		}

		public Pen printPen
		{
			get;
			set;
		}

		public float startX
		{
			get;
			set;
		}

		public float startY
		{
			get;
			set;
		}

		public float width
		{
			get;
			set;
		}

		public ReceiptPenLine()
		{
		}
	}
}