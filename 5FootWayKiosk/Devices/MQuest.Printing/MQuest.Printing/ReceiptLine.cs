using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptLine
	{
		public bool donAddHeight
		{
			get;
			set;
		}

		public float paddingLeft
		{
			get;
			set;
		}

		public float paddingRight
		{
			get;
			set;
		}

		public float printOffsetAfter
		{
			get;
			set;
		}

		public ReceiptLine()
		{
		}

		public static float GetReceiptItemTotalHeight(float marginTop, List<ReceiptLine> receiptItemList)
		{
			return marginTop + receiptItemList.Sum<ReceiptLine>((ReceiptLine x) => (x.GetType() == typeof(ReceiptStringLine) ? 0f : x.printOffsetAfter) + (x.donAddHeight ? 0f : (x.GetType() == typeof(ReceiptImageLine) ? ((ReceiptImageLine)x).height : (x.GetType() == typeof(ReceiptStringLine) ? ((ReceiptStringLine)x).printHeight : (x.GetType() == typeof(ReceiptPenLine) ? ((ReceiptPenLine)x).height : 0f)))));
		}
	}
}