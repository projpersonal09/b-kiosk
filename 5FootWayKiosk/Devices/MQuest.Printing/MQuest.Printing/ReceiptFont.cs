using System;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptFont : IDisposable
	{
		public Font font
		{
			get;
			set;
		}

		public FontStyle style
		{
			get;
			set;
		}

		public ReceiptFont()
		{
			this.style = FontStyle.Regular;
		}

		public ReceiptFont(Font font)
		{
			this.font = font;
			this.style = FontStyle.Regular;
		}

		public ReceiptFont(Font font, FontStyle style)
		{
			this.font = font;
			this.style = style;
		}

		public void Dispose()
		{
			if (this.font != null)
			{
				this.font.Dispose();
			}
		}
	}
}