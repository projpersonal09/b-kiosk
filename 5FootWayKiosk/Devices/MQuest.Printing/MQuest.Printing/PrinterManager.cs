using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Printing;

namespace MQuest.Printing
{
	public class PrinterManager : IPrinter, IDisposable
	{
		private string printerName;

		private readonly static IDictionary<CimType, Type> Cim2TypeTable;

		static PrinterManager()
		{
			Dictionary<CimType, Type> cimTypes = new Dictionary<CimType, Type>()
			{
				{ CimType.Boolean, typeof(bool) },
				{ CimType.Char16, typeof(string) },
				{ CimType.DateTime, typeof(DateTime) },
				{ CimType.Object, typeof(object) },
				{ CimType.Real32, typeof(decimal) },
				{ CimType.Real64, typeof(decimal) },
				{ CimType.Reference, typeof(object) },
				{ CimType.SInt16, typeof(short) },
				{ CimType.SInt32, typeof(int) },
				{ CimType.SInt8, typeof(sbyte) },
				{ CimType.String, typeof(string) },
				{ CimType.UInt8, typeof(byte) },
				{ CimType.UInt16, typeof(ushort) },
				{ CimType.UInt32, typeof(uint) },
				{ CimType.UInt64, typeof(ulong) }
			};
			PrinterManager.Cim2TypeTable = cimTypes;
		}

		public PrinterManager()
		{
		}

		public void CheckPrinterStatus()
		{
			PrintQueue printQueue;
			try
			{
				using (LocalPrintServer localPrintServer = new LocalPrintServer())
				{
					printQueue = (!string.IsNullOrEmpty(this.printerName) ? localPrintServer.GetPrintQueue(this.printerName) : localPrintServer.DefaultPrintQueue);
					using (printQueue)
					{
						printQueue.Refresh();
						if (printQueue.IsOffline)
						{
							throw new Exception("Printer is offline.");
						}
						if (printQueue.IsPaperJammed)
						{
							throw new Exception("Paper is jam.");
						}
						if (printQueue.NeedUserIntervention)
						{
							throw new Exception("Need attention.");
						}
						if (printQueue.HasPaperProblem)
						{
							throw new Exception("Paper problem.");
						}
						if (!printQueue.HasToner)
						{
							throw new Exception("Printer out of toner.");
						}
						if (printQueue.IsTonerLow)
						{
							throw new Exception("Priter toner is running low.");
						}
						if (printQueue.IsBusy)
						{
							throw new Exception("Printer is busy.");
						}
						if (printQueue.IsDoorOpened)
						{
							throw new Exception("Printer cover is opened.");
						}
						if (printQueue.IsInError)
						{
							throw new Exception("Printer is in error.");
						}
						if (printQueue.IsNotAvailable)
						{
							throw new Exception("Printer is not available.");
						}
						if (printQueue.IsOutOfPaper)
						{
							throw new Exception("Printer is out of paper.");
						}
						if (printQueue.IsOutOfMemory)
						{
							throw new Exception("Printer is out of memory.");
						}
						if (printQueue.IsPaused)
						{
							throw new Exception("Printer is paused.");
						}
					}
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public Type Cim2SystemType(PropertyData data)
		{
			Type item = PrinterManager.Cim2TypeTable[data.Type];
			if (data.IsArray)
			{
				item = item.MakeArrayType();
			}
			return item;
		}

		public string Cim2SystemValue(PropertyData data)
		{
			if (data.Value != null)
			{
				Type type = this.Cim2SystemType(data);
				if (data.Type == CimType.DateTime)
				{
					DateTime dateTime = DateTime.ParseExact(data.Value.ToString(), "yyyyMMddHHmmss.ffffff-000", CultureInfo.InvariantCulture);
					return dateTime.ToString();
				}
				if (!type.IsArray)
				{
					return Convert.ChangeType(data.Value, type).ToString();
				}
				Array value = (Array)data.Value;
				if (value.Length > 0)
				{
					string empty = string.Empty;
					foreach (object obj in value)
					{
						empty = string.Concat(empty, obj, ", ");
					}
					empty = empty.Remove(empty.Length - 2, 2);
					return empty;
				}
			}
			return null;
		}

		public void DeleteAllPrintQueue()
		{
			try
			{
				using (PrintServer localPrintServer = new LocalPrintServer())
				{
					foreach (PrintQueue printQueue in localPrintServer.GetPrintQueues())
					{
						PrintServer.DeletePrintQueue(printQueue);
					}
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Dispose()
		{
			this.printerName = null;
		}

		public List<string> GetPrinterList()
		{
			return PrinterSettings.InstalledPrinters.Cast<string>().ToList<string>();
		}

		public void Init(string printerName)
		{
			try
			{
				this.printerName = printerName;
				if (string.IsNullOrEmpty(printerName))
				{
					throw new Exception("Printer is not selected.");
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}
	}
}