using System;

namespace MQuest.Printing
{
	public enum PrinterState
	{
		Idle,
		Paused,
		Error,
		Pending_Deletion,
		Paper_Jam,
		Paper_Out,
		Manual_Feed,
		Paper_Problem,
		Offline,
		IO_Active,
		Busy,
		Printing,
		Output_Bin_Full,
		Not_Available,
		Waiting,
		Processing,
		Initialization,
		Warming_Up,
		Toner_Low,
		No_Toner,
		Page_Punt,
		User_Intervention_Required,
		Out_of_Memory,
		Door_Open,
		Server_Unknown,
		Power_Save
	}
}