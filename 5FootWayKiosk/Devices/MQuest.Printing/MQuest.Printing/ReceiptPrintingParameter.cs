using KioskBusinessEntity.Kiosk_Entity;
using KioskBusinessEntity.Web_Service_Entity;
using MQuest.HardwareInterface.CreditCardReader.Ingenico;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptPrintingParameter
	{
		public ReceiptImageLine BankLogoImage
		{
			get;
			set;
		}

		public KioskBusinessEntity.Web_Service_Entity.BookingData BookingData
		{
			get;
			set;
		}

		public List<BookingChargeData> ChargeData
		{
			get;
			set;
		}

		public string FileName
		{
			get;
			set;
		}

		public KioskCheckInData GuestData
		{
			get;
			set;
		}

		public string InvoiceNumber
		{
			get;
			set;
		}

		public KioskBusinessEntity.Kiosk_Entity.KioskData KioskData
		{
			get;
			set;
		}

		public ReceiptImageLine LogoImage
		{
			get;
			set;
		}

		public System.Drawing.Printing.Margins Margins
		{
			get;
			set;
		}

		public KioskBusinessEntity.Web_Service_Entity.RoomStayData RoomStayData
		{
			get;
			set;
		}

		public ReceiptImageLine SignatureImage
		{
			get;
			set;
		}

		public TransactionDetail Transaction
		{
			get;
			set;
		}

		public ReceiptPrintingParameter()
		{
		}
	}
}