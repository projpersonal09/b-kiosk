using System;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace MQuest.Printing
{
	public class ReceiptImageLine : ReceiptLine
	{
		public float height
		{
			get;
			set;
		}

		public bool isWeb
		{
			get;
			set;
		}

		public StringFormat printFormat
		{
			get;
			set;
		}

		public string printImagePath
		{
			get;
			set;
		}

		public float startX
		{
			get;
			set;
		}

		public float startY
		{
			get;
			set;
		}

		public float width
		{
			get;
			set;
		}

		public ReceiptImageLine()
		{
			this.printFormat = new StringFormat()
			{
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
		}
	}
}