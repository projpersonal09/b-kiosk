using System;

namespace MQuest.HardwareInterface.BarcodeScanner
{
	public class BarcodeScannerException : Exception
	{
		public bool showDetail;

		public BarcodeScannerException()
		{
		}

		public BarcodeScannerException(string message) : base(message)
		{
		}

		public BarcodeScannerException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}