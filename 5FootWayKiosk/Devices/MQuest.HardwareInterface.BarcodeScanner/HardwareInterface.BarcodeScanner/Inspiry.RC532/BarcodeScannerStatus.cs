using System;

namespace MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532
{
	public enum BarcodeScannerStatus
	{
		failed_on_start = -2,
		lost = -1,
		reseted = 0,
		started = 1,
		readed = 2,
		pending = 3,
		stopped = 4
	}
}