using System;
using System.IO;
using System.Text;

namespace MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532
{
	public class BarcodeScanner : IDisposable
	{
		private int libraryHandler;

		private bool isOn;

		public BarcodeScanner()
		{
			this.libraryHandler = dll_camera.LoadLibrary("dll_camera.dll");
			if (this.libraryHandler == 0)
			{
				throw new FileNotFoundException("Cannot load library FTPCtrl.dll.");
			}
		}

		public bool CheckDevice()
		{
			bool device;
			try
			{
				device = dll_camera.GetDevice() == 1;
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return device;
		}

		public void Dispose()
		{
			try
			{
				this.OffDevice();
				if (this.libraryHandler != 0)
				{
					dll_camera.FreeLibrary(this.libraryHandler);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Init(int beepTime, bool QR_Enable, bool DM_Enable, bool barcode_Enable)
		{
			dll_camera.setBarcode(barcode_Enable);
			dll_camera.setDMable(DM_Enable);
			dll_camera.setQRable(QR_Enable);
			dll_camera.SetBeepTime(beepTime);
		}

		public void OffDevice()
		{
			try
			{
				if (this.isOn)
				{
					this.isOn = false;
					dll_camera.ReleaseDevice();
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public bool OnDevice()
		{
			bool flag;
			try
			{
				this.isOn = dll_camera.StartDevice();
				flag = this.isOn;
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return flag;
		}

		public void ReleaseDeviceData()
		{
			try
			{
				dll_camera.ReleaseLostDevice();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public string Scan()
		{
			string str;
			try
			{
				StringBuilder stringBuilder = new StringBuilder()
				{
					Length = 1024
				};
				dll_camera.GetDecodeString(stringBuilder);
				str = stringBuilder.ToString();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return str;
		}
	}
}