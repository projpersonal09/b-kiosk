using Helper;
using MQuest.HardwareInterface.BarcodeScanner;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532
{
	public class BarcodeScannerManager : IDisposable
	{
		private MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532.BarcodeScanner scanner;

		private bool QR_Enable;

		private bool DM_Enable;

		private bool barcode_Enable;

		private int beepTime;

		private int checkInterval;

		private int tryAttempts;

		private MyLog LogFile = new MyLog();

		public bool stopThreadLoop
		{
			get;
			set;
		}

		public BarcodeScannerManager(IntPtr appHandler)
		{
			this.checkInterval = 100;
			this.tryAttempts = 3;
			this.SetAppHandler(appHandler);
		}

		public BarcodeScannerManager(IntPtr appHandler, int checkInterval)
		{
			this.checkInterval = checkInterval;
			this.tryAttempts = 3;
			this.SetAppHandler(appHandler);
		}

		public BarcodeScannerManager(IntPtr appHandler, int checkInterval, int tryAttempts)
		{
			this.checkInterval = checkInterval;
			this.tryAttempts = tryAttempts;
			this.SetAppHandler(appHandler);
		}

		public void Dispose()
		{
			try
			{
				this.stopThreadLoop = true;
				this.scanner.Dispose();
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Init(int beepTime, bool QR_Enable, bool DM_Enable, bool barcode_Enable)
		{
			try
			{
				if (!this.scanner.CheckDevice())
				{
					throw new BarcodeScannerException("Barcode scanner not found.");
				}
				this.beepTime = beepTime;
				this.QR_Enable = QR_Enable;
				this.DM_Enable = DM_Enable;
				this.barcode_Enable = barcode_Enable;
				this.scanner.Init(beepTime, QR_Enable, DM_Enable, barcode_Enable);
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		private void LogException(string message)
		{
			this.LogFile.Log(MyLog.LogType.Error, message);
		}

		private void LogException(Exception ex)
		{
			this.LogFile.LogException(ex);
		}

		private void OnAndScanThread(Control control, Action<string> barcodeAction, Action<BarcodeScannerStatus> statusAction, Action<Exception, bool> exception)
		{
			try
			{
				try
				{
					this.stopThreadLoop = false;
					bool flag = true;
					bool flag1 = false;
					int num = 0;
					while (!this.stopThreadLoop)
					{
						if (this.scanner.CheckDevice())
						{
							if (!flag1)
							{
								if (!this.scanner.OnDevice())
								{
									num++;
									if (num >= this.tryAttempts)
									{
										throw new BarcodeScannerException(string.Concat("Re-tried ", num, " times to reconnect barcode scanner."));
									}
									try
									{
										this.scanner.OffDevice();
									}
									catch
									{
									}
									try
									{
										this.scanner.ReleaseDeviceData();
									}
									catch
									{
									}
									try
									{
										this.scanner.Dispose();
										this.SetAppHandler(IntPtr.Zero);
									}
									catch
									{
									}
									if (statusAction != null)
									{
										if (!control.InvokeRequired)
										{
											statusAction(BarcodeScannerStatus.failed_on_start);
										}
										else
										{
											object[] objArray = new object[] { BarcodeScannerStatus.failed_on_start };
											control.BeginInvoke(statusAction, objArray);
										}
									}
								}
								else
								{
									num = 0;
									this.Init(this.beepTime, this.QR_Enable, this.DM_Enable, this.barcode_Enable);
									flag1 = true;
									if (flag)
									{
										flag = false;
										if (statusAction != null)
										{
											if (!control.InvokeRequired)
											{
												statusAction(BarcodeScannerStatus.started);
											}
											else
											{
												object[] objArray1 = new object[] { BarcodeScannerStatus.started };
												control.BeginInvoke(statusAction, objArray1);
											}
										}
									}
									else if (statusAction != null)
									{
										if (!control.InvokeRequired)
										{
											statusAction(0);
										}
										else
										{
											object[] objArray2 = new object[] { BarcodeScannerStatus.reseted };
											control.BeginInvoke(statusAction, objArray2);
										}
									}
								}
							}
							if (flag1)
							{
								string str = this.Scan();
								if (!string.IsNullOrEmpty(str))
								{
									if (barcodeAction != null)
									{
										if (!control.InvokeRequired)
										{
											barcodeAction(str);
										}
										else
										{
											object[] objArray3 = new object[] { str };
											control.BeginInvoke(barcodeAction, objArray3);
										}
									}
									if (statusAction != null)
									{
										if (!control.InvokeRequired)
										{
											statusAction(BarcodeScannerStatus.readed);
										}
										else
										{
											object[] objArray4 = new object[] { BarcodeScannerStatus.readed };
											control.BeginInvoke(statusAction, objArray4);
										}
									}
								}
								else if (statusAction != null)
								{
									if (!control.InvokeRequired)
									{
										statusAction(BarcodeScannerStatus.pending);
									}
									else
									{
										object[] objArray5 = new object[] { BarcodeScannerStatus.pending };
										control.BeginInvoke(statusAction, objArray5);
									}
								}
							}
						}
						else if (flag1)
						{
							this.scanner.ReleaseDeviceData();
							flag1 = false;
							if (statusAction != null)
							{
								if (!control.InvokeRequired)
								{
									statusAction(BarcodeScannerStatus.lost);
								}
								else
								{
									object[] objArray6 = new object[] { BarcodeScannerStatus.lost };
									control.BeginInvoke(statusAction, objArray6);
								}
							}
						}
						if (this.stopThreadLoop)
						{
							continue;
						}
						Thread.Sleep(this.checkInterval);
					}
				}
				catch (Exception exception2)
				{
					Exception exception1 = exception2;
					if (exception != null)
					{
						if (!control.InvokeRequired)
						{
							exception(exception1, false);
						}
						else
						{
							object[] objArray7 = new object[] { exception1, false };
							control.BeginInvoke(exception, objArray7);
						}
					}
				}
			}
			finally
			{
				this.scanner.OffDevice();
				if (statusAction != null)
				{
					if (!control.InvokeRequired)
					{
						statusAction(BarcodeScannerStatus.stopped);
					}
					else
					{
						object[] objArray8 = new object[] { BarcodeScannerStatus.stopped };
						control.BeginInvoke(statusAction, objArray8);
					}
				}
			}
		}

		public string Scan()
		{
			string str;
			try
			{
				str = this.scanner.Scan();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return str;
		}

		private void SetAppHandler(IntPtr appHandler)
		{
			try
			{
				this.scanner = new MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532.BarcodeScanner();
				if (appHandler != IntPtr.Zero)
				{
					dll_camera.GetAppHandle(appHandler);
				}
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void StartOnAndScanThread(Control control, Action<string> barcodeAction, Action<BarcodeScannerStatus> statusAction, Action<Exception, bool> exception)
		{
			Thread thread = new Thread(() => this.OnAndScanThread(control, barcodeAction, statusAction, exception))
			{
				IsBackground = true
			};
			thread.Start();
		}
	}
}