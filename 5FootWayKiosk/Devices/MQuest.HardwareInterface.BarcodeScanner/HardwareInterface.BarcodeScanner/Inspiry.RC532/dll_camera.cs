using System;
using System.Runtime.InteropServices;
using System.Text;

namespace MQuest.HardwareInterface.BarcodeScanner.Inspiry.RC532
{
	public class dll_camera
	{
		public dll_camera()
		{
		}

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern bool FreeLibrary(int libraryHandler);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void GetAppHandle(IntPtr hWnd);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void GetDecodeString(StringBuilder sb);

		[DllImport("dll_camera.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int GetDevice();

		[DllImport("kernel32", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int LoadLibrary(string strDllName);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void ReleaseDevice();

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void ReleaseLostDevice();

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void setBarcode(bool bbarcode);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void SetBeepTime(int BeepTime);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void setDMable(bool bdm);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern void setQRable(bool bqr);

		[DllImport("dll_camera.dll", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.None, ExactSpelling=false)]
		public static extern bool StartDevice();
	}
}