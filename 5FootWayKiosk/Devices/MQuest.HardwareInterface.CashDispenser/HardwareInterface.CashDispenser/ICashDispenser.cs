using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CashDispenser
{
	public interface ICashDispenser
	{
		bool LogOutput
		{
			get;
			set;
		}

		bool CheckPayoutAvailable(decimal amountToWithdraw);

		void Dispose();

		List<CashNoteInformation> GetNoteCount(List<int> noteValueList);

		void Init(List<int> noteToRouteList);

		void StartDepositThread(Control threadControl, Action<decimal, decimal> finalAction, Action<int> updateStep, Action<decimal, decimal> cashAccepted, Action<Exception, bool> exception, decimal totalAmountDeposit);

		void StartEmptyThread(Control threadControl, Action<decimal> actionComplete, Action<decimal> actionSuccess, Action<int> actionProgress, Action<Exception, bool> exception);

		void StartRefillThread(Control threadControl, Action<decimal> finalAction, Action<int> updateStep, Action<int, decimal> cashAccepted, Action<Exception, bool> exception, int numberOfCashNote);

		void StartWithdrawThread(Control threadControl, Action<decimal> actionComplete, Action<int> actionProcessing, Action<decimal> actionSuccess, Action<Exception, bool> exception, decimal amountToWithdraw);

		void StopThread();
	}
}