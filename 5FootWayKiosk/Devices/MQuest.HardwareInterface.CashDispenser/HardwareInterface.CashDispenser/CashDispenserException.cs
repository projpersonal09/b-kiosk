using System;

namespace MQuest.HardwareInterface.CashDispenser
{
	public class CashDispenserException : Exception
	{
		public bool showDetail;

		public static CashDispenserException ControlMissing;

		public static CashDispenserException UnknownError;

		static CashDispenserException()
		{
			CashDispenserException.ControlMissing = new CashDispenserException("Control cannot be null");
			CashDispenserException.UnknownError = new CashDispenserException("Unknown.");
		}

		public CashDispenserException()
		{
		}

		public CashDispenserException(string message) : base(message)
		{
		}

		public CashDispenserException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}