using ITLlib;
using MQuest.HardwareInterface.CashDispenser;
using System;

namespace MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200
{
	public class CashDispenser
	{
		public const byte MINIMUM_PROTOCOL_VERSION = 6;

		public const string currencyType = "SGD";

		public string defaultFixedKey = "0123456701234567";

		public int defaultTimeoutInterval = 1000;

		public int defaultRetrieveTimeout = 5000;

		public string portName;

		public SSPComms SSPCom;

		public SSP_KEYS SSPKey;

		public SSP_FULL_KEY SSPFullKey;

		public bool isEncypted;

		public SSP_COMMAND SSPCommand;

		public static object lockAction;

		public readonly decimal[] channelValue = new decimal[] { new decimal(2), new decimal(5), new decimal(10), new decimal(50) };

		public CashDispenser(int portNumber, bool isEncypted)
		{
			this.portName = string.Concat("COM", portNumber);
			this.isEncypted = isEncypted;
			this.SSPCom = new SSPComms();
			this.SSPKey = new SSP_KEYS();
			this.SSPFullKey = new SSP_FULL_KEY();
			this.SSPCommand = new SSP_COMMAND();
			MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction = new object();
		}

		public bool closePort()
		{
			bool flag;
			lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
			{
				flag = this.SSPCom.CloseComPort();
			}
			return flag;
		}

		public static string convertResponse(byte b)
		{
			switch (b)
			{
				case 201:
				{
					return "NOTE TRANSFERRED TO STACKER RESPONSE";
				}
				case 202:
				case 203:
				case 205:
				case 207:
				case 208:
				case 209:
				case 211:
				case 212:
				case 213:
				case 214:
				case 215:
				case 216:
				case 217:
				case 220:
				case 221:
				case 222:
				case 223:
				case 229:
				case 247:
				case 249:
				{
					return b.ToString("X");
				}
				case 204:
				{
					return "STACKING RESPONSE";
				}
				case 206:
				{
					return "NOTE IN BEZEL";
				}
				case 210:
				{
					return "NOTE DISPENSED RESPONSE";
				}
				case 218:
				{
					return "NOTE DISPENSING RESPONSE";
				}
				case 219:
				{
					return "NOTE STORED RESPONSE";
				}
				case 224:
				{
					return "NOTE PATH OPEN";
				}
				case 225:
				{
					return "NOTE CLEARED FROM FRONT RESPONSE";
				}
				case 226:
				{
					return "NOTE CLEARED TO CASHBOX RESPONSE";
				}
				case 227:
				{
					return "CASHBOX REMOVED RESPONSE";
				}
				case 228:
				{
					return "CASHBOX REPLACED RESPONSE";
				}
				case 230:
				{
					return "FRAUD ATTEMPT RESPONSE";
				}
				case 231:
				{
					return "STACKER FULL RESPONSE";
				}
				case 232:
				{
					return "DISABLED RESPONSE";
				}
				case 233:
				{
					return "UNSAFE JAM RESPONSE";
				}
				case 234:
				{
					return "SAFE JAM RESPONSE";
				}
				case 235:
				{
					return "STACKED RESPONSE";
				}
				case 236:
				{
					return "REJECTED RESPONSE";
				}
				case 237:
				{
					return "REJECTING RESPONSE";
				}
				case 238:
				{
					return "CREDIT RESPONSE";
				}
				case 239:
				{
					return "NOTE READ RESPONSE";
				}
				case 240:
				{
					return "OK RESPONSE";
				}
				case 241:
				{
					return "RESET RESPONSE";
				}
				case 242:
				{
					return "UNKNOWN RESPONSE";
				}
				case 243:
				{
					return "WRONG PARAMS RESPONSE";
				}
				case 244:
				{
					return "PARAM OUT OF RANGE RESPONSE";
				}
				case 245:
				{
					return "CANNOT PROCESS RESPONSE";
				}
				case 246:
				{
					return "SOFTWARE ERROR RESPONSE";
				}
				case 248:
				{
					return "FAIL RESPONSE";
				}
				case 250:
				{
					return "KEY NOT SET RESPONSE";
				}
				default:
				{
					return b.ToString("X");
				}
			}
		}

		public bool disableDevice()
		{
			int num;
			bool flag;
			try
			{
				this.isEncypted = true;
				byte[] numArray = new byte[] { Convert.ToByte(Command.Disable) };
				byte[] numArray1 = this.sendESSPCommand(numArray, out num);
				if (numArray1 == null)
				{
					flag = false;
				}
				else
				{
					if (!numArray1[3].Equals(Convert.ToByte(Response.OK)))
					{
						throw new Exception(BitConverter.ToString(numArray1, 0, num));
					}
					flag = true;
				}
			}
			catch
			{
				throw;
			}
			return flag;
		}

		public bool emptyAllCashNote()
		{
			int num;
			bool flag;
			try
			{
				this.isEncypted = true;
				byte[] numArray = new byte[] { Convert.ToByte(Command.SMARTempty) };
				byte[] numArray1 = this.sendESSPCommand(numArray, out num);
				if (numArray1 == null)
				{
					flag = false;
				}
				else
				{
					if (!numArray1[3].Equals(Convert.ToByte(Response.OK)))
					{
						throw new Exception(BitConverter.ToString(numArray1, 0, num));
					}
					flag = true;
				}
			}
			catch
			{
				throw;
			}
			return flag;
		}

		public bool enableDevice()
		{
			int num;
			bool flag;
			try
			{
				this.isEncypted = true;
				byte[] numArray = new byte[] { Convert.ToByte(Command.Enable) };
				byte[] numArray1 = this.sendESSPCommand(numArray, out num);
				if (numArray1 == null)
				{
					flag = false;
				}
				else
				{
					if (!numArray1[3].Equals(Convert.ToByte(Response.OK)))
					{
						throw new Exception(BitConverter.ToString(numArray1));
					}
					flag = true;
				}
			}
			catch
			{
				throw;
			}
			return flag;
		}

		public bool enableSMARTPayout()
		{
			int num;
			bool flag;
			try
			{
				this.isEncypted = true;
				byte[] numArray = new byte[] { Convert.ToByte(Command.EnablePayoutDevice) };
				byte[] numArray1 = this.sendESSPCommand(numArray, out num);
				if (numArray1 == null)
				{
					flag = false;
				}
				else
				{
					if (!numArray1[3].Equals(Convert.ToByte(Response.OK)))
					{
						throw new Exception(BitConverter.ToString(numArray1));
					}
					flag = true;
				}
			}
			catch
			{
				throw;
			}
			return flag;
		}

		public void findMaxPayoutProtocolVersion()
		{
			byte num = 6;
			bool flag = true;
			while (flag)
			{
				if (!this.setProtocolVersion(num)[0].Equals(Convert.ToByte(Response.FAIL)))
				{
					num = (byte)(num + 1);
					if (num <= 20)
					{
						continue;
					}
					this.setProtocolVersion(6);
					flag = false;
				}
				else
				{
					flag = false;
				}
			}
		}

		public bool negotiateKeys()
		{
			int i;
			int num;
			this.SSPCommand.CommandData = new byte[255];
			this.SSPCommand.CommandData[0] = Convert.ToByte(Command.Synchronisation);
			this.SSPCommand.CommandDataLength = 1;
			if (this.sendESSPCommand(this.SSPCommand, out num) == null)
			{
				return false;
			}
			lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
			{
				this.SSPCom.InitiateSSPHostKeys(this.SSPKey, this.SSPCommand);
			}
			this.SSPCommand.CommandData = new byte[255];
			this.SSPCommand.CommandData[0] = Convert.ToByte(Command.SetGenerator);
			this.SSPCommand.CommandDataLength = 9;
			for (i = 0; i < 8; i++)
			{
				this.SSPCommand.CommandData[i + 1] = (byte)(this.SSPKey.Generator >> (8 * i & 63));
			}
			if (this.sendESSPCommand(this.SSPCommand, out num) == null)
			{
				return false;
			}
			this.SSPCommand.CommandData = new byte[255];
			this.SSPCommand.CommandData[0] = Convert.ToByte(Command.SetModulus);
			this.SSPCommand.CommandDataLength = 9;
			for (i = 0; i < 8; i++)
			{
				this.SSPCommand.CommandData[i + 1] = (byte)(this.SSPKey.Modulus >> (8 * i & 63));
			}
			if (this.sendESSPCommand(this.SSPCommand, out num) == null)
			{
				return false;
			}
			this.SSPCommand.CommandData = new byte[255];
			this.SSPCommand.CommandData[0] = Convert.ToByte(Command.RequestKeyExchange);
			this.SSPCommand.CommandDataLength = 9;
			for (i = 0; i < 8; i++)
			{
				this.SSPCommand.CommandData[i + 1] = (byte)(this.SSPKey.HostInter >> (8 * i & 63));
			}
			if (this.sendESSPCommand(this.SSPCommand, out num) == null)
			{
				return false;
			}
			this.SSPKey.SlaveInterKey = (ulong)0;
			for (i = 0; i < 8; i++)
			{
				SSP_KEYS sSPKey = this.SSPKey;
				sSPKey.SlaveInterKey = sSPKey.SlaveInterKey + ((ulong)this.SSPCommand.ResponseData[1 + i] << (8 * i & 63));
			}
			lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
			{
				this.SSPCom.CreateSSPHostEncryptionKey(this.SSPKey);
			}
			this.SSPFullKey.FixedKey = 81985526925837671L;
			this.SSPFullKey.VariableKey = this.SSPKey.KeyHost;
			return true;
		}

		public bool openPort()
		{
			bool flag;
			lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
			{
				this.SSPCommand.EncryptionStatus = false;
				this.SSPCommand.SSPAddress = 0;
				this.SSPCommand.ComPort = this.portName;
				this.SSPCommand.BaudRate = 9600;
				this.SSPCommand.Timeout = Convert.ToUInt32(this.defaultRetrieveTimeout);
				flag = this.SSPCom.OpenSSPComPort(this.SSPCommand);
			}
			return flag;
		}

		public byte[] receiveESSPResponse(out int responseLength)
		{
			byte[] packetData;
			responseLength = 0;
			try
			{
				lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
				{
					byte[] num = new byte[] { Convert.ToByte(Command.Poll) };
					this.SSPCommand.EncryptionStatus = this.isEncypted;
					this.SSPCommand.CommandData = new byte[255];
					Buffer.BlockCopy(num, 0, this.SSPCommand.CommandData, 0, (int)num.Length);
					this.SSPCommand.CommandDataLength = Convert.ToByte((int)num.Length);
					this.SSPCommand.Key = this.SSPFullKey;
					SSP_COMMAND_INFO sSPCOMMANDINFO = new SSP_COMMAND_INFO();
					this.SSPCom.SSPSendCommand(this.SSPCommand, sSPCOMMANDINFO);
					if (this.SSPCommand.ResponseStatus.Equals((PORT_STATUS)3))
					{
						SSP_PACKET preEncryptedRecieve = sSPCOMMANDINFO.PreEncryptedRecieve;
						responseLength = preEncryptedRecieve.PacketLength;
						packetData = preEncryptedRecieve.PacketData;
						return packetData;
					}
				}
				return null;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new CashDispenserException(string.Concat("Send ESSP Command Error, Port Status : ", this.SSPCommand.ResponseStatus.ToString()), exception);
			}
			return packetData;
		}

		public byte[] sendESSPCommand(byte[] byteArr, out int responseLength)
		{
			byte[] packetData;
			responseLength = 0;
			try
			{
				lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
				{
					this.SSPCommand.CommandData = new byte[255];
					Buffer.BlockCopy(byteArr, 0, this.SSPCommand.CommandData, 0, (int)byteArr.Length);
					this.SSPCommand.CommandDataLength = Convert.ToByte((int)byteArr.Length);
					this.SSPCommand.Key = this.SSPFullKey;
					this.SSPCommand.EncryptionStatus = this.isEncypted;
					SSP_COMMAND_INFO sSPCOMMANDINFO = new SSP_COMMAND_INFO();
					this.SSPCom.SSPSendCommand(this.SSPCommand, sSPCOMMANDINFO);
					if (this.SSPCommand.ResponseStatus.Equals((PORT_STATUS)3))
					{
						SSP_PACKET preEncryptedRecieve = sSPCOMMANDINFO.PreEncryptedRecieve;
						responseLength = preEncryptedRecieve.PacketLength;
						packetData = preEncryptedRecieve.PacketData;
						return packetData;
					}
				}
				return null;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new CashDispenserException(string.Concat("Send ESSP Command Error, Port Status : ", this.SSPCommand.ResponseStatus.ToString()), exception);
			}
			return packetData;
		}

		public byte[] sendESSPCommand(SSP_COMMAND SSPCommand, out int responseLength)
		{
			byte[] packetData;
			responseLength = 0;
			try
			{
				lock (MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser.lockAction)
				{
					SSPCommand.EncryptionStatus = this.isEncypted;
					SSPCommand.Key = this.SSPFullKey;
					SSP_COMMAND_INFO sSPCOMMANDINFO = new SSP_COMMAND_INFO();
					this.SSPCom.SSPSendCommand(SSPCommand, sSPCOMMANDINFO);
					if (SSPCommand.ResponseStatus.Equals((PORT_STATUS)3))
					{
						SSP_PACKET preEncryptedRecieve = sSPCOMMANDINFO.PreEncryptedRecieve;
						responseLength = preEncryptedRecieve.PacketLength;
						packetData = preEncryptedRecieve.PacketData;
						return packetData;
					}
				}
				return null;
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new CashDispenserException(string.Concat("Send ESSP Command Error, Port Status : ", SSPCommand.ResponseStatus.ToString()), exception);
			}
			return packetData;
		}

		public byte[] setProtocolVersion(byte version)
		{
			int num;
			byte[] numArray = new byte[] { Convert.ToByte(Command.HostProtocolVersion), version };
			return this.sendESSPCommand(numArray, out num);
		}
	}
}