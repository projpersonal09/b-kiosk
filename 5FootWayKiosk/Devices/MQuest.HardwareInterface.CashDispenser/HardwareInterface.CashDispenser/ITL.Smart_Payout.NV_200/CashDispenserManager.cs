using Helper;
using MQuest.HardwareInterface.CashDispenser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200
{
	public class CashDispenserManager : IDisposable, ICashDispenser
	{
		private MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser dispenser;

		private List<decimal> availableAmount;

		private List<decimal> noteToRouteList;

		private string _currency;

		private bool _stopThreadLoop;

		private MyLog _logFile;

		private readonly static object lockAction;

		private readonly static object lockState;

		public bool LogOutput
		{
			get;
			set;
		}

		private bool stopThreadLoop
		{
			get
			{
				bool flag;
				lock (CashDispenserManager.lockState)
				{
					flag = this._stopThreadLoop;
				}
				return flag;
			}
			set
			{
				lock (CashDispenserManager.lockState)
				{
					this._stopThreadLoop = value;
				}
			}
		}

		static CashDispenserManager()
		{
			CashDispenserManager.lockAction = new object();
			CashDispenserManager.lockState = new object();
		}

		public CashDispenserManager(int comPortNumber, List<decimal> availableAmount, bool logOutput)
		{
			this._currency = "SGD";
			this.availableAmount = availableAmount;
			this.LogOutput = logOutput;
			this._logFile = new MyLog();
			this.dispenser = new MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.CashDispenser(comPortNumber, false);
			this.dispenser.closePort();
		}

		private bool checkMachineStatus(byte[] responseData)
		{
			if (responseData[3].Equals(Convert.ToByte(Response.OK)))
			{
				return true;
			}
			if (responseData[3].Equals(Convert.ToByte(Response.CommandCannotProcessed)))
			{
				CashDispenserException cashDispenserException = null;
				switch (responseData[4])
				{
					case 1:
					{
						cashDispenserException = new CashDispenserException("Not enough value to payout.");
						break;
					}
					case 2:
					{
						cashDispenserException = new CashDispenserException("Cannot Pay exact amount.");
						break;
					}
					case 3:
					{
						Thread.Sleep(500);
						break;
					}
					case 4:
					{
						this.dispenser.enableDevice();
						break;
					}
					default:
					{
						cashDispenserException = CashDispenserException.UnknownError;
						break;
					}
				}
				if (cashDispenserException != null)
				{
					throw cashDispenserException;
				}
			}
			return false;
		}

		public bool CheckPayoutAvailable(decimal amountToWithdraw)
		{
			int num;
			bool flag = false;
			try
			{
				if (amountToWithdraw < new decimal(0))
				{
					throw new CashDispenserException("Cannot withdraw, please check the cash note in cash acceptor.");
				}
				this.dispenser.isEncypted = true;
				while (!flag)
				{
					byte[] numArray = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.PayoutAmount), 0, 0, 0, 0, 0, 0, 0, 0 };
					byte[] bytes = BitConverter.GetBytes(Convert.ToInt32(amountToWithdraw * new decimal(100)));
					numArray[1] = bytes[0];
					numArray[2] = bytes[1];
					numArray[3] = bytes[2];
					numArray[4] = bytes[3];
					numArray[5] = Convert.ToByte(this._currency[0]);
					numArray[6] = Convert.ToByte(this._currency[1]);
					numArray[7] = Convert.ToByte(this._currency[2]);
					numArray[8] = 25;
					if (!this.checkMachineStatus(this.dispenser.sendESSPCommand(numArray, out num)))
					{
						continue;
					}
					flag = true;
				}
			}
			catch
			{
				throw;
			}
			return flag;
		}

		public void Deposit(Control threadControl, Action<decimal, decimal> actionComplete, Action<int> actionProcessing, Action<decimal, decimal> actionSuccess, Action<Exception, bool> exception, decimal totalAmountDeposit)
		{
			int num;
			try
			{
				try
				{
					decimal num1 = totalAmountDeposit;
					if (this.dispenser.enableDevice() && this.dispenser.enableSMARTPayout())
					{
						this.resetStateVariable();
						decimal num2 = new decimal(0);
						bool flag = true;
						bool flag1 = false;
						byte num3 = 0;
						while (num1 > new decimal(0) && (!this.stopThreadLoop || !flag))
						{
							byte[] numArray = this.dispenser.receiveESSPResponse(out num);
							int num4 = 4;
							while (num4 < num - 2)
							{
								int num5 = num4;
								num4 = num5 + 1;
								num3 = numArray[num5];
								if (num3.Equals(Convert.ToByte(Response.Read)))
								{
									int num6 = num4;
									num4 = num6 + 1;
									int num7 = Convert.ToInt32(numArray[num6]);
									if (num7 <= 0 || num7 > (int)this.dispenser.channelValue.Length)
									{
										continue;
									}
									flag = false;
									num2 = this.dispenser.channelValue[num7 - 1];
									if (!this.availableAmount.Contains(num2))
									{
										byte[] numArray1 = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.Reject) };
										byte[] numArray2 = this.dispenser.sendESSPCommand(numArray1, out num);
										this.checkMachineStatus(numArray2);
									}
									this.logInfo(string.Concat("Cash Machine: Read ", num2.ToString(), ", Amount to Deposit: ", num1.ToString()), false);
								}
								else if (num3.Equals(Convert.ToByte(Response.Credit)))
								{
									int num8 = num4;
									num4 = num8 + 1;
									int num9 = Convert.ToInt32(numArray[num8]);
									if (num9 <= 0 || num9 > (int)this.dispenser.channelValue.Length)
									{
										continue;
									}
									num2 = this.dispenser.channelValue[num9 - 1];
									this.logInfo(string.Concat("Cash Machine: Credit ", num2.ToString(), ", Amount to Deposit: ", num1.ToString()), false);
								}
								else if (num3.Equals(Convert.ToByte(Response.Rejecting)))
								{
									flag = false;
								}
								else if (num3.Equals(Convert.ToByte(Response.Rejected)))
								{
									this.logInfo(string.Concat("Cash Machine: Rejected ", num2.ToString(), ", Amount to Deposit: ", num1.ToString()), false);
									flag = true;
									num2 = new decimal(0);
									this.logInfo(string.Concat("Cash Machine: Rejected. Remaining amount to Deposit: ", num1.ToString()), false);
								}
								else if (num3.Equals(Convert.ToByte(Response.Stacking)))
								{
									flag = false;
								}
								else if (num3.Equals(Convert.ToByte(Response.Stacked)))
								{
									this.logInfo(string.Concat("Cash Machine: Stacked ", num2.ToString(), ", Amount to Deposit: ", num1.ToString()), false);
									actionSuccess(num1, num2);
									num1 -= num2;
									num2 = new decimal(0);
									flag = true;
									this.logInfo(string.Concat("Cash Machine: Stacked. Remaining amount to Deposit: ", num1.ToString()), false);
								}
								else if (!num3.Equals(Convert.ToByte(Response.NoteStoredInPayout)))
								{
									if (num3.Equals(Convert.ToByte(Response.PayoutJammed)))
									{
										num4++;
										decimal num10 = new decimal(0);
										if (numArray[num4] != 0)
										{
											byte[] numArray3 = new byte[4];
											int num11 = num4;
											num4 = num11 + 1;
											numArray3[0] = numArray[num11];
											int num12 = num4;
											num4 = num12 + 1;
											numArray3[1] = numArray[num12];
											int num13 = num4;
											num4 = num13 + 1;
											numArray3[2] = numArray[num13];
											numArray3[3] = numArray[num4];
											num10 = Convert.ToDecimal(BitConverter.ToInt32(numArray3, 0)) / new decimal(100);
											num4 += 3;
										}
										object[] objArray = new object[] { "Cash ", this._currency, " ", num2, " is jammed in Cash Machine." };
										CashDispenserException cashDispenserException = new CashDispenserException(string.Concat(objArray))
										{
											showDetail = true
										};
										num1 = totalAmountDeposit - num10;
										num2 = new decimal(0);
										this.logInfo(string.Concat("Cash Machine: Payout Jammed ", num10.ToString(), ", Amount To Deposit ", num1.ToString()), true);
										throw cashDispenserException;
									}
									if (num3.Equals(Convert.ToByte(Response.Disabled)))
									{
										this.dispenser.enableDevice();
										flag = true;
									}
									else if (!num3.Equals(Convert.ToByte(Response.PayoutOutOfService)))
									{
										this.getExceptionByResponse(num3, false);
										this.logInfo(string.Concat("Cash Machine: Event ", num3.ToString("X2"), ", Amount to Deposit: ", num1.ToString()), false);
									}
									else
									{
										this.dispenser.enableSMARTPayout();
									}
								}
								else
								{
									this.logInfo(string.Concat("Cash Machine: Note Stored In Payout ", num2.ToString(), ", Amount to Deposit: ", num1.ToString()), false);
									actionSuccess(num1, num2);
									num1 -= num2;
									num2 = new decimal(0);
									flag = true;
									this.logInfo(string.Concat("Cash Machine: Note Stored In Payout. Remaining amount to Deposit: ", num1.ToString()), false);
								}
							}
							Thread.Sleep(50);
						}
						if (num2 != new decimal(0))
						{
							this.logInfo(string.Concat("Cash Machine: Last Event ", num3.ToString("X2"), ", Remaining Note Value: ", num2.ToString()), true);
						}
						lock (CashDispenserManager.lockAction)
						{
							string[] str = new string[] { "Cash Machine: Remaining Amount to Deposit: ", num1.ToString(), ", Stop Thread Loop: ", null, null, null };
							str[3] = this.stopThreadLoop.ToString();
							str[4] = ", Machine Ready: ";
							str[5] = flag.ToString();
							this.logInfo(string.Concat(str), true);
							if (!flag1)
							{
								if (num1 <= new decimal(0))
								{
									num1 = new decimal(0);
								}
								if (actionComplete != null)
								{
									actionComplete(-num1, totalAmountDeposit);
								}
								flag1 = true;
							}
						}
					}
				}
				catch (CashDispenserException cashDispenserException2)
				{
					CashDispenserException cashDispenserException1 = cashDispenserException2;
					if (exception != null)
					{
						if (threadControl == null || !threadControl.InvokeRequired)
						{
							exception(cashDispenserException1, cashDispenserException1.showDetail);
						}
						else
						{
							object[] objArray1 = new object[] { cashDispenserException1, cashDispenserException1.showDetail };
							threadControl.BeginInvoke(exception, objArray1);
						}
					}
				}
				catch (Exception exception2)
				{
					Exception exception1 = exception2;
					if (exception != null)
					{
						if (threadControl == null || !threadControl.InvokeRequired)
						{
							exception(exception1, false);
						}
						else
						{
							object[] objArray2 = new object[] { exception1, false };
							threadControl.BeginInvoke(exception, objArray2);
						}
					}
				}
			}
			finally
			{
				this.dispenser.disableDevice();
			}
		}

		public void Dispose()
		{
			try
			{
				this.StopThread();
				this.dispenser.disableDevice();
				this.dispenser.closePort();
				Thread.Sleep(200);
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}

		public void Empty(Control threadControl, Action<decimal> actionComplete, Action<decimal> actionSuccess, Action<int> actionProgress, Action<Exception, bool> exception)
		{
			int num;
			try
			{
				try
				{
					if (this.dispenser.enableDevice() && this.dispenser.enableSMARTPayout())
					{
						this.resetStateVariable();
						this.dispenser.emptyAllCashNote();
						bool flag = false;
						bool flag1 = false;
						while (!flag1 && !this.stopThreadLoop)
						{
							byte[] numArray = this.dispenser.receiveESSPResponse(out num);
							int num1 = 4;
							while (num1 < num - 2)
							{
								if (numArray[num1].Equals(Convert.ToByte(Response.SMARTEmptying)))
								{
									num1++;
									int num2 = num1;
									num1 = num2 + 1;
									if (numArray[num2] == 0)
									{
										continue;
									}
									byte[] numArray1 = new byte[4];
									int num3 = num1;
									num1 = num3 + 1;
									numArray1[0] = numArray[num3];
									int num4 = num1;
									num1 = num4 + 1;
									numArray1[1] = numArray[num4];
									int num5 = num1;
									num1 = num5 + 1;
									numArray1[2] = numArray[num5];
									int num6 = num1;
									num1 = num6 + 1;
									numArray1[3] = numArray[num6];
									decimal num7 = Convert.ToDecimal(BitConverter.ToInt32(numArray1, 0)) / new decimal(100);
									num1++;
									if (num7 != new decimal(0) && actionSuccess != null)
									{
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionSuccess(num7);
										}
										else
										{
											object[] objArray = new object[] { num7 };
											threadControl.BeginInvoke(actionSuccess, objArray);
										}
									}
									if (!threadControl.InvokeRequired)
									{
										actionProgress(10);
									}
									else
									{
										object[] objArray1 = new object[] { 10 };
										threadControl.BeginInvoke(actionProgress, objArray1);
									}
								}
								else if (numArray[num1].Equals(Convert.ToByte(Response.SMARTEmptyed)))
								{
									num1++;
									int num8 = num1;
									num1 = num8 + 1;
									if (numArray[num8] == 0)
									{
										continue;
									}
									byte[] numArray2 = new byte[4];
									int num9 = num1;
									num1 = num9 + 1;
									numArray2[0] = numArray[num9];
									int num10 = num1;
									num1 = num10 + 1;
									numArray2[1] = numArray[num10];
									int num11 = num1;
									num1 = num11 + 1;
									numArray2[2] = numArray[num11];
									int num12 = num1;
									num1 = num12 + 1;
									numArray2[3] = numArray[num12];
									decimal num13 = Convert.ToDecimal(BitConverter.ToInt32(numArray2, 0)) / new decimal(100);
									num1++;
									if (!threadControl.InvokeRequired)
									{
										actionProgress(11);
									}
									else
									{
										object[] objArray2 = new object[] { 11 };
										threadControl.BeginInvoke(actionProgress, objArray2);
									}
									lock (CashDispenserManager.lockAction)
									{
										if (!flag)
										{
											if (actionComplete != null)
											{
												actionComplete(num13);
											}
											flag = true;
											flag1 = true;
										}
									}
								}
								else if (numArray[num1].Equals(Convert.ToByte(Response.Disabled)))
								{
									this.dispenser.enableDevice();
									num1++;
								}
								else if (!numArray[num1].Equals(Convert.ToByte(Response.PayoutOutOfService)))
								{
									this.getExceptionByResponse(numArray[num1], false);
									num1++;
								}
								else
								{
									this.dispenser.enableSMARTPayout();
									num1++;
								}
							}
						}
					}
				}
				catch (Exception exception2)
				{
					Exception exception1 = exception2;
					if (!threadControl.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray3 = new object[] { exception1, false };
						threadControl.BeginInvoke(exception, objArray3);
					}
				}
			}
			finally
			{
				this.dispenser.disableDevice();
			}
		}

		private void getExceptionByResponse(byte response, bool payout)
		{
			Response response1 = (Response)response;
			if (response1 == Response.Halted)
			{
				if (payout)
				{
					throw new CashDispenserException("Device is halted.");
				}
				return;
			}
			if (response1 == Response.Timeout)
			{
				throw new CashDispenserException("Device response timeout.");
			}
			switch (response1)
			{
				case Response.FraudAttempt:
				{
					throw new CashDispenserException("Fraud Attempt.");
				}
				case Response.StackerFull:
				{
					throw new CashDispenserException("Cash box is full.");
				}
				default:
				{
					return;
				}
			}
		}

		public List<CashNoteInformation> GetNoteCount(List<int> noteValueList)
		{
			int num;
			List<CashNoteInformation> cashNoteInformations = new List<CashNoteInformation>();
			foreach (int num1 in noteValueList)
			{
				byte[] numArray = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.GetNoteAmount), 0, 0, 0, 0, 0, 0, 0 };
				byte[] bytes = BitConverter.GetBytes(num1 * 100);
				numArray[1] = bytes[0];
				numArray[2] = bytes[1];
				numArray[3] = bytes[2];
				numArray[4] = bytes[3];
				numArray[5] = Convert.ToByte(this._currency[0]);
				numArray[6] = Convert.ToByte(this._currency[1]);
				numArray[7] = Convert.ToByte(this._currency[2]);
				byte[] numArray1 = this.dispenser.sendESSPCommand(numArray, out num);
				CashNoteInformation cashNoteInformation = new CashNoteInformation();
				if (!this.checkMachineStatus(numArray1))
				{
					cashNoteInformation.cashNoteStoredCount = -1;
				}
				else
				{
					cashNoteInformation.cashNoteStoredCount = Convert.ToInt32(numArray1[num - 4]);
				}
				cashNoteInformation.cashNoteValue = num1;
				cashNoteInformation.currency = this._currency;
				cashNoteInformations.Add(cashNoteInformation);
			}
			return cashNoteInformations;
		}

		public void Init(List<int> noteToRouteList)
		{
			int num;
			int num1;
			try
			{
				this.resetStateVariable();
				this.noteToRouteList = (
					from x in noteToRouteList
					select Convert.ToDecimal(x)).ToList<decimal>();
				bool flag = this.dispenser.openPort();
				bool flag1 = this.dispenser.negotiateKeys();
				if (!flag)
				{
					throw new CashDispenserException(string.Concat("Cannot open ", this.dispenser.portName, " port."));
				}
				if (!flag1)
				{
					throw new CashDispenserException("Cannot generate exchange key.");
				}
				this.dispenser.isEncypted = true;
				this.dispenser.findMaxPayoutProtocolVersion();
				byte[] numArray = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.SetInhibits), 252, 255 };
				this.dispenser.sendESSPCommand(numArray, out num);
				decimal[] numArray1 = this.dispenser.channelValue;
				for (int i = 0; i < (int)numArray1.Length; i++)
				{
					int num2 = Convert.ToInt32(numArray1[i]);
					numArray = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.SetRouting), 0, 0, 0, 0, 0, 0, 0, 0 };
					if (!this.noteToRouteList.Contains(num2))
					{
						numArray[1] = 1;
					}
					else
					{
						numArray[1] = 0;
					}
					byte[] bytes = BitConverter.GetBytes(num2 * 100);
					numArray[2] = bytes[0];
					numArray[3] = bytes[1];
					numArray[4] = bytes[2];
					numArray[5] = bytes[3];
					numArray[6] = Convert.ToByte(this._currency[0]);
					numArray[7] = Convert.ToByte(this._currency[1]);
					numArray[8] = Convert.ToByte(this._currency[2]);
					this.dispenser.sendESSPCommand(numArray, out num1);
				}
			}
			catch
			{
				throw;
			}
		}

		private void logInfo(string message, bool force)
		{
			if (force || this.LogOutput)
			{
				this._logFile.Log(MyLog.LogType.Info, message);
			}
		}

		public void Refill(Control threadControl, Action<decimal> actionComplete, Action<int> actionProcessing, Action<int, decimal> actionSuccess, Action<Exception, bool> exception, int numberOfCashNote)
		{
			int num;
			try
			{
				try
				{
					int num1 = numberOfCashNote;
					if (this.dispenser.enableDevice() && this.dispenser.enableSMARTPayout())
					{
						this.resetStateVariable();
						decimal num2 = new decimal(0);
						decimal num3 = new decimal(0);
						bool flag = true;
						bool flag1 = false;
						while (num1 > 0 && (!this.stopThreadLoop || !flag))
						{
							byte[] numArray = this.dispenser.receiveESSPResponse(out num);
							flag = true;
							if (numArray[4].Equals(Convert.ToByte(Response.Read)))
							{
								if (actionProcessing != null)
								{
									if (threadControl == null || !threadControl.InvokeRequired)
									{
										actionProcessing(2);
									}
									else
									{
										object[] objArray = new object[] { 2 };
										threadControl.BeginInvoke(actionProcessing, objArray);
									}
								}
								int num4 = Convert.ToInt32(numArray[5]);
								if (num4 > 0 && num4 <= (int)this.dispenser.channelValue.Length)
								{
									num2 = this.dispenser.channelValue[num4 - 1];
									if (!this.availableAmount.Contains(num2))
									{
										byte[] numArray1 = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.Reject) };
										this.dispenser.sendESSPCommand(numArray1, out num);
										if (actionProcessing != null)
										{
											if (threadControl == null || !threadControl.InvokeRequired)
											{
												actionProcessing(4);
											}
											else
											{
												object[] objArray1 = new object[] { 4 };
												threadControl.BeginInvoke(actionProcessing, objArray1);
											}
										}
									}
									else if (actionProcessing != null)
									{
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionProcessing(3);
										}
										else
										{
											object[] objArray2 = new object[] { 3 };
											threadControl.BeginInvoke(actionProcessing, objArray2);
										}
									}
									this.logInfo(string.Concat("Cash Machine: Read ", num2.ToString(), ", Total Note Value: ", num3.ToString()), false);
								}
								flag = false;
							}
							else if (numArray[4].Equals(Convert.ToByte(Response.Rejecting)))
							{
								if (actionProcessing != null)
								{
									if (threadControl == null || !threadControl.InvokeRequired)
									{
										actionProcessing(5);
									}
									else
									{
										object[] objArray3 = new object[] { 5 };
										threadControl.BeginInvoke(actionProcessing, objArray3);
									}
								}
								flag = false;
							}
							else if (!numArray[4].Equals(Convert.ToByte(Response.Rejected)))
							{
								if (numArray[4].Equals(Convert.ToByte(Response.PayoutJammed)))
								{
									if (numArray[6] != 0)
									{
										byte[] numArray2 = new byte[] { numArray[6], numArray[7], numArray[8], numArray[9] };
										num2 = Convert.ToDecimal(BitConverter.ToInt32(numArray2, 0)) / new decimal(100);
									}
									if (actionProcessing != null)
									{
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionProcessing(6);
										}
										else
										{
											object[] objArray4 = new object[] { 6 };
											threadControl.BeginInvoke(actionProcessing, objArray4);
										}
									}
									if (actionSuccess != null)
									{
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionSuccess(num1, num2);
										}
										else
										{
											object[] objArray5 = new object[] { num1, num2 };
											threadControl.BeginInvoke(actionSuccess, objArray5);
										}
									}
									object[] objArray6 = new object[] { this._currency, " ", num2, " is jammed in machine." };
									CashDispenserException cashDispenserException = new CashDispenserException(string.Concat(objArray6))
									{
										showDetail = true
									};
									this.logInfo(string.Concat("Cash Machine: Payout Jammed ", num2.ToString(), ", Total Note Value: ", num3.ToString()), true);
									num1--;
									num3 += num2;
									num2 = new decimal(0);
									throw cashDispenserException;
								}
								if (numArray[4].Equals(Convert.ToByte(Response.Credit)))
								{
									int num5 = Convert.ToInt32(numArray[5]);
									if (num5 <= 0 || num5 > (int)this.dispenser.channelValue.Length)
									{
										continue;
									}
									num2 = this.dispenser.channelValue[num5 - 1];
									if (actionSuccess != null)
									{
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionSuccess(num1, num2);
										}
										else
										{
											object[] objArray7 = new object[] { num1, num2 };
											threadControl.BeginInvoke(actionSuccess, objArray7);
										}
									}
									num3 += num2;
									num1--;
									flag = true;
									this.logInfo(string.Concat("Cash Machine: Credit ", num2.ToString(), ", Total Note Value: ", num3.ToString()), false);
								}
								else if (numArray[4].Equals(Convert.ToByte(Response.Disabled)))
								{
									this.dispenser.enableDevice();
									flag = true;
								}
								else if (!numArray[4].Equals(Convert.ToByte(Response.PayoutOutOfService)))
								{
									this.getExceptionByResponse(numArray[3], false);
								}
								else
								{
									this.dispenser.enableSMARTPayout();
								}
							}
							else
							{
								if (actionProcessing != null)
								{
									if (threadControl == null || !threadControl.InvokeRequired)
									{
										actionProcessing(2);
									}
									else
									{
										object[] objArray8 = new object[] { 2 };
										threadControl.BeginInvoke(actionProcessing, objArray8);
									}
								}
								this.logInfo(string.Concat("Cash Machine: Rejected ", num2.ToString(), ", Total Note Value: ", num3.ToString()), false);
								num2 = new decimal(0);
								flag = true;
								this.logInfo(string.Concat("Cash Machine: Rejected ", num2.ToString(), ", Total Note Value: ", num3.ToString()), false);
							}
						}
						lock (CashDispenserManager.lockAction)
						{
							if (!flag1)
							{
								if (actionComplete != null)
								{
									actionComplete(num3);
								}
								flag1 = true;
							}
						}
					}
				}
				catch (CashDispenserException cashDispenserException2)
				{
					CashDispenserException cashDispenserException1 = cashDispenserException2;
					if (exception != null)
					{
						if (threadControl == null || !threadControl.InvokeRequired)
						{
							exception(cashDispenserException1, cashDispenserException1.showDetail);
						}
						else
						{
							object[] objArray9 = new object[] { cashDispenserException1, cashDispenserException1.showDetail };
							threadControl.BeginInvoke(exception, objArray9);
						}
					}
				}
				catch (Exception exception2)
				{
					Exception exception1 = exception2;
					if (exception != null)
					{
						if (threadControl == null || !threadControl.InvokeRequired)
						{
							exception(exception1, false);
						}
						else
						{
							object[] objArray10 = new object[] { exception1, false };
							threadControl.BeginInvoke(exception, objArray10);
						}
					}
				}
			}
			finally
			{
				this.dispenser.disableDevice();
			}
		}

		private void resetStateVariable()
		{
			this.stopThreadLoop = false;
		}

		public void StartDepositThread(Control threadControl, Action<decimal, decimal> actionComplete, Action<int> actionProcessing, Action<decimal, decimal> actionSuccess, Action<Exception, bool> exception, decimal totalAmountDeposit)
		{
			if (threadControl == null)
			{
				throw CashDispenserException.ControlMissing;
			}
			if (actionSuccess != null)
			{
				Thread thread = new Thread(() => this.Deposit(threadControl, actionComplete, actionProcessing, actionSuccess, exception, totalAmountDeposit))
				{
					IsBackground = true
				};
				thread.Start();
			}
		}

		public void StartEmptyThread(Control threadControl, Action<decimal> actionComplete, Action<decimal> actionUpdate, Action<int> actionProgress, Action<Exception, bool> exception)
		{
			if (threadControl == null)
			{
				throw CashDispenserException.ControlMissing;
			}
			Thread thread = new Thread(() => this.Empty(threadControl, actionComplete, actionUpdate, actionProgress, exception))
			{
				IsBackground = true
			};
			thread.Start();
		}

		public void StartRefillThread(Control threadControl, Action<decimal> actionComplete, Action<int> actionProcessing, Action<int, decimal> actionSuccess, Action<Exception, bool> exception, int numberOfCashNote)
		{
			if (threadControl == null)
			{
				throw CashDispenserException.ControlMissing;
			}
			if (actionSuccess != null)
			{
				Thread thread = new Thread(() => this.Refill(threadControl, actionComplete, actionProcessing, actionSuccess, exception, numberOfCashNote))
				{
					IsBackground = true
				};
				thread.Start();
			}
		}

		public void StartWithdrawThread(Control threadControl, Action<decimal> actionComplete, Action<int> actionProcessing, Action<decimal> actionSuccess, Action<Exception, bool> exception, decimal amountToWithdraw)
		{
			if (threadControl == null)
			{
				throw CashDispenserException.ControlMissing;
			}
			if (actionSuccess != null)
			{
				Thread thread = new Thread(() => this.Withdraw(threadControl, actionComplete, actionProcessing, actionSuccess, exception, amountToWithdraw))
				{
					IsBackground = true
				};
				thread.Start();
			}
		}

		public void StopThread()
		{
			this.stopThreadLoop = true;
		}

		public void Withdraw(Control threadControl, Action<decimal> actionComplete, Action<int> actionProcessing, Action<decimal> actionSuccess, Action<Exception, bool> exception, decimal amountToWithdraw)
		{
			int num;
			try
			{
				bool flag = false;
				if (amountToWithdraw != new decimal(0))
				{
					if (amountToWithdraw < new decimal(0))
					{
						throw new CashDispenserException(string.Concat("Cannot withdraw amount ", amountToWithdraw, " , please check the cash note in cash acceptor."));
					}
					if (this.dispenser.enableDevice() && this.dispenser.enableSMARTPayout())
					{
						this.resetStateVariable();
						this.dispenser.isEncypted = true;
						bool flag1 = false;
						while (!this.stopThreadLoop && !flag1)
						{
							byte[] numArray = new byte[] { Convert.ToByte(MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200.Command.PayoutAmount), 0, 0, 0, 0, 0, 0, 0, 0 };
							byte[] bytes = BitConverter.GetBytes(Convert.ToInt32(amountToWithdraw * new decimal(100)));
							numArray[1] = bytes[0];
							numArray[2] = bytes[1];
							numArray[3] = bytes[2];
							numArray[4] = bytes[3];
							numArray[5] = Convert.ToByte(this._currency[0]);
							numArray[6] = Convert.ToByte(this._currency[1]);
							numArray[7] = Convert.ToByte(this._currency[2]);
							numArray[8] = 25;
							byte[] numArray1 = this.dispenser.sendESSPCommand(numArray, out num);
							if (!this.checkMachineStatus(numArray1))
							{
								continue;
							}
							numArray[8] = 88;
							this.dispenser.sendESSPCommand(numArray, out num);
							decimal num1 = new decimal(0);
							while (!flag1)
							{
								byte[] numArray2 = this.dispenser.receiveESSPResponse(out num);
								int num2 = 4;
								while (num2 < num - 2)
								{
									if (numArray2[num2].Equals(Convert.ToByte(Response.Dispensing)))
									{
										num2++;
										int num3 = num2;
										num2 = num3 + 1;
										if (numArray2[num3] == 0)
										{
											continue;
										}
										int num4 = num2;
										num2 = num4 + 1;
										bytes[0] = numArray2[num4];
										int num5 = num2;
										num2 = num5 + 1;
										bytes[1] = numArray2[num5];
										int num6 = num2;
										num2 = num6 + 1;
										bytes[2] = numArray2[num6];
										int num7 = num2;
										num2 = num7 + 1;
										bytes[3] = numArray2[num7];
										decimal num8 = Convert.ToDecimal(BitConverter.ToInt32(bytes, 0)) / new decimal(100);
										num1 = num8;
										num2++;
										if (actionProcessing != null)
										{
											if (threadControl == null || !threadControl.InvokeRequired)
											{
												actionProcessing(7);
											}
											else
											{
												object[] objArray = new object[] { 7 };
												threadControl.BeginInvoke(actionProcessing, objArray);
											}
										}
										if (!(num8 != new decimal(0)) || actionSuccess == null)
										{
											continue;
										}
										if (threadControl == null || !threadControl.InvokeRequired)
										{
											actionSuccess(num8);
										}
										else
										{
											object[] objArray1 = new object[] { num8 };
											threadControl.BeginInvoke(actionSuccess, objArray1);
										}
									}
									else if (numArray2[num2].Equals(Convert.ToByte(Response.Dispensed)))
									{
										num2++;
										int num9 = num2;
										num2 = num9 + 1;
										if (numArray2[num9] == 0)
										{
											continue;
										}
										int num10 = num2;
										num2 = num10 + 1;
										bytes[0] = numArray2[num10];
										int num11 = num2;
										num2 = num11 + 1;
										bytes[1] = numArray2[num11];
										int num12 = num2;
										num2 = num12 + 1;
										bytes[2] = numArray2[num12];
										int num13 = num2;
										num2 = num13 + 1;
										bytes[3] = numArray2[num13];
										decimal num14 = Convert.ToDecimal(BitConverter.ToInt32(bytes, 0)) / new decimal(100);
										num1 = num14;
										num2++;
										if (actionProcessing != null)
										{
											if (threadControl == null || !threadControl.InvokeRequired)
											{
												actionProcessing(8);
											}
											else
											{
												object[] objArray2 = new object[] { 8 };
												threadControl.BeginInvoke(actionProcessing, objArray2);
											}
										}
										if (num14 != new decimal(0))
										{
											lock (CashDispenserManager.lockAction)
											{
												if (!flag)
												{
													if (actionComplete != null)
													{
														actionComplete(num14);
													}
													flag = true;
												}
											}
										}
										flag1 = true;
									}
									else if (numArray2[num2].Equals(Convert.ToByte(Response.PayoutJammed)))
									{
										num2++;
										int num15 = num2;
										num2 = num15 + 1;
										if (numArray2[num15] == 0)
										{
											continue;
										}
										int num16 = num2;
										num2 = num16 + 1;
										bytes[0] = numArray2[num16];
										int num17 = num2;
										num2 = num17 + 1;
										bytes[1] = numArray2[num17];
										int num18 = num2;
										num2 = num18 + 1;
										bytes[2] = numArray2[num18];
										int num19 = num2;
										num2 = num19 + 1;
										bytes[3] = numArray2[num19];
										decimal num20 = Convert.ToDecimal(BitConverter.ToInt32(bytes, 0)) / new decimal(100);
										num2++;
										if (actionProcessing != null)
										{
											if (threadControl == null || !threadControl.InvokeRequired)
											{
												actionProcessing(6);
											}
											else
											{
												object[] objArray3 = new object[] { 6 };
												threadControl.BeginInvoke(actionProcessing, objArray3);
											}
										}
										if (actionSuccess != null)
										{
											if (threadControl == null || !threadControl.InvokeRequired)
											{
												actionSuccess(num20);
											}
											else
											{
												object[] objArray4 = new object[] { num20 };
												threadControl.BeginInvoke(actionSuccess, objArray4);
											}
										}
										object[] objArray5 = new object[] { this._currency, " ", num20, " is jammed in machine." };
										CashDispenserException cashDispenserException = new CashDispenserException(string.Concat(objArray5))
										{
											showDetail = true
										};
										throw cashDispenserException;
									}
									else if (numArray2[num2].Equals(Convert.ToByte(Response.Disabled)))
									{
										bytes = BitConverter.GetBytes(Convert.ToInt32((amountToWithdraw - num1) * new decimal(100)));
										numArray[1] = bytes[0];
										numArray[2] = bytes[1];
										numArray[3] = bytes[2];
										numArray[4] = bytes[3];
										this.dispenser.sendESSPCommand(numArray, out num);
										num2++;
									}
									else if (!numArray2[num2].Equals(Convert.ToByte(Response.PayoutOutOfService)))
									{
										this.getExceptionByResponse(numArray1[3], true);
										num2++;
									}
									else
									{
										this.dispenser.enableSMARTPayout();
										num2++;
									}
								}
							}
						}
					}
				}
				else
				{
					lock (CashDispenserManager.lockAction)
					{
						if (!flag)
						{
							if (actionComplete != null)
							{
								actionComplete(amountToWithdraw);
							}
							flag = true;
						}
					}
				}
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (threadControl == null || !threadControl.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						object[] objArray6 = new object[] { exception1, false };
						threadControl.BeginInvoke(exception, objArray6);
					}
				}
			}
		}
	}
}