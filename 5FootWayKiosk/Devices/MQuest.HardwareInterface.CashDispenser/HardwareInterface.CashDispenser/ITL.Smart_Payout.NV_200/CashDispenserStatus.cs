using System;

namespace MQuest.HardwareInterface.CashDispenser.ITL.Smart_Payout.NV_200
{
	public enum CashDispenserStatus
	{
		Initialzing = 0,
		Ready_To_Use = 1,
		Ready_To_Insert = 2,
		Accepting = 3,
		Rejecting = 4,
		Rejected = 5,
		Jammed = 6,
		Dispensing = 7,
		Dispensed = 8,
		Emptying = 10,
		Emptyed = 11
	}
}