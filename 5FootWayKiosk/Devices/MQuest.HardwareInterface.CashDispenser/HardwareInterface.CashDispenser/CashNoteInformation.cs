using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CashDispenser
{
	public class CashNoteInformation
	{
		public int cashNoteStoredCount
		{
			get;
			set;
		}

		public decimal cashNoteValue
		{
			get;
			set;
		}

		public string currency
		{
			get;
			set;
		}

		public CashNoteInformation()
		{
		}
	}
}