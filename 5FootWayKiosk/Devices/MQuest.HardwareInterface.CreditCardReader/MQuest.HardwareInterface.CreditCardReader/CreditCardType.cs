using System;

namespace MQuest.HardwareInterface.CreditCardReader
{
	public enum CreditCardType
	{
		AMERICAN_EXPRESS = 65,
		CUP = 67,
		DINERS = 68,
		EZ_LINK = 69,
		STAFF_DISCOUNT = 70,
		JCB = 74,
		MASTERCARD = 77,
		NETS_FLASHPAY = 78,
		NETS_PIN_DEBIT = 83,
		VISA = 86
	}
}