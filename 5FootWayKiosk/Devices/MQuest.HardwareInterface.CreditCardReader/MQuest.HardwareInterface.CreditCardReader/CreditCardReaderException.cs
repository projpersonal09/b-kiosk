using System;

namespace MQuest.HardwareInterface.CreditCardReader
{
	public class CreditCardReaderException : Exception
	{
		public static CreditCardReaderException ControlMissing;

		public static CreditCardReaderException unknownError;

		public static CreditCardReaderException Timeout;

		public static CreditCardReaderException DataNotAccepted;

		public static CreditCardReaderException TerminalNotFound;

		public static CreditCardReaderException CommandTooLong;

		public static CreditCardReaderException InvalidData;

		public static CreditCardReaderException PortUsed;

		public static CreditCardReaderException PortNotFound;

		static CreditCardReaderException()
		{
			CreditCardReaderException.ControlMissing = new CreditCardReaderException("Control cannot be null");
			CreditCardReaderException.unknownError = new CreditCardReaderException("Unconfigure port error is occur.");
			CreditCardReaderException.Timeout = new CreditCardReaderException("Timeout.");
			CreditCardReaderException.DataNotAccepted = new CreditCardReaderException("Data is not accepted by terminal, invalid command.");
			CreditCardReaderException.TerminalNotFound = new CreditCardReaderException("Terminal cannot be reach, invalid COM port or cable not in good condition.");
			CreditCardReaderException.CommandTooLong = new CreditCardReaderException("Command Length is too long, maximum length is 1024Bytes.");
			CreditCardReaderException.InvalidData = new CreditCardReaderException("Invalid Data from terminal, update terminal or contract manufacturer.");
			CreditCardReaderException.PortUsed = new CreditCardReaderException("COM port is used by other device, please ensure this is the correct COM port.");
			CreditCardReaderException.PortNotFound = new CreditCardReaderException("COM port not found, please ensure the port is exist.");
		}

		public CreditCardReaderException()
		{
		}

		public CreditCardReaderException(string message) : base(message)
		{
		}

		public CreditCardReaderException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}