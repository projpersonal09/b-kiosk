using System;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class PaymentCommandHeader
	{
		public const string Credit_Card_COMMAND = "C";

		public const string NETS_COMMAND = "N";

		public const string MIFARE_COMMAND = "M";

		public PaymentCommandHeader()
		{
		}
	}
}