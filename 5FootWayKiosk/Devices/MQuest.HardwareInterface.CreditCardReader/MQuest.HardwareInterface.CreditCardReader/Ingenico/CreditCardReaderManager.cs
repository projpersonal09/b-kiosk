using MQuest.HardwareInterface.CreditCardReader;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class CreditCardReaderManager : IDisposable
	{
		public Control control;

		public bool cancel;

		private MQuest.HardwareInterface.CreditCardReader.Ingenico.CreditCardReader reader;

		public bool stopThreadLoop
		{
			get;
			set;
		}

		public CreditCardReaderManager(Control control)
		{
			this.cancel = false;
			this.control = control;
		}

		public void Dispose()
		{
			try
			{
				this.stopThreadLoop = true;
				this.reader.SendCancel();
				Thread.Sleep(100);
			}
			catch
			{
				throw;
			}
		}

		public void InitCreditCardReader(int comPortNumber)
		{
			try
			{
				this.reader = new MQuest.HardwareInterface.CreditCardReader.Ingenico.CreditCardReader(comPortNumber);
				this.reader.Test();
			}
			catch
			{
				throw;
			}
		}

		private void Result(Action<TransactionDetail, string> actionSuccess, Action<string, string> actionCallback, Action<Exception, bool> exception, string paymentType)
		{
			string str;
			try
			{
				this.stopThreadLoop = false;
				int num = 0;
				string empty = string.Empty;
				do
				{
					try
					{
						empty = this.reader.RecieveResponse(1000);
						if (empty.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.Message_Callback))))
						{
							if (this.reader.getTagValue(empty, Convert.ToInt32(PaymentTag.Message), out str) == 0)
							{
								string str1 = str.Substring(2, str.Length - 2);
								string str2 = str.Substring(0, 2);
								if (!this.control.InvokeRequired)
								{
									actionCallback(str2, str1);
								}
								else
								{
									Control control = this.control;
									object[] objArray = new object[] { str2, str1 };
									control.BeginInvoke(actionCallback, objArray);
								}
							}
							num = 0;
						}
						else if (empty.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.Batch_Total_Callback))) || empty.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.Card_Total_Callback))) || empty.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.NETS_Total_Callback))))
						{
							if (!this.control.InvokeRequired)
							{
								actionCallback("", empty);
							}
							else
							{
								Control control1 = this.control;
								object[] objArray1 = new object[] { "", empty };
								control1.BeginInvoke(actionCallback, objArray1);
							}
							num = 0;
						}
						else
						{
							num++;
							break;
						}
					}
					catch (CreditCardReaderException creditCardReaderException)
					{
						if (creditCardReaderException != CreditCardReaderException.Timeout)
						{
							break;
						}
						else if (num >= 90)
						{
							break;
						}
						else if (this.cancel)
						{
							this.reader.SendCancel();
							this.cancel = false;
						}
					}
					catch (Exception exception2)
					{
						Exception exception1 = exception2;
						if (exception != null)
						{
							if (!this.control.InvokeRequired)
							{
								exception(exception1, false);
							}
							else
							{
								Control control2 = this.control;
								object[] objArray2 = new object[] { exception1, false };
								control2.BeginInvoke(exception, objArray2);
							}
						}
						break;
					}
				}
				while (!this.stopThreadLoop);
				if (!string.IsNullOrEmpty(empty) && actionSuccess != null)
				{
					if (!this.control.InvokeRequired)
					{
						actionSuccess(this.reader.parseResult(empty), paymentType);
					}
					else
					{
						Control control3 = this.control;
						object[] objArray3 = new object[] { this.reader.parseResult(empty), paymentType };
						control3.BeginInvoke(actionSuccess, objArray3);
					}
				}
			}
			catch (Exception exception4)
			{
				Exception exception3 = exception4;
				if (exception != null)
				{
					if (!this.control.InvokeRequired)
					{
						exception(exception3, false);
					}
					else
					{
						Control control4 = this.control;
						object[] objArray4 = new object[] { exception3, false };
						control4.BeginInvoke(exception, objArray4);
					}
				}
			}
		}

		public void StartTransactionThread(Action<TransactionDetail, string> actionSuccess, Action<string, string> actionCallback, Action<Exception, bool> exception, int commandIdentifier, string paymentType, int paymentCode, string paymentContent)
		{
			Thread thread = new Thread(() => this.Transaction(actionSuccess, actionCallback, exception, commandIdentifier, paymentType, paymentCode, paymentContent))
			{
				IsBackground = true
			};
			thread.Start();
		}

		private void Transaction(Action<TransactionDetail, string> actionSuccess, Action<string, string> actionCallback, Action<Exception, bool> exception, int commandIdentifier, string paymentType, int paymentCode, string paymentContent)
		{
			try
			{
				string str = string.Concat(string.Empty, paymentType);
				str = string.Concat(str, paymentCode);
				str = string.Concat(str, paymentContent);
				this.reader.SendCommand(str);
				this.Result(actionSuccess, actionCallback, exception, paymentType);
			}
			catch (Exception exception2)
			{
				Exception exception1 = exception2;
				if (exception != null)
				{
					if (!this.control.InvokeRequired)
					{
						exception(exception1, false);
					}
					else
					{
						Control control = this.control;
						object[] objArray = new object[] { exception1, false };
						control.BeginInvoke(exception, objArray);
					}
				}
			}
		}
	}
}