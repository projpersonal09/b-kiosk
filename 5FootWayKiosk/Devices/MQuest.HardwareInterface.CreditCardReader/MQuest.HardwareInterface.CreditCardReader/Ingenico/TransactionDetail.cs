using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class TransactionDetail
	{
		public string approvalCode
		{
			get;
			set;
		}

		public string batch
		{
			get;
			set;
		}

		public string cardHolderName
		{
			get;
			set;
		}

		public string cardLabel
		{
			get;
			set;
		}

		public string cardNumber
		{
			get;
			set;
		}

		public EMVData EMV
		{
			get;
			set;
		}

		public string entryMode
		{
			get;
			set;
		}

		public string host
		{
			get;
			set;
		}

		public string invoice
		{
			get;
			set;
		}

		public string mechantId
		{
			get;
			set;
		}

		public string paymentCommand
		{
			get;
			set;
		}

		public MQuest.HardwareInterface.CreditCardReader.Ingenico.PrintingFlag PrintingFlag
		{
			get;
			set;
		}

		public string responseCode
		{
			get;
			set;
		}

		public string retrievalReference
		{
			get;
			set;
		}

		public int runningNumber
		{
			get;
			set;
		}

		public string status
		{
			get;
			set;
		}

		public string terminalId
		{
			get;
			set;
		}

		public string terminalInfo
		{
			get;
			set;
		}

		public string title
		{
			get;
			set;
		}

		public decimal transactionAmount
		{
			get;
			set;
		}

		public DateTime transactionDateTime
		{
			get;
			set;
		}

		public char type
		{
			get;
			set;
		}

		public string uniqueTraceReference
		{
			get;
			set;
		}

		public TransactionDetail()
		{
			this.EMV = new EMVData();
			this.PrintingFlag = new MQuest.HardwareInterface.CreditCardReader.Ingenico.PrintingFlag();
		}
	}
}