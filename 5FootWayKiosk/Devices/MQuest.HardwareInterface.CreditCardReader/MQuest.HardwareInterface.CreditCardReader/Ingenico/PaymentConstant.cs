using System;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class PaymentConstant
	{
		public PaymentConstant()
		{
		}

		public class EntryMode
		{
			public const char manualEntryTag = 'E';

			public const string manualEntry = "MANUAL ENTRY";

			public const char magstripeTag = 'M';

			public const string magstripe = "MAGSTRIPE";

			public const char fallBackTag = 'F';

			public const string fallBack = "FALLBACK";

			public const char chipTag = 'C';

			public const string chip = "CHIP";

			public const char contactlessTag = 'P';

			public const string contactless = "CONTACTLESS";

			public EntryMode()
			{
			}
		}

		public class ResponseCode
		{
			public const string approved = "00";

			public const string cardIssue = "01";

			public const string cardIssue_SpecialCondition = "02";

			public const string errorCallHelpSN = "03";

			public const string doNotHonour = "05";

			public const string invalidTransaction = "12";

			public const string invalidAmount = "13";

			public const string invalidCardNumber = "14";

			public const string reEnterTransaction = "19";

			public const string unnableToLocateRecordOnFile = "25";

			public const string formatError = "30";

			public const string bankNotSupportedBySwitch = "31";

			public const string lostCard = "41";

			public const string stolenCard = "43";

			public const string transactionDecline = "51";

			public const string expiredCard = "54";

			public const string incorrectPin = "55";

			public const string transactionNotPermitted = "56";

			public const string invalidProductCode = "76";

			public const string reconcileError = "77";

			public const string traceNotFound = "78";

			public const string badTerminalId = "89";

			public const string issueSwitchInoperative = "91";

			public const string duplicateTransmission = "94";

			public const string systemMalfunction = "96";

			public const string terminalFull = "SE";

			public const string pinEntryError = "PE";

			public const string invalidCard = "IC";

			public const string cardExpired = "EC";

			public ResponseCode()
			{
			}
		}

		public class Status
		{
			public const string approved = "APPROVED";

			public const string failed = "FAILED";

			public Status()
			{
			}
		}

		public class Title
		{
			public const string sale = "SALE";

			public const string preAuth = "PREAUTH";

			public const string offline = "OFFLINE";

			public const string refund = "REFUND";

			public const string tranasction = "TRANSACTION";

			public const string void_payment = "VOID";

			public const string action = "ACTION";

			public Title()
			{
			}
		}
	}
}