using System;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class ResponseMessage
	{
		public ResponseMessage()
		{
		}

		public string parseErrorCode(string paymentType, string errorCode, out bool retryable)
		{
			retryable = false;
			string empty = string.Empty;
			string str = paymentType;
			string str1 = str;
			if (str == null)
			{
				empty = "UNKNOWN PAYMENT TYPE";
				return empty;
			}
			else if (str1 == "C")
			{
				string str2 = errorCode;
				string str3 = str2;
				if (str2 != null)
				{
					switch (str3)
					{
						case "00":
						{
							empty = "APPROVED";
							return empty;
						}
						case "01":
						{
							empty = "REFER TO CARD ISSUER, PLEASE CALL BANK";
							return empty;
						}
						case "02":
						{
							empty = "REFER TO CARD ISSUER’S SPECIAL CONDITION, PLEASE CALL BANK";
							return empty;
						}
						case "03":
						{
							empty = "ERROR CALL HELP SN, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "05":
						{
							empty = "DO NOT HONOUR, PLEASE CALL BANK";
							return empty;
						}
						case "12":
						{
							empty = "INVALID TRANSACTION (HELP TR), PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "13":
						{
							empty = "INVALID AMOUNT (HELP AM), PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "14":
						{
							empty = "INVALID CARD NUMBER (HELP RE), PLEASE CHECK CARD";
							retryable = true;
							return empty;
						}
						case "19":
						{
							empty = "PLEASE RE-ENTER TRANSACTION";
							retryable = true;
							return empty;
						}
						case "25":
						{
							empty = "UNABLE TO LOCATE RECORD ON FILE (HELP NT), PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "30":
						{
							empty = "FORMAT ERROR (HELP FE), PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "31":
						{
							empty = "BANK NOT SUPPORTED BY SWITCH (HELP NS), PLEASE CALL BANK";
							return empty;
						}
						case "41":
						{
							empty = "LOST CARD, PLEASE CALL BANK";
							return empty;
						}
						case "43":
						{
							empty = "STOLEN CARD PICK UP, PLEASE CALL BANK";
							return empty;
						}
						case "51":
						{
							empty = "TRANSACTION DECLINED, PLEASE CALL BANK";
							return empty;
						}
						case "54":
						{
							empty = "EXPIRED CARD, PLEASE CHECK CARD EXPIRY OR USE DIFFERENT CARD";
							retryable = true;
							return empty;
						}
						case "55":
						{
							empty = "INCORRECT PIN, PLEASE REENTER PIN";
							retryable = true;
							return empty;
						}
						case "58":
						{
							empty = "TRANSACTION NOT PERMITTED IN TERMINAL, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "76":
						{
							empty = "INVALID PRODUCT CODES (HELP DC), PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "77":
						{
							empty = "RECONCILE ERROR, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "78":
						{
							empty = "TRACE# NOT FOUND, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "89":
						{
							empty = "BAD TERMINAL ID, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "91":
						{
							empty = "ISSUER/SWITCH INOPERATIVE, PLEASE CALL BANK";
							return empty;
						}
						case "94":
						{
							empty = "DUPLICATE TRANSMISSION, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "96":
						{
							empty = "SYSTEM MALFUNCTION, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "SE":
						{
							empty = "TERMINAL FULL, PLEASE INITIATE SETTLEMENT";
							return empty;
						}
						case "PE":
						{
							empty = "PIN ENTRY ERROR, PLEASE REDO TRANSACTION";
							retryable = true;
							return empty;
						}
						case "IC":
						{
							empty = "INVALID CARD, PLEASE CARD IS NOT SUPPORTED BY THIS TERMINAL, USE A DIFFERENT CARD";
							retryable = true;
							return empty;
						}
						case "EC":
						{
							empty = "CARD IS EXPIRED, PLEASE USE A DIFFERENT CARD";
							retryable = true;
							return empty;
						}
						case "CE":
						{
							empty = "CONNECTION ERROR, PLEASE RETRY";
							retryable = true;
							return empty;
						}
						case "RE":
						{
							empty = "RECORD NOT FOUND, PLEASE CHECK INVOICE NUMBER / TRACE";
							retryable = true;
							return empty;
						}
						case "HE":
						{
							empty = "WRONG HOST NUMBER PROVIDED, PLEASE CHECK HOST";
							return empty;
						}
						case "LE":
						{
							empty = "LINE ERROR, PLEASE CHECK PHONE LINE";
							retryable = true;
							return empty;
						}
						case "VB":
						{
							empty = "TRANSACTION ALREADY VOIDED";
							return empty;
						}
						case "FE":
						{
							empty = "FILE EMPTY / NO TRANSACTION TO VOID";
							return empty;
						}
						case "WC":
						{
							empty = "CARD NUMBER DOES NOT MATCH, PELASE CHECK CARD";
							retryable = true;
							return empty;
						}
						case "TA":
						{
							empty = "TRANSACTION ABORTED BY CUSTOMER";
							return empty;
						}
						case "AE":
						{
							empty = "AMOUNT DID NOT MATCH, PLEASE CHECK AMOUNT";
							retryable = true;
							return empty;
						}
						case "XX":
						{
							empty = "TERMINAL NOT PROPERLY SETUP, PLEASE CONTACT TERMINAL VENDOR";
							return empty;
						}
						case "DL":
						{
							empty = "LOGON NOT DONE, PLEASE DO LOGON";
							return empty;
						}
						case "BT":
						{
							empty = "BAD TLV COMMAND FORMAT, PLEASE CHECK YOUR COMMAND PACKET FORMAT";
							return empty;
						}
						case "IS":
						{
							empty = "TRANSACTION NOT FOUND, INQUIRY SUCCESSFUL, YOU MAY CONTINUE TO START A NEW TRANSACTION";
							return empty;
						}
						case "CD":
						{
							empty = "CARD DECLINED TRANSACTION, PLEASE TRY AGAIN, TRY NOT TO REMOVE CARD FROM TERMINAL WHILE TRANSACTION IS PROCESSING. PLEASE PROMPT CUSTOMER TO ENTER PIN IF REQUIRED.";
							retryable = true;
							return empty;
						}
						case "LH":
						{
							empty = "LOYALTY HOST IS TEMPORARY OFFLINE, REBATES WILL ONLY BE ISSUED THE FOLLOWING DAY. CUSTOMER CANNOT REDEEM FOR NOW.";
							return empty;
						}
						case "IN":
						{
							empty = "INVALID CARD, EZLINK CARD IS REFUNDED or BLACKLISTED";
							retryable = true;
							return empty;
						}
						case "CO":
						{
							empty = "CARD NOT READ PROPERLY TRY AGAIN";
							retryable = true;
							return empty;
						}
						case "TL":
						{
							empty = "TOP UP LIMIT EXCEEDED";
							return empty;
						}
						case "PL":
						{
							empty = "PAYMENT LIMIT EXCEEDED";
							return empty;
						}
					}
				}
				empty = "UNKNOWN ERROR";
			}
			else
			{
				if (str1 != "M")
				{
					empty = "UNKNOWN PAYMENT TYPE";
					return empty;
				}
				empty = "CARD NOT READ PROPERLY TRY AGAIN";
			}
			return empty;
		}
	}
}