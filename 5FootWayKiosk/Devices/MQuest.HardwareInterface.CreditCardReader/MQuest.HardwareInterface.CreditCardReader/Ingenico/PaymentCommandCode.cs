using System;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public enum PaymentCommandCode
	{
		NETS_Logon = 101,
		CreditCard_Sales = 200,
		NETS_Sales = 200,
		CreditCard_PreAuth = 201,
		CreditCard_PreAuth_Capture_or_Offline = 202,
		CreditCard_Refund = 203,
		CreditCard_CashAdvance = 204,
		CreditCard_CardData = 209,
		MIFARE_Read_Card_UID = 209,
		CreditCard_Void = 300,
		NETS_Reversal = 300,
		CreditCard_PreAuth_Cancellation = 301,
		CreditCard_INQUIRY = 400,
		NETS_Inquiry = 400,
		Tip_Adjustment = 500,
		Sales_ECRPE = 600,
		PreAuth_ECRPE = 601,
		PreAuth_Capture_or_Offline_ECRPE = 602,
		Refund_ECPRE = 603,
		CEPAS_Sales = 610,
		NETS_Contactless_Sales = 610,
		CEPAS_Topup = 613,
		CEPAS_Balance = 618,
		NETS_Contactless_Balance = 618,
		EZLink_E_TS_Sales = 623,
		NETS_Contactless_Sales_Tap = 630,
		CEPAS_Abort = 699,
		NETS_Settlement = 700,
		Settlement = 700,
		EZLink_Settlement = 710,
		Echo_Test = 902,
		NETS_Status = 904,
		Card_Data = 922
	}
}