using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class PrintingFlag
	{
		public bool Disclaimer1
		{
			get;
			set;
		}

		public bool Disclaimer2
		{
			get;
			set;
		}

		public bool Disclaimer3
		{
			get;
			set;
		}

		public bool Disclaimer4
		{
			get;
			set;
		}

		public bool Disclaimer5
		{
			get;
			set;
		}

		public bool Disclaimer6
		{
			get;
			set;
		}

		public bool Disclaimer7
		{
			get;
			set;
		}

		public bool Signature
		{
			get;
			set;
		}

		public int? UnmaskFirstDigit
		{
			get;
			set;
		}

		public int? UnmaskLastDigit
		{
			get;
			set;
		}

		public PrintingFlag()
		{
			this.Signature = false;
			this.Disclaimer1 = false;
			this.Disclaimer2 = false;
			this.Disclaimer3 = false;
			this.Disclaimer4 = false;
			this.Disclaimer5 = false;
			this.Disclaimer6 = false;
			this.Disclaimer7 = false;
		}
	}
}