using MQuest.HardwareInterface.CreditCardReader;
using System;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class TagGenerator
	{
		public TagGenerator()
		{
		}

		public int getFieldLength(PaymentTag tag, string value)
		{
			int num = Convert.ToInt32(tag);
			switch (num)
			{
				case 2:
				case 8:
				{
					return value.Length;
				}
				case 3:
				case 5:
				case 6:
				{
					throw new CreditCardReaderException("Undefined Tag");
				}
				case 4:
				{
					return 12;
				}
				case 7:
				{
					return 10;
				}
				default:
				{
					switch (num)
					{
						case 14:
						case 83:
						{
							return 4;
						}
						case 15:
						case 16:
						case 17:
						case 18:
						case 19:
						case 20:
						case 21:
						case 23:
						case 24:
						case 25:
						case 26:
						case 27:
						case 28:
						case 29:
						case 30:
						case 31:
						case 32:
						case 33:
						case 34:
						case 36:
						case 40:
						case 43:
						case 44:
						case 45:
						case 46:
						case 47:
						case 48:
						case 49:
						case 50:
						case 51:
						case 67:
						case 70:
						case 71:
						case 78:
						{
							throw new CreditCardReaderException("Undefined Tag");
						}
						case 22:
						case 38:
						case 55:
						case 57:
						case 64:
						case 68:
						case 74:
						case 75:
						case 76:
						case 77:
						case 81:
						case 82:
						case 84:
						{
							return 6;
						}
						case 35:
						case 42:
						case 52:
						case 53:
						case 54:
						case 58:
						case 59:
						case 61:
						case 63:
						case 65:
						case 66:
						case 69:
						case 79:
						case 80:
						{
							return value.Length;
						}
						case 37:
						{
							return 12;
						}
						case 39:
						case 62:
						case 72:
						case 85:
						{
							return 2;
						}
						case 41:
						{
							return 8;
						}
						case 56:
						{
							return 1;
						}
						case 60:
						case 73:
						{
							return 20;
						}
						default:
						{
							switch (num)
							{
								case 96:
								case 97:
								case 98:
								case 99:
								{
									return value.Length;
								}
								default:
								{
									throw new CreditCardReaderException("Undefined Tag");
								}
							}
							break;
						}
					}
					break;
				}
			}
		}

		public string getTagLengthValue(PaymentTag tag, string value)
		{
			int fieldLength = this.getFieldLength(tag, value);
			int num = Convert.ToInt32(tag);
			return string.Concat(num.ToString().PadLeft(2, '0'), fieldLength.ToString().PadLeft(2, '0'), value.ToString().PadLeft(fieldLength, '0'));
		}
	}
}