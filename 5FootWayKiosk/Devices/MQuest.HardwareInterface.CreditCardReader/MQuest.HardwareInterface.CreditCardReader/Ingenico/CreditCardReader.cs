using Helper;
using MagIC3DBSECRLib;
using MQuest.HardwareInterface.CreditCardReader;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class CreditCardReader
	{
		public const int Default_Wait_Timeout = 90000;

		public const int Default_Receive_Timeout = 1000;

		public const string Default_Receive_Header = "R";

		public string portName;

		public CreditCardReader(int portNumber)
		{
			this.portName = string.Concat("COM", portNumber);
		}

		private bool _parseBool(char value)
		{
			switch (value)
			{
				case '0':
				{
					return false;
				}
				case '1':
				{
					return true;
				}
			}
			return false;
		}

		private void _parseMaskedCardNumber(TransactionDetail transaction)
		{
			int? nullable;
			int? nullable1;
			StringBuilder stringBuilder = new StringBuilder();
			string str = transaction.cardNumber;
			char chr = 'X';
			for (int i = 0; i < str.Length; i++)
			{
				stringBuilder.Append(chr);
			}
			if (transaction.PrintingFlag.UnmaskFirstDigit.HasValue)
			{
				int num = 0;
				while (true)
				{
					int num1 = num;
					int? unmaskFirstDigit = transaction.PrintingFlag.UnmaskFirstDigit;
					if ((num1 >= unmaskFirstDigit.GetValueOrDefault() ? true : !unmaskFirstDigit.HasValue))
					{
						break;
					}
					stringBuilder[num] = str[num];
					num++;
				}
			}
			if (transaction.PrintingFlag.UnmaskLastDigit.HasValue)
			{
				int length = str.Length - 1;
				while (true)
				{
					int num2 = length;
					int length1 = str.Length;
					int? unmaskLastDigit = transaction.PrintingFlag.UnmaskLastDigit;
					if (unmaskLastDigit.HasValue)
					{
						nullable = new int?(length1 - unmaskLastDigit.GetValueOrDefault());
					}
					else
					{
						nullable = null;
					}
					int? nullable2 = nullable;
					if (nullable2.HasValue)
					{
						nullable1 = new int?(nullable2.GetValueOrDefault() - 1);
					}
					else
					{
						nullable1 = null;
					}
					int? nullable3 = nullable1;
					if ((num2 <= nullable3.GetValueOrDefault() ? true : !nullable3.HasValue))
					{
						break;
					}
					stringBuilder[length] = str[length];
					length--;
				}
			}
			transaction.cardNumber = stringBuilder.ToString();
		}

		private void _parsePrintingFlag(TransactionDetail transaction, string printingFlag)
		{
			transaction.PrintingFlag = new PrintingFlag();
			if (string.IsNullOrEmpty(printingFlag))
			{
				return;
			}
			int num = 0;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Signature = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer1 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer2 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer3 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer4 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer5 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer6 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			transaction.PrintingFlag.Disclaimer7 = this._parseBool(printingFlag[num]);
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			PrintingFlag nullable = transaction.PrintingFlag;
			char chr = printingFlag[num];
			nullable.UnmaskFirstDigit = new int?(Convert.ToInt32(chr.ToString()));
			num++;
			if (printingFlag.Length == num)
			{
				return;
			}
			PrintingFlag nullable1 = transaction.PrintingFlag;
			char chr1 = printingFlag[num];
			nullable1.UnmaskLastDigit = new int?(Convert.ToInt32(chr1.ToString()));
			this._parseMaskedCardNumber(transaction);
		}

		public void getExceptionByPortStat(int portStatus)
		{
			int num = portStatus;
			if (num <= 13)
			{
				if (num == 2)
				{
					throw CreditCardReaderException.PortNotFound;
				}
				if (num == 5)
				{
					throw CreditCardReaderException.PortUsed;
				}
				if (num == 13)
				{
					throw CreditCardReaderException.InvalidData;
				}
			}
			else if (num > 258)
			{
				if (num == 592)
				{
					throw CreditCardReaderException.DataNotAccepted;
				}
				if (num == 1460)
				{
					throw CreditCardReaderException.Timeout;
				}
			}
			else
			{
				if (num == 24)
				{
					throw CreditCardReaderException.CommandTooLong;
				}
				if (num == 258)
				{
					throw CreditCardReaderException.TerminalNotFound;
				}
			}
			throw CreditCardReaderException.unknownError;
		}

		public int getTagValue(string ResponseString, int inTag, out string Value)
		{
			Value = null;
			int num = 0;
			if (ResponseString != null)
			{
				num = 4;
				while (num < ResponseString.Length)
				{
					int num1 = int.Parse(ResponseString.Substring(num, 2));
					num += 2;
					int num2 = int.Parse(ResponseString.Substring(num, 2));
					num += 2;
					if (num2 <= 0)
					{
						continue;
					}
					if (num1 == inTag)
					{
						Value = ResponseString.Substring(num, num2);
						return 0;
					}
					num += num2;
				}
			}
			return -1;
		}

		public TransactionDetail parseResult(string response)
		{
			string str;
			string str1;
			TransactionDetail transactionDetail = new TransactionDetail();
			MyLog myLog = new MyLog();
			myLog.Log(MyLog.LogType.Info, "Respone from Credit Card:");
			myLog.Log(MyLog.LogType.Info, response);
			if (response.Length >= 4)
			{
				transactionDetail.paymentCommand = response.Substring(0, 4);
			}
			if (response.Length != 0 && (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Sales))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_PreAuth))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_PreAuth_Capture_or_Offline))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Refund))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Void))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.NETS_Contactless_Sales_Tap))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Sales))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Balance))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Sales))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Topup))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Balance))) || response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Abort)))))
			{
				if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Response_Code), out str) == 0)
				{
					transactionDetail.responseCode = str;
					if (!transactionDetail.responseCode.Equals("00"))
					{
						transactionDetail.title = "ACTION";
						transactionDetail.status = "FAILED";
					}
					else
					{
						transactionDetail.status = "APPROVED";
						this.getTagValue(response, Convert.ToInt32(PaymentTag.Command_Identifier), out str1);
						transactionDetail.runningNumber = Convert.ToInt32(str1);
						if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Sales))))
						{
							transactionDetail.title = "SALE";
						}
						else if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_PreAuth))))
						{
							transactionDetail.title = "PREAUTH";
						}
						else if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_PreAuth_Capture_or_Offline))))
						{
							transactionDetail.title = "OFFLINE";
						}
						else if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Refund))))
						{
							transactionDetail.title = "REFUND";
						}
						else if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CreditCard_Void))))
						{
							transactionDetail.title = "VOID";
						}
						else if (response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.CEPAS_Sales))))
						{
							transactionDetail.title = "SALE";
						}
						else if (!response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.NETS_Contactless_Sales_Tap))))
						{
							transactionDetail.title = "TRANSACTION";
						}
						else
						{
							transactionDetail.title = "SALE";
						}
						this.getTagValue(response, Convert.ToInt32(PaymentTag.Terminal_Id), out str1);
						transactionDetail.terminalId = str1;
						this.getTagValue(response, Convert.ToInt32(PaymentTag.Mechant_Id), out str1);
						transactionDetail.mechantId = str1;
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Date_Time), out str1) == 0)
						{
							DateTime now = DateTime.Now;
							transactionDetail.transactionDateTime = new DateTime(now.Year, Convert.ToInt32(str1.Substring(0, 2)), Convert.ToInt32(str1.Substring(2, 2)), Convert.ToInt32(str1.Substring(4, 2)), Convert.ToInt32(str1.Substring(6, 2)), Convert.ToInt32(str1.Substring(8, 2)));
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Invoice_Number_or_NETS_Reference_Number), out str1) == 0)
						{
							transactionDetail.invoice = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Batch_Number), out str1) == 0)
						{
							transactionDetail.batch = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Unique_Trace_Reference), out str1) == 0)
						{
							transactionDetail.uniqueTraceReference = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Card_Label), out str1) == 0)
						{
							transactionDetail.cardLabel = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Card_Number), out str1) == 0)
						{
							transactionDetail.cardNumber = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Host_Label), out str1) == 0)
						{
							transactionDetail.host = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Card_Type), out str1) == 0)
						{
							transactionDetail.type = str1[0];
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Retrieval_Reference), out str1) == 0)
						{
							transactionDetail.retrievalReference = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Approval_Code), out str1) == 0)
						{
							transactionDetail.approvalCode = str1;
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Entry_Mode), out str1) == 0)
						{
							if (str1[0].Equals('E'))
							{
								transactionDetail.entryMode = "MANUAL ENTRY";
							}
							else if (str1[0].Equals('M'))
							{
								transactionDetail.entryMode = "MAGSTRIPE";
							}
							else if (str1[0].Equals('F'))
							{
								transactionDetail.entryMode = "FALLBACK";
							}
							else if (str1[0].Equals('C'))
							{
								transactionDetail.entryMode = "CHIP";
							}
							else if (!str1[0].Equals('P'))
							{
								transactionDetail.entryMode = str1;
							}
							else
							{
								transactionDetail.entryMode = "CONTACTLESS";
							}
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.EMV_Data), out str1) == 0)
						{
							transactionDetail.EMV.TVR = str1.Substring(0, 10).Trim();
							transactionDetail.EMV.TSI = str1.Substring(10, 4).Trim();
							transactionDetail.EMV.AID = str1.Substring(14, 16).Trim();
							transactionDetail.EMV.appLabel = str1.Substring(30, 16).Trim();
							transactionDetail.EMV.TC = str1.Substring(46, 16).Trim();
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.transaction_Amount), out str1) == 0)
						{
							transactionDetail.transactionAmount = Convert.ToDecimal(str1) / new decimal(100);
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Additional_Printing_Flags), out str1) == 0)
						{
							this._parsePrintingFlag(transactionDetail, str1);
						}
						if (this.getTagValue(response, Convert.ToInt32(PaymentTag.Card_Holder_Name), out str1) == 0)
						{
							transactionDetail.cardHolderName = str1.Trim();
						}
					}
				}
			}
			else if (response.Length != 0 && response.StartsWith(string.Concat("R", Convert.ToInt32(PaymentReceiveCode.Terminal_Info))))
			{
				transactionDetail.terminalInfo = response.Substring(4);
			}
			else if (response.Length != 0 && this.getTagValue(response, Convert.ToInt32(PaymentTag.Response_Code), out str) == 0)
			{
				transactionDetail.responseCode = str;
				transactionDetail.title = "ACTION";
				if (!transactionDetail.responseCode.Equals("00"))
				{
					transactionDetail.status = "FAILED";
				}
				else
				{
					transactionDetail.status = "APPROVED";
				}
			}
			return transactionDetail;
		}

		public string RecieveResponse(int timeout)
		{
			string str;
			IMagIC3Connector variable = (MagIC3Connector)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("5DC9FDA1-B975-49BF-8A89-05068714BAFD")));
			int num = variable.RecvResponse(this.portName, out str, timeout);
			if (num == 0)
			{
				return str;
			}
			this.getExceptionByPortStat(num);
			return null;
		}

		public int SendAbort()
		{
			IMagIC3Connector variable = (MagIC3Connector)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("5DC9FDA1-B975-49BF-8A89-05068714BAFD")));
			int num = variable.SendAbort();
			if (num == 0)
			{
				return 1;
			}
			this.getExceptionByPortStat(num);
			return 0;
		}

		public int SendCancel()
		{
			IMagIC3Connector variable = (MagIC3Connector)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("5DC9FDA1-B975-49BF-8A89-05068714BAFD")));
			int num = variable.Cancel(this.portName);
			if (num == 0)
			{
				return 1;
			}
			this.getExceptionByPortStat(num);
			return 0;
		}

		public int SendCommand(string command)
		{
			IMagIC3Connector variable = (MagIC3Connector)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("5DC9FDA1-B975-49BF-8A89-05068714BAFD")));
			int num = variable.SendCommand(this.portName, command);
			if (num == 0)
			{
				return 1;
			}
			this.getExceptionByPortStat(num);
			return 0;
		}

		public string Test()
		{
			string str;
			IMagIC3Connector variable = (MagIC3Connector)Activator.CreateInstance(Marshal.GetTypeFromCLSID(new Guid("5DC9FDA1-B975-49BF-8A89-05068714BAFD")));
			int num = variable.EchoTest(this.portName, out str);
			if (num == 0)
			{
				return str;
			}
			this.getExceptionByPortStat(num);
			return null;
		}
	}
}