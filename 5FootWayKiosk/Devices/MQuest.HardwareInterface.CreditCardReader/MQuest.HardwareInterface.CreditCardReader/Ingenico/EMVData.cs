using System;
using System.Runtime.CompilerServices;

namespace MQuest.HardwareInterface.CreditCardReader.Ingenico
{
	public class EMVData
	{
		public string AID
		{
			get;
			set;
		}

		public string appLabel
		{
			get;
			set;
		}

		public string TC
		{
			get;
			set;
		}

		public string TSI
		{
			get;
			set;
		}

		public string TVR
		{
			get;
			set;
		}

		public EMVData()
		{
		}
	}
}