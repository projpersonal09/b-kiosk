﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_Window_Service
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_Window_Service
  {
    public static string connStr = ConstantVariable.connStr;
    public static int updateHour = Convert.ToInt32(ConfigurationManager.AppSettings["updateHour"]);
    public static int updateMinute = Convert.ToInt32(ConfigurationManager.AppSettings["updateMinute"]);
    public static int updateSecond = Convert.ToInt32(ConfigurationManager.AppSettings["updateSecond"]);
    public static int maxTryAttempts = Convert.ToInt32(ConfigurationManager.AppSettings["maxTryAttempts"]);
    public static int intervalMinute = Convert.ToInt32(ConfigurationManager.AppSettings["intervalMinute"]);
    public const string updateHourString = "updateHour";
    public const string updateMinuteString = "updateMinute";
    public const string updateSecondString = "updateSecond";
  }
}
