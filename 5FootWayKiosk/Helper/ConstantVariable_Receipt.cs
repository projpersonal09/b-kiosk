﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_Receipt
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_Receipt
  {
    public static readonly string Disclaimer1 = "I AGREE TO PAY THE ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT";
    public static readonly string Disclaimer2 = "NO INTEREST WILL BE REFUNDED FOR EARLY REPAYMENT";
    public static readonly string Disclaimer3 = "THE CARDHOLDER HAS ELECTED TO SETTLE THE TRANSACTION IN THE CURRENCY SHOWN ABOVE PLEASE TICK HERE -> [  ] ";
    public static readonly string Disclaimer4 = "I HAVE CHOSEN NOT TO USE THE MASTERCARD CURRENCY CONVERSION PROCESS AND AGREE THAT I WILL HAVE NO RECOURSE AGAINST MASTERCARD CONCERNING THE CURRENCY CONVERSION OR ITS DISCLOSURE.";
    public static readonly string Disclaimer5 = "I AGREE; I WAS OFFERED A CHOICE OF CURRENCIES FOR PAYMENT INCLUDING SGD; THE TRANSACTION CURRENCY SHOWN WAS CHOSEN BY ME AND IS FINAL; CURRENCY CONVERSION IS CONDUCTED BY THE MERCHANT USING TRAVELEX EXCHANGE RATES AND IS NOT ASSOCIATED WITH, OR ENDORSED BY VISA.";
    public static readonly string Disclaimer6 = "PLEASE NOTE: eVOUCHER AMOUNT CAN NOT BE VOIDED / REFUNDED";
    public static readonly string Disclaimer7 = "PLEASE BE ADVISED THAT YOUR CASH REBATE FOR THIS TRANSACTION WILL BE AWARDED ON THE NEXT BUSINESS DAY";
    public List<string> addressList = new List<string>();
    public List<string> contactList = new List<string>();
    public List<string> companyList = new List<string>();
    public List<string> TandCList = new List<string>();
    public string appreciateTerm;
    public bool physicalReceipt;

    public ConstantVariable_Receipt()
    {
        this.appreciateTerm = ConfigurationManager.AppSettings["appreciateTerm"];
        this.physicalReceipt = Convert.ToBoolean(ConfigurationManager.AppSettings["physicalReceipt"]);
      for (int index = 1; ConfigurationManager.AppSettings["addressLine" + (object) index] != null; ++index)
        this.addressList.Add(ConfigurationManager.AppSettings["addressLine" + (object) index]);
      for (int index = 1; ConfigurationManager.AppSettings["contactLine" + (object) index] != null; ++index)
        this.contactList.Add(ConfigurationManager.AppSettings["contactLine" + (object) index]);
      for (int index = 1; ConfigurationManager.AppSettings["companyLine" + (object) index] != null; ++index)
        this.companyList.Add(ConfigurationManager.AppSettings["companyLine" + (object) index]);
      for (int index = 1; ConfigurationManager.AppSettings["TandCLine" + (object) index] != null; ++index)
        this.TandCList.Add(ConfigurationManager.AppSettings["TandCLine" + (object) index]);
    }

    public ConstantVariable_Receipt(List<string> addressList)
    {
        this.appreciateTerm = ConfigurationManager.AppSettings["appreciateTerm"];
        this.physicalReceipt = Convert.ToBoolean(ConfigurationManager.AppSettings["physicalReceipt"]);
      this.addressList = addressList;
      for (int index = 1; ConfigurationManager.AppSettings["contactLine" + (object) index] != null; ++index)
        this.contactList.Add(ConfigurationManager.AppSettings["contactLine" + (object) index]);
      for (int index = 1; ConfigurationManager.AppSettings["companyLine" + (object) index] != null; ++index)
        this.companyList.Add(ConfigurationManager.AppSettings["companyLine" + (object) index]);
      for (int index = 1; ConfigurationManager.AppSettings["TandCLine" + (object) index] != null; ++index)
        this.TandCList.Add(ConfigurationManager.AppSettings["TandCLine" + (object) index]);
    }
  }
}
