﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_Web
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_Web
  {
    public static string connStr = ConstantVariable.connStr;
    public static string hotelName = ConfigurationManager.AppSettings["hotelName"];
    public static Decimal timeoutThreehold = Convert.ToDecimal(ConfigurationManager.AppSettings["timeoutThreehold"]);
    public static int checkInCompletePageTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CheckInCompletePageTimeout"]);
    public static int checkOutCompletePageTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CheckOutCompletePageTimeout"]);
    public static int slideShowDuraction = Convert.ToInt32(ConfigurationManager.AppSettings["SlideShowDuraction"]);
    public static string homeImageFolderPath = ConfigurationManager.AppSettings["homeImageFolder"];
    public static string loadingImageFolderPath = ConfigurationManager.AppSettings["loadingImageFolder"];
    public static int checkInStepCount = 18;
    public static List<int> checkInPointList = new List<int>()
    {
      1,
      2,
      9,
      14,
      15,
      17,
      18
    };
    public static List<int> checkOutPointList = new List<int>()
    {
      0,
      1,
      2,
      3
    };
    public static int checkOutStepCount = 3;
    public static bool enableCheckout = Convert.ToBoolean(ConfigurationManager.AppSettings["enableCheckout"]);
    public const string enableCheckoutString = "enableCheckout";
    public const string done = "visited";
    public const string active = "active";
    public const string pending = "";
  }
}
