﻿// Decompiled with JetBrains decompiler
// Type: Helper.DateTimeHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;

namespace Helper
{
  public class DateTimeHelper
  {
    public static DateTime RemoveTimezone(DateTime date)
    {
      return new DateTime(date.Ticks, DateTimeKind.Unspecified);
    }
  }
}
