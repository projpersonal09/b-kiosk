﻿// Decompiled with JetBrains decompiler
// Type: Helper.RoomTypeGender
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

namespace Helper
{
  public class RoomTypeGender
  {
    public static readonly string Mixed = "MI";
    public static readonly string Male = "MA";
    public static readonly string Female = "FE";
  }
}
