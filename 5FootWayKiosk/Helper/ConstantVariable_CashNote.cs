﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_CashNote
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_CashNote
  {
    public List<Decimal> acceptCashNoteList = new List<Decimal>();
    public List<Decimal> allCashNoteList = new List<Decimal>();
    public List<int> appRouteCashNoteList = new List<int>();
    public List<int> managementRouteCashNoteList = new List<int>();

    public ConstantVariable_CashNote()
    {
      for (int index = 1; ConfigurationManager.AppSettings["acceptCashNote" + (object) index] != null; ++index)
        this.acceptCashNoteList.Add(Convert.ToDecimal(ConfigurationManager.AppSettings["acceptCashNote" + (object) index]));
      for (int index = 1; ConfigurationManager.AppSettings["appRouteCashNote" + (object) index] != null; ++index)
        this.appRouteCashNoteList.Add(Convert.ToInt32(ConfigurationManager.AppSettings["appRouteCashNote" + (object) index]));
      for (int index = 1; ConfigurationManager.AppSettings["managementRouteCashNote" + (object) index] != null; ++index)
        this.managementRouteCashNoteList.Add(Convert.ToInt32(ConfigurationManager.AppSettings["managementRouteCashNote" + (object) index]));
      for (int index = 1; ConfigurationManager.AppSettings["allCashNote" + (object) index] != null; ++index)
        this.allCashNoteList.Add((Decimal) Convert.ToInt32(ConfigurationManager.AppSettings["allCashNote" + (object) index]));
    }
  }
}
