﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConfigurationHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Helper
{
  public class ConfigurationHelper
  {
    public static string ReadValue(string key)
    {
      try
      {
        return ConfigurationManager.AppSettings[key];
      }
      catch
      {
        throw;
      }
    }

    public static void updateKeyValue(string[] keyList, string[] newValueList)
    {
      try
      {
        System.Configuration.Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        KeyValueConfigurationCollection settings = configuration.AppSettings.Settings;
        for (int index = 0; index < ((IEnumerable<string>) keyList).Count<string>(); ++index)
        {
          if (settings[keyList[index]] != null)
            settings[keyList[index]].Value = newValueList[index];
        }
        configuration.Save(ConfigurationSaveMode.Modified);
        ConfigurationManager.RefreshSection(configuration.AppSettings.SectionInformation.Name);
      }
      catch
      {
        throw;
      }
    }
  }
}
