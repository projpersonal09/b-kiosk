﻿// Decompiled with JetBrains decompiler
// Type: Helper.MyLog
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Helper
{
  public class MyLog
  {
    private bool _writeInnerException = true;
    private bool _writeExceptionStackTrace = true;
    private const string LogDateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private const string ExceptionLogFileName = "Exception.log";
    private const string WarningLogFileName = "Warning.log";
    private const string InfoLogFileName = "Info.log";
    private bool _throwExceptionOnLogging;
    private List<string> _fileNames;

    public bool WriteInnerException
    {
      get
      {
        return this._writeInnerException;
      }
      set
      {
        this._writeInnerException = value;
      }
    }

    public bool WriteExceptionStackTrace
    {
      get
      {
        return this._writeExceptionStackTrace;
      }
      set
      {
        this._writeExceptionStackTrace = value;
      }
    }

    public bool ThrowExceptionOnLogging
    {
      get
      {
        return this._throwExceptionOnLogging;
      }
      set
      {
        this._throwExceptionOnLogging = value;
      }
    }

    public MyLog()
      : this("Exception.log", "Warning.log", "Info.log")
    {
    }

    public MyLog(string exceptionFileName, string warningFileName, string infoFileName)
    {
      this._fileNames = new List<string>()
      {
        exceptionFileName,
        warningFileName,
        infoFileName
      };
    }

    public bool Log(MyLog.LogType logType, string logMessage)
    {
      try
      {
        using (StreamWriter streamWriter = File.AppendText(this._fileNames[Convert.ToInt32((object) logType)]))
        {
          streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "".PadRight(5) + logType.ToString().PadRight(10) + logMessage);
          if (logType == MyLog.LogType.Error || logType == MyLog.LogType.Warning)
            streamWriter.WriteLine(string.Empty);
          return true;
        }
      }
      catch
      {
        if (!this.ThrowExceptionOnLogging)
          return false;
        throw;
      }
    }

    public bool LogException(Exception exc)
    {
      bool flag1 = true;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendLine("Exception Type : " + exc.GetType().Name);
      stringBuilder.Append("Exception Message : ".PadLeft(30) + exc.Message);
      if (this.WriteExceptionStackTrace)
      {
        stringBuilder.AppendLine("");
        stringBuilder.Append("Exception Stack Trace : ".PadLeft(30) + exc.StackTrace);
      }
      bool flag2 = flag1 && this.Log(MyLog.LogType.Error, stringBuilder.ToString());
      for (Exception innerException = exc.InnerException; flag2 && innerException != null; innerException = innerException.InnerException)
      {
        stringBuilder.Clear();
        stringBuilder.AppendLine("Inner Exception Type : " + innerException.GetType().Name);
        stringBuilder.Append("Inner Exception Message : ".PadLeft(34) + innerException.Message);
        if (this.WriteExceptionStackTrace)
        {
          stringBuilder.AppendLine("");
          stringBuilder.Append("Inner Exception Stack Trace : ".PadLeft(34) + innerException.StackTrace);
        }
        flag2 = flag2 && this.Log(MyLog.LogType.Error, stringBuilder.ToString());
      }
      return flag2;
    }

    public enum LogType
    {
      Error,
      Warning,
      Info,
    }
  }
}
