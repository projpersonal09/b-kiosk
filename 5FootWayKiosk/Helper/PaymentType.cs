﻿// Decompiled with JetBrains decompiler
// Type: Helper.PaymentType
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

namespace Helper
{
  public class PaymentType
  {
    public const string MIFARE = "M";
    public const string CREDIT_CARD = "C";
    public const string NETS = "N";
  }
}
