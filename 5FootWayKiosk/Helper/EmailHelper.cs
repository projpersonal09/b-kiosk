﻿// Decompiled with JetBrains decompiler
// Type: Helper.EmailHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Mail;

namespace Helper
{
  public class EmailHelper
  {
    private const string tag = "Email";

    public static bool SendEmail(string sender, List<string> recipients, string subject, string messageBody, List<string> attachmentPathList, bool isBodyHtml)
    {
      MyLog myLog = new MyLog();
      MailAddress from = new MailAddress(sender);
      Exception exception = (Exception) null;
      foreach (string recipient in recipients)
      {
        if (!string.IsNullOrEmpty(recipient))
        {
          MailAddress to = new MailAddress(recipient);
          using (MailMessage message = new MailMessage(from, to))
          {
            try
            {
              message.Subject = subject;
              message.Body = messageBody;
              message.IsBodyHtml = isBodyHtml;
              if (attachmentPathList != null && attachmentPathList.Count > 0)
              {
                foreach (string attachmentPath in attachmentPathList)
                {
                  Attachment attachment = new Attachment(attachmentPath);
                  message.Attachments.Add(attachment);
                }
              }
              using (SmtpClient smtpClient = new SmtpClient())
              {
                smtpClient.Timeout = ConstantVariable_App.MailSendTimeout;
                smtpClient.Send(message);
              }
              myLog.Log(MyLog.LogType.Info, "Send email to " + recipient + " for " + sender + " is completed");
            }
            catch (Exception ex)
            {
              exception = ex;
              myLog.Log(MyLog.LogType.Info, "Send email to " + recipient + " for " + sender + " is failed");
              myLog.LogException(ex);
            }
            finally
            {
              if (message != null && message.Attachments != null)
              {
                foreach (AttachmentBase attachment in (Collection<Attachment>) message.Attachments)
                  attachment.Dispose();
              }
            }
          }
        }
      }
      if (exception != null)
        throw exception;
      return true;
    }
  }
}
