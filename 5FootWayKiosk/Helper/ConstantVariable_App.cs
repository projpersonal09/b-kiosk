﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_App
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_App
  {
    public static string connStr = ConstantVariable.connStr;
    public static string terminalCode = ConfigurationManager.AppSettings["TerminalCode"];
    public static string kioskLocation = ConfigurationManager.AppSettings["KioskLocation"];
    public static Decimal chipImageRate = Convert.ToDecimal(ConfigurationManager.AppSettings["chipImageRate"]);
    public static Decimal documentImageRate = Convert.ToDecimal(ConfigurationManager.AppSettings["documentImageRate"]);
    public static int creditCardReaderPort = Convert.ToInt32(ConfigurationManager.AppSettings["Credit_Card_Reader_COM_Port"]);
    public static int cashDispenserPort = Convert.ToInt32(ConfigurationManager.AppSettings["Cash_Dispenser_COM_Port"]);
    public static string cardReaderLibraryDll = ConfigurationManager.AppSettings["CardReader_LibraryDll"];
    public static string cardDispenserPortName = ConfigurationManager.AppSettings["Card_Dispenser_RS323_Port_Name"];
    public static uint cardDispenserBaudRate = Convert.ToUInt32(ConfigurationManager.AppSettings["Card_Dispenser_RS323_Baud_Rate"]);
    public static byte cardDispenserAddress = Convert.ToByte(ConfigurationManager.AppSettings["Card_Dispenser_RS323_Address"]);
    public static bool cardDispenserRetractCounter = Convert.ToBoolean(ConfigurationManager.AppSettings["Card_Dispenser_Enable_Retract_Counter"]);
    public static int leftMargin = Convert.ToInt32(ConfigurationManager.AppSettings["leftMargin"]);
    public static int rightMargin = Convert.ToInt32(ConfigurationManager.AppSettings["rightMargin"]);
    public static int topMargin = Convert.ToInt32(ConfigurationManager.AppSettings["topMargin"]);
    public static int bottomMargin = Convert.ToInt32(ConfigurationManager.AppSettings["bottomMargin"]);
    public static string visaFolder = ConfigurationManager.AppSettings["visaFolder"];
    public static string passportFolder = ConfigurationManager.AppSettings["passportFolder"];
    public static string passportHeadFolder = ConfigurationManager.AppSettings["passportHeadFolder"];
    public static string snapShotFolder = ConfigurationManager.AppSettings["snapShotFolder"];
    public static string signatureFolder = ConfigurationManager.AppSettings["signatureFolder"];
    public static string logoPath = ConfigurationManager.AppSettings["logoPath"];
    public static string bankLogoPath = ConfigurationManager.AppSettings["bankLogoPath"];
    public static string imageFolder = ConfigurationManager.AppSettings["imageFolder"];
    public static string receiptFolder = ConfigurationManager.AppSettings["receiptFolder"];
    public static string immigrationChopFolder = ConfigurationManager.AppSettings["immigrationChopFolder"];
    public static bool isOfflineEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["isOfflineEnabled"]);
    public static string printerName = ConfigurationManager.AppSettings["printerName"];
    public static int maximumRetryAttempts = Convert.ToInt32(ConfigurationManager.AppSettings["maximumRetryAttempts"]);
    public static int maximumRecognitionRetryAttempts = Convert.ToInt32(ConfigurationManager.AppSettings["maximumRecognitionRetryAttempts"]);
    public static string mailFrom = ConfigurationManager.AppSettings["mailFrom"];
    public static bool barcodeScanner_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["barcodeScanner_Enable"]);
    public static int barcodeScanner_BeepTime = Convert.ToInt32(ConfigurationManager.AppSettings["barcodeScanner_BeepTime"]);
    public static bool barcodeScanner_QR_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["barcodeScanner_QR_Enable"]);
    public static bool barcodeScanner_DM_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["barcodeScanner_DM_Enable"]);
    public static bool barcodeScanner_Barcode_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["barcodeScanner_Barcode_Enable"]);
    public static int minCashNoteLoaded = Convert.ToInt32(ConfigurationManager.AppSettings["minCashNoteLoaded"]);
    public static string tabTipFolderLoc = ConfigurationManager.AppSettings["tabTipFolder"];
    public static int cardMinQty = Convert.ToInt32(ConfigurationManager.AppSettings["Card_Dispenser_Card_Min_Qty"]);
    public static bool faceDetection_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["faceDetection_Enable"]);
    public static bool faceRecognition_Enable = Convert.ToBoolean(ConfigurationManager.AppSettings["faceRecognition_Enable"]);
    public static Decimal faceRecognition_DefaultRate = new Decimal(100);
    public static int faceDetection_DefaultWaitingTime = 1000;
    public static int faceRecognition_DefaultWaitingTime = 2000;
    public const string creditCardReaderPortString = "Credit_Card_Reader_COM_Port";
    public const string cardDispenserPortString = "Card_Dispenser_RS323_Port_Number";
    public const string cashDispenserPortString = "Cash_Dispenser_COM_Port";
    public const string chipImageString = "chipImageRate";
    public const string documentImageString = "documentImageRate";
    public const string printerNameString = "printerName";

    public static int HardwareCheckInterval
    {
      get
      {
        int result = 0;
        int.TryParse(ConfigurationManager.AppSettings["HardwareCheckInterval"], out result);
        if (result == 0)
          result = 60;
        return result * 60000;
      }
    }

    public static int MailSendTimeout
    {
      get
      {
        int result = 0;
        int.TryParse(ConfigurationManager.AppSettings["MailSendTimeout"], out result);
        if (result == 0)
          result = 2;
        return result * 60000;
      }
    }
  }
}
