﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_ImageFolder
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Collections.Generic;
using System.IO;

namespace Helper
{
  public class ConstantVariable_ImageFolder
  {
    public List<string> imageList = new List<string>();

    public ConstantVariable_ImageFolder(string folderPath)
    {
      try
      {
        foreach (string file in Directory.GetFiles(FileHelper.GetServerFolderPath(folderPath)))
          this.imageList.Add(file);
      }
      catch (Exception ex)
      {
        new MyLog().LogException(ex);
      }
    }
  }
}
