﻿// Decompiled with JetBrains decompiler
// Type: Helper.Database
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System.Collections.Generic;

namespace Helper
{
  public class Database
  {
    public class Nationality
    {
      public const string code3 = "Code_3";
      public const string code = "Code";
      public const string Name = "Name";
      public const string Id = "Id";
    }

    public class Setting
    {
      public const string id = "Id";
      public const string code = "Code";
      public const string name = "Name";
      public const string value = "Value";
    }

    public class User
    {
      public const string id = "Id";
      public const string username = "UserName";
      public const string password = "Password";
      public const string name = "Name";
    }

    public class CashNoteTransaction
    {
      public const string transactionId = "Transaction_Id";
      public const string transactionDate = "Transaction_Date";
      public const string currencyCode = "Currency_Code";
      public const string terminalCode = "Terminal_Code";
      public const string cashNoteValue1 = "Cash_Note_1_Value";
      public const string cashNoteValue2 = "Cash_Note_2_Value";
      public const string cashNoteValue3 = "Cash_Note_3_Value";
      public const string cashNoteValue4 = "Cash_Note_4_Value";
      public const string cashNoteCount1 = "Cash_Note_1_Count";
      public const string cashNoteCount2 = "Cash_Note_2_Count";
      public const string cashNoteCount3 = "Cash_Note_3_Count";
      public const string cashNoteCount4 = "Cash_Note_4_Count";
      public const string totalCashNoteValue = "Total_Cash_Note_Value";
      public readonly List<string> cashNoteValueList;
      public readonly List<string> cashNoteCountList;

      public CashNoteTransaction()
      {
        this.cashNoteValueList = new List<string>()
        {
          "Cash_Note_1_Value",
          "Cash_Note_2_Value",
          "Cash_Note_3_Value",
          "Cash_Note_4_Value"
        };
        this.cashNoteCountList = new List<string>()
        {
          "Cash_Note_1_Count",
          "Cash_Note_2_Count",
          "Cash_Note_3_Count",
          "Cash_Note_4_Count"
        };
      }
    }

    public class Booking
    {
      public const string id = "Id";
      public const string bookingId = "BookingId";
      public const string bookingStatusCode = "BookingStatusCode";
    }

    public class BookingPayment
    {
      public const string id = "Id";
      public const string bookingId = "BookingId";
      public const string roomStayId = "RoomStayId";
      public const string paymentModeCode = "PaymentModeCode";
      public const string amountPaid = "AmountPaid";
      public const string surcharge = "Surcharge";
      public const string currencyCode = "CurrencyCode";
      public const string invoiceNumber = "InvoiceNumber";
      public const string paymentDateTime = "PaymentDateTime";
    }

    public class Guest
    {
      public const string id = "Id";
      public const string roomStayId = "RoomStayId";
      public const string firstName = "FirstName";
      public const string lastName = "LastName";
      public const string passportNumber = "PassportNumber";
      public const string nationality = "Nationality";
      public const string dateOfBirth = "DateOfBirth";
      public const string email = "Email";
      public const string gender = "Gender";
      public const string address = "Address";
      public const string phone = "Phone";
      public const string postalCode = "PostalCode";
      public const string company = "Company";
      public const string guestCategoryCode = "GuestCategoryCode";
      public const string bookingId = "BookingId";
      public const string occupation = "Occupation";
      public const string prevDestination = "PrevDestination";
      public const string nextDestination = "NextDestination";
    }

    public class RoomGuestCard
    {
      public const string id = "Id";
      public const string cardUID = "CardUID";
      public const string issueDate = "IssueDate";
      public const string startDate = "StartDate";
      public const string endDate = "EndDate";
      public const string guestId = "GuestId";
      public const string bookingId = "BookingId";
      public const string roomId = "RoomId";
      public const string bedId = "BedId";
      public const string lockerId = "LockerId";
      public const string doorId = "DoorId";
      public const string returnDate = "ReturnDate";
      public const string cardNo = "CardNo";
      public const string roomStayId = "RoomStayId";
    }

    public class SecurityDeposit
    {
      public const string id = "Id";
      public const string amount = "Amount";
      public const string securityDepositType = "Type";
      public const string date = "Date";
      public const string bookingId = "BookingId";
      public const string roomStayId = "RoomStayId";
      public const string currencyCode = "CurrencyCode";
      public const string invoiceNumber = "InvoiceNumber";
    }

    public class CashInOut
    {
      public const string id = "Id";
      public const string amount = "Amount";
      public const string currencyCode = "CurrencyCode";
      public const string transactionDate = "TransactionDate";
      public const string paymentModeCode = "PaymentModeCode";
      public const string remarks = "Remarks";
      public const string shiftId = "ShiftId";
      public const string isUploaded = "IsUploaded";
      public const string startDate = "StartDate";
      public const string endDate = "EndDate";
      public const string cashFill = "CashFill";
      public const string cashIn = "CashIn";
      public const string cashOut = "CashOut";
      public const string cashSubmitted = "CashSubmitted";
      public const string cashCollected = "CashCollected";
      public const string cashLeft = "CashLeft";
    }

    public class TransactionDetail
    {
      public const string id = "id";
      public const string machineCode = "MachineCode";
      public const string date = "Date";
      public const string actionType = "ActionType";
      public const string actionName = "ActionName";
      public const string errorMessage = "ErrorMessage";
    }

    public class TransactionSummary
    {
      public const string id = "id";
      public const string machineCode = "MachineCode";
      public const string date = "Date";
      public const string cardInQuantity = "CardInQuantity";
      public const string cardOutQuantity = "CardOutQuantity";
      public const string cardBoxQuantity = "CardBoxQuantity";
      public const string cardBinQuantity = "CardBinQuantity";
    }

    public class Others
    {
      public const string startDate = "StartDate";
      public const string endDate = "EndDate";
      public const string returnValue = "ReturnValue";
      public const string result = "Result";
    }

    public class Occupation
    {
      public const string Id = "Id";
      public const string Name = "Name";
    }

    public class ShiftSummary
    {
      public const string id = "Id";
      public const string propertyId = "PropertyId";
      public const string shiftId = "ShiftId";
      public const string shiftNumber = "ShiftNumber";
      public const string currencyCode = "CurrencyCode";
      public const string startDate = "StartDate";
      public const string startByName = "StartByName";
      public const string endDate = "EndDate";
      public const string endByName = "EndByName";
      public const string floatStart = "FloatStart";
      public const string floatEnd = "FloatEnd";
      public const string cashIn = "CashIn";
      public const string cashOut = "CashOut";
      public const string cashCounted = "CashCounted";
      public const string cashExpected = "CashExpected";
    }

    public class StoreProcedure
    {
      public const string checkVisaRequired = "Nationality_CheckVisaRequired";
      public const string getNationalityList = "Nationality_GetList";
      public const string checkChopBypass = "Nationality_CheckChopBypass";
      public const string getOccupationList = "Occupation_GetList";
      public const string getUser = "User_Get";
      public const string getSetting = "Setting_Get";
      public const string updateUser = "User_Update";
      public const string updateSetting = "Setting_Update";
      public const string insertCashInOut = "CashInOut_Insert";
      public const string insertCashInOutDetail = "CashInOutDetail_Insert";
      public const string insertOrUpdateCashNoteTransaction = "CashNoteTransaction_InsertOrUpdate";
      public const string insertBookingPayment = "BookingPayment_Insert";
      public const string insertBooking = "Booking_Insert";
      public const string insertSecurityDeposit = "SecurityDeposit_Insert";
      public const string insertGuest = "Guest_Insert";
      public const string insertRoomGuestCard = "RoomGuestCard_Insert";
      public const string getCashInOutListUpload = "CashInOut_GetListForUpload";
      public const string getCashInOutList = "CashInOut_GetList";
      public const string getCashSummary = "CashInOut_GetCashSummary";
      public const string updateCashInOut = "CashInOut_Update";
      public const string getBookingList = "Booking_GetList";
      public const string updateBooking = "Booking_Update";
      public const string getBookingPaymentList = "BookingPayment_GetList";
      public const string updateBookingPayment = "BookingPayment_Update";
      public const string getGuestList = "Guest_GetList";
      public const string updateGuest = "Guest_Update";
      public const string getRoomGuestCardList = "RoomGuestCard_GetList";
      public const string updateRoomGuestCard = "RoomGuestCard_Update";
      public const string getSecurityDepositList = "SecurityDeposit_GetList";
      public const string updateSecurityDeposit = "SecurityDeposit_Update";
      public const string shift_startShift = "ShiftSummary_Start";
      public const string shift_endShift = "ShiftSummary_End";
      public const string shift_getList = "ShiftSummary_GetList";
      public const string getCardTransactionDetailList = "GetCardTransactionDetailList";
      public const string insertCardTransactionDetail = "InsertCardTransactionDetail";
      public const string getCardTransactionSummaryList = "GetCardTransactionSummaryList";
      public const string insertOrUpdateCardTransactionSummary = "InsertOrUpdateCardTransactionSummary";
      public const string updateCardTransactionSummary = "UpdateCardTransactionSummary";
      public const string getCardTransactionSummary = "GetCardTransactionSummary";
    }
  }
}
