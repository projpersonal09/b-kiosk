﻿// Decompiled with JetBrains decompiler
// Type: Helper.WebPage
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

namespace Helper
{
  public class WebPage
  {
    public const string personalDataAgreement = "CheckIn/PDA";
    public const string checkIn = "CheckIn/CheckIn";
    public const string scanPassport = "CheckIn/ScanPassport";
    public const string scanChop = "CheckIn/ScanChop";
    public const string scanVisa = "CheckIn/ScanVisa";
    public const string personalParticular = "CheckIn/PersonalParticular";
    public const string capturePhoto = "CheckIn/CapturePhoto";
    public const string addOn = "CheckIn/AddOn";
    public const string verifyDetail = "CheckIn/VerifyDetail";
    public const string payment = "CheckIn/Payment";
    public const string securityDeposit = "CheckIn/SecurityDeposit";
    public const string checkInComplete = "CheckIn/CheckInComplete";
    public const string termAndCondition = "CheckIn/TandC";
    public const string roomBedList = "CheckIn/RoomBedList";
    public const string signature = "CheckIn/Signature";
    public const string collectCard = "CheckIn/CollectCard";
    public const string rateExperience = "CheckIn/RateExperience";
    public const string scanCard = "CheckOut/CheckOut";
    public const string checkOutComplete = "CheckOut/CheckOutComplete";
    public const string returnCard = "CheckOut/ReturnCard";
    public const string returnDeposit = "CheckOut/ReturnDeposit";
    public const string home = "";
    public const string home2 = "Home/Index";
    public const string checkInIncomplete = "CheckIn/CheckInIncomplete";
    public const string checkOutIncomplete = "CheckOut/CheckOutIncomplete";
    public const string class_link_rate_opt = "link_rate_opt";
    public const string name_link_rate_opt_1 = "link_rate_opt_1";
    public const string name_link_rate_opt_2 = "link_rate_opt_2";
    public const string name_link_rate_opt_3 = "link_rate_opt_3";
    public const string name_link_rate_opt_4 = "link_rate_opt_4";
    public const string name_link_rate_opt_5 = "link_rate_opt_5";
  }
}
