﻿// Decompiled with JetBrains decompiler
// Type: Helper.DefaultValue
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

namespace Helper
{
  public class DefaultValue
  {
    public const string defaultDateFormat = "yyyy-MM-dd";
    public const string defaultDatePickerFormat = "yyyy-mm-dd";
    public const string defaultDateTimeFormat = "yyyy-MM-dd hh:mm:ss";
    public const string defaultJavaScriptDateTimeFormat = "yyyy/MM/dd HH:mm:ss";
    public const string defaultReceiptDateTimeFormat = "dd/MM/yyyy HH:mm";
    public const string defaultReceiptDateFormat = "dd/MM/yyyy";
    public const string others = "Others";
    public const string registerString = "Register";
    public const string dispenseString = "Dispense";
    public const string returnString = "Return";
    public const string emptyString = "Empty";
    public const string fillString = "Fill";
    public const string adjustString = "Adjust";
    public const string updateString = "Update";
    public const string errorString = "Error";
  }
}
