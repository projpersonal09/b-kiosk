﻿// Decompiled with JetBrains decompiler
// Type: Helper.ExceptionHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;

namespace Helper
{
  public class ExceptionHelper
  {
    public static string GetFullExceptionMessage(Exception ex, string newLine)
    {
      string str = ex.Message;
      while (ex.InnerException != null)
      {
        ex = ex.InnerException;
        str = str + newLine + ex.Message;
      }
      return str;
    }
  }
}
