﻿// Decompiled with JetBrains decompiler
// Type: Helper.ConstantVariable_StaffEmail
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System.Collections.Generic;
using System.Configuration;

namespace Helper
{
  public class ConstantVariable_StaffEmail
  {
    public List<string> staffEmailList = new List<string>();

    public ConstantVariable_StaffEmail()
    {
      for (int index = 1; ConfigurationManager.AppSettings["staffEmail" + (object) index] != null; ++index)
        this.staffEmailList.Add(ConfigurationManager.AppSettings["staffEmail" + (object) index]);
    }
  }
}
