﻿// Decompiled with JetBrains decompiler
// Type: Helper.DatabaseHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Data;
using System.Data.SqlClient;

namespace Helper
{
  public class DatabaseHelper
  {
    public static void ConnectDatabase(string connStr)
    {
      try
      {
        using (SqlConnection sqlConnection = new SqlConnection(connStr))
        {
          sqlConnection.Open();
          if (!sqlConnection.State.Equals((object) ConnectionState.Open))
            throw new Exception("Unable to establish connection to " + sqlConnection.ConnectionString + ", Connection State : " + (object) sqlConnection.State + ".");
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public static string CheckStringNull(object data)
    {
      if (!Convert.IsDBNull(data))
        return Convert.ToString(data);
      return string.Empty;
    }

    public static int CheckInt32Null(object data)
    {
      if (!Convert.IsDBNull(data))
        return Convert.ToInt32(data);
      return 0;
    }

    public static long CheckInt64Null(object data)
    {
      if (!Convert.IsDBNull(data))
        return Convert.ToInt64(data);
      return 0;
    }

    public static Decimal CheckDecimalNull(object data)
    {
      if (!Convert.IsDBNull(data))
        return Convert.ToDecimal(data);
      return new Decimal(0);
    }

    public static DateTime CheckDateTimeNull(object data)
    {
      if (!Convert.IsDBNull(data))
        return Convert.ToDateTime(data);
      return DateTime.MinValue;
    }

    public static object CheckEmptyString(string data)
    {
      if (string.IsNullOrEmpty(data))
        return (object) DBNull.Value;
      return (object) data;
    }
  }
}
