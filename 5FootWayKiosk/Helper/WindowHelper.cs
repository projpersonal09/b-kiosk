﻿// Decompiled with JetBrains decompiler
// Type: Helper.WindowHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace Helper
{
  public class WindowHelper
  {
    [DllImport("user32.dll", CharSet = CharSet.Unicode)]
    private static extern IntPtr FindWindow(string sClassName, string sAppName);

    [DllImport("user32.dll", CharSet = CharSet.Unicode)]
    private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

    [DllImport("User32.Dll", EntryPoint = "PostMessageA")]
    private static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

    public static void showKeyboard()
    {
      Version version = Environment.OSVersion.Version;
      if (version.Major == 6 && version.Minor == 3)
        WindowHelper.callTabTipWin10();
      else
        WindowHelper.callTabTipDefault();
    }

    public static void closeKeyboard()
    {
      foreach (Process process in Process.GetProcesses())
      {
        if (process.ProcessName == "TabTip")
        {
          process.Kill();
          break;
        }
      }
    }

    private static void callTabTipDefault()
    {
      Process.Start(Path.Combine(ConstantVariable_App.tabTipFolderLoc, "TabTip.exe"));
    }

    private static void callTabTipWin10()
    {
      IntPtr window = WindowHelper.FindWindow("Shell_TrayWnd", (string) null);
      IntPtr childAfter = new IntPtr(0);
      if (!(window != childAfter))
        return;
      IntPtr windowEx1 = WindowHelper.FindWindowEx(window, childAfter, "TrayNotifyWnd", (string) null);
      if (!(windowEx1 != childAfter))
        return;
      IntPtr windowEx2 = WindowHelper.FindWindowEx(windowEx1, childAfter, "TIPBand", (string) null);
      if (!(windowEx2 != childAfter))
        return;
      WindowHelper.PostMessage(windowEx2, 513U, 1, 65537);
      WindowHelper.PostMessage(windowEx2, 514U, 1, 65537);
    }

    public enum WMessages
    {
      WH_KEYBOARD_LL = 13, // 0x0000000D
      WH_MOUSE_LL = 14, // 0x0000000E
      WM_KEYDOWN = 256, // 0x00000100
      WM_KEYUP = 257, // 0x00000101
      WM_LBUTTONDOWN = 513, // 0x00000201
      WM_LBUTTONUP = 514, // 0x00000202
    }
  }
}
