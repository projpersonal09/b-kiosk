﻿// Decompiled with JetBrains decompiler
// Type: Helper.FileHelper
// Assembly: Helper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8F5F4549-0FF0-464B-8743-2BAC84804938
// Assembly location: D:\DQ documents\PMS project\KIOSK\5footway Application\Helper.dll

using System;
using System.IO;

namespace Helper
{
  public class FileHelper
  {
    public static string GetLocalFolderPath(string folderName)
    {
      string path = Environment.CurrentDirectory + "\\" + folderName;
      if (!Directory.Exists(path))
        Directory.CreateDirectory(path);
      return path;
    }

    public static string GetServerFolderPath(string folderPath)
    {
      if (!Directory.Exists(folderPath))
        Directory.CreateDirectory(folderPath);
      return folderPath;
    }

    public static string GetGuestPassportHeadImagePath(string passportNumber, string guestName)
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.passportHeadFolder) + "\\" + passportNumber + "_" + guestName + ".jpeg";
    }

    public static string GetGuestPhotoImagePath(string passportNumber, string guestName, int retryNumber)
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.snapShotFolder) + "\\" + passportNumber + "_" + guestName + "_" + (object) retryNumber + ".jpeg";
    }

    public static string GetGuestSignatureImagePath(string bookingOrderCode, string guestName)
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.signatureFolder) + "\\" + bookingOrderCode + "_" + guestName + ".png";
    }

    public static string GetPropertyLogoImagePath()
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.imageFolder) + "\\" + ConstantVariable_App.logoPath;
    }

    public static string GetBankLogoImagePath()
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.imageFolder) + "\\" + ConstantVariable_App.bankLogoPath;
    }

    public static string GetReceiptFilePath(string fileName)
    {
      return FileHelper.GetLocalFolderPath(ConstantVariable_App.receiptFolder) + "\\" + fileName + ".pdf";
    }

    public static string GetReceiptFileName(string bookingOrderCode, string guestName)
    {
      return "Receipt_" + bookingOrderCode + "_" + guestName;
    }
  }
}
