using System;
using System.Collections.Generic;
using WebBusinessEntity;
using WebDataLayer;

namespace WebBusinessLayer
{
	public class NationalityBL
	{
		public NationalityBL()
		{
		}

		public List<Nationality> GetList()
		{
			List<Nationality> list;
			NationalityDL nationalityDL = new NationalityDL();
			try
			{
				list = nationalityDL.GetList();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return list;
		}

		public bool RequiredVisa(Nationality n)
		{
			bool flag;
			NationalityDL nationalityDL = new NationalityDL();
			try
			{
				flag = nationalityDL.RequiredVisa(n);
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return flag;
		}

		public bool SkipImmigrationChop(Nationality n)
		{
			bool flag;
			try
			{
				flag = (new NationalityDL()).SkipImmigrationChop(n);
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return flag;
		}
	}
}