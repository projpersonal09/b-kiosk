using System;
using System.Collections.Generic;
using WebBusinessEntity;
using WebDataLayer;

namespace WebBusinessLayer
{
	public class OccupationBL
	{
		public OccupationBL()
		{
		}

		public List<Occupation> GetList()
		{
			List<Occupation> list;
			try
			{
				list = (new OccupationDL()).GetList();
			}
			catch (Exception exception)
			{
				throw exception;
			}
			return list;
		}
	}
}