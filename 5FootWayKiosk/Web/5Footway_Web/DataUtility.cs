using Helper;
using KioskBusinessEntity.Database_Entity;
using KioskBusinessLayer;
using System;

namespace _5FootWay
{
	public static class DataUtility
	{
		public static bool GetSkipOtherParticular()
		{
			bool flag;
			try
			{
				SettingBL settingBL = new SettingBL(ConstantVariable_Web.connStr);
				Setting setting = new Setting()
				{
					code = "SkipOtherParticular"
				};
				settingBL.Get(setting);
				bool flag1 = false;
				bool.TryParse(setting.@value, out flag1);
				flag = flag1;
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		public static bool GetSkipRateExperience()
		{
			bool flag;
			try
			{
				SettingBL settingBL = new SettingBL(ConstantVariable_Web.connStr);
				Setting setting = new Setting()
				{
					code = "SkipRateExp"
				};
				settingBL.Get(setting);
				bool flag1 = false;
				bool.TryParse(setting.@value, out flag1);
				flag = flag1;
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		public static bool GetStopCollectDeposit()
		{
			bool flag;
			try
			{
				SettingBL settingBL = new SettingBL(ConstantVariable_Web.connStr);
				Setting setting = new Setting()
				{
					code = "StopCollDep"
				};
				settingBL.Get(setting);
				bool flag1 = false;
				bool.TryParse(setting.@value, out flag1);
				flag = flag1;
			}
			catch
			{
				flag = false;
			}
			return flag;
		}

		public static TimeSpan? GetStopCollectDepositEndTime()
		{
			TimeSpan timeSpan;
			TimeSpan? nullable;
			try
			{
				SettingBL settingBL = new SettingBL(ConstantVariable_Web.connStr);
				Setting setting = new Setting()
				{
					code = "StopCollDepEndTime"
				};
				settingBL.Get(setting);
				if (!TimeSpan.TryParse(setting.@value, out timeSpan))
				{
					nullable = null;
				}
				else
				{
					nullable = new TimeSpan?(timeSpan);
				}
			}
			catch
			{
				nullable = null;
			}
			return nullable;
		}

		public static TimeSpan? GetStopCollectDepositStartTime()
		{
			TimeSpan timeSpan;
			TimeSpan? nullable;
			try
			{
				SettingBL settingBL = new SettingBL(ConstantVariable_Web.connStr);
				Setting setting = new Setting()
				{
					code = "StopCollDepStartTime"
				};
				settingBL.Get(setting);
				if (!TimeSpan.TryParse(setting.@value, out timeSpan))
				{
					nullable = null;
				}
				else
				{
					nullable = new TimeSpan?(timeSpan);
				}
			}
			catch
			{
				nullable = null;
			}
			return nullable;
		}
	}
}