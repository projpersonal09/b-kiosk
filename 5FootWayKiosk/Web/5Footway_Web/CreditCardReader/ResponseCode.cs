using MyResources;
using System;

namespace _5FootWay.CreditCardReader
{
	public class ResponseCode
	{
		public ResponseCode()
		{
		}

		public string parseErrorCode(string paymentType, string errorCode)
		{
			string empty = string.Empty;
			string str = paymentType;
			string str1 = str;
			if (str == null)
			{
				empty = CreditCard_Resources.CCR_ContactVendor;
				return empty;
			}
			else if (str1 == "C")
			{
				string str2 = errorCode;
				string str3 = str2;
				if (str2 != null)
				{
					switch (str3)
					{
						case "00":
						{
							empty = CreditCard_Resources.CCR_Approved;
							return empty;
						}
						case "01":
						case "02":
						case "05":
						case "31":
						case "41":
						case "43":
						case "51":
						case "91":
						{
							empty = CreditCard_Resources.CCR_CallBank;
							return empty;
						}
						case "03":
						case "12":
						case "13":
						case "25":
						case "30":
						case "58":
						case "76":
						case "77":
						case "78":
						case "89":
						case "94":
						case "96":
						case "RE":
						case "HE":
						case "LE":
						case "FE":
						case "AE":
						case "XX":
						case "BT":
						{
							empty = CreditCard_Resources.CCR_ContactVendor;
							return empty;
						}
						case "14":
						{
							empty = CreditCard_Resources.CCR_CheckCard;
							return empty;
						}
						case "19":
						{
							empty = CreditCard_Resources.CCR_RetryTransaction;
							return empty;
						}
						case "54":
						{
							empty = CreditCard_Resources.CCR_CardExpired;
							return empty;
						}
						case "55":
						{
							empty = CreditCard_Resources.CCR_ReEnterPin;
							return empty;
						}
						case "SE":
						{
							empty = CreditCard_Resources.CCR_InitiateSettlement;
							return empty;
						}
						case "PE":
						{
							empty = CreditCard_Resources.CCR_RedoTransaction;
							return empty;
						}
						case "IC":
						case "EC":
						{
							empty = CreditCard_Resources.CCR_UseDifferentCard;
							return empty;
						}
						case "CE":
						{
							empty = CreditCard_Resources.CCR_Retry;
							return empty;
						}
						case "VB":
						{
							empty = CreditCard_Resources.CCR_TransactionVoid;
							return empty;
						}
						case "WC":
						{
							empty = CreditCard_Resources.CCR_CheckCard;
							return empty;
						}
						case "TA":
						{
							empty = CreditCard_Resources.CCR_TransactionAbort;
							return empty;
						}
						case "DL":
						{
							empty = CreditCard_Resources.CCR_DoLogon;
							return empty;
						}
						case "IS":
						{
							empty = CreditCard_Resources.CCR_TransactionNotFound;
							return empty;
						}
						case "CD":
						{
							empty = CreditCard_Resources.CCR_CardDeclineTransaction;
							return empty;
						}
						case "LH":
						{
							empty = CreditCard_Resources.CCR_LoyaltyHost;
							return empty;
						}
						case "IN":
						{
							empty = CreditCard_Resources.CCR_RefundOrBlacklisted;
							return empty;
						}
						case "CO":
						{
							empty = CreditCard_Resources.CCR_CardNotReadProperly;
							return empty;
						}
						case "TL":
						{
							empty = CreditCard_Resources.CCR_TopupExceed;
							return empty;
						}
						case "PL":
						{
							empty = CreditCard_Resources.CCR_PaymentExceed;
							return empty;
						}
						case "CT":
						{
							empty = CreditCard_Resources.CCR_CardReadTimeout;
							return empty;
						}
						case "TT":
						{
							empty = CreditCard_Resources.CCR_CardTapTimeout;
							return empty;
						}
						case "MT":
						{
							empty = CreditCard_Resources.CCR_MaxTry;
							return empty;
						}
					}
				}
				empty = CreditCard_Resources.CCR_ContactVendor;
			}
			else
			{
				if (str1 != "M")
				{
					empty = CreditCard_Resources.CCR_ContactVendor;
					return empty;
				}
				empty = CreditCard_Resources.CCR_CardNotReadProperly;
			}
			return empty;
		}
	}
}