using _5FootWay;
using _5FootWay.CreditCardReader;
using _5FootWay.Models;
using Helper;
using Microsoft.CSharp.RuntimeBinder;
using MyResources;
using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using WebBusinessEntity;
using WebBusinessLayer;

namespace _5FootWay.Controllers
{
	public class CheckInController : BaseController
	{
		private DefaultListItem defaultListItem = new DefaultListItem();

		public CheckInController()
		{
		}

		[HttpGet]
		public ActionResult AddOn()
		{

            AddOnModel addOnModel = new AddOnModel();
            addOnModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
            addOnModel.checkInData = this.initializeCheckInData(addOnModel.checkInData);
            if (addOnModel.checkInData != null)
			{
				return base.View(addOnModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult AddOn(string command, AddOnModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				if (command.Equals("Back"))
				{
					return base.RedirectToAction("TandC");
				}
				return base.View(CM);
			}
			base.Session["addOnList"] = CM.addOnList;
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (((CM.checkInData.totalCharge + CM.checkInData.totalSurcharge) - CM.checkInData.totalPaid) > new decimal(0))
			{
				return base.RedirectToAction("VerifyBill");
			}
			return base.RedirectToAction("Signature");
		}

		[HttpGet]
		public ActionResult CapturePhoto(string config)
		{
			CapturePhotoModel capturePhotoModel = new CapturePhotoModel()
			{
				progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 2, ConstantVariable_Web.checkInPointList)
			};
			if (!string.IsNullOrEmpty(config))
			{
				if (config.Equals("camera"))
				{
					return base.View(capturePhotoModel);
				}
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			capturePhotoModel.checkInData = base.initializeCheckInData(capturePhotoModel.checkInData);
			if (capturePhotoModel.checkInData != null)
			{
				return base.View(capturePhotoModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult CapturePhoto(string command, CapturePhotoModel CM)
		{
			if (base.Session["passportDetail"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			bool flag = false;
			try
			{
				PassportDetailView item = (PassportDetailView)base.Session["passportDetail"];
				Nationality nationality = new Nationality()
				{
					alphabet = item.nationalityCode
				};
				flag = (new NationalityBL()).SkipImmigrationChop(nationality);
			}
			catch
			{
			}
			if (flag)
			{
				return base.RedirectToAction("PersonalParticular");
			}
			return base.RedirectToAction("ScanChop");
		}

		[HttpGet]
		public ActionResult CheckIn()
		{
			CheckInModel checkInModel = new CheckInModel()
			{
				progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 1, ConstantVariable_Web.checkInPointList)
			};
			return base.View(checkInModel);
		}

		[HttpPost]
		public ActionResult CheckIn(string command, CheckInModel CM)
		{
			if (base.Session["checkInData"] == null)
			{
				if (!string.IsNullOrEmpty(command))
				{
					return base.View(CM);
				}
				return this.RedirectToAction("Index", "Home", null);
			}
			if (((CheckInData)base.Session["checkInData"]).haveGuest)
			{
				return base.RedirectToAction("RoomBedList");
			}
			return base.RedirectToAction("ScanPassport");
		}

		[HttpGet]
		public ActionResult CheckInComplete()
		{
            CheckInCompleteModel checkInCompleteModel = new CheckInCompleteModel();
            checkInCompleteModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 18, ConstantVariable_Web.checkInPointList);
            checkInCompleteModel.checkInData = this.initializeCheckInData(checkInCompleteModel.checkInData);
            if (checkInCompleteModel.checkInData != null)
			{
				return base.View(checkInCompleteModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult CheckInComplete(CheckInCompleteModel CM)
		{
			base.Session.Abandon();
			return base.RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public ActionResult CheckInIncomplete()
		{
            CheckInIncompleteModel checkInIncompleteModel = new CheckInIncompleteModel();
            checkInIncompleteModel.checkInData = this.initializeCheckInData(checkInIncompleteModel.checkInData);

            if (checkInIncompleteModel.checkInData == null)
			{
				checkInIncompleteModel.checkInData = new CheckInData();
			}
			return base.View(checkInIncompleteModel);
		}

		[HttpPost]
		public ActionResult CheckInIncomplete(CheckInCompleteModel CM)
		{
			base.Session.Abandon();
			return base.RedirectToAction("Index", "Home");
		}

		[NonAction]
		private bool checkStopCollectDeposit(TimeSpan currentTime)
		{
			bool valueOrDefault;
			if (DataUtility.GetStopCollectDeposit())
			{
				return true;
			}
			TimeSpan? stopCollectDepositStartTime = DataUtility.GetStopCollectDepositStartTime();
			TimeSpan? stopCollectDepositEndTime = DataUtility.GetStopCollectDepositEndTime();
			if (stopCollectDepositStartTime.HasValue && stopCollectDepositEndTime.HasValue)
			{
				TimeSpan timeSpan = currentTime;
				TimeSpan? nullable = stopCollectDepositStartTime;
				if ((nullable.HasValue ? timeSpan < nullable.GetValueOrDefault() : true))
				{
					valueOrDefault = false;
					return valueOrDefault;
				}
				TimeSpan timeSpan1 = currentTime;
				TimeSpan? nullable1 = stopCollectDepositEndTime;
				if (nullable1.HasValue)
				{
					valueOrDefault = timeSpan1 <= nullable1.GetValueOrDefault();
					return valueOrDefault;
				}
				else
				{
					valueOrDefault = false;
					return valueOrDefault;
				}
			}
			valueOrDefault = false;
			return valueOrDefault;
		}

		[HttpGet]
		public ActionResult CollectCard()
		{
            CollectCardModel collectCardModel = new CollectCardModel();
            collectCardModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 15, ConstantVariable_Web.checkInPointList);
            collectCardModel.checkInData = this.initializeCheckInData(collectCardModel.checkInData);
            if (collectCardModel.checkInData != null)
			{
				return base.View(collectCardModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult CollectCard(string command, CollectCardModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			if (!DataUtility.GetSkipRateExperience())
			{
				return base.RedirectToAction("RateExperience");
			}
			return base.RedirectToAction("CheckInComplete");
		}

		[HttpGet]
		public JsonResult GetResponseMessage(string paymentType, string responseCode)
		{
			string str = (new ResponseCode()).parseErrorCode(paymentType, responseCode);
			return base.Json(str, JsonRequestBehavior.AllowGet);
		}

		[NonAction]
		private RateExperienceModel initializeRateExperienceModel(RateExperienceModel CM)
		{
			if (CM == null)
			{
				CM = new RateExperienceModel();
			}
			CM.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 17, ConstantVariable_Web.checkInPointList);
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			return CM;
		}

		[NonAction]
		private ActionResult LogException(Exception ex)
		{
			(new MyLog()).LogException(ex);
			return base.RedirectToAction("Index", "Home", new { errorMessage = "serverFailed" });
		}

		[HttpGet]
		public ActionResult Payment()
		{
            PaymentModel paymentModel = new PaymentModel();
            paymentModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 14, ConstantVariable_Web.checkInPointList);
            paymentModel.checkInData = this.initializeCheckInData(paymentModel.checkInData);
            if (paymentModel.checkInData != null)
			{
				return base.View(paymentModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult Payment(string command, PaymentModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData != null)
			{
				return base.RedirectToAction("Signature");
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpGet]
		public ActionResult PDA()
		{
			PDAModel pDAModel = new PDAModel()
			{
				progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 0, ConstantVariable_Web.checkInPointList)
			};
			return base.View(pDAModel);
		}

		[HttpPost]
		public ActionResult PDA(string command, PDAModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.View(CM);
			}
			if (!command.Equals("Next"))
			{
				if (!command.Equals("Back"))
				{
					return base.View(CM);
				}
				return this.RedirectToAction("Index", "Home", null);
			}
			if (CM.isAgree)
			{
				return base.RedirectToAction("CheckIn");
			}
			((dynamic)base.ViewBag).Message = Resources.PDAUncheckedMessage;
			return base.View(CM);
		}

		[HttpGet]
		public ActionResult PersonalParticular()
		{
            PersonalParticularModel personalParticularModel = new PersonalParticularModel();
            personalParticularModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
            personalParticularModel.checkInData = this.initializeCheckInData(personalParticularModel.checkInData);
            if (personalParticularModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (base.Session["passportDetail"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			personalParticularModel.passportDetail = (PassportDetailView)base.Session["passportDetail"];
			if (base.Session["otherParticular"] != null)
			{
				personalParticularModel.otherParticular = (OtherParticularView)base.Session["otherParticular"];
			}
			personalParticularModel.SkipOtherParticular = DataUtility.GetSkipOtherParticular();
			return base.View(personalParticularModel);
		}

		[HttpPost]
		public ActionResult PersonalParticular(string command, PersonalParticularModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			base.Session["otherParticular"] = CM.otherParticular;
			return base.RedirectToAction("VerifyDetail");
		}

		[HttpGet]
		public ActionResult RateExperience()
		{
			RateExperienceModel rateExperienceModel = new RateExperienceModel();
			this.initializeRateExperienceModel(rateExperienceModel);
			if (rateExperienceModel.checkInData != null)
			{
				return base.View(rateExperienceModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult RateExperience(RateExperienceModel CM)
		{
			this.initializeRateExperienceModel(CM);
			if (!string.IsNullOrWhiteSpace(CM.selectedRate))
			{
				return base.RedirectToAction("CheckInComplete");
			}
			if (CM.checkInData != null)
			{
				return base.View(CM);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpGet]
		public ActionResult RoomBedList()
		{
            RoomBedListModel roomBedListModel = new RoomBedListModel();
            roomBedListModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
            roomBedListModel.checkInData = this.initializeCheckInData(roomBedListModel.checkInData);
            if (roomBedListModel.checkInData != null)
			{
				return base.View(roomBedListModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult RoomBedList(string command, RoomBedListModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (command.Equals("Next"))
			{
				CM.checkInData = base.initializeCheckInData(CM.checkInData);
				if (CM.checkInData == null)
				{
					return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
				}
				CM.checkInData.totalSecurityDeposit = CM.checkInData.securityDepositPerCard * (
					from x in CM.checkInData.roomBedList
					where x.guestNameList.Count > 0
					select x).Count<RoomBed>();
				base.Session["checkInData"] = CM.checkInData;
				return base.RedirectToAction("TandC");
			}
			char[] chrArray = new char[] { '-' };
			string[] strArrays = command.Split(chrArray, 2);
			command = strArrays[0];
			int num = -1;
			if (strArrays.Count<string>() > 1)
			{
				num = Convert.ToInt32(strArrays[1]);
			}
			if (!command.Equals("MoreGuest"))
			{
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			CM.checkInData.currentRoom = num;
			base.Session["checkInData"] = CM.checkInData;
			return base.RedirectToAction("ScanPassport");
		}

		[HttpGet]
		public ActionResult ScanChop()
		{
            ScanChopModel scanChopModel = new ScanChopModel();
            scanChopModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 2, ConstantVariable_Web.checkInPointList);
            scanChopModel.checkInData = this.initializeCheckInData(scanChopModel.checkInData);
            if (scanChopModel.checkInData != null)
			{
				return base.View(scanChopModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult ScanChop(string command, ScanChopModel CM)
		{
			if (base.Session["passportDetail"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			bool flag = true;
			try
			{
				PassportDetailView item = (PassportDetailView)base.Session["passportDetail"];
				Nationality nationality = new Nationality()
				{
					alphabet = item.nationalityCode
				};
				flag = (new NationalityBL()).RequiredVisa(nationality);
			}
			catch
			{
			}
			if (flag)
			{
				return base.RedirectToAction("ScanVisa");
			}
			return base.RedirectToAction("PersonalParticular");
		}

		[HttpGet]
		public ActionResult ScanPassport()
		{
            ScanPassportModel scanPassportModel = new ScanPassportModel();
            scanPassportModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 2, ConstantVariable_Web.checkInPointList);
            scanPassportModel.checkInData = this.initializeCheckInData(scanPassportModel.checkInData);
            if (scanPassportModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			try
			{
				foreach (Nationality list in (new NationalityBL()).GetList())
				{
					scanPassportModel.nationalityList.Add(new DefaultList(list.name, list.alphabet));
				}
			}
			catch
			{
			}
			scanPassportModel.genderList = this.defaultListItem.GetGenderList();
			return base.View(scanPassportModel);
		}

		[HttpPost]
		public ActionResult ScanPassport(string command, ScanPassportModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (command.Equals("Back"))
			{
				return base.RedirectToAction("RoomBedList");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			try
			{
				CM.passportDetail.nationality = (
					from x in (new NationalityBL()).GetList()
					where x.alphabet.Equals(CM.passportDetail.nationalityCode)
					select x.name).FirstOrDefault<string>();
			}
			catch
			{
			}
			CM.passportDetail.gender = (
				from x in this.defaultListItem.GetGenderList()
				where x.Value.Equals(CM.passportDetail.gender)
				select x.Text).FirstOrDefault<string>();
			base.Session["passportDetail"] = CM.passportDetail;
			return base.RedirectToAction("CapturePhoto");
		}

		[HttpGet]
		public ActionResult ScanVisa()
		{
            ScanVisaModel scanVisaModel = new ScanVisaModel();
            scanVisaModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 2, ConstantVariable_Web.checkInPointList);
            scanVisaModel.checkInData = this.initializeCheckInData(scanVisaModel.checkInData);
            if (scanVisaModel.checkInData != null)
			{
				return base.View(scanVisaModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult ScanVisa(string command, ScanVisaModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (command.Equals("Next"))
			{
				return base.RedirectToAction("PersonalParticular");
			}
			return base.View(CM);
		}

		[HttpGet]
		public ActionResult SecurityDeposit()
		{
            SecurityDepositModel securityDepositModel = new SecurityDepositModel();
            securityDepositModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 14, ConstantVariable_Web.checkInPointList);
            securityDepositModel.checkInData = this.initializeCheckInData(securityDepositModel.checkInData);
            if (securityDepositModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			securityDepositModel.amountPayable = securityDepositModel.checkInData.totalSecurityDeposit - securityDepositModel.checkInData.paidSecurityDeposit;
			securityDepositModel.amountInserted = new decimal(0);
			securityDepositModel.amountChange = new decimal(0);
			return base.View(securityDepositModel);
		}

		[HttpPost]
		public ActionResult SecurityDeposit(string command, SecurityDepositModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			bool skipRateExperience = DataUtility.GetSkipRateExperience();
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (CM.checkInData.roomBedList.Any<RoomBed>((RoomBed x) => string.IsNullOrEmpty(x.cardUID)))
			{
				return base.RedirectToAction("CollectCard");
			}
			if (!skipRateExperience)
			{
				return base.RedirectToAction("RateExperience");
			}
			return base.RedirectToAction("CheckInComplete");
		}

		[HttpGet]
		public ActionResult Signature()
		{
            SignatureModel signatureModel = new SignatureModel();
            signatureModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 14, ConstantVariable_Web.checkInPointList);
            signatureModel.checkInData = this.initializeCheckInData(signatureModel.checkInData);
            if (signatureModel.checkInData != null)
			{
				return base.View(signatureModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult Signature(string command, SignatureModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			bool flag = this.checkStopCollectDeposit(DateTime.Now.TimeOfDay);
			bool skipRateExperience = DataUtility.GetSkipRateExperience();
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (!flag && (CM.checkInData.totalSecurityDeposit - CM.checkInData.paidSecurityDeposit) > new decimal(0))
			{
				return base.RedirectToAction("SecurityDeposit");
			}
			if (CM.checkInData.roomBedList.Any<RoomBed>((RoomBed x) => string.IsNullOrEmpty(x.cardUID)))
			{
				return base.RedirectToAction("CollectCard");
			}
			if (!skipRateExperience)
			{
				return base.RedirectToAction("RateExperience");
			}
			return base.RedirectToAction("CheckInComplete");
		}

		[HttpGet]
		public ActionResult TandC()
		{

            TandCModel tandCmodel = new TandCModel();
            tandCmodel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
            tandCmodel.checkInData = this.initializeCheckInData(tandCmodel.checkInData);

            if (tandCmodel.checkInData != null)
			{
				return base.View(tandCmodel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult TandC(string command, TandCModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				if (command.Equals("Back"))
				{
					return base.RedirectToAction("RoomBedList");
				}
				return base.View(CM);
			}
			if (CM.isAgree)
			{
				return base.RedirectToAction("AddOn");
			}
			((dynamic)base.ViewBag).Message = Resources.TandCUncheckedMessage;
			return base.View(CM);
		}

		[HttpGet]
		public ActionResult VerifyBill()
		{
            VerifyBillModel verifyBillModel = new VerifyBillModel();
            verifyBillModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
            verifyBillModel.checkInData = this.initializeCheckInData(verifyBillModel.checkInData);
            if (verifyBillModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			for (int i = 1; i <= verifyBillModel.checkInData.paymentList.Count; i++)
			{
				List<PaymentView> paymentViews = verifyBillModel.paymentList;
				PaymentView paymentView = new PaymentView()
				{
					serialNumber = i,
					description = verifyBillModel.checkInData.paymentList[i - 1].description,
					price = verifyBillModel.checkInData.paymentList[i - 1].price,
					quantity = verifyBillModel.checkInData.paymentList[i - 1].quantity
				};
				paymentViews.Add(paymentView);
			}
			verifyBillModel.paid = verifyBillModel.checkInData.totalPaid;
			verifyBillModel.charge = verifyBillModel.checkInData.totalCharge;
			verifyBillModel.surcharge = verifyBillModel.checkInData.totalSurcharge;
			verifyBillModel.payable = (verifyBillModel.charge + verifyBillModel.surcharge) - verifyBillModel.paid;
			verifyBillModel.securityDeposit = verifyBillModel.checkInData.totalSecurityDeposit - verifyBillModel.checkInData.paidSecurityDeposit;
			verifyBillModel.StopCollectDeposit = this.checkStopCollectDeposit(DateTime.Now.TimeOfDay);
			return base.View(verifyBillModel);
		}

		[HttpPost]
		public ActionResult VerifyBill(string command, VerifyBillModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckInIncomplete");
			}
			if (!command.Equals("Next"))
			{
				if (command.Equals("Back"))
				{
					return base.RedirectToAction("AddOn");
				}
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			CM.paid = CM.checkInData.totalPaid;
			CM.charge = CM.checkInData.totalCharge;
			CM.surcharge = CM.checkInData.totalSurcharge;
			CM.payable = (CM.charge + CM.surcharge) - CM.paid;
			if (CM.payable > new decimal(0))
			{
				return base.RedirectToAction("Payment");
			}
			return base.RedirectToAction("Signature");
		}

		[HttpGet]
		public ActionResult VerifyDetail()
		{
             VerifyDetailModel verifyDetailModel = new VerifyDetailModel();
             verifyDetailModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkInStepCount, 9, ConstantVariable_Web.checkInPointList);
             verifyDetailModel.checkInData = this.initializeCheckInData(verifyDetailModel.checkInData);

            if (verifyDetailModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (base.Session["passportDetail"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			verifyDetailModel.passportDetail = (PassportDetailView)base.Session["passportDetail"];
			if (base.Session["otherParticular"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			verifyDetailModel.otherParticular = (OtherParticularView)base.Session["otherParticular"];
			verifyDetailModel.SkipOtherParticular = DataUtility.GetSkipOtherParticular();
			return base.View(verifyDetailModel);
		}

		[HttpPost]
		public ActionResult VerifyDetail(string command, VerifyDetailModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.View(CM);
			}
			if (!command.Equals("Next"))
			{
				if (command.Equals("Back"))
				{
					return base.RedirectToAction("PersonalParticular");
				}
				return base.View(CM);
			}
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			if (base.Session["passportDetail"] == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			PassportDetailView item = (PassportDetailView)base.Session["passportDetail"];
			if (CM.checkInData.roomBedList.Count > CM.checkInData.currentRoom)
			{
				CM.checkInData.roomBedList[CM.checkInData.currentRoom].guestNameList.Add(item.guestName);
			}
			CM.checkInData.haveGuest = true;
			base.Session["checkInData"] = CM.checkInData;
			base.Session["passportDetail"] = null;
			base.Session["otherParticular"] = null;
			return base.RedirectToAction("RoomBedList");
		}

		public class CheckInCommand
		{
			public const string back = "Back";

			public const string next = "Next";

			public const string moreGuest = "MoreGuest";

			public const string backToHome = "BackToHome";

			public CheckInCommand()
			{
			}
		}
	}
}