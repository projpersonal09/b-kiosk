using _5FootWay.Models;
using Helper;
using MyResources;
using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace _5FootWay.Controllers
{
	public class CheckOutController : BaseController
	{
		public CheckOutController()
		{
		}

		[HttpGet]
		public ActionResult CheckOut()
		{
			CheckOutModel checkOutModel = new CheckOutModel()
			{
				progressBar = new ProgressBarModel(ConstantVariable_Web.checkOutStepCount, 0, ConstantVariable_Web.checkOutPointList)
			};
			return base.View(checkOutModel);
		}

		[HttpPost]
		public ActionResult CheckOut(string command, CheckOutModel CM)
		{
			if (base.Session["checkInData"] != null)
			{
				return base.RedirectToAction("ReturnCard");
			}
			if (string.IsNullOrEmpty(command))
			{
				return base.View(CM);
			}
			if (!command.Equals("Back"))
			{
				return base.View(CM);
			}
			return this.RedirectToAction("Index", "Home", null);
		}

		[HttpGet]
		public ActionResult CheckOutComplete()
		{
			
            CheckOutCompleteModel checkOutCompleteModel = new CheckOutCompleteModel();
            checkOutCompleteModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkOutStepCount, 3, ConstantVariable_Web.checkOutPointList);
            checkOutCompleteModel.checkInData = this.initializeCheckInData(checkOutCompleteModel.checkInData);
            if (checkOutCompleteModel.checkInData != null)
			{
				return base.View(checkOutCompleteModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult CheckOutComplete(CheckOutCompleteModel CM)
		{
			base.Session.Abandon();
			return base.RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public ActionResult CheckOutIncomplete()
		{
            CheckOutIncompleteModel checkOutIncompleteModel = new CheckOutIncompleteModel();
            checkOutIncompleteModel.checkInData = this.initializeCheckInData(checkOutIncompleteModel.checkInData);
            if (checkOutIncompleteModel.checkInData != null)
			{
				return base.View(checkOutIncompleteModel);
			}
			return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
		}

		[HttpPost]
		public ActionResult CheckOutIncomplete(CheckOutIncompleteModel CM)
		{
			base.Session.Abandon();
			return base.RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public ActionResult ReturnCard()
		{
            ReturnCardModel returnCardModel = new ReturnCardModel();
            returnCardModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkOutStepCount, 1, ConstantVariable_Web.checkOutPointList);
            returnCardModel.checkInData = this.initializeCheckInData(returnCardModel.checkInData);
            if (returnCardModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			for (int i = 0; i < returnCardModel.checkInData.roomBedList.Count; i++)
			{
				if (!string.IsNullOrEmpty(returnCardModel.checkInData.roomBedList[i].cardUID))
				{
					returnCardModel.cardList.Add(new Card(Resources.ReturnCardTip1, returnCardModel.checkInData.roomBedList[i].cardUID));
				}
				else
				{
					returnCardModel.cardList.Add(new Card(Resources.ReturnCardTip4, returnCardModel.checkInData.roomBedList[i].cardUID));
				}
			}
			return base.View(returnCardModel);
		}

		[HttpPost]
		public ActionResult ReturnCard(string command, ReturnCardModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckOutIncomplete");
			}
			if (!command.Equals("Next"))
			{
				return base.View(CM);
			}
			decimal cM = CM.checkInData.paidSecurityDeposit;
			CM.checkInData = base.initializeCheckInData(CM.checkInData);
			if (CM.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			CM.checkInData.paidSecurityDeposit = cM;
			base.Session["checkInData"] = CM.checkInData;
			if (CM.ableToReturnSecurityDeposit)
			{
				return base.RedirectToAction("ReturnDeposit");
			}
			return base.RedirectToAction("CheckOutIncomplete");
		}

		[HttpGet]
		public ActionResult ReturnDeposit()
		{
            ReturnDepositModel returnDepositModel = new ReturnDepositModel();
            returnDepositModel.progressBar = new ProgressBarModel(ConstantVariable_Web.checkOutStepCount, 2, ConstantVariable_Web.checkOutPointList);
            returnDepositModel.checkInData = this.initializeCheckInData(returnDepositModel.checkInData);
            if (returnDepositModel.checkInData == null)
			{
				return base.RedirectToAction("Index", "Home", new { errorMessage = "missingData" });
			}
			returnDepositModel.amountToReturn = returnDepositModel.checkInData.paidSecurityDeposit;
			return base.View(returnDepositModel);
		}

		[HttpPost]
		public ActionResult ReturnDeposit(string command, ReturnDepositModel CM)
		{
			if (string.IsNullOrEmpty(command))
			{
				return base.RedirectToAction("CheckOutIncomplete");
			}
			if (command.Equals("Next"))
			{
				return base.RedirectToAction("CheckOutComplete");
			}
			return base.View(CM);
		}

		public class CheckOutCommand
		{
			public const string next = "Next";

			public const string back = "Back";

			public const string backToHome = "BackToHome";

			public CheckOutCommand()
			{
			}
		}
	}
}