using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class PaymentView
	{
		public string description
		{
			get;
			set;
		}

		public decimal price
		{
			get;
			set;
		}

		public int quantity
		{
			get;
			set;
		}

		public int serialNumber
		{
			get;
			set;
		}

		public PaymentView()
		{
		}
	}
}