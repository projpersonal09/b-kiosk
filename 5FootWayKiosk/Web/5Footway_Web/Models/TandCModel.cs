using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class TandCModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public bool isAgree
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public TandCModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}