using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class PersonalParticularModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public List<DefaultList> occupationList
		{
			get;
			set;
		}

		public OtherParticularView otherParticular
		{
			get;
			set;
		}

		public PassportDetailView passportDetail
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public bool SkipOtherParticular
		{
			get;
			set;
		}

		public PersonalParticularModel()
		{
			this.otherParticular = new OtherParticularView();
			this.passportDetail = new PassportDetailView();
			this.checkInData = new CheckInData();
			this.occupationList = new List<DefaultList>();
			this.SkipOtherParticular = false;
		}
	}
}