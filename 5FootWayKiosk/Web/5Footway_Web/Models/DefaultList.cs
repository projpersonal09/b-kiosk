using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class DefaultList
	{
		public string Text
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}

		public DefaultList()
		{
		}

		public DefaultList(string Text, string Value)
		{
			this.Text = Text;
			this.Value = Value;
		}
	}
}