using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class CapturePhotoModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public string title
		{
			get;
			set;
		}

		public CapturePhotoModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}