using Helper;
using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class CheckInCompleteModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public int pageTimeout
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public CheckInCompleteModel()
		{
			this.checkInData = new CheckInData();
			this.pageTimeout = ConstantVariable_Web.checkInCompletePageTimeout;
		}
	}
}