using Helper;
using System;
using System.Collections.Generic;

namespace _5FootWay.Models
{
	public class DefaultListItem
	{
		public DefaultListItem()
		{
		}

		public List<DefaultList> GetGenderList()
		{
			List<DefaultList> defaultLists = new List<DefaultList>()
			{
				new DefaultList("MALE", GuestGender.Male),
				new DefaultList("FEMALE", GuestGender.Female)
			};
			return defaultLists;
		}
	}
}