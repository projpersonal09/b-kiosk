using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ScanVisaModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public string visaNumber
		{
			get;
			set;
		}

		public ScanVisaModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}