using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class CheckOutModel
	{
		public CheckOutView checkOut
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public CheckOutModel()
		{
			this.checkOut = new CheckOutView();
		}
	}
}