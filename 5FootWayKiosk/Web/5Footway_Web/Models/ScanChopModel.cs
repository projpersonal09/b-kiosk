using SharedBusinessEntity;
using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ScanChopModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public ScanChopModel()
		{
			this.checkInData = new CheckInData();
		}
	}
}