using System;

namespace _5FootWay.Models
{
	public class CheckOutView
	{
		public string guestName;

		public string roomBedNumber;

		public decimal securityDeposit;

		public string checkOutDate;

		public string checkInDate;

		public CheckOutView()
		{
		}
	}
}