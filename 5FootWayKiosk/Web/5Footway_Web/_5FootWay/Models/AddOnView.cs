using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class AddOnView
	{
		public string description
		{
			get;
			set;
		}

		public int maximumQuantity
		{
			get;
			set;
		}

		public decimal price
		{
			get;
			set;
		}

		public int quantity
		{
			get;
			set;
		}

		public int serialNumber
		{
			get;
			set;
		}

		public AddOnView()
		{
		}
	}
}