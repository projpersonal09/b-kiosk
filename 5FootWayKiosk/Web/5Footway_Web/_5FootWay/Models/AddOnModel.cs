using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class AddOnModel
	{
		public List<AddOnView> addOnList
		{
			get;
			set;
		}

		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public List<DefaultList> quantityList
		{
			get;
			set;
		}

		public AddOnModel()
		{
			this.addOnList = new List<AddOnView>();
			this.checkInData = new CheckInData();
			this.quantityList = new List<DefaultList>();
		}
	}
}