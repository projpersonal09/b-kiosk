using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class VerifyBillModel
	{
		public decimal charge
		{
			get;
			set;
		}

		public CheckInData checkInData
		{
			get;
			set;
		}

		public decimal paid
		{
			get;
			set;
		}

		public decimal payable
		{
			get;
			set;
		}

		public List<PaymentView> paymentList
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public decimal securityDeposit
		{
			get;
			set;
		}

		public bool StopCollectDeposit
		{
			get;
			set;
		}

		public decimal surcharge
		{
			get;
			set;
		}

		public VerifyBillModel()
		{
			this.paymentList = new List<PaymentView>();
			this.checkInData = new CheckInData(); 
			//this.StopCollectDeposit = false; //original code
            this.StopCollectDeposit = true;
        }
	}
}