using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ScanPassportModel
	{
		public CheckInData checkInData
		{
			get;
			set;
		}

		public List<DefaultList> genderList
		{
			get;
			set;
		}

		public List<DefaultList> nationalityList
		{
			get;
			set;
		}

		public PassportDetailView passportDetail
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public ScanPassportModel()
		{
			this.checkInData = new CheckInData();
			this.passportDetail = new PassportDetailView();
			this.genderList = new List<DefaultList>();
			this.nationalityList = new List<DefaultList>();
		}
	}
}