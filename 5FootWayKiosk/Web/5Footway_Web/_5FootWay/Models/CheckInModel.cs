using System;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class CheckInModel
	{
		public string bookingNumber
		{
			get;
			set;
		}

		public string guestName
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public CheckInModel()
		{
		}
	}
}