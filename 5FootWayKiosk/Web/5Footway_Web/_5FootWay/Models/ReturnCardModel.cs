using SharedBusinessEntity;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace _5FootWay.Models
{
	public class ReturnCardModel
	{
		public bool ableToReturnSecurityDeposit
		{
			get;
			set;
		}

		public List<Card> cardList
		{
			get;
			set;
		}

		public CheckInData checkInData
		{
			get;
			set;
		}

		public ProgressBarModel progressBar
		{
			get;
			set;
		}

		public ReturnCardModel()
		{
			this.checkInData = new CheckInData();
			this.cardList = new List<Card>();
		}
	}
}